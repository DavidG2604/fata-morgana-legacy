FROM php:8.3-apache

RUN a2enmod rewrite
RUN a2enmod headers

RUN apt-get update \
	&& apt-get install -y libpq-dev git locales vim curl libicu-dev libzip-dev \
	&& docker-php-ext-configure intl \
	&& docker-php-ext-install pgsql pdo pdo_pgsql \
	&& docker-php-ext-install intl zip \
	&& docker-php-ext-enable intl zip

ENV APACHE_DOCUMENT_ROOT=/var/www/html/public 
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf 
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

WORKDIR /var/www/html

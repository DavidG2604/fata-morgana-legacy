# Fata Morgana

## Setup requirements

### Webserver 

- Recommended is Apache 2 with modules `rewrite`, `headers` and the directive `AllowOverride All` enabled, as .htaccess files are used.
- PHP version 8.3 is the most up-to-date version working, extensions including php-xml, php-pgsql, php-json, php-gd, php-mbstring must be enabled.
- Latest tested DB server is PostgreSQL 16.
- If you want a GUI for the DB, pgAdmin4 is recommend.

### Docker container

Repository includes a [Dockerfile](./Dockerfile) which defines a PHP 8.3 image with required Apache modules, composer and pgsql enabled. The [Docker Compose](./docker_compose.yml) file can be used to spin up a VM for local development with PHP 8.3 and Postgres 13.

```bash
docker-compose up -d
```
The web container will be available at [localhost:8000](http://localhost:8000).

## Installation

- Clone the repository and fire up your webserver or the included Docker setup.
- Inside the docroot, run `composer install` to install the required PHP packages.
- Restore the DB backup empty_pgsql_db_backuped_by_pgadmin from docroot into your Postgres DB.
- Copy the file app/Config/Database.php.default to Database.php and enter DSN and/or credentials for your DB.
- Copy the file "env" to ".env" and add your own private appkey (more on that below) and your own random 32 characters long jwtkey.
- Make sure your webserver is able to write under data/common, this is the only place where FM is writing to the file system. Item and building files are created here, FM will learn all items and outside buildings and store them. If you want complete files, ping MisterD.

## App key

If you do not already have an appkey from the game, send a message to Brainbox that you want to register your own application for development. Within the MH game inside the app directory (book icon in the upper left corner), you will be able to find/generate the secret appkey. That needs to be placed in the appkey_model.php as mentioned.


## Overview of all request routes

See app/Config/Routes.php for defined routes.
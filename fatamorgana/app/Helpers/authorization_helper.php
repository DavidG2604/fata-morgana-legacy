<?php

use CodeIgniter\CodeIgniter;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
/**
 * Validates a JWT token and checks if its timestamp is within the allowed timeframe.
 *
 * @param string $token The JWT token to validate.
 * @return mixed Returns the decoded token if valid and within the timeframe, otherwise FALSE.
 */
function validateTimestamp($token)
{
    /** @type {Codeigniter\Config\JWT} */
    $token = validateToken($token);
    if ($token && (now() < strtotime($_ENV['TOKENTIMEOUT'], $token->timestamp))) {
        return $token;
    }
    return FALSE;
}

/**
 * Validates/decodes a JWT token.
 *
 * @param string $token The JWT token to validate.
 * @return mixed Returns the decoded token if valid, otherwise FALSE.
 */
function validateToken($token)
{
    /** @type {CodeIgniter\Config\JWT} */
    try {
        return JWT::decode($token, new Key($_ENV['JWTKEY'], 'HS256'));
    } catch (Exception $exc) {
        return FALSE;
    }
}

/**
 * Generates a JWT token with the given data.
 *
 * @param array $data The data to include in the token.
 * @return string Returns the generated JWT token or FALSE if an error occurs.
 */
function generateToken($data)
{
    /** @type {CodeIgniter\Config\JWT} */
    try {
        return JWT::encode($data, $_ENV['JWTKEY'], 'HS256');
    } catch (Exception $exc) {
        return FALSE;
    }
}
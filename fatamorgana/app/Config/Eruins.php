<?php 

namespace Config;

use CodeIgniter\Config\BaseConfig;

class Eruins extends BaseConfig {

    /**
     * --------------------------------------------------------------------------
     * Explorable Ruins blueprint list
     * --------------------------------------------------------------------------
     *
     * All possible blueprints that can be found in explorable ruins.
     */
    public $eruins = [
        "bunker" => [
            "51", // Creameto-Cue
            "47", // Divining Rods
            "111", // Fortifications
            "33", // Grapeboom
            "44", // Mist Spray
            "100", // Small Trebuchet
            "134", // Air Strike
            "66", // Coffin Catapult
            "13", // Evolutive Wall
            "149", // Grenade Launcher
            "101", // Guard Tower
            "162", // Manual grinder
            "160", // Pigsty
            "152", // Technicians Workbench
            "154", // Underground City
            "42", // Water Turrets
            "129", // Giant BRD
            "123", // Giant Sandcastle
            "125", // Ministry of Slavery
            "158", // Pool
            "126", // Reactor
        ],
        "hospital" => [
            "35", // Eden Project
            "144", // Hammam
            "68", // Henhouse
            "11", // Inner Wall
            "61", // Screaming Saws
            "67", // Small Cemetary
            "39", // Sprinkler System
            "66", // Coffin Catapult
            "105", // False Town
            "32", // Fertilizer
            "164", // Filtering gutters
            "74", // Infirmary
            "148", // Nature area of the Survivalists
            "102", // Scouts Lair
            "150", // Vitamin Mines
            "132", // Crow Statue
            "119", // Dump Upgrade
            "129", // Giant BRD
            "135", // Hot Air Balloon
            "136", // No Holes Barred

        ],
        "hotel" => [
            "73", // Builder’s Merchant
            "12", // Concrete Reinforcement
            "70", // Defensive Adjustment
            "55", // Railgun
            "151", // Ravaged pumpkins
            "155", // Urban plan
            "49", // Apple Tree
            "106", // Faucet
            "127", // Labyrinth
            "110", // People’s Court
            "108", // Scarecrows
            "98", // Swedish Workshop
            "165", // Tamer Experiment Center
            "107", // The Big Rebuild
            "159", // Blue Gold Thermal baths
            "133", // Cinema
            "48", // Divining Rods
            "158", // Pool
            "130", // Buzzard's Wonder Wheel
            "10", // Third Wall
        ]
    ];

}

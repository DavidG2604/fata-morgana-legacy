<?php

namespace App\Config;

use CodeIgniter\Config\BaseConfig;

class ConstructionsList extends BaseConfig
{
    /**
     * --------------------------------------------------------------------------
     * Constructions order
     * --------------------------------------------------------------------------
     *
     * Order for constructions: parent => [child, child]
     */
    public $constructionsOrder = [
        28 => [2, 7, 24, 17, 6, 22, 19, 20, 21, 23, 142, 94], // Defensive Wall => [ Great Pit, Wall Upgrade v2, Slip 'n' Slide, Wooden Fencing, Barbed Wire, Plywood, Armour Plating, Armour Plating v2, Armour Plating v3, Extrawall, Portal Lock, Emergency Supplies ]
        2 => [1, 4, 16, 147], // Great Pit => [ Great Moat, Spiked pit, Old-school traps, Scavenger's Gallery ]
        7 => [11, 8, 13, 14, 9], // Wall Upgrade v2 => [Inner Wall, Überwall, Evolutive Wall, Metal Patches, Spiked Wall]
        11 => [10], // Inner Wall => [Third Wall]
        24 => [3, 15, 61], // Slip 'n' Slide => [ Shredder Wall, Zombie-shredder, Screaming Saws ]
        17 => [18], // Wooden Fencing => [Small Fence]
        6 => [5], // Barbed Wire => [Bait]
        22 => [12], // Plywood => [Concrete Reinforcement]
        142 => [140, 138, 141, 59, 38], // Portal Lock => [Automatic Piston Lock, Reinforced Gates, Ventilation System, War Mill, Saniflow Macerator]
        140 => [139], // Automatic Piston Lock => [Last-Minute Lock]
        94 => [82, 83, 85, 87, 88, 89], // Emergency Supplies => [Wood Plating, Guerilla Traps, Rubbish Heap, Bomb Factory, Abject Panic, Frat House]
        85 => [84, 81, 86], // Rubbish Heap => [Mount Killaman-Jaro, Spikes, Pits]

        50 => [43, 36, 35, 48, 31, 27, 42, 99, 41, 34], // Pump => [Water Catcher, Drilling Rig, Eden Project, Divining Rods, Water Purifier, Vaporiser, Water Turrets,Shooting Gallery, Hydraulic Network, Vegetable Plot]
        36 => [136, 47], // Drilling Rig => [No Holes Barred, Divining Rocket]
        35 => [104], // Eden Project => [Mechanical Pump]
        31 => [30], // Water Purifier => [Water Filter]
        27 => [37, 26, 25, 39, 44], // Vaporiser => [Boiling Fog, Acid Spray, Gas Gun, Sprinkler System, Mist Spray]
        41 => [106, 46, 45, 40, 148], // Hydraulic Network => [Faucet, Decon Shower, Sluice, Shower, Nature area of the Survivalists]
        34 => [32, 33, 49, 151, 29], // Vegetable Plot => [Fertilizer, Grapeboom, Apple Tree, Ravaged pumpkins, Mines]
        33 => [149, 150], // Grapeboom => [Grenade Launcher, Vita-Mines]
        151 => [108], // Ravaged pumpkins => [Scarecrows]

        76 => [60, 63, 52, 68, 71, 65, 75, 64, 145, 109], // Workshop => [Factory, Building Registry, Butcher, Henhouse, Central Cafeteria, Small Café, Central Laboratory, Gallows, Chocolate Cross, Meat Locker]
        63 => [70, 110, 152, 125, 122], // Building Registry => [Defensive Adjustment, People’s Court, Technicians Workbench, Ministry of Slavery, Altar]
        52 => [69, 51, 160], // Butcher => [Abbatoir, Cremato-Cue, Pigsty]

        103 => [153, 79, 121, 143, 96, 80, 58, 146], // Watchtower => [Observation platform, Predictor, Lighthouse, Battlements, Catapult, Searchtower, Cannon Mounds, Public lights]
        153 => [77, 78, 102], // Observation platform => [Scanner, Upgraded Map, Scouts Lair]
        143 => [101, 161, 97, 165], // Battlements => [Guard Tower, Guardroom, Miniature Armory, Tamer Experiment Center]
        97 => [98, 162, 163, 164], // Miniature Armory => [Swedish Workshop, Manual grinder, Pet Shop, Filtering gutters]
        96 => [95, 100], // Catapult => [Upgraded Catapult, Small Trebuchet]
        58 => [54, 55, 56, 57], // Cannon Mounds => [Rock Cannon, Railgun, Plate Gun, Brutal Cannon]

        137 => [107, 105, 154, 127, 120, 128, 111, 53, 133, 134, 131, 124, 135, 129, 130, 123, 126], // Foundations => [The Big Rebuild, False Town, Underground city, Labyrinth, Garbage Dump, All or Nothing, Fortifications, Defensive Focus, Cinema, Air Strike, Firework Foray, Bonfire, Hot Air Balloon, Giant BRD, Buzzard's Wonder Wheel, Giant Sandcastle, Reactor]
        120 => [115, 119], // Garbage Dump => [Organized Dump, Dump Upgrade]
        111 => [72, 155], // Fortifications => [Community Involvement, Urban plan]

        156 =>  [157, 158, 67, 74, 62, 132, 159, 91, 93, 92, 90], // Sanctuary => [Soul Purifying Source, Pool, Small Cemetary, Infirmary, Architect’s Study, Crow Statue, Blue Gold Thermal baths, XXL Voodoo Doll, Sword of Bokor, Spirit Mirage, Holy Raincloud]
        157 => [144], // Soul Purifying Source => [Hammam]
        67 => [66], // Small Cemetary => [Coffin Catapult]
        62 => [73], // Architect’s Study => [Builder’s Merchant]

    ];

    /**
     * --------------------------------------------------------------------------
     * Water Added per Constructions list
     * --------------------------------------------------------------------------
     *
     * Water added of constructions
     */
    public $waterAdded = [
        "50" => 15, // Pump
        "43" => 2, // Water Catcher
        "36" => 50, // Drilling Rig
        "136" => 100, // No Holes Barred
        "47" => 60, // Divining Rocket
        "35" => 50, // Eden Project
        "104" => 75, // Mechanical Pump
        "48" => 100, // Divining Rods
        "41" => 5, // Hydraulic Network
    ];

    /**
     * --------------------------------------------------------------------------
     * Workshop conversions and respective number of conversions
     * --------------------------------------------------------------------------
     *
     * Workshop conversions, lower number of conversions first!
     */
    public $workshopConversions = [
        "49" => [["139","1"], ["141","1"]], // Twisted Plank => [[rotting log, 1], [Quality Log, 1]]
        "136" => [["49","1"], ["139", "2"], ["141", "2"]], // Patchwork Beam => [[Twisted Plank, 1], [Rotting Log, 2], [Quality Log, 2]]
        "50" => [["138","1"]], // Wrought Iron => [[Scrap Metal, 1]]
        "137" => [["50","1"], ["138", "2"]], // Metal Support => [[Wrought Iron, 1], [Scrap Metal, 2]]
        "52" => [["77","1"]], // Sheet Metal => [[Sheet Metal (parts), 1]]
        "91" => [["90","1"]], // Repair Kit (damaged) => [[Repair Kit, 1]]
    ];

}

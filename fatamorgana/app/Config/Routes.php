<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */

$routes->match(['GET', 'POST'], '/',                 'Login::index');
$routes->match(['GET', 'POST'], '/logout',           'Login::logout');

$routes->get('/',                                   'Login::index');
$routes->get('/login',                              'Login::index');
$routes->get('/login/skey',                         'Login::getSkey');
$routes->get('/login/skey/(:segment)',              'Login::getSkey/$1');
$routes->get('/login/skey/(:segment)/(:segment)',   'Login::getSkey/$1/$2');

$routes->get('/map',                                'Map::index');
$routes->get('/spy/town/(:num)',                    'Spy::town/$1');
$routes->get('/spy/town/(:num)/(:num)',             'Spy::town/$1/$2');

$routes->post('/',                                  'Login::index');
$routes->post('/login/auth',                        'Login::postAuth');
$routes->post('/map/update',                        'Map::update');
$routes->post('/map/updateRoutes',                  'Map::updateRoutes');
$routes->post('/map/updateExpedition',              'Map::updateExpedition');
//$routes->post('/map/updateBpsTracking',             'Map::updateBpsTracking');
$routes->post('/map/updateTabsInfo',                'Map::updateTabsInfo');
$routes->post('/map/updatemyzone',                  'Map::updatemyzone');
$routes->post('/map/updateAriadne',                 'Map::updateAriadne');
$routes->post('/map/updateFromExternal',                 'Map::updateFromExternal');

$routes->match(['GET', 'POST'], '/update',              'Update::index');
//$routes->match(['GET', 'POST'], '/update/mapindex',     'Update::mapindex');
//$routes->match(['GET', 'POST'], '/update/manual',       'Update::manual');
//$routes->match(['GET', 'POST'], '/update/constructions','Update::constructions');
$routes->match(['GET', 'POST'], '/getdata',              'GetData::index');
<?php

namespace App\Controllers;

use App\Models\SystemModel;
use App\Models\TownModel;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

class Map extends BaseController
{

	public string $debug = '';
	public ?string $key = null;
	public $xml = null;
	public $xmlSmall = null;
	private SystemModel $systemModel;
	private TownModel $townModel;

	/**
	 * Constructor.
	 * 
	 * Checks if user is logged in and has valid token. If no valid token is found, 
	 * creating a new one using the secure key from the town is tried. If that also
	 * fails, user is redirected to the login page.
	 */
	function __construct()
	{
		$this->systemModel = model('SystemModel');
		$this->townModel = model('TownModel');
	}

	public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Be sure to call the parent
        parent::initController($request, $response, $logger);

        // Get the current token data.
		$check = $this->systemModel->check();

		// Check if current user token is valid.
		if (!$check) {
			// Nope, let's try to find a valid key.
			$key = NULL;
			// (A) Try POST/GET data.
			if ((isset($_POST['key']) || isset($_GET['key'])) && isset($this->request)) {
				$key = $this->request->getGetPost('key');
			}
			// (B) Try token data (even though it is expired) from cookie.
			if (!$key) {
				$key = $this->systemModel->getSkey();
			}
			if ($key && trim($key) !== '') {
				// Got a key, now validate it and create new token.
				$xml = $this->systemModel->retrieveXMLsecureSmall($key);
				if (is_int($xml)) {
					return redirect()->to('login/skey/error/error_code_' . $xml);
				}
				if ($xml) {
					$game = $xml->headers->game;
					$gid = (int) $game['id'];
					$owner = $xml->headers->owner->citizen;
					$userid = (string) $owner['id'];
					$username = (string) $owner['name'];
					$cookie = $this->systemModel->createNewCookie($key, $gid, $username, $userid);
					$this->response->setCookie($cookie);
					$this->key = $key;
					$this->xmlSmall = $xml;
				} else {
					// Key seems to be invalid. Return to login page.
					return redirect()->to('login/skey/' . $key);
				}
			} else {
				// No key provided, redirect to login page.
				return redirect()->to('login');
			}
		} else {
			// Valid token, save the key.
			$this->key = $check->user_skey;
		}
    }

	/**
	 * Generate game map data.
	 */
	public function index() {
		if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
			$linkscheme = "https";
		else
			$linkscheme = "http";

		$data['main_content'] = 'map_view';
		$data['gamemap'] = $this->townModel->generateData();
		if (!$data['gamemap'] || $data['gamemap'] == FALSE) {
			if ($this->xmlSmall) {
				$xmlerror = $this->xmlSmall->error;
				$error_code = (string)$xmlerror['code'];
				if (isset($error_code) && $error_code != '') {
					return redirect()->to('login/skey/error/' . $error_code);
				}
			}
			// No game data found, new town or expired token.
			return redirect()->to('login/skey/error');
		}
		elseif (is_int($data['gamemap'])) {
			return redirect()->to('login/skey/error/error_code_' . $data['gamemap']);
		}
		if ($data['gamemap'] == "wrong_town") {
			$key = $this->systemModel->getSkey();
			return redirect()->to('login/skey/' . $key);
		}
		$data['bookmark'] = $linkscheme . '://' . $_SERVER['HTTP_HOST'] . '/login/skey/' . $this->key;

		return view('includes/template', $data);
	}

//	/**
//	 * Update BPS tracking.
//	 */
//	public function updateBpsTracking() {
//		$action = $this->request->getPost('action');
//
//		$gid = $this->systemModel->getGid();
//		$userid = $this->systemModel->getUserid();
//		$username = $this->systemModel->getUsername();
//
//		$data = $this->townModel->getTownData($gid);
//
//		// Check if game exists.
//		if ($data == NULL) {
//			$msg = 'Game does not exist.';
//			$result = json_encode([
//				'success' => FALSE,
//				'zoneupdate' => FALSE,
//				'error' => $msg,
//			]);
//			echo view('includes/apitemplate', [
//				'response' => $result,
//			]);
//			$data['response'] = 'ajaxInfoBad(__("MYZONEUPDATEDFAILEDNOTFOUND"));';
//			echo view('includes/ajaxtemplate', $data);
//			return;
//		}
//
//		switch ($action) {
//			case "ADD-BP": {
//					$bp_id = $this->request->getPost('bpid');
//					$data['bpsAndConstructions']->$bp_id->found = TRUE;
//					$data['bpsAndConstructions']->$bp_id->username = $username;
//					break;
//				}
//			case "REFRESH-BP": {
//					// Do nothing
//					// Just return the bp data
//					break;
//				}
//			case "REMOVE-BP": {
//					$bp_id = $this->request->getPost('bpid');
//					$data['bpsAndConstructions']->$bp_id->found = FALSE;
//					$data['bpsAndConstructions']->$bp_id->username = $username;
//					foreach ($data['bpsAndConstructions'] as $bpid => $bp) {
//						if ((string) $bp->parent === (string) $bp_id) {
//							$data['bpsAndConstructions']->$bpid->found = FALSE;
//							$data['bpsAndConstructions']->$bp_id->username = $username;
//						}
//					}
//					break;
//				}
//		}
//
//		if ($action !== "REFRESH-BP") {
//			$this->townModel->saveTowndata($gid, $data['system']['day'], $userid, serialize($data));
//		}
//
//		$res_scr = "updateLocalBps(" . json_encode($data['bpsAndConstructions']) . ");";
//		$append = "";
//		$data['response'] = $res_scr . "\n" . 'ajaxInfo(__("saved"));' . $append;
//
//		return view('includes/ajaxtemplate', $data);
//	}

    /**
     * Update Tabs info.
     */
    public function updateTabsInfo() {
        $gid = $this->systemModel->getGid();
        $userid = $this->systemModel->getUserid();
        $username = $this->systemModel->getUsername();

        $data = $this->townModel->getTownData($gid);

        // Check if game exists.
        if ($data == NULL) {
            $msg = 'Game does not exist.';
            $result = json_encode([
                'success' => FALSE,
                'zoneupdate' => FALSE,
                'error' => $msg,
            ]);
            echo view('includes/apitemplate', [
                'response' => $result,
            ]);
            $data['response'] = 'ajaxInfoBad(__("MYZONEUPDATEDFAILEDNOTFOUND"));';
            echo view('includes/ajaxtemplate', $data);
            return;
        }

        // Check if we are dead.
        $dead = 1;
        foreach ($data["citizens"] as $citizen) {
            if ($citizen["name"] === $username) {
                $dead = 0;
            }
        }
        if ($dead === 1) {
            $data['response'] = 'ajaxInfoBad(__("MYZONEUPDATEDFAILEDDEAD"));';
            return view('includes/ajaxtemplate', $data);
        }

        // Get already unlocked constructions
        $constructions_in_town = $this->systemModel->retrieveBuildingsInTownSecure();
        foreach ($constructions_in_town->map->city->chantiers as $construction) {
            if ($construction->rarity > 0) {
                $data['bpsAndConstructions']->{$construction->id}->found = TRUE;
            }
            if ($construction->actions < $data['bpsAndConstructions']->{$construction->id}->pa) {
                $data['bpsAndConstructions']->{$construction->id}->paleft = $construction->actions;
            }
        }

        foreach ($constructions_in_town->map->city->buildings as $building) {

            if ($data['bpsAndConstructions']->{$building->id}->rarity > 0 && (!isset($data['bpsAndConstructions']->{$building->id}->found) || $data['bpsAndConstructions']->{$building->id}->found === FALSE)) {
                $data['bpsAndConstructions']->{$building->id}->found = TRUE;
            }

            if (!array_key_exists($building->id, $data['constructions'])) {
                $data['constructions'][$building->id] = [
                    'image' => $building->icon,
                    'name' => $building->name,
                    'temp' => $building->temporary ? 1 : 0,
                ];
            }
        }

        $this->townModel->saveTowndata($gid, $data['system']['day'], $userid, serialize($data));

        $res_scr = "updateTabsInfo(" . json_encode($data['bpsAndConstructions']) . ", ". json_encode($data['constructions']) . ");";
        $append = "";
        $data['response'] = $res_scr . "\n" . 'ajaxInfo(__("info-updated"));' . $append;

        return view('includes/ajaxtemplate', $data);
    }

	public function updateExpedition() {
		$action = $this->request->getPost('action');
		$expeditionId = $this->request->getPost('expeditionid');
		$day = (int) $this->request->getPost('day');
		$name = $this->request->getPost('name');
		$type = $this->request->getPost('type');
		$ap = (int) $this->request->getPost('ap');
		$color = $this->request->getPost('color');

		// Base array for template variables.
		$template = [];
		$gid = $this->systemModel->getGid();
		$username = $this->systemModel->getUsername();
		$userid = $this->systemModel->getUserid();
		$data = $this->townModel->getTownData($gid);

		// Check if game exists.
		if ($data === FALSE) {
			$template['response'] = 'ajaxInfoBad(__("MYZONEUPDATEDFAILEDNOTFOUND"));';
			return view('includes/ajaxtemplate', $template);
		}

		// Check if we are dead.
		$dead = 1;
		foreach ($data["citizens"] as $citizen) {
			if ($citizen["name"] === $username) {
				$dead = 0;
			}
		}
		if ($dead === 1) {
			$data['response'] = 'ajaxInfoBad(__("MYZONEUPDATEDFAILEDDEAD"));';
			return view('includes/ajaxtemplate', $data);
		}

		$dbday = $data['system']['day'];

		$time = time();

		switch ($action) {
			case "ADD-ROUTE": {
					$route = $this->request->getPost('route');

					$calculatedAps = 0;
					$rp = explode('_', $route);
					array_pop($rp);
					$startX = null;
					$startY = null;
					foreach ($rp as $ro) {
						$points = explode('-', $ro);
						if ($startX !== null) {
							$calculatedAps += abs($startX - abs($points[0]));
							$calculatedAps += abs($startY - abs($points[1]));
						}
						$startX = abs($points[0]);
						$startY = abs($points[1]);
					}

					$newroute = array(
						'creator' => $userid,
						'creatorName' => $username,
						'day' => $day,
						'name' => $name . ' [' . $calculatedAps . 'AP] (' . $username . ' via FM)',
						'route' => $route,
						'type' => $type,
						'color' => $color,
					);
					if ($expeditionId === "null") {
						$data['expeditions']['fm.' . $userid . '.' . $time] = $newroute;
					} else {  // It is an edit of an existing route
						// Check if we have permission to change route
						if ($type !== 'collaborative' && $username !== $data['expeditions'][$expeditionId]['creatorName']) {
							$data['response'] = 'ajaxInfoBad(__("MYZONEUPDATEDFAILEDDEAD"));';
							return view('includes/ajaxtemplate', $data);
						}
						$exploded = explode('.', $expeditionId);
						$time = end($exploded);
						$newroute['editor'] = $username;
						$newroute['name'] = $name . ' [' . $ap . 'AP] (' . $data['expeditions'][$expeditionId]['creatorName'] . ', edited by ' . $username . ' via FM)';
						$newroute['creator'] = $data['expeditions'][$expeditionId]['creator'];
						$newroute['creatorName'] = $data['expeditions'][$expeditionId]['creatorName'];
						$data['expeditions'][$expeditionId] = $newroute;
					}

					break;
				}

			case "DELETE-ROUTE": {
					if (
						$data['expeditions'][$expeditionId]['creator'] === $userid ||
						($data['expeditions'][$expeditionId]['type'] === 'collaborative' && $username === $data['expeditions'][$expeditionId]['creatorName'])
					) {
						unset($data['expeditions'][$expeditionId]);
					}
					break;
				}
		}

		$this->townModel->saveTowndata($gid, $data['system']['day'], $userid, serialize($data));

		$res_scr = "time=" . $time . ";"; // Returning time to js, so it can autoload route
		$append = "saveExpeditions();";
		$data['response'] = $res_scr . "\n" . 'ajaxInfo(__("saved"));' . $append;

		return view('includes/ajaxtemplate', $data);
	}

	public function updateRoutes() {
		$action = $this->request->getPost('action');
		$route = $this->request->getPost('route');
		$rname = $this->request->getPost('rname');
		$key = $this->request->getPost('key');

		$xml = $this->xmlSmall;
		if (!$xml) {
			$xml = $this->systemModel->Sm($key);
		}

		$game = $xml->headers->game;
		$day = (int) $game['days'];
		$gid = (int) $game['id'];
		$owner = $xml->headers->owner->citizen;
		$username = (string) $owner['name'];
		$userid = (string) $owner['id'];

		// Base array for template variables.
		$template = [];

		$data = $this->townModel->getTownData($gid);

		// Check if game exists.
		if ($data === FALSE) {
			$template['response'] = 'ajaxInfoBad(__("MYZONEUPDATEDFAILEDNOTFOUND"));';
			return view('includes/ajaxtemplate', $template);
		}

		$dbday = $data['system']['day'];

		// Check if newest day in DB matches the day on API.
		if ($dbday != $day) {
			$template['response'] = 'ajaxInfoBad(__("MYZONEUPDATEDFAILEDWRONGDAY"));';
			return view('includes/ajaxtemplate', $template);
		}

		// Check if we are dead.
		$dead = (int) $owner['dead'];
		if ($dead === 1) {
			$template['response'] = 'ajaxInfoBad(__("MYZONEUPDATEDFAILEDDEAD"));';
			return view('includes/ajaxtemplate', $template);
		}

		switch ($action) {
			case "ADDROUTE": {
					$path = [['x' => $data['tx'], 'y' => $data['ty']]];
					$curX = $data['tx'];
					$curY = $data['ty'];
					$rpoints = explode('_', $route);
					foreach ($rpoints as $rp) {
						$rc = explode('-', $rp);
						if (count($rc) === 2) {
							if (!($rc[0] === $curX && $rc[1] === $curY)) {
								if ($rc[0] === $curX) {
									if ($rc[1] < $curY) {
										for ($i = $curY - 1; $i >= $rc[1]; $i--) {
											$path[] = [
												'x' => (int) $rc[0],
												'y' => (int) $i,
											];
										}
										$curY = $rc[1];
									} else {
										for ($i = $curY + 1; $i <= $rc[1]; $i++) {
											$path[] = [
												'x' => (int) $rc[0],
												'y' => (int) $i,
											];
										}
										$curY = $rc[1];
									}
								} elseif ($rc[1] === $curY) {
									if ($rc[0] < $curX) {
										for ($i = $curX - 1; $i >= $rc[0]; $i--) {
											$path[] = [
												'x' => (int) $i,
												'y' => (int) $rc[1],
											];
										}
										$curX = $rc[0];
									} else {
										for ($i = $curX + 1; $i <= $rc[0]; $i++) {
											$path[] = [
												'x' => (int) $i,
												'y' => (int) $rc[1],
											];
										}
										$curX = $rc[0];
									}
								}
							}
						}
					}
					array_shift($path);
					$newroute = [
						'creator' => $userid,
						'day' => $day,
						'name' => 'Tag ' . $day . ': ' . $rname . ' (' . $username . ' via FM) [' . count($path) . 'AP]',
						'route' => (object) $path,
					];

					$newroute['route'] = (object) $path;
					$data['expeditions']['fm.' . $userid . '.' . time()] = $newroute;
					$res_scr = '';
					$res_msg = 'Route wurde gespeichert. Bitte FM neu laden, um sie im Kartenmodus zu sehen.';
				}
		}

		$append = '';

		$this->townModel->saveTowndata($gid, $data['system']['day'], $userid, serialize($data));
		$data['response'] = 'ajaxInfo(__("' . $action . '"));' . $append;

		return view('includes/ajaxtemplate', $data);
	}

	public function updateAriadne() {
		$gid = $this->systemModel->getGid();
		$userid = $this->systemModel->getUserid();
		$username = $this->systemModel->getUsername();
		$x = $this->request->getPost('x');
		$y = $this->request->getPost('y');

		$data = $this->townModel->getTownData($gid);

		// Check if game exists.
		if ($data == NULL) {
			$data['response'] = 'ajaxInfoBad(__("MYZONEUPDATEDFAILEDNOTFOUND"));';
			return view('includes/ajaxtemplate', $data);
		}

		// Check if we are dead.
		$dead = 1;
		foreach ($data["citizens"] as $citizen) {
			if ($citizen["name"] === $username) {
				$dead = 0;
			}
		}
		if ($dead === 1) {
			$data['response'] = 'ajaxInfoBad(__("MYZONEUPDATEDFAILEDDEAD"));';
			return view('includes/ajaxtemplate', $data);
		}

		$data['map']['y' . $y]['x' . $x]['building']['ruin']['ariadne'] = $this->request->getPost('route');

		$res_scr = 'data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["ariadne"] = \'' . $this->request->getPost('route') . '\';';

		$this->townModel->saveTowndata($gid, $data['system']['day'], $userid, serialize($data));
		$data['response'] = $res_scr . "\n" . 'ajaxInfo(__("ariadne-saved"));';

		return view('includes/ajaxtemplate', $data);
	}

	public function updateFromExternal() {
		$key = $this->systemModel->getSkey();
		$gid = $this->systemModel->getGid();
		$userid = $this->systemModel->getUserid();
		$username = $this->systemModel->getUsername();
		$source = $this->request->getGetPost('source');
		
		$data = $this->townModel->getTownData($gid);

		$item_file = dirname(dirname(dirname(__FILE__))) . '/data/common/items_all';
		if (file_exists($item_file)) {
			$item_stat = stat($item_file);
			$item_data = unserialize(file_get_contents($item_file));
		}


		// Check if game exists.
		if ($data == NULL) {
			$data['response'] = 'ajaxInfoBad(__("MYZONEUPDATEDFAILEDNOTFOUND"));';
			return view('includes/ajaxtemplate', $data);
		}

		// Check if we are dead.
		$dead = 1;
		foreach ($data["citizens"] as $citizen) {
			if ($citizen["name"] === $username) {
				$dead = 0;
			}
		}
		if ($dead === 1) {
			$data['response'] = 'ajaxInfoBad(__("MYZONEUPDATEDFAILEDDEAD"));';
			return view('includes/ajaxtemplate', $data);
		}

		$externalmapdata = $this->systemModel->retrieveExternalMap($key, $source, $gid);

		//check if there is data from external
		if (!$externalmapdata || !isset($externalmapdata->map) || !isset($externalmapdata->map->id)) {
			$data['response'] = 'ajaxInfoBad(__("ERROREXTERNALDATANOTFOUND"));';
			return view('includes/ajaxtemplate', $data);
		}

		//check if its the right map
		if ($externalmapdata->map->id !== $gid) {
			$data['response'] = 'ajaxInfoBad(__("ERROREXTERNALDATAWRONG"));';
			return view('includes/ajaxtemplate', $data);
		}

		//check if its the right day -- required?
		if ($externalmapdata->map->days !== $data['system']['day']) {
			$data['response'] = 'ajaxInfoBad(__("ERROREXTERNALDATAWRONGDAY"));';
			return view('includes/ajaxtemplate', $data);
		}

		//return json_encode($data); // debug line
		//return json_encode($item_data); // debug line
		
		foreach ($externalmapdata->map->zones as $importzone) {
			if (isset($data["map"]) && isset($data["map"]["y" . $importzone->y]) && isset($data["map"]["y" . $importzone->y]["x" . $importzone->x]) && isset($importzone->extra) && isset($importzone->extra->updatedOn) && isset($importzone->extra->updatedOnDay)) {
				
				if ((isset($importzone->extra->updatedOn) && !isset($data["map"]["y" . $importzone->y]["x" . $importzone->x]["updatedOn"])) || ($importzone->extra->updatedOn -2 > $data["map"]["y" . $importzone->y]["x" . $importzone->x]["updatedOn"])) {

					//zone dried
					if (isset($importzone->details->dried) && $importzone->details->dried === TRUE) {
						$data["map"]["y" . $importzone->y]["x" . $importzone->x]["dried"] = 1;
					}  else {
						$data["map"]["y" . $importzone->y]["x" . $importzone->x]["dried"] = 0;
					}

					//zombies
					if (isset($importzone->details->z) && $importzone->details->z > 0) {
						$data["map"]["y" . $importzone->y]["x" . $importzone->x]["z"] = $importzone->details->z;
					}  else {
						$data["map"]["y" . $importzone->y]["x" . $importzone->x]["z"] = 0;
					}

					//max zombies
					if ($data['system']['day'] === $importzone->extra->updatedOnDay && isset($importzone->extra->deadz) && $importzone->extra->deadz > 0) {
						$data["map"]["y" . $importzone->y]["x" . $importzone->x]["zm"] = $importzone->extra->deadz + $data["map"]["y" . $importzone->y]["x" . $importzone->x]["z"];
					} elseif ($data['system']['day'] === $importzone->extra->updatedOnDay && $data["map"]["y" . $importzone->y]["x" . $importzone->x]["z"] > 0) {
						$data["map"]["y" . $importzone->y]["x" . $importzone->x]["zm"] = $data["map"]["y" . $importzone->y]["x" . $importzone->x]["z"];
					} else {
						$data["map"]["y" . $importzone->y]["x" . $importzone->x]["zm"] = -1;
					}

					//building
					if (isset($importzone->building) && isset($data["map"]["y" . $importzone->y]["x" . $importzone->x]["building"])) {
						//building dried
						if (isset($importzone->building->dried) && $importzone->building->dried === TRUE) {
							$data["map"]["y" . $importzone->y]["x" . $importzone->x]["building"]["dried"] = 1;
						}  else {
							$data["map"]["y" . $importzone->y]["x" . $importzone->x]["building"]["dried"] = 0;
						}
						//building camped
						if (isset($importzone->building->camped) && $importzone->building->camped === TRUE) {
							$data["map"]["y" . $importzone->y]["x" . $importzone->x]["building"]["blueprint"] = 1;
						}  else {
							$data["map"]["y" . $importzone->y]["x" . $importzone->x]["building"]["blueprint"] = 0;
						}
					}

					//updatedOn
					if (isset($importzone->extra->updatedOn)) {
						$data["map"]["y" . $importzone->y]["x" . $importzone->x]["updatedOn"] = $importzone->extra->updatedOn;
					}

					//updatedOnDay
					if (isset($importzone->extra->updatedOnDay)) {
						$data["map"]["y" . $importzone->y]["x" . $importzone->x]["updatedOnDay"] = $importzone->extra->updatedOnDay;
					}

					//updatedOnById
					if (isset($importzone->extra->updatedById)) {
						$data["map"]["y" . $importzone->y]["x" . $importzone->x]["updatedById"] = $importzone->extra->updatedById;
					}

					//updatedOnBy
					if (isset($importzone->extra->updatedById)) {

						$updatedBy = "unknown_name";

						if (isset($data['citizens'][$importzone->extra->updatedById])) {
							$updatedBy = $data['citizens'][$importzone->extra->updatedById]['name'];
						} elseif (isset($data['cadavers'])) {
							foreach ($data['cadavers'] as $cadaverday) {
								if (isset($cadaverday[$importzone->extra->updatedById])) {
									$updatedBy = $cadaverday[$importzone->extra->updatedById]['name'];
								}
							}
						}

						$data["map"]["y" . $importzone->y]["x" . $importzone->x]["updatedBy"] = $updatedBy;
					}
					
					//items
					if (isset($importzone->items)) {
						$data["map"]["y" . $importzone->y]["x" . $importzone->x]["items"] = [];
						foreach ($importzone->items as $importitem) {
							if (array_key_exists($importitem->id, $item_data)) {
								$item = [
									"id" => $importitem->broken === TRUE ? $importitem->id * -1 : $importitem->id,
									"broken" => $importitem->broken === TRUE ? 1 : 0,
									"count" => $importitem->count,
								];
								$data["map"]["y" . $importzone->y]["x" . $importzone->x]["items"][] = $item;
							}
						}
					}
				}

			}

		}



		/*todo:
		mark GH updates as sourced from GH
		delete this marker on every zone update type
		test username recognition from imports through citizen list and cadaver list
		make frontent show GH import marker, also get name from citizen/cadaver list, if possible
		*/

		$this->townModel->saveTowndata($gid, $data['system']['day'], $userid, serialize($data));

		$data['response'] = 'ajaxInfo(__("IMPORTDONE_RELOADMAP"));';
		return view('includes/ajaxtemplate', $data);
	}

	public function update() {
		$action = $this->request->getPost('action');
		$x = (int) $this->request->getPost('x');
		$y = (int) $this->request->getPost('y');
		$z = $this->request->getPost('z');
		$zm = $this->request->getPost('zm');
		$floor = $this->request->getPost('floor');
		$error = [];

		$gid = $this->systemModel->getGid();
		
		// Base array for template variables.
		$template = [];

		$data = $this->townModel->getTownData($gid);
		$day = $data['system']['day'];

		$username = $this->systemModel->getUsername();
		$userid = $this->systemModel->getUserid();
		$userjob = $data["citizens"][$userid]["job"];
		
		//check if game exists
		if ($data == FALSE) {
			$template['response'] = 'ajaxInfoBad(__("MYZONEUPDATEDFAILEDNOTFOUND"));';
			return view('includes/ajaxtemplate', $template);
		}

		// Check if we are dead.
		$dead = 1;
		foreach ($data["citizens"] as $citizen) {
			if ($citizen["name"] === $username) {
				$dead = 0;
			}
		}
		if ($dead === 1) {
			$data['response'] = 'ajaxInfoBad(__("MYZONEUPDATEDFAILEDDEAD"));';
			return view('includes/ajaxtemplate', $data);
		}

		$zoneUpdate = FALSE;
		$scoutUpdate = FALSE;
		$soulUpdate = FALSE;

		$ruinInit = [
			'tile' => 0,
			'z' => -1,
			'door' => 0,
			'lock' => 0,
			'comment' => ''
		];

		switch ($action) {
			case "ZONE-REGENERATE": {
					$data['map']['y' . $y]['x' . $x]['dried'] = 0;
					$res_scr = 'data.map["y' . $y . '"]["x' . $x . '"]["dried"] = 0;'
						. 'replaceMapZone(' . $y . ',' . $x . ');'
						. 'checkCitidots();'
						. 'document.getElementById("x' . $data['map']['y' . $y]['x' . $x]['rx'] . 'y' . $data['map']['y' . $y]['x' . $x]['ry'] . '").click();';
					$res_msg = 'Zone wurde regeneriert.';
					$zoneUpdate = TRUE;
					break;
				}
			case "ZONE-DEPLETE": {
					$data['map']['y' . $y]['x' . $x]['dried'] = 1;
					$res_scr = 'data.map["y' . $y . '"]["x' . $x . '"]["dried"] = 1;'
						. 'replaceMapZone(' . $y . ',' . $x . ');'
						. 'checkCitidots();'
						. 'document.querySelector("#x' . $data['map']['y' . $y]['x' . $x]['rx'] . 'y' . $data['map']['y' . $y]['x' . $x]['ry'] . '").click();';
					$res_msg = 'Zone wurde geleert.';
					$zoneUpdate = TRUE;
					break;
				}
			case "BUILDING-REGENERATE": {
					$data['map']['y' . $y]['x' . $x]['building']['dried'] = 0;
					$res_scr = 'data.map["y' . $y . '"]["x' . $x . '"]["building"]["dried"] = 0;'
						. 'replaceMapZone(' . $y . ',' . $x . ');'
						. 'document.querySelector("#x' . $data['map']['y' . $y]['x' . $x]['rx'] . 'y' . $data['map']['y' . $y]['x' . $x]['ry'] . '").click();';
					$res_msg = 'Gebäude wurde regeneriert.';
					$zoneUpdate = TRUE;
					break;
				}
			case "BUILDING-DEPLETE": {
					$data['map']['y' . $y]['x' . $x]['building']['dried'] = 1;
					$res_scr = 'data.map["y' . $y . '"]["x' . $x . '"]["building"]["dried"] = 1;'
						. 'replaceMapZone(' . $y . ',' . $x . ');'
						. 'checkCitidots();'
						. 'document.querySelector("#x' . $data['map']['y' . $y]['x' . $x]['rx'] . 'y' . $data['map']['y' . $y]['x' . $x]['ry'] . '").click();';
					$res_msg = 'Gebäude wurde geleert.';
					$zoneUpdate = TRUE;
					break;
				}
			case "BLUEPRINT-AVAILABLE": {
					$data['map']['y' . $y]['x' . $x]['building']['blueprint'] = 0;
					$res_scr = 'data.map["y' . $y . '"]["x' . $x . '"]["building"]["blueprint"] = 0;'
						. 'replaceMapZone(' . $y . ',' . $x . ');'
						. 'checkCitidots();'
						. 'document.querySelector("#x' . $data['map']['y' . $y]['x' . $x]['rx'] . 'y' . $data['map']['y' . $y]['x' . $x]['ry'] . '").click();';
					$res_msg = 'Blueprint ist wieder verfügbar.';
					$zoneUpdate = TRUE;
					break;
				}
			case "BLUEPRINT-FOUND": {
					$data['map']['y' . $y]['x' . $x]['building']['blueprint'] = 1;
					$res_scr = 'data.map["y' . $y . '"]["x' . $x . '"]["building"]["blueprint"] = 1;'
						. 'replaceMapZone(' . $y . ',' . $x . ');'
						. 'checkCitidots();'
						. 'document.querySelector("#x' . $data['map']['y' . $y]['x' . $x]['rx'] . 'y' . $data['map']['y' . $y]['x' . $x]['ry'] . '").click();';
					$res_msg = 'Blueprint wurde gefunden.';
					$zoneUpdate = TRUE;
					break;
				}
			case "BUILDING-GUESS": {
					if ($z === -2 || $z === "-2") {
						unset($data['map']['y' . $y]['x' . $x]['building']['guess']);
						$res_scr = 'delete(data.map["y' . $y . '"]["x' . $x . '"]["building"]["guess"]);'
						. 'replaceMapZone(' . $y . ',' . $x . ');'
						. 'checkCitidots();'
						. 'document.querySelector("#x' . $data['map']['y' . $y]['x' . $x]['rx'] . 'y' . $data['map']['y' . $y]['x' . $x]['ry'] . '").click();';
					} else {
						$data['map']['y' . $y]['x' . $x]['building']['guess'] = $z;
						$res_scr = 'data.map["y' . $y . '"]["x' . $x . '"]["building"]["guess"] = ' . $z . ';'
						. 'replaceMapZone(' . $y . ',' . $x . ');'
						. 'checkCitidots();'
						. 'document.querySelector("#x' . $data['map']['y' . $y]['x' . $x]['rx'] . 'y' . $data['map']['y' . $y]['x' . $x]['ry'] . '").click();';
					}
					$res_msg = 'Vermutung wurde eingetragen.';
					$zoneUpdate = FALSE;
					break;
				}
			case "ZONE-ADDSOUL": {
					if (!array_key_exists('souls', $data)) {
						$data['souls'] = [];
					}
					if (!array_key_exists('y' . $y, $data['souls'])) {
						$data['souls']['y' . $y] = [];
					}
					if (!array_key_exists('x' . $x, $data['souls']['y' . $y])) {
						$data['souls']['y' . $y]['x' . $x] = [];
					}
					$data['souls']['y' . $y]['x' . $x]['lostsoul'] = 1;
					$res_scr = 'data.souls["y' . $y . '"]["x' . $x . '"]["lostsoul"] = 1;';
					$res_msg = 'Verlorene Seele hinzugefügt.';
					$soulUpdate = TRUE;
					break;
				}
			case "ZONE-DELSOUL": {
					if (!array_key_exists('souls', $data)) {
						$data['souls'] = [];
					}
					if (!array_key_exists('y' . $y, $data['souls'])) {
						$data['souls']['y' . $y] = [];
					}
					if (!array_key_exists('x' . $x, $data['souls']['y' . $y])) {
						$data['souls']['y' . $y]['x' . $x] = [];
					}
					$data['souls']['y' . $y]['x' . $x]['lostsoul'] = 0;
					$res_scr = 'data.souls["y' . $y . '"]["x' . $x . '"]["lostsoul"] = 0;';
					$res_msg = 'Verlorene Seele entfernt.';
					$soulUpdate = TRUE;
					break;
				}
			case "UPDATE-ZOMBIES": {
					$data['map']['y' . $y]['x' . $x]['z'] = $z;
					if ($data['map']['y' . $y]['x' . $x]['nvt'] == 1) {
						$data['map']['y' . $y]['x' . $x]['danger'] = null;
					}
					if ($data['map']['y' . $y]['x' . $x]['danger'] == null) {
						$danger = "null";
					} else {
						$danger = (string) $data['map']['y' . $y]['x' . $x]['danger'];
					}

					if ($data['map']['y' . $y]['x' . $x]['zm'] < $z) {
						$data['map']['y' . $y]['x' . $x]['zm'] = $z;
					}
					$res_scr = 'data.map["y' . $y . '"]["x' . $x . '"]["danger"] = ' . $danger . ';data.map["y' . $y . '"]["x' . $x . '"]["z"] = ' . $z . ';data.map["y' . $y . '"]["x' . $x . '"]["zm"] = ' . $data['map']['y' . $y]['x' . $x]['zm'] . ';'
						. 'replaceMapZone(' . $y . ',' . $x . ');'
						. 'checkCitidots();'
						. 'document.querySelector("#x' . $data['map']['y' . $y]['x' . $x]['rx'] . 'y' . $data['map']['y' . $y]['x' . $x]['ry'] . '").click();';
					$res_msg = 'Zombies wurden gespeichert.';
					$zoneUpdate = TRUE;
					break;
				}
			case "UPDATE-MAXZOMBIES": {
					$data['map']['y' . $y]['x' . $x]['zm'] = $zm;
					$res_scr = 'data.map["y' . $y . '"]["x' . $x . '"]["zm"] = ' . $zm . ';'
						. 'replaceMapZone(' . $y . ',' . $x . ');'
						. 'checkCitidots();'
						. 'document.querySelector("#x' . $data['map']['y' . $y]['x' . $x]['rx'] . 'y' . $data['map']['y' . $y]['x' . $x]['ry'] . '").click();';
					$res_msg = 'Max. Zombies wurden gespeichert.';
					$zoneUpdate = TRUE;
					break;
				}
			case "UPDATE-SCOUTZOMBIES": {
					if (!isset($data['scout'])) {
						$data['scout'] = [];
					}
					if (!isset($data['scout']['y' . $y . 'x' . $x])) {
						$data['scout']['y' . $y . 'x' . $x] = [];
					}
					if (!isset($data['scout']['y' . $y . 'x' . $x]['pbl'])) {
						$data['scout']['y' . $y . 'x' . $x]['pbl'] = 0;
					}
					$data['scout']['y' . $y . 'x' . $x]['zom'] = $z;

					$res_scr = 'data.scout["y' . $y . 'x' . $x . '"] = new Array();data.scout["y' . $y . 'x' . $x . '"]["zom"] = ' . $z . ';data.scout["y' . $y . 'x' . $x . '"]["pbl"] = ' . $data['scout']['y' . $y . 'x' . $x]['pbl'] . ';';
					$res_msg = 'Zombies (Scout) wurden gespeichert.';
					$scoutUpdate = TRUE;
					break;
				}
			case "BUILDING-PROBABLE": {
					if (!isset($data['scout'])) {
						$data['scout'] = [];
					}
					if (!isset($data['scout']['y' . $y . 'x' . $x])) {
						$data['scout']['y' . $y . 'x' . $x] = [];
					}
					if (!isset($data['scout']['y' . $y . 'x' . $x]['zom'])) {
						$data['scout']['y' . $y . 'x' . $x]['zom'] = 0;
					}
					$data['scout']['y' . $y . 'x' . $x]['pbl'] = 1;
					$res_scr = 'data.scout["y' . $y . 'x' . $x . '"] = new Array();data.scout["y' . $y . 'x' . $x . '"]["pbl"] = 1;data.scout["y' . $y . 'x' . $x . '"]["zom"] = ' . $data['scout']['y' . $y . 'x' . $x]['zom'] . ';';
					$res_msg = 'Gebäudevermutung wurde gespeichert.';
					$scoutUpdate = TRUE;
					break;
				}
			case "BUILDING-NOTPROBABLE": {
					if (!isset($data['scout'])) {
						$data['scout'] = [];
					}
					if (!isset($data['scout']['y' . $y . 'x' . $x])) {
						$data['scout']['y' . $y . 'x' . $x] = [];
					}
					if (!isset($data['scout']['y' . $y . 'x' . $x]['zom'])) {
						$data['scout']['y' . $y . 'x' . $x]['zom'] = 0;
					}
					$data['scout']['y' . $y . 'x' . $x]['pbl'] = 0;
					$res_scr = 'data.scout["y' . $y . 'x' . $x . '"] = new Array();data.scout["y' . $y . 'x' . $x . '"]["pbl"] = 0;data.scout["y' . $y . 'x' . $x . '"]["zom"] = ' . $data['scout']['y' . $y . 'x' . $x]['zom'] . ';';
					$res_msg = 'Gebäudevermutung wurde entfernt.';
					$scoutUpdate = TRUE;
					break;
				}
			case "CITIZEN-LOCATION": {
				    $lastX = $lastY = NULL;
					$oid = $userid;
					foreach ($data['map'] as $ly => $ydata) {
						foreach ($ydata as $lx => $zone) {
							if (isset($zone['citizens'][$oid])) {
								$lastX = $lx;
								$lastY = $ly;
								unset($data['map'][$ly][$lx]['citizens'][$oid]);
							}
						}
					}
					$data['map']['y' . $y]['x' . $x]['citizens'][$oid]['name'] = $userid;
					$data['citizens'][$oid]['x'] = (int) $x;
					$data['citizens'][$oid]['y'] = (int) $y;
					$data['citizens'][$oid]['rx'] = (int) $x - $data['tx'];
					$data['citizens'][$oid]['ry'] = (int) $data['ty'] - $y;
					$res_scr = 'reMoveCitizen("' . $oid . '");'
						. 'if ( data.map["y' . $y . '"]["x' . $x . '"]["citizens"] == undefined ) { data.map["y' . $y . '"]["x' . $x . '"]["citizens"] = new Array(); }'
						. 'data.map["y' . $y . '"]["x' . $x . '"]["citizens"]["' . $oid . '"] = new Array();'
						. 'data.map["y' . $y . '"]["x' . $x . '"]["citizens"]["' . $oid . '"]["name"] = "' . $username . '";'
						. 'data.map["y' . $y . '"]["x' . $x . '"]["citizens"]["' . $oid . '"]["job"] = "' . $userjob . '";'
						. 'data.citizens["' . $oid . '"]["x"] = ' . ((int) $x) . ';'
						. 'data.citizens["' . $oid . '"]["y"] = ' . ((int) $y) . ';'
						. 'data.citizens["' . $oid . '"]["rx"] = ' . ((int) $x - $data['tx']) . ';'
						. 'data.citizens["' . $oid . '"]["ry"] = ' . ((int) $data['ty'] - $y) . ';'
						. 'reMoveCitidot("'. $x .'", "'. $y .'");'
						. 'document.querySelector("#x' . $data['map']['y' . $y]['x' . $x]['rx'] . 'y' . $data['map']['y' . $y]['x' . $x]['ry'] . '").click();'
						. 'generateCitizenList()';
					$res_msg = 'Deine Position wurde gespeichert.';
					$zoneUpdate = TRUE;
					break;
				}
			case "ZONE-ITEMS": {
					$jitems = unserialize($z);
					// a:1:{i:0;a:3:{s:2:"id";i:81;s:5:"count";i:1;s:6:"broken";i:0;}}
					$items = [];
					foreach ($jitems as $id => $count) {
						if ($count > 0) {
							$items[] = [
								'id' => $id,
								'count' => $count,
								'broken' => ($id < 0 ? 1 : 0),
							];
						}
					}
					#array_filter($items);
					$data['map']['y' . $y]['x' . $x]['items'] = $items;
					$res_scr = 'var newItems = ' . json_encode($items) . ';'
						. 'data.map["y' . $y . '"]["x' . $x . '"]["items"] = newItems;'
						. 'replaceMapZone(' . $y . ',' . $x . ');'
						. 'checkCitidots();'
						. 'document.querySelector("#x' . $data['map']['y' . $y]['x' . $x]['rx'] . 'y' . $data['map']['y' . $y]['x' . $x]['ry'] . '").click();';
					$res_msg = 'Gegenstände wurden gespeichert.';
					$zoneUpdate = TRUE;
					break;
				}
			case "RUIN-TILE": {
					$zdata = explode("|", $z);
					$zdata[0] = $zdata[0] + 7;
					if (!isset($data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]])) {
						$data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]] = [
							'x' . $zdata[0] => [
								'tile' => 0,
								'z' => -1,
								'door' => 0,
								'lock' => 0,
								'comment' => '',
							]
						];
					} elseif (!isset($data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]]['x' . $zdata[0]])) {
						$data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]]['x' . $zdata[0]] = [
							'tile' => 0,
							'z' => -1,
							'door' => 0,
							'lock' => 0,
							'comment' => ''
						];
					}
					$data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]]['x' . $zdata[0]]['tile'] = $zdata[2];
					$data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]]['x' . $zdata[0]]['updatedOn'] = time();
					$data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]]['x' . $zdata[0]]['updatedBy'] = $username;

					$res_scr = 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"] = new Array(); } '
						. 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"]["y' . $zdata[1] . '"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"]["y' . $zdata[1] . '"] = new Array(); } '
						. 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"] = new Array(); } '
						. 'data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"]["tile"] = ' . $zdata[2] . ';'
						. 'generateRuinMap(' . $x . ',' . $y . '); $("#ruinmap-wrapper").slideDown(500);'
						. '$("#' . $floor . 'ruinmap #x' . ($zdata[0] - 7) . 'y' . $zdata[1] . '").click();';
					$res_msg = 'Ruinenfeld gespeichert.';
					$zoneUpdate = FALSE;
					break;
				}
			case "RUIN-DOORLOCK": {
					$zdata = explode("|", $z);
					$zdata[0] = $zdata[0] + 7;
					if (!isset($data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]])) {
						$data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]] = [
							'x' . $zdata[0] => $ruinInit
						];
					} elseif (!isset($data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]]['x' . $zdata[0]])) {
						$data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]]['x' . $zdata[0]] = $ruinInit;
					}
					if (!isset($data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]]['x' . $zdata[0]]['doorlock'])) {
						$data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]]['x' . $zdata[0]]['doorlock'] = 0;
					}
					if (($zdata[2] == '0' && $data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]]['x' . $zdata[0]]['doorlock'] == 6) || ($zdata[2] == '6' || $zdata[2] == 6)) {
						$data['map']['y' . $y]['x' . $x]['building']['ruin']['lower']['y' . $zdata[1]]['x' . $zdata[0]]['doorlock'] = $zdata[2];
						$data['map']['y' . $y]['x' . $x]['building']['ruin']['upper']['y' . $zdata[1]]['x' . $zdata[0]]['doorlock'] = $zdata[2];
						$res_scr = 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["upper"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["upper"] = new Array(); } '
							. 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["lower"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["lower"] = new Array(); } '
							. 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["upper"]["y' . $zdata[1] . '"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["upper"]["y' . $zdata[1] . '"] = new Array(); } '
							. 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["lower"]["y' . $zdata[1] . '"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["lower"]["y' . $zdata[1] . '"] = new Array(); } '
							. 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["upper"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["upper"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"] = new Array(); } '
							. 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["lower"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["lower"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"] = new Array(); } '
							. 'data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["upper"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"]["doorlock"] = ' . $zdata[2] . ';'
							. 'data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["lower"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"]["doorlock"] = ' . $zdata[2] . ';'
							. 'generateRuinMap(' . $x . ',' . $y . '); $("#ruinmap-wrapper").slideDown(500);'
							. '$("#' . $floor . 'ruinmap #x' . ($zdata[0] - 7) . 'y' . $zdata[1] . '").click();';
					} elseif ($data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]]['x' . $zdata[0]]['doorlock'] == 6) {
						$inverse = 'lower';
						if ($floor == 'lower') $inverse = 'upper';
						$data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]]['x' . $zdata[0]]['doorlock'] = $zdata[2];
						$data['map']['y' . $y]['x' . $x]['building']['ruin'][$inverse]['y' . $zdata[1]]['x' . $zdata[0]]['doorlock'] = 0;
						$res_scr = 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["upper"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["upper"] = new Array(); } '
							. 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["lower"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["lower"] = new Array(); } '
							. 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["upper"]["y' . $zdata[1] . '"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["upper"]["y' . $zdata[1] . '"] = new Array(); } '
							. 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["lower"]["y' . $zdata[1] . '"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["lower"]["y' . $zdata[1] . '"] = new Array(); } '
							. 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["upper"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["upper"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"] = new Array(); } '
							. 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["lower"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["lower"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"] = new Array(); } '
							. 'data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"]["doorlock"] = ' . $zdata[2] . ';'
							. 'data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $inverse . '"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"]["doorlock"] = 0;'
							. 'generateRuinMap(' . $x . ',' . $y . '); $("#ruinmap-wrapper").slideDown(500);'
							. '$("#' . $floor . 'ruinmap #x' . ($zdata[0] - 7) . 'y' . $zdata[1] . '").click();';
					} else {
						$data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]]['x' . $zdata[0]]['doorlock'] = $zdata[2];
						$res_scr = 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"] = new Array(); } '
							. 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"]["y' . $zdata[1] . '"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"]["y' . $zdata[1] . '"] = new Array(); } '
							. 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"] = new Array(); } '
							. 'data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"]["doorlock"] = ' . $zdata[2] . ';'
							. 'generateRuinMap(' . $x . ',' . $y . '); $("#ruinmap-wrapper").slideDown(500);'
							. '$("#' . $floor . 'ruinmap #x' . ($zdata[0] - 7) . 'y' . $zdata[1] . '").click();';
					}

					$res_msg = 'Ruinenfeld gespeichert.';
					$zoneUpdate = FALSE;
					break;
				}
			case "RUIN-DOOR":
				//this isn't used
				{
					$zdata = explode("|", $z);
					$zdata[0] = $zdata[0] + 7;
					if (!isset($data['map']['y' . $y]['x' . $x]['building']['ruin']['y' . $zdata[1]])) {
						$data['map']['y' . $y]['x' . $x]['building']['ruin']['y' . $zdata[1]] = [
							'x' . $zdata[0] => $ruinInit
						];
					} elseif (!isset($data['map']['y' . $y]['x' . $x]['building']['ruin']['y' . $zdata[1]]['x' . $zdata[0]])) {
						$data['map']['y' . $y]['x' . $x]['building']['ruin']['y' . $zdata[1]]['x' . $zdata[0]] = $ruinInit;
					}
					$data['map']['y' . $y]['x' . $x]['building']['ruin']['y' . $zdata[1]]['x' . $zdata[0]]['door'] = $zdata[2];

					$res_scr = 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["y' . $zdata[1] . '"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["y' . $zdata[1] . '"] = new Array(); } '
						. 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"] = new Array(); } '
						. 'data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"]["door"] = ' . $zdata[2] . ';'
						. 'generateRuinMap(' . $x . ',' . $y . '); $("#ruinmap-wrapper").slideDown(500);'
						. '$("#' . $floor . 'ruinmap #x' . ($zdata[0] - 7) . 'y' . $zdata[1] . '").click();';
					$res_msg = 'Ruinenfeld gespeichert.';
					$zoneUpdate = FALSE;
					break;
				}
			case "RUIN-LOCK":
				//this isn't used
				{
					$zdata = explode("|", $z);
					$zdata[0] = $zdata[0] + 7;
					if (!isset($data['map']['y' . $y]['x' . $x]['building']['ruin']['y' . $zdata[1]])) {
						$data['map']['y' . $y]['x' . $x]['building']['ruin']['y' . $zdata[1]] = [
							'x' . $zdata[0] => $ruinInit
						];
					} elseif (!isset($data['map']['y' . $y]['x' . $x]['building']['ruin']['y' . $zdata[1]]['x' . $zdata[0]])) {
						$data['map']['y' . $y]['x' . $x]['building']['ruin']['y' . $zdata[1]]['x' . $zdata[0]] = $ruinInit;
					}
					$data['map']['y' . $y]['x' . $x]['building']['ruin']['y' . $zdata[1]]['x' . $zdata[0]]['lock'] = $zdata[2];

					$res_scr = 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["y' . $zdata[1] . '"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["y' . $zdata[1] . '"] = new Array(); } '
						. 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"] = new Array(); } '
						. 'data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"]["lock"] = ' . $zdata[2] . ';'
						. 'generateRuinMap(' . $x . ',' . $y . '); $("#ruinmap-wrapper").slideDown(500);'
						. '$("#' . $floor . 'ruinmap #x' . ($zdata[0] - 7) . 'y' . $zdata[1] . '").click();';
					$res_msg = 'Ruinenfeld gespeichert.';
					$zoneUpdate = FALSE;
					break;
				}
			case "RUIN-ZOMBIE": {
					$zdata = explode("|", $z);
					$zdata[0] = $zdata[0] + 7;
					if (!isset($data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]])) {
						$data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]] = [
							'x' . $zdata[0] => $ruinInit
						];
					} elseif (!isset($data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]]['x' . $zdata[0]])) {
						$data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]]['x' . $zdata[0]] = $ruinInit;
					}
					$data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]]['x' . $zdata[0]]['z'] = $zdata[2];

					$res_scr = 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"] = new Array(); }'
						. 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"]["y' . $zdata[1] . '"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"]["y' . $zdata[1] . '"] = new Array(); } '
						. 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"] = new Array(); } '
						. 'data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"]["z"] = ' . $zdata[2] . ';'
						. 'generateRuinMap(' . $x . ',' . $y . '); $("#ruinmap-wrapper").slideDown(500);'
						. '$("#' . $floor . 'ruinmap #x' . ($zdata[0] - 7) . 'y' . $zdata[1] . '").click();';
					$res_msg = 'Ruinenfeld gespeichert.';
					$zoneUpdate = FALSE;
					break;
				}
			case "RUIN-COMMENT": {
					$z = strip_tags($z);
					$z = str_replace(["\n", "\r", '"', "'", '`'], ['\n', '\r', '´', '´', '´'], $z);
					$zdata = explode("|", $z);
					$zdata[0] = $zdata[0] + 7;
					if (!isset($data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]])) {
						$data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]] = [
							'x' . $zdata[0] => $ruinInit
						];
					} elseif (!isset($data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]]['x' . $zdata[0]])) {
						$data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]]['x' . $zdata[0]] = $ruinInit;
					}
					$data['map']['y' . $y]['x' . $x]['building']['ruin'][$floor]['y' . $zdata[1]]['x' . $zdata[0]]['comment'] = $zdata[2];

					$res_scr = 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"]["y' . $zdata[1] . '"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"]["y' . $zdata[1] . '"] = new Array(); } '
						. 'if (data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"] == undefined) { data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"] = new Array(); } '
						. 'data.map["y' . $y . '"]["x' . $x . '"]["building"]["ruin"]["' . $floor . '"]["y' . $zdata[1] . '"]["x' . $zdata[0] . '"]["comment"] = "' . $zdata[2] . '";'
						. 'generateRuinMap(' . $x . ',' . $y . '); $("#ruinmap-wrapper").slideDown(500);'
						. '$("#' . $floor . 'ruinmap #x' . ($zdata[0] - 7) . 'y' . $zdata[1] . '").click();';
					$res_msg = 'Ruinenfeld gespeichert.';
					$zoneUpdate = FALSE;
					break;
				}
			case "UPDATE-STORM": {
					if (!array_key_exists('stormstamp', $data)) {
						$data['stormstamp'] = [];
					}
					$data['stormstamp'][$data['system']['day']] = time();
					$data['storm'][$data['system']['day']] = $z;
					$res_scr = 'data.storm["' . $data['system']['day'] . '"] = ' . $z . ';'
						. 'generateStormList();';
					$res_msg = 'Sturm wurde gespeichert.';
					$zoneUpdate = FALSE;
					break;
				}
		}
		if ($zoneUpdate == TRUE) {
			$data['map']['y' . $y]['x' . $x]['updatedOn'] = time();
			$data['map']['y' . $y]['x' . $x]['updatedBy'] = $username;
			$data['map']['y' . $y]['x' . $x]['updatedById'] = (int) $userid;
			$data['map']['y' . $y]['x' . $x]['updatedOnDay'] = $data['system']['day'];
			$res_scr = 'if(data.map["y' . $y . '"] === undefined){data.map["y' . $y . '"]=[];}if(data.map["y' . $y . '"]["x' . $x . '"] === undefined){data.map["y' . $y . '"]["x' . $x . '"]=[];}data.map["y' . $y . '"]["x' . $x . '"]["updatedOn"] = ' . time() . ';'
				. 'data.map["y' . $y . '"]["x' . $x . '"]["updatedBy"] = "' . $username . '";'
				. 'data.map["y' . $y . '"]["x' . $x . '"]["updatedById"] = "' . (int) $userid . '";'
				. 'data.map["y' . $y . '"]["x' . $x . '"]["updatedOnDay"] = "' . $data['system']['day'] . '";'
				. $res_scr;
		} elseif ($scoutUpdate == TRUE) {
			$rx = $x - $data['tx'];
			$ry = $data['ty'] - $y;
			if ($action === 'UPDATE-SCOUTZOMBIES') {
				$data['scout']['y' . $y . 'x' . $x]['updatedOn'] = time();
				$data['scout']['y' . $y . 'x' . $x]['updatedBy'] = $username;
				$data['scout']['y' . $y . 'x' . $x]['updatedById'] = (int) $userid;
				$data['scout']['y' . $y . 'x' . $x]['updatedOnDay'] = $data['system']['day'];
				$res_scr = $res_scr
					. 'data.scout["y' . $y . 'x' . $x . '"]["updatedOn"] = ' . time() . ';'
					. 'data.scout["y' . $y . 'x' . $x . '"]["updatedBy"] = "' . $username . '";'
					. 'data.scout["y' . $y . 'x' . $x . '"]["updatedById"] = "' . (int) $userid . '";'
					. 'data.scout["y' . $y . 'x' . $x . '"]["updatedOnDay"] = "' . $data['system']['day'] . '";';
			}
			elseif (isset($data['scout']['y' . $y . 'x' . $x]['updatedOn'])) {
				$res_scr = $res_scr
					. 'data.scout["y' . $y . 'x' . $x . '"]["updatedOn"] = ' . $data['scout']['y' . $y . 'x' . $x]['updatedOn'] . ';'
					. 'data.scout["y' . $y . 'x' . $x . '"]["updatedBy"] = "' . $data['scout']['y' . $y . 'x' . $x]['updatedBy'] . '";'
					. 'data.scout["y' . $y . 'x' . $x . '"]["updatedById"] = "' . $data['scout']['y' . $y . 'x' . $x]['updatedById'] . '";'
					. 'data.scout["y' . $y . 'x' . $x . '"]["updatedOnDay"] = "' . $data['scout']['y' . $y . 'x' . $x]['updatedOnDay'] . '";';
				
			}
			$res_scr = $res_scr
					. 'replaceMapZone(' . $y . ',' . $x . ');'
					. 'checkCitidots();'
					. 'document.querySelector("#x' . $rx . 'y' . $ry . '").click();';
		} elseif ($soulUpdate == TRUE) {
			$rx = $x - $data['tx'];
			$ry = $data['ty'] - $y;
			$res_scr = 'if (data.souls === undefined) { data.souls = {}; }' . 
				'if (data.souls["y' . $y . '"] === undefined) { data.souls["y' . $y . '"] = {}; }' .
				'if (data.souls["y' . $y . '"]["x' . $x . '"] === undefined) { data.souls["y' . $y . '"]["x' . $x . '"] = {}; }' .
				$res_scr;
			$res_scr = $res_scr
				. 'replaceMapZone(' . $y . ',' . $x . ');'
				. 'document.querySelector("#x' . $rx . 'y' . $ry . '").click();';
		}

		if ($zoneUpdate || $scoutUpdate || $soulUpdate) {
			if (isset($userid) && isset($data['citizens'][(string) $userid]) && isset($data['citizens'][(string) $userid]['name'])) {
				//last update timestamp
					$data['citizens'][(string) $userid]['lastUpdateTime'] = time();
					$data['citizens'][(string) $userid]['lastUpdateDay'] = $day;
				}
		}

		$append = '';
		if (count($error) === 0) {
			$this->townModel->saveTowndata($gid, $data['system']['day'], $userid, serialize($data));
			$data['response'] = $res_scr . "\n" . 'ajaxInfo(__("' . $action . '"));' . $append;
		} elseif (count($error) > 0) {
			$msg = implode(" ", $error);
			$data['response'] = 'alert(__("' . $msg . '"));';
		}
		return view('includes/ajaxtemplate', $data);
	}

	public function updatemyzone() {
		$xml = $this->xmlSmall;
		if (!$xml) {
			$key = $this->systemModel->getSkey();
			$xml = $this->systemModel->retrieveXMLsecureSmall($key);
		}
		$xmlerror = $xml->error;
		$error_code = (string)$xmlerror['code'];
		if (isset($error_code) && $error_code != '') {
			$data['response'] = 'ajaxInfoBad(__("' . $error_code . '"));';
		
			return view('includes/ajaxtemplate', $data);
		}

		$game = $xml->headers->game;
		$day = (int) $game['days'];
		$gid = (int) $game['id'];
		$owner = $xml->headers->owner->citizen;
		$username = (string) $owner['name'];
		$userid = (string) $owner['id'];
		$dead = (int) $owner['dead'];
		$out = (int) $owner['out'];

		$myzone = $xml->headers->owner->myZone;

		// Fetch data from DB.
		$data = $this->townModel->getTownData($gid);
		// Get chaos state from DB.
		$chaos = (int) $data['system']['chaos'];
		// Get current day from DB to compare to API.
		$dbday = (int) $data['system']['day'];

		// Check if game exists.
		if ($data === NULL) {
			$data['response'] = 'ajaxInfoBad(__("MYZONEUPDATEDFAILEDNOTFOUND"));';
			return view('includes/ajaxtemplate', $data);
		}

		// Check if user is dead.
		if ($dead === 1) {
			$data['response'] = 'ajaxInfoBad(__("MYZONEUPDATEDFAILEDDEAD"));';
			return view('includes/ajaxtemplate', $data);
		}
		// Check if auth cookie matches town
		if ($this->systemModel->getGid() != $gid) {
			$data['response'] = 'ajaxInfoBad(__("MYZONEUPDATEDFAILEDNOTFOUND"));';
			return view('includes/ajaxtemplate', $data);
		}

		// Check if newest day in DB matches the day on API.
		if ($dbday !== $day) {
			$data['response'] = 'ajaxInfoBad(__("MYZONEUPDATEDFAILEDWRONGDAY"));';
			return view('includes/ajaxtemplate', $data);
		}

		// Check for chaos.
		if ($chaos !== 0) {
			$data['response'] = 'ajaxInfoBad(__("MYZONEUPDATEDFAILEDCHAOS"));';
			return view('includes/ajaxtemplate', $data);
		}

		// Check if we are outside.
		if ($out === 0) {
			$data['response'] = 'ajaxInfoBad(__("MYZONEUPDATEDFAILEDINSIDE"));';
			return view('includes/ajaxtemplate', $data);
		}

		$item_stat = FALSE;
		$item_file = dirname(dirname(dirname(__FILE__))) . '/data/common/items_all';
		if (file_exists($item_file)) {
			$item_stat = stat($item_file);
			$item_data = unserialize(file_get_contents($item_file));
		}

		// MyZone update.
		$zoneUpdate = TRUE;
		$items = [];
		if (count((array) $myzone) > 0) {
			foreach ($myzone->item as $item) {
				$items[] = [
					'id' => pow(-1, ((int)$item['broken'])) * ((int) $item['id']),
					'count' => (int) $item['count'],
					'broken' => (int) $item['broken'],
				];

				$brokenId = pow(-1, ((int) $item['broken'])) * ((int) $item['id']);
				$defaultId = (int)$item['id'];
				//itemsfile with name updates
				if (
					!isset($item_data) || !isset($item_data[$defaultId]) ||
					$item_data[$defaultId]['name']['de'] != (string) $item['name-de'] ||
					$item_data[$defaultId]['name']['en'] != (string) $item['name-en'] ||
					$item_data[$defaultId]['name']['fr'] != (string) $item['name-fr'] ||
					$item_data[$defaultId]['name']['es'] != (string) $item['name-es'] ||
					$item_data[$defaultId]['category'] != (string) $item['cat'] ||
					$item_data[$defaultId]['image'] != (string) $item['img']
				) {
					$item_data[$defaultId] = [
						'id' => $defaultId,
						'name' => [
							'de' => (string) $item['name-de'],
							'en' => (string) $item['name-en'],
							'fr' => (string) $item['name-fr'],
							'es' => (string) $item['name-es'],
						],
						'category' => (string) $item['cat'],
						'image' => (string) $item['img'],
						'broken' => (int) $item['broken'],
					];
					$updateItems = TRUE;
				}
				if (
					(($brokenId === -$defaultId) && isset($item_data[$defaultId])) &&
					(!isset($item_data) || !isset($item_data[$brokenId]) ||
						$item_data[$brokenId]['name']['de'] != (string) $item['name-de'] ||
						$item_data[$brokenId]['name']['en'] != (string) $item['name-en'] ||
						$item_data[$brokenId]['name']['fr'] != (string) $item['name-fr'] ||
						$item_data[$brokenId]['name']['es'] != (string) $item['name-es'] ||
						$item_data[$brokenId]['category'] != (string) $item['cat'] ||
						$item_data[$brokenId]['image'] != (string) $item['img'])
				) {
					$item_data[$brokenId] = $item_data[$defaultId];
					$item_data[$brokenId]['broken'] = 1;
					$item_data[$brokenId]['id'] = $brokenId;
					$updateItems = TRUE;
				}
			}
		}
		$oid = (int) $owner['id'];
		$x = (int) $owner['x'];
		$y = (int) $owner['y'];
		$data['map']['y' . $y]['x' . $x]['nyv'] = 0;
		$data['map']['y' . $y]['x' . $x]['nvt'] = 0;
		$data['map']['y' . $y]['x' . $x]['items'] = $items;
		$data['map']['y' . $y]['x' . $x]['dried'] = (int) $myzone['dried'];
		$data['map']['y' . $y]['x' . $x]['z'] = (int) $myzone['z'];
		$data['map']['y' . $y]['x' . $x]['updatedOn'] = time();
		$data['map']['y' . $y]['x' . $x]['updatedOnDay'] = $data['system']['day'];
		$data['map']['y' . $y]['x' . $x]['updatedBy'] = (string) $owner['name'];
		$data['map']['y' . $y]['x' . $x]['updatedById'] = (int) $owner['id'];
		if (!array_key_exists('zm', $data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])])) {
			$data['map']['y' . ((int)$owner['y'])]['x' . ((int) $owner['x'])]['zm'] = -1;
		}
		$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['zm'] = max($data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['zm'], (int) $myzone['z']);
		$data['map']['y' . $y]['x' . $x]['rx'] = (int) $x - $data['tx'];
		$data['map']['y' . $y]['x' . $x]['ry'] = $data['ty'] - (int) $y;
		if (!array_key_exists('citizens', $data['map']['y' . $y]['x' . $x])) {
			$data['map']['y' . $y]['x' . $x]['citizens'] = [];
		}

		//last update timestamp
		if (isset($owner) && isset($data['citizens'][(string) $owner['id']]) && isset($data['citizens'][(string) $owner['id']]['name'])) {
			$data['citizens'][(string) $owner['id']]['lastUpdateTime'] = time();
			$data['citizens'][(string) $owner['id']]['lastUpdateDay'] = $day;

			//reset citizen position
			$data['citizens'][(string) $owner['id']]['out'] = $out;
			$data['citizens'][(string) $owner['id']]['rx'] = $x - $data['tx'];
			$data['citizens'][(string) $owner['id']]['ry'] = $data['ty'] - $y;
			$data['citizens'][(string) $owner['id']]['x'] = $x;
			$data['citizens'][(string) $owner['id']]['y'] = $y;
		}

		//erase user from all cells, then set to current cell
		foreach ($data['map'] as $xrow) {
			foreach ($xrow as $zdata) {
				if (array_key_exists('citizens', $zdata) && array_key_exists((string) $owner['id'], $zdata['citizens'])) {
					unset($data['map']['y' . ($data['ty'] - $zdata['ry'])]['x' . ($data['tx'] + $zdata['rx'])]['citizens'][(string) $owner['id']]);
				}
			}
		}
		$data['map']['y' . $y]['x' . $x]['citizens'][(string) $owner['id']] = ['name' => (string) $owner['name'], 'job' => (string) $owner['job']];

		//ruin searched and camped status updates
		if (isset($data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['building']['type']) && !in_array((int) $data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['building']['type'], [61, 62, 63])) {
			$buildingjson = $this->systemModel->retrieveRuinInfoSecure($key);

			if (!isset($buildingjson->error)) {
				foreach ($buildingjson->map->zones as $jsonzone) {
					if ($jsonzone->x == ((int) $owner['x']) && $jsonzone->y == ((int) $owner['y'])) {
						if (isset($jsonzone->building->dried) && $jsonzone->building->dried == TRUE) {
							$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['building']['dried'] = 1;
						}
						if (isset($jsonzone->building->dried) && $jsonzone->building->dried == FALSE) {
							$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['building']['dried'] = 0;
						}
						if (isset($jsonzone->building->camped) && $jsonzone->building->camped == TRUE) {
							$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['building']['blueprint'] = 1;
						}
						if (isset($jsonzone->building->camped) && $jsonzone->building->camped == FALSE) {
							$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['building']['blueprint'] = 0;
						}
					}
				}
			}
		}

		// ### JS Update
		$res_scr = '';
		// status
		$res_scr .= 'data.map["y' . $y . '"]["x' . $x . '"]["nyv"] = 0;';
		$res_scr .= 'data.map["y' . $y . '"]["x' . $x . '"]["nvt"] = 0;';
		// position
		$res_scr .= 'reMoveCitizen("' . $oid . '");';
		$res_scr .= 'if ( data.map["y' . $y . '"]["x' . $x . '"]["citizens"] === undefined ) { data.map["y' . $y . '"]["x' . $x . '"]["citizens"] = new Array(); }'
			. 'data.map["y' . $y . '"]["x' . $x . '"]["citizens"]["' . $oid . '"] = new Array();'
			. 'data.map["y' . $y . '"]["x' . $x . '"]["citizens"]["' . $oid . '"]["name"] = "' . ((string)$owner['name']) . '";'
			. 'data.map["y' . $y . '"]["x' . $x . '"]["citizens"]["' . $oid . '"]["job"] = "' . ((string)$owner['job']) . '";';
		// zone items?
		$res_scr .= 'var newItems = ' . json_encode($items) . ';'
			. 'data.map["y' . $y . '"]["x' . $x . '"]["items"] = newItems;';
		// zone dried?
		$res_scr .= 'data.map["y' . $y . '"]["x' . $x . '"]["dried"] = ' . $data['map']['y' . $y]['x' . $x]['dried'] . ';';

		// building status updates
		if (isset($data['map']['y' . $y]['x' . $x]["building"]['dried'])) {
			$res_scr .= 'data.map["y' . $y . '"]["x' . $x . '"]["building"]["dried"] = ' . $data['map']['y' . $y]['x' . $x]["building"]['dried'] . ';';
		}
		if (isset($data['map']['y' . $y]['x' . $x]["building"]['blueprint'])) {
			$res_scr .= 'data.map["y' . $y . '"]["x' . $x . '"]["building"]["blueprint"] = ' . $data['map']['y' . $y]['x' . $x]["building"]['blueprint'] . ';';
		}

		// zone zombies?
		$res_scr .= 'data.map["y' . $y . '"]["x' . $x . '"]["z"] = ' . $data['map']['y' . $y]['x' . $x]['z'] . ';';
		$res_scr .= 'data.map["y' . $y . '"]["x' . $x . '"]["zm"] = ' . $data['map']['y' . $y]['x' . $x]['zm'] . ';';


		$res_scr .= 'replaceMapZone(' . $y . ',' . $x . ');';
		$res_scr .= 'checkCitidots();';
		$res_scr .= 'document.querySelector("#x' . $data['map']['y' . $y]['x' . $x]['rx'] . 'y' . $data['map']['y' . $y]['x' . $x]['ry'] . '").click();';

		//todohere

		if ($zoneUpdate == TRUE) {
			$res_scr = 'if(data.map["y' . $y . '"] === undefined){data.map["y' . $y . '"]=[];}if(data.map["y' . $y . '"]["x' . $x . '"] === undefined){data.map["y' . $y . '"]["x' . $x . '"]=[];}data.map["y' . $y . '"]["x' . $x . '"]["updatedOn"] = ' . time() . ';'
				. 'data.map["y' . $y . '"]["x' . $x . '"]["updatedBy"] = "' . $username . '";'
				. 'data.map["y' . $y . '"]["x' . $x . '"]["updatedById"] = "' . (int) $userid . '";'
				. 'data.map["y' . $y . '"]["x' . $x . '"]["updatedOnDay"] = "' . $data['system']['day'] . '";'
				. $res_scr
				. 'data.citizens["' . $oid . '"]["x"] = ' . ((int) $x) . ';'
				. 'data.citizens["' . $oid . '"]["y"] = ' . ((int) $y) . ';'
				. 'data.citizens["' . $oid . '"]["rx"] = ' . ((int) $x - $data['tx']) . ';'
				. 'data.citizens["' . $oid . '"]["ry"] = ' . ((int) $data['ty'] - $y) . ';'
				. 'generateCitizenList();'
				. 'popZone(' . $y . ', ' . $x . ');';
		}

		$append = '';
		$this->townModel->saveTowndata($gid, $data['system']['day'], $userid, serialize($data));
		$data['response'] = $res_scr . "\n" . 'ajaxInfo(__("MYZONEUPDATED"));' . $append;

		return view('includes/ajaxtemplate', $data);
	}

	# Order array by date.
	static function cmp($a, $b) {
		if ($a['count'] == $b['count']) return 0;
		return ($a['count'] < $b['count']) ? -1 : 1;
	}

	# Calculate distance between town and zone.
	static function km($x, $y) {
		return round(sqrt(pow(abs($x), 2) + pow(abs($y), 2)));
	}
}

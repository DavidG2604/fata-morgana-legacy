<?php

namespace App\Controllers;

use App\Models\SystemModel;
use App\Models\TownModel;

class Spy extends BaseController {

	private SystemModel $systemModel;
	private TownModel $townModel;

	public function __construct() {
		$this->systemModel = model('SystemModel');
		$this->townModel = model('TownModel');
	}
	
	public function town($tid = 0, $day = null) {
		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
                    $linkscheme = "https";
                else
                    $linkscheme = "http";

		$data['main_content'] = 'map_view';
		$data['gamemap'] = $this->generateTownData($tid, $day);
		$data['debug'] = '';
		$data['bookmark'] = $linkscheme . '://' . $_SERVER['HTTP_HOST'] . '/spy/town/' . $tid;
//		$data['qrcode'] = 'Als Spion steht Dir leider kein QR Code zur Verfügung.';
		
		return view('includes/template', $data);
	}
	
	public function generateTownData($gid, $day) {
		if ( $data = $this->townModel->getTownData($gid, $day) ) {
			// continue
			$data['system']['days'] = $this->townModel->getTownDays($gid);
		}
		else {
			// fail
			$data = [];
		}
		
		$item_stat = false;
		$item_file = dirname(dirname(dirname( __FILE__ ))) . '/data/common/items';
		if ( file_exists($item_file) ) {
			$item_stat = stat($item_file);
			$item_data = unserialize(file_get_contents($item_file));
		}
		
		// owner update
		$data['system']['owner_name'] = 'Spion';
		$data['system']['owner_id'] = 0;
		$data['system']['autoUpdateEnabled'] = 0;
		$data['spy'] = 1;
		if ($this->systemModel->getGid() == $gid) {
			$data['spyUndercover'] = 1;
		}
		if (isset($data['tx'])) {
			$data['ox'] = $data['tx'];
			$data['oy'] = $data['ty'];
		}
		else {
			$data['ox'] = 0;
			$data['oy'] = 0;
		}

        // Remove private expeditions
		if (array_key_exists('expeditions', $data)) {
			foreach ($data['expeditions'] as $expId => $exp) {

				if (isset($exp["type"]) && $exp["type"] === "private") {
					unset($data['expeditions'][$expId]);
				}
			}
		}

		return json_encode($data);
	}
}
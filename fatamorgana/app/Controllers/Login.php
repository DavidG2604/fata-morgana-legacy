<?php

namespace App\Controllers;

use App\Models\SystemModel;

class Login extends BaseController {

    private SystemModel $systemModel;

    function __construct() {
        $this->systemModel = model('SystemModel');
    }
    
    /**
    * Retrieves a secure key and create JWT token.
    *
    * @param string $key 
    *   The secure key to be retrieved. Default is an empty string.
    * @param string $error_code
    *   Error code from the XML response.
    *
    * @return mixed 
    *   Returns a view or a redirect response.
    */
    public function getSkey($key = '', $error_code = '') {
        if ($key === 'error') {
            $data['main_content'] = 'login_form_fail';
            $data['error_code'] = $error_code;

            return view('includes/template', $data);
        } 
        else {
            $xml = $this->systemModel->retrieveXMLsecureSmall($key);
            if (is_int($xml)) {
                return redirect()->to('login/skey/error/error_code_' . $xml);
            }
            if (!is_null($key) && $key !== '' && $xml && !isset($xml->error)) {
                $game = $xml->headers->game;
                $gid = (int) $game['id'];
                $owner = $xml->headers->owner->citizen;
                $userid = (string) $owner['id'];
                $username = (string) $owner['name'];
                $cookie = $this->systemModel->createNewCookie($key, $gid, $username, $userid);
				$this->response->setCookie($cookie);
                return redirect()->to('map')->withCookies();
            } 
            elseif (isset($xml->error)) {
                $xmlerror = $xml->error;
                $error_code = (string)$xmlerror['code'];
                if (isset($error_code) && $error_code != '') {
                    return redirect()->to('login/skey/error/' . $error_code);
                }
            }
            elseif (is_int($xml)) {
                return redirect()->to('login/skey/error/error_code_' . $xml);
            }
            else {
                $data['main_content'] = 'login_form';
                return view('includes/template', $data);
            }
        }
    }

    /**
     * Handles the initial login routing process.
     *
     * @return mixed
     *   Returns a redirect response to the appropriate route.
     */
    public function index() {
        $check = $this->systemModel->check();
        if (isset($_POST['key'])) {
            return redirect()->to('login/skey/' . $this->request->getPost('key') . '/true');
        } 
        else if ($check) {
            return redirect()->to('map')->withCookies();
        } 
        else if ($this->systemModel->getSkey()) {
            return redirect()->to('login/skey/' . $this->systemModel->getSkey());
        } 
        else {
            return redirect()->to('login/skey');
        }
    }

    /**
     * Handles the authentication process after a user submits their secure key.
     *
     * @return mixed
     *   Returns a redirect response to the appropriate route or a view with an error message.
     */
    public function postAuth() {
        /** @var string $key The secure key provided by the user. */
        $key = $this->request->getPost('user_skey');

        /** @var SimpleXMLElement $xml The XML data retrieved using the secure key. */
        $xml = $this->systemModel->retrieveXMLsecureSmall($key);

        if (is_int($xml)) {
            return redirect()->to('login/skey/error/error_code_' . $xml);
        }
        if (!is_null($key) && $key !== '' && $xml) {
            // Check for errors.
            if (isset($xml->error['code'])) {
                return redirect()->to('login/skey/error/' . $xml->error['code']);
            }

            /** @var SimpleXMLElement $game The game data from the XML. */
            $game = $xml->headers->game;
            $gid = (int) $game['id'];

            /** @var SimpleXMLElement $owner The owner data from the XML. */
            $owner = $xml->headers->owner->citizen;
            $userid = (string) $owner['id'];
            $username = (string) $owner['name'];

            /** @var array $cookie The new authentication cookie. */
            $cookie = $this->systemModel->createNewCookie($key, $gid, $username, $userid);

            $this->response->setCookie($cookie);
            return redirect()->to('map')->withCookies();
        } 
        else {
            return redirect()->to('login/skey/error');
        }
    }

    /**
     * Handles the logout process by invalidating the authentication cookie.
     *
     * @return \CodeIgniter\HTTP\RedirectResponse
     *   Returns a redirect response to the login page.
     */
    public function logout() {
        $cookie = [
            'name'   => 'auth_token',
            'value'  => '',
            'expire' => 0,
            'secure' => false,  // Set to true in production
            'httponly' => false, // Accessible only through the HTTP protocol
        ];
        $this->response->setCookie($cookie);
        return redirect()->to('login')->withCookies();
    }
}
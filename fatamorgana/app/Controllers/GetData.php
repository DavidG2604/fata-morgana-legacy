<?php

namespace App\Controllers;

use App\Models\SystemModel;
use App\Models\TownModel;

class GetData extends BaseController
{

	public ?int $town = NULL;

	private SystemModel $systemModel;
	private TownModel $townModel;

	public mixed $debug = false;

	public function __construct()
	{
		$this->systemModel = model('SystemModel');
		$this->townModel = model('TownModel');

	}

	public function initRequest() {
		$this->town = $this->request->getGetPost('town');
	}

	public function index()
	{

		$this->initRequest();

		//check if we got a town
		if (!$this->town) {
			return $this->response->setStatusCode(404)->setBody(json_encode(["error" => "No town specified"]));
		}

		$data = $this->townModel->getTownData($this->town);

		//check if we have that town
		if (!$data) {
			return $this->response->setStatusCode(404)->setBody(json_encode(["error" => "Town not found"]));
		}


		//define base structure
		$output = [
			"map" => [
				"id" => $data['system']['gameid'],
				"days" => $data['system']['day'],
				"zones" => []
			],
		];

		foreach ($data['map'] as $ly => $ydata) {
			foreach ($ydata as $lx => $zone) {
				if (isset($zone['updatedById']) && !($zone["rx"] === 0 && $zone["ry"] === 0)) {
					$output_zone = [
						"x" => $zone["rx"] + $data["tx"],
						"y" => $data["ty"] - $zone["ry"],
						"details" => [
							"dried" => $zone["dried"] == 1 ? TRUE : FALSE,
							"z" => (int) $zone["z"],
						],

						"items" => [
						]
					];

					$extra = [
						"updatedOn" => (int) $zone["updatedOn"],
						"updatedOnDay" => (int) $zone["updatedOnDay"],
						"updatedBy" => $zone["updatedBy"],
						"updatedById" => (int) $zone["updatedById"],
					];
					if (isset($zone["zm"]) && $zone["zm"] >= 0 && $zone["zm"] != $zone["z"]) {
						$extra["deadz"] = (int) $zone["zm"] - (int) $zone["z"];
					}
					$output_zone["extra"] = $extra;

					if (isset($zone["building"]) && isset($zone["building"]["type"])) {
						$building = [
							"type" => (int) $zone["building"]["type"],
						];
						if ($zone["building"]["type"] !== -1 && $zone["building"]["type"] !== 61 && $zone["building"]["type"] !== 62 && $zone["building"]["type"] !== 63) {
							if (isset($zone["building"]["dried"]) && $zone["building"]["dried"] === 1) {
								$building["dried"] = TRUE;
							} else {
								$building["dried"] = FALSE;
							}
							
							if (isset($zone["building"]["blueprint"]) && $zone["building"]["blueprint"] === 1) {
								$building["camped"] = TRUE;
							} else {
								$building["camped"] = FALSE;
							}
						}
						$output_zone["building"] = $building;
					}

					if (isset($zone["items"])) {
						$items = [];
						foreach ($zone["items"] as $item) {
							$output_item = [
								"id" => (int) $item["id"] > 0 ? (int) $item["id"] : (int) $item["id"] * -1,
								"count" => (int) $item["count"],
								"broken" => $item["broken"] == 1 ? TRUE : FALSE,
							];
							$items[] = $output_item;
						}
						$output_zone["items"] = $items;
					}

	
					$output["map"]["zones"][] = $output_zone;
				}
				
			}
		}


		return $this->response->setHeader('Content-Type', 'application/json')->setBody(json_encode($output));


	}


}

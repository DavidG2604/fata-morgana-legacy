<?php

namespace App\Controllers;

use App\Models\SystemModel;
use App\Models\TownModel;

class Update extends BaseController
{

	public ?string $key = NULL;
	public ?int $chaosx = NULL;
	public ?int $chaosy = NULL;
	public ?int $deadzombies = NULL;

	private SystemModel $systemModel;
	private TownModel $townModel;

	public mixed $debug = false;

	public function __construct()
	{
		$this->systemModel = model('SystemModel');
		$this->townModel = model('TownModel');

	}

	public function initRequest() {
		$this->key = $this->request->getGetPost('key');
		$this->chaosx = $this->request->getGetPost('chaosx');
		$this->chaosy = $this->request->getGetPost('chaosy');
		$this->deadzombies = $this->request->getGetPost('deadzombies');
	}

	public function mapindex()
	{
		$this->initRequest();
		if (!$this->key) {
			return;
		}
		if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
			$linkscheme = "https";
		else
			$linkscheme = "http";

		$data['main_content'] = 'map_view';
		$data['gamemap'] = $this->townModel->generateData(TRUE, $this->key);
		$data['debug'] = $this->debug;

		return view('includes/template', $data);
	}

	public function index()
	{
		$this->initRequest();
		if (!$this->key) {
			return $this->response->setStatusCode(401)->setBody('Authentication failure');
		}
		$xml = $this->systemModel->retrieveXMLsecureSmall($this->key);
		if (is_int($xml)) {
			return redirect()->to('login/skey/error/error_code_' . $xml);
		}
		
		$xmlerror = $xml->error;
		if ($xmlerror) {
			$error_code = (string) $xmlerror['code'];
			return $this->response->setStatusCode(401)->setBody($error_code);
		}

		$game = $xml->headers->game;
		$day = (int) $game['days'];
		$gid = (int) $game['id'];
		$owner = $xml->headers->owner->citizen;
		$out = (int) $owner['out'];
		$username = (string) $owner['name'];
		$userid = (string) $owner['id'];
		$x = (string) $owner['x'];
		$y = (string) $owner['y'];
		$myzone = $xml->headers->owner->myZone;

		//load from DB
		$data = $this->townModel->getTownData($gid);

		$response1 = $response2 = $response3 = '';

		//check if we got something from DB
		if ($data === FALSE) {
			$success = $this->townModel->generateData(TRUE, $this->key);
			//check if we got something at all
			if ($success == NULL) {
				return $this->response->setStatusCode(401)->setBody('Authentication failure');
			}
			$response1 = ' (Town was created)';
			$data = $this->townModel->getTownData($gid);
		}

		//get current day from DB to compare to API
		$dbday = $data['system']['day'];

		//check if newest day in DB matches the day on API
		if ($dbday != $day) {
			$success = $this->townModel->generateData(TRUE, $this->key);
			if ($success == NULL) {
				return $this->response->setStatusCode(401)->setBody('Authentication failure');
			}
			$response2 = ' (New day was created)';
			$data = $this->townModel->getTownData($gid);
		}


		//check if we are dead
		$dead = (string) $owner['dead'];

		if ($dead == 1) {
			return $this->response->setStatusCode(400)->setBody('You are dead. Stop updating the map.');
		}

		//get chaos state from DB
		$chaos = $data['system']['chaos'];

		//check for chaos
		if ($chaos != 0) {
			if ($out == 1) {

				if ($this->chaosx === NULL) {
					return $this->response->setStatusCode(400)->setBody('Chaos Info missing. Are Chaos-Updates enabled on MHO?');
				}

				if ($this->chaosy === NULL) {
					return $this->response->setStatusCode(400)->setBody('Chaos Info missing. Are Chaos-Updates enabled on MHO?');
				}

				//get coordinates
				$x = $this->chaosx + $data['tx'];
				$y = $data['ty'] - $this->chaosy;
			} else {
				$x = $data['tx'];
				$y = $data['ty'];
			}

			//adjust citizen position
			$oid = (string) $owner['id'];
			foreach ($data['map'] as $ly => $ydata) {
				foreach ($ydata as $lx => $zone) {
					if (isset($zone['citizens'][$oid])) {
						unset($data['map'][$ly][$lx]['citizens'][$oid]);
					}
				}
			}

			// Check if 'y' level exists, if not, initialize it as an empty array
			if (!isset($data['map']['y' . $y])) {
				$data['map']['y' . $y] = [];
			}
			// Check if 'x' level exists, if not, initialize it as an empty array
			if (!isset($data['map']['y' . $y]['x' . $x])) {
				$data['map']['y' . $y]['x' . $x] = [];
			}
			// Check if 'citizens' key exists, if not, initialize it as an empty array
			if (!array_key_exists('citizens', $data['map']['y' . $y]['x' . $x])) {
				$data['map']['y' . $y]['x' . $x]['citizens'] = [];
			}

			// Set citizen info in zone.
			$data['map']['y' . $y]['x' . $x]['citizens'][$oid] = ['name' => (string) $owner['name'], 'job' => (string) $owner['job']];

			$data['citizens'][$oid]['x'] = (int) $x;
			$data['citizens'][$oid]['y'] = (int) $y;
			$data['citizens'][$oid]['rx'] = (int) $x - $data['tx'];
			$data['citizens'][$oid]['ry'] = (int) $data['ty'] - $y;
			$data['citizens'][$oid]['out'] = $out;

			//response
			$response3 = ' (Chaos mode)';
		}

		//basics
		$data['map']['y' . $y]['x' . $x]['nyv'] = 0;
		$data['map']['y' . $y]['x' . $x]['nvt'] = 0;
		$data['map']['y' . $y]['x' . $x]['rx'] = (int) $x - $data['tx'];
		$data['map']['y' . $y]['x' . $x]['ry'] = $data['ty'] - (int) $y;

		// regeneration
		$data['map']['y' . $y]['x' . $x]['dried'] = (int) $myzone['dried'];
		// zombies
		$data['map']['y' . $y]['x' . $x]['z'] = (int) $myzone['z'];
		$data['map']['y' . $y]['x' . $x]['zm'] = 0;

		// max zombies
		if ($data['map']['y' . $y]['x' . $x]['zm'] < (int) $myzone['z']) {
			$data['map']['y' . $y]['x' . $x]['zm'] = (int) $myzone['z'];
		}
		//max zombies from MHO
		if ($this->deadzombies != NULL) {
			$data['map']['y' . $y]['x' . $x]['zm'] = $this->deadzombies + (int) $myzone['z'];
		}

		// items
		$items = [];
		if (count((array) $myzone->item) > 0) {
			foreach ($myzone->item as $item) {
				$items[] = [
					'id' => ((int) $item['broken'] == 1 ? ((-1) * ((int) $item['id'])) : ((int) $item['id'])),
					'count' => (int) $item['count'],
					'broken' => (int) $item['broken'],
				];
			}
		}
		$data['map']['y' . $y]['x' . $x]['items'] = $items;

		//ruin searched and camped status updates
		if (isset($data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['building']['type']) && !in_array((int) $data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['building']['type'], [61, 62, 63])) {
			$buildingjson = $this->systemModel->retrieveRuinInfoSecure($this->key);
			
			if (!isset($buildingjson->error)) {
				foreach ($buildingjson->map->zones as $jsonzone) {
					if ($jsonzone->x == ((int) $owner['x']) && $jsonzone->y == ((int) $owner['y'])) {
						if (isset($jsonzone->building->dried) && $jsonzone->building->dried == TRUE) {
							$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['building']['dried'] = 1;
						}
						if (isset($jsonzone->building->dried) && $jsonzone->building->dried == FALSE) {
							$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['building']['dried'] = 0;
						}
						if (isset($jsonzone->building->camped) && $jsonzone->building->camped == TRUE) {
							$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['building']['blueprint'] = 1;
						}
						if (isset($jsonzone->building->camped) && $jsonzone->building->camped == FALSE) {
							$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['building']['blueprint'] = 0;
						}
					}
				}
			}
		}

		// user
		$data['map']['y' . $y]['x' . $x]['updatedOn'] = time();
		$data['map']['y' . $y]['x' . $x]['updatedOnDay'] = $day;
		$data['map']['y' . $y]['x' . $x]['updatedBy'] = $username;
		$data['map']['y' . $y]['x' . $x]['updatedById'] = (int) $userid;

		//last update timestamp
		if (isset($owner) && isset($data['citizens'][(string) $owner['id']]) && isset($data['citizens'][(string) $owner['id']]['name'])) {
			$data['citizens'][(string) $owner['id']]['lastUpdateTime'] = time();
			$data['citizens'][(string) $owner['id']]['lastUpdateDay'] = $day;

			//reset citizen position
			$data['citizens'][(string) $owner['id']]['out'] = $out;
			$data['citizens'][(string) $owner['id']]['rx'] = $x - $data['tx'];
			$data['citizens'][(string) $owner['id']]['ry'] = $data['ty'] - $y;
			$data['citizens'][(string) $owner['id']]['x'] = $x;
			$data['citizens'][(string) $owner['id']]['y'] = $y;
		}

		//erase user from all cells, then set to current cell
		foreach ($data['map'] as $xrow) {
			foreach ($xrow as $zdata) {
				if (array_key_exists('citizens', $zdata) && array_key_exists((string) $owner['id'], $zdata['citizens'])) {
					unset($data['map']['y' . ($data['ty'] - $zdata['ry'])]['x' . ($data['tx'] + $zdata['rx'])]['citizens'][(string) $owner['id']]);
				}
			}
		}
		$data['map']['y' . $y]['x' . $x]['citizens'][(string) $owner['id']] = ['name' => (string) $owner['name'], 'job' => (string) $owner['job']];

		// save 2 db
		$this->townModel->saveTowndata($gid, $data['system']['day'], $userid, serialize($data));

		// response

		$response = 'Fata Morgana was updated.';
		return $this->response->setStatusCode(200)->setBody($response . $response1 . $response2 . $response3);
	}

	public function manual() {
		$this->initRequest();
		if (!$this->key) {
			return;
		}
		$xml = $this->systemModel->retrieveXMLsecure($this->key);
		if (is_int($xml)) {
			return redirect()->to('login/skey/error/error_code_' . $xml);
		}
		$owner = $xml->headers->owner->citizen;
		$username = (string) $owner['name'];
		$userid = (string) $owner['id'];
		$x = (string) $owner['x'];
		$y = (string) $owner['y'];
		$game = $xml->headers->game;
		$gid = (int) $game['id'];

		$append = '';
		if ($data = $this->townModel->getTownData($gid)) {
			// MANUAL UPDATE
			$item_stat = false;
			$item_file = dirname(dirname(dirname(__FILE__))) . '/data/common/items';
			if (file_exists($item_file)) {
				$item_stat = stat($item_file);
				$item_data = unserialize(file_get_contents($item_file));
			}

			$transfer = explode(';', (string) $_GET['gil']);
			$gil = $items = [];
			foreach ($transfer as $timg) {
				$broken = 0;
				if (substr($timg, 0, 7) == 'broken-') {
					$timg = substr($timg, 7);
					$broken = 1;
				}
				foreach ($item_data as $i => $d) {
					if ($i < 1) {
						continue;
					}
					if ($d['image'] == $timg) {
						$iid = $d['id'];
						if ($broken == 1) {
							$iid *= -1;
						}
						if (isset($gil[$iid])) {
							$gil[$iid]++;
						} else {
							$gil[$iid] = 1;
						}
						break;
					}
				}
			}
			$msg = 'Es wurden folgende Gegenstände registriert:\n';
			foreach ($gil as $id => $count) {
				$msg .= $count . 'x ' . str_replace("'", '`', $item_data[$id]['name']) . ($id < 0 ? ' (kaputt)' : '') . '\n';
				$items[] = [
					'id' => $id,
					'count' => $count,
					'broken' => ($id < 0 ? 1 : 0),
				];
			}
			$msg .= '\n';

			$msg .= 'Die Zone wurde als ' . ((int) $_GET['d'] == 1 ? 'nicht ' : '') . 'buddelbar markiert.\n';
			$msg .= (int) $_GET['z'] . ' Zombie(s) wurden eingetragen.';

			$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['items'] = $items;
			$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['dried'] = (int) $_GET['d'];
			$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['z'] = (int) $_GET['z'];
			$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['updatedOn'] = time();
			$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['updatedBy'] = (string) $owner['name'];

			if ($this->townModel->saveTowndata($data['system']['gameid'], $data['system']['day'], $userid, serialize($data))) {
				$msg .= '\n\nAktualisierung erfolgreich.';
			} else {
				$msg .= '\n\nFehler beim Schreiben der Daten.';
			}
		} else {
			$msg = 'Die Stadtdaten konnten nicht korrekt geladen werden.\nBitte versuche es erneut oder besuche die Fata Morgana.';
		}

		$data['msg'] = $msg;
		header('Content-type: text/javascript');
		return view('manualupdate', $data);
	}

	public function constructions()
	{
		$this->initRequest();
		if (!$this->key) {
			return;
		}
		$xml = $this->systemModel->retrieveXMLsecure($this->key);
		if (is_int($xml)) {
			return redirect()->to('login/skey/error/error_code_' . $xml);
		}
		$owner = $xml->headers->owner->citizen;
		$username = (string) $owner['name'];
		$userid = (string) $owner['id'];
		$x = (string) $owner['x'];
		$y = (string) $owner['y'];
		$game = $xml->headers->game;
		$gid = (int) $game['id'];

		$append = $dvdata = $msg = '';
		if ($data = $this->townModel->getTownData($gid)) {
			// MANUAL UPDATE
			unset($data['mconstructions']);
			$dvdata = json_decode("[" . $_POST['data'] . "]");
			foreach ($dvdata as $b) {
				$data['mconstructions'][] = $b;
			}
			$data['mconststamp'] = date('d.m.Y, H:i', time());

			if ($this->townModel->saveTowndata($data['system']['gameid'], $data['system']['day'], $userid, serialize($data))) {
				$msg .= 'Aktualisierung erfolgreich.';
			} else {
				$msg .= 'Fehler beim Schreiben der Daten.';
			}
		} else {
			$msg = 'Die Stadtdaten konnten nicht korrekt geladen werden.\nBitte versuche es erneut oder besuche die Fata Morgana.';
		}

		$data['msg'] = $msg;
		#$data['debug'] = print_r($dvdata,true);
		return view('constructionsupdate', $data);
	}
}

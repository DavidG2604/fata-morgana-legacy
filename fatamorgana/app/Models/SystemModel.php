<?php

namespace App\Models;

use CodeIgniter\Model;
use Config\Services;

class SystemModel extends Model {

	public $secureXML = NULL;
	private string $appkey;
	protected $db;
	protected $table = 'towndata';

	
	public function __construct() {
		parent::__construct();
		$this->appkey = $_ENV['APPKEY'];
		$this->db = \Config\Database::connect();
		helper('authorization');
        helper('cookie');
		helper('date');
	}
	
	/**
	 * Check if user is logged in and has valid token.
	 */
	public function check()
    {
        // Check login status and if ok return info
        $token = Services::request()->getCookie('auth_token');
        if (isset($token)) {
            return validateTimestamp($token);
        }
		return FALSE;
    }

    public function getSkey()
    {
        // Get token from cookie.
        $token = Services::request()->getCookie('auth_token');
        if (isset($token)) {
            // If a token is found, validate it and return the secure user key.
            $decodedToken = validateToken($token);
            if ($decodedToken) {
                return $decodedToken->user_skey;
            } else {
                return FALSE;
            }
        }
        return FALSE;
    }

    public function getGid()
    {
        // Get token from cookie.
        $token = Services::request()->getCookie('auth_token');
        if (isset($token)) {
            $decodedToken = validateToken($token);
            if ($decodedToken) {
                return $decodedToken->gid;
            } else {
                return FALSE;
            }
        }
        return FALSE;
    }

    public function getUsername()
    {
        // Get token from cookie.
        $token = Services::request()->getCookie('auth_token');
        if (isset($token)) {
            $decodedToken = validateToken($token);
            if ($decodedToken) {
                return $decodedToken->username;
            } else {
                return FALSE;
            }
        }
        return FALSE;
    }

    public function getUserid()
    {
        // Get token from cookie.
        $token = Services::request()->getCookie('auth_token');
        if (isset($token)) {
            $decodedToken = validateToken($token);
            if ($decodedToken) {
                return $decodedToken->userid;
            } else {
                return FALSE;
            }
        }
        return FALSE;
    }

    public function setNewToken($key, $gid, $username, $userid)
    {
        $tokenData = [];
        $tokenData['user_skey'] = $key;
        $tokenData['gid'] = $gid;
        $tokenData['username'] = $username;
        $tokenData['userid'] = $userid;
        $tokenData['logged_in'] = TRUE;
        $tokenData['timestamp'] = now();
        $token = generateToken($tokenData);

        return $token;
    }

    public function createNewCookie($key, $gid, $username, $userid) {
        $token = $this->setNewToken($key, $gid, $username, $userid);
        $cookie = [
            'name'   => 'auth_token',
            'value'  => $token,
            'expire' => strtotime('+90 days'),
            'secure' => false,  // Set to true in production
            'httponly' => false, // Accessible only through the HTTP protocol
        ];

        return $cookie;
    }

	public function retrieveBuildingsSecure($skey = NULL) {
        //get the userkey
        if ($skey == NULL) {
            $skey = $this->getSkey();
        }

        $json = file_get_contents('https://myhord.es/api/x/json/buildings?fields=img%2Cdef%2Cpa%2Ctemporary%2Cparent%2Cresources%2Cname%2Crarity%2Cid&userkey=' . $skey . '&appkey=' . $this->appkey);
        return json_decode($json);
    }

    public function retrieveBuildingsInTownSecure($skey = NULL) {
        //get the userkey
        if ($skey == NULL) {
            $skey = $this->getSkey();
        }

        $json = file_get_contents('https://myhordes.eu/api/x/json/me?fields=map.fields(city.fields(buildings.fields(id%2Cname%2Cicon%2Ctemporary)%2Cchantiers.fields(id%2Cname%2Crarity%2Cpa%2Cactions)))&languages=en%2Cfr%2Ces%2Cde&userkey=' . $skey . '&appkey=' . $this->appkey);
        return json_decode($json);
    }


    public function retrieveWindSecure($skey = NULL) {
        //get the userkey
        if ($skey == NULL) {
            $skey = $this->getSkey();
        }


        $json = file_get_contents('https://myhordes.eu/api/x/json/me?fields=map.fields%28days%2Ccity.fields%28news.fields%28regenDir%29%29%29&appkey=' . $this->appkey . '&userkey=' . $skey . '&languages=en');
        //$json = file_get_contents('http://localhost/apisample/nowind.json');
        return json_decode($json);
    }


	public function retrieveRuinInfoSecure($skey = NULL) {
		// Get the userkey.
		if ($skey === NULL) {
			$skey = $this->getSkey();
		}

		$json = file_get_contents('https://myhord.es/api/x/json/me?fields=map.fields%28zones.fields%28x%2Cy%2Cbuilding.fields%28type%2Cdig%2Ccamped%2Cdried%29%29%29&appkey=' . $this->appkey . '&userkey=' . $skey);
		//$json = file_get_contents('http://localhost/apisample/townwithruin.json');

		return json_decode($json);
	}

    public function retrieveExternalMap($skey = NULL, $source = NULL, $gid = NULL) {
        // Get the userkey.
		if ($skey === NULL) {
			$skey = $this->getSkey();
		}
        if (!$source) {return;}
        if (!$gid) {return;}

        if ($source === "1" || $source === 1) {
            $json = file_get_contents('https://fatamorgana.md26.eu/getdata?town=' . $gid);
            return json_decode($json);
        }
        
        return;
    }

    /*
	public function getXMLsecure() {
		if ( $this->secureXML !== NULL ) {
			return $this->secureXML;
		}
		elseif ($skey = $this->getSkey()) {
			return $this->retrieveXMLsecure($skey);
		}
		return FALSE;
	}

	public function getXMLsecureSmall() {
		if ( $this->secureXML !== NULL ) {
			return $this->secureXML;
		}
		elseif ($skey = $this->getSkey()) {
			return $this->retrieveXMLsecureSmall($skey);
		}
		return FALSE;
	}
    */

	/**
	 * Retrieve the secure XML data for the town.
	 * 
	 * @param string $skey The secure key for the user.
	 * @return SimpleXMLElement|FALSE The secure XML data for the town.
	 */
	public function retrieveXMLsecure($skey, $headerOnly = FALSE) {
        $url = 'https://myhord.es/api/x/v2/xml/town?userkey=' . $skey . '&appkey=' . $this->appkey . '&lang=all';
		if ($headerOnly) { $url .= '&header_only=true'; }
        //$url = 'http://localhost/apisample/townhcopti.xml';
        //$url = 'http://localhost/apisample/town.xml';
        //$url = 'http://localhost/apisample/townchaos.xml';
        //$url = 'http://localhost/apisample/townmindi.xml';
        //$url = 'http://localhost/apisample/townsmall.xml';
        //$url = 'http://localhost/apisample/townwithruin.xml';
        //$url = 'http://localhost/apisample/town_attack.xml';
        //$url = 'http://localhost/apisample/notintown.xml';
        //$url = 'http://localhost/apisample/ratelimitexceeded.xml';

        libxml_use_internal_errors(true);
        $xml = simplexml_load_file($url);
        if (!$headerOnly) { $this->secureXML = $xml; }
        if (!$xml) {
            //return;
            $xml = simplexml_load_string('<hordes><error code="unknown_error_while_connecting_to_game"/></hordes>');
            //$xml->error = ['code'] = 'test';
            
            //return ['error']['code'] = "test"; // todo
        }
        return $xml;
	}
	
	public function retrieveXMLsecureSmall($skey) {
		return $this->retrieveXMLsecure($skey, TRUE);
	}

}

<?php

namespace App\Models;

use CodeIgniter\Model;

class TownModel extends Model {

	public $secureXML = NULL;
	private SystemModel $systemModel;
	protected $db;
	protected $table = 'towndata';

	
	public function __construct() {
		parent::__construct();
		$this->systemModel = model('SystemModel');
		$this->db = \Config\Database::connect();
		helper('authorization');
		helper('date');
	}
	
	/**
	 * Retrieve the town data for the given town ID and day from DB.
	 * 
	 * @param int $tid The town ID.
	 * @param int $day The day (optional).
	 * @return array|FALSE The town data for the given town ID and day if stored.
	 */
	public function getTownData($tid, $day = NULL): array|FALSE {
		$sql = "SELECT data FROM towndata WHERE tid = ? " . (!is_null($day) ? 'AND day = ' . $day : '')." ORDER BY timestamp DESC LIMIT 1"; 
		$q = $this->db->query($sql, [$tid]);
		if ($q->getNumRows() > 0) {
			 $row = $q->getRow(); 
			 return unserialize($row->data);
		}
		return FALSE;
	}

	public function saveTownData($tid, $day, $uid, $data) {
		// MySQL / MariaDB
		#$sql = "INSERT INTO towndata VALUES (?,?,?,?) ON DUPLICATE KEY UPDATE data = ?, timestamp = ?"; 
		#$q = $this->db->query($sql, [$tid, $day, $data, time(), $data, time()]);

		// PostgreSQL 15+
		#$sql = "MERGE INTO towndata AS target USING (VALUES (?, ?, ?, ?)) AS source (tid, day, data, timestamp) ON target.tid = source.tid AND target.day = source.day WHEN MATCHED THEN UPDATE SET data = source.data, timestamp = source.timestamp WHEN NOT MATCHED THEN INSERT (tid, day, data, timestamp) VALUES (source.tid, source.day, source.data, source.timestamp);";
		#$q = $this->db->query($sql, [$tid, $day, $data, time()]);
		
		// PostgresSQL 9.5+
		#$sql = "INSERT INTO towndata VALUES(?, ?, ?, ?) ON CONFLICT (`tid`, `day`) DO UPDATE SET `data` = ?, `timestamp` = ?;";
        #$q = $this->db->query($sql, [$tid, $day, $data, time(), $data, time()]);

		// PostgresSQL Function
		$sql = "SELECT savetowndata(CAST(? AS INT),CAST(? AS SMALLINT),CAST(? AS INT),CAST(? AS TEXT),CAST(? AS INT))";
		$q = $this->db->query($sql, [$tid, $day, $uid, $data, time()]);
		
		return $q;
	}

	public function getTownDays($tid) {
		$days = [];
		$sql = "SELECT DISTINCT day FROM towndata WHERE tid = ? ORDER BY day DESC LIMIT 100";
		$q = $this->db->query($sql, [$tid]);
		if ($q->getNumRows() > 0) {
			foreach ($q->getResult() as $row) {
				$days[] = $row->day;
			} 
			return $days;
		}
		return FALSE;
	}

	public function generateData($update = FALSE, $key = NULL) {
		if (!$key) {
			$key = $this->systemModel->getSkey();
		}

		$xml = $this->systemModel->retrieveXMLsecure($key);

		// Catch XML parsing error.
		if (!$xml) {
			return json_encode($xml);
			return FALSE;
		}
		// Catch response code.
		if (is_int($xml)) {
			return $xml;
		}
		$error = $xml->error;
		$error_code = (string) $error['code'];

		if (isset($xml->error) && $error_code != 'horde_attacking') {
			//not attack, some other error
			return FALSE;
		}

		if (isset($xml->error) && $error_code == 'horde_attacking') {
			//attack, try to load data from database.
			$gid = $this->systemModel->getGid();
			$userid = $this->systemModel->getUserid();
			$username = $this->systemModel->getUsername();

			$data = $this->getTownData($gid);

			$langcode = $data['langcode'];
			$day = $data['system']['day'];

			//owner id/name override for attack
			$data['system']['owner_id'] = $userid;
			$data['system']['owner_name'] = $username;

			if (!$data) {
				$headers = $xml->headers;
				$owner = $xml->headers->owner->citizen;
				$data = [];
				$data['system']['owner_name'] = (string) $owner['name'];
				$data['system']['error_code'] = $error_code;
				if ($update) {
					//return NULL;
				}
				return FALSE;
			}
		} else {

			$headers = $xml->headers;
			$game = $xml->headers->game;
			$owner = $xml->headers->owner->citizen;
			$myzone = $xml->headers->owner->myZone;

			$xmldata = $xml->data;
			$map = $xmldata->map;
			$city = $xmldata->city;
			$citizens = $xmldata->citizens;
			$cadavers = $xmldata->cadavers;
			$upgrades = $xmldata->upgrades;
			$bank = $xmldata->bank;
			$expeditions = $xmldata->expeditions;

			$day = (int) $game['days'];
			$gid = (int) $game['id'];
			$langcode = (string) $city['region'];

			//check if town from auth token town from xml
			if (!$update && $this->systemModel->getGid() != $gid) {
				return "wrong_town";
			}

			if ($data = $this->getTownData($gid)) {
				$data['system']['days'] = $this->getTownDays($gid);
			} else {
				$data = [];
			}
		}
		
		if ($langcode == "multi") {
			$langcode = "en";
		}

		$append = '';

		$item_stat = FALSE;
		$item_file = dirname(dirname(dirname(__FILE__))) . '/data/common/items_all';
		if (file_exists($item_file)) {
			$item_stat = stat($item_file);
			$item_data = unserialize(file_get_contents($item_file));
		}
		$updateItems = FALSE;

		$building_stat = FALSE;
		$building_file = dirname(dirname(dirname(__FILE__))) . '/data/common/buildings_all';
		if (file_exists($building_file)) {
			$building_stat = stat($building_file);
			$building_data = unserialize(file_get_contents($building_file));
		}
		$updateBuildings = FALSE;

		$newday = TRUE;
		if (array_key_exists('system', $data) && array_key_exists('day', $data['system']) && $day === $data['system']['day']) {
			$newday = FALSE;
		}
		$data['newday'] = $newday ? 1 : 0;

		// owner update
		if (isset($owner)) {
			$data['langcode'] = $langcode;
			$data['system']['owner_name'] = (string) $owner['name'];
			$data['system']['owner_id'] = (string) $owner['id'];
			$data['ox'] = (int) $owner['x'];
			$data['oy'] = (int) $owner['y'];
		}
		$data['system']['day'] = $day;
		$data['system']['gameid'] = $gid;

		if (isset($city)) {
			$data['system']['gamename'] = (string) $city['city'];
			$data['system']['hard'] = $hard = (int) $city['hard'];
			$data['system']['chaos'] = $chaos = (int) $city['chaos'];
			$data['system']['devast'] = $devast = (int) $city['devast'];
			$data['system']['autoUpdateEnabled'] = $aue = ($chaos == 0 && $devast == 0 ? TRUE : FALSE);
			if (!isset($data['scout'])) {
				$data['scout'] = [];
			}

			if (!isset($data['system']['gametype'])) {
				$size = (int) $map['wid'];
				$data['system']['gametype'] = $size >= 25 ? 'far' : 'uhu';
			}

			$data['height'] = (int) $map['hei'];
			$data['width'] = (int) $map['wid'];
			$data['tx'] = (int) $city['x'];
			$data['ty'] = (int) $city['y'];

			// Water
			$data['water']['well'] = (int) $city['water'];
			$data['water']['bank'] = 0; // updated in bank loop

			// Constructions & Votes
			unset($data['constructions']);
			$data['constructions'] = [];
			foreach ($city->children() as $con) {
				if ((int) $con['id'] > 0) {
					$data['constructions'][(int) $con['id']] = [
						'name' => [
							'de' => (string) $con['name-de'],
							'en' => (string) $con['name-en'],
							'fr' => (string) $con['name-fr'],
							'es' => (string) $con['name-es'],
						],
						'temp' => (int) $con['temporary'],
						'image' => (string) $con['img']
					];
				}
			}
			if (isset($data['constructions'])) {
				foreach ($upgrades->children() as $up) {
					if ((int) $up['buildingId'] > 0) {
						$data['constructions'][(int) $up['buildingId']]['level'] = (int) $up['level'];
					}
				}
			}

			//wind info
			if (!isset($data['storm'])) {
				$data['storm'] = [];
			}
			if (!isset($data['stormnames'])) {
				$data['stormnames'] = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW'];
			}
			if (!isset($data['stormstamp'])) {
				$data['stormstamp'] = [];
			}
			//unset($data['stormApiTryDay']); // enable this for testing

			if (!isset($data['stormApiTryDay'])) {
				$data['stormApiTryDay'] = 0;
			}
			if (!isset($data['stormSetByApi'])) {
				$data['stormSetByApi'] = 0;
			}

			if (isset($data['constructions']) && array_key_exists(80, $data['constructions']) && $data['system']['day'] != $data['stormApiTryDay']) {
				$data['stormApiTryDay'] = $data['system']['day'];
				$data['stormSetByApi'] = 0;

				$windjson = $this->systemModel->retrieveWindSecure($key);
				if (!isset($windjson->error)) {
					$windnews = $windjson->map->city->news;
				}

				if (isset($windnews) && isset($windnews->regenDir)) {
					$wind = $windnews->regenDir;
				} else {
					$wind = NULL;
				}

				if ($wind) {
					$windmap = [
						"North" => 0,
						"North-East" => 1,
						"East" => 2,
						"South-East" => 3,
						"South" => 4,
						"South-West" => 5,
						"West" => 6,
						"North-West" => 7,
					];
					$data['storm'][$data['system']['day']] = (string) ($windmap[$wind] + 1);
					$data['stormstamp'][$data['system']['day']] = time();
					$data['stormSetByApi'] = 1;
				}
			}

		}

        // Constructions and BPs tabs and prepopulate it.
        // Probably we can delete some logic here, as we are already doing it everytime we open the tabs.
        //unset($data['bpsAndConstructions']);  // just for debug
        $constructions_config = config('App\Config\ConstructionsList');
        if (!isset($data['bpsAndConstructions']) || !isset($data['bpsAndConstructions']->{1}->children)) { // if first access to the town or old structure
            $data['bpsAndConstructions'] = (object)[];
            $bps = $this->systemModel->retrieveBuildingsSecure($key);
            $hardcodedIds = array_reduce($constructions_config->constructionsOrder, function ($carry, $item) {
                return array_merge($carry, is_array($item) ? $item : [$item]);
            }, []);

            // Construct list of constructions and bps
            foreach ($bps as $bpnameid => $bp) {
                $bpid = $bp->id;
                if (array_key_exists($bpid, $data['constructions'])) $bp->found = TRUE;  // Mark all BP constructions already built as found
                $bpparent = $bp->parent;

                if (isset($data['bpsAndConstructions']->{$bpid})) {  // It can be already set, in the case that it have the children array but no more info
                    $bp->children = $data['bpsAndConstructions']->{$bpid}->children;
                    $data['bpsAndConstructions']->{$bpid} = $bp;
                } else {
                    $data['bpsAndConstructions']->{$bpid} = $bp;

                    // Loads children with hardcoded array
                    if (array_key_exists($bpid, $constructions_config->constructionsOrder)) {
                        $data['bpsAndConstructions']->{$bpid}->children = $constructions_config->constructionsOrder[$bpid];
                    } else {
                        $data['bpsAndConstructions']->{$bpid}->children = array();
                    }
                }
                if ($bpparent == 0) continue; // If parent is 0, then this construction is not child of any other construction

                if (!isset($data['bpsAndConstructions']->{$bpparent})) { // If the parent was not already created, creates an empty one one with children info, but no other info (the other info will be updated in the iteration of that construction)
                    $data['bpsAndConstructions']->{$bpparent} = (object)[];

                    // Loads children with hardcoded array
                    if (array_key_exists($bpparent, $constructions_config->constructionsOrder)) {
                        $data['bpsAndConstructions']->{$bpparent}->children = $constructions_config->constructionsOrder[$bpparent];
                    } else {
                        $data['bpsAndConstructions']->{$bpparent}->children = array();
                    }
                }

                // Add only if it is not already added by the hardcoded array
                if (!in_array($bpid, $hardcodedIds)) {
                    $data['bpsAndConstructions']->{$bpparent}->children[] = $bpid;
                }
            }
            $eruins_config = config('Eruins');
            foreach ($eruins_config->eruins as $eruin => $eruinbps) {
                foreach ($eruinbps as $bpid) {
                    if (!isset($data['bpsAndConstructions']->{$bpid}->eruin)) {
                        $data['bpsAndConstructions']->{$bpid}->eruin = [];
                    }
                    $data['bpsAndConstructions']->{$bpid}->eruin[] = $eruin;
                }
            }
            foreach ($constructions_config->waterAdded as $constructionId => $water) {
                if (!isset($data['bpsAndConstructions']->{$constructionId}->water)) {
                    $data['bpsAndConstructions']->{$constructionId}->water = $water;
                }
            }

            // Get already unlocked constructions
            $constructions_in_town = $this->systemModel->retrieveBuildingsInTownSecure($key)->map->city->chantiers;
            foreach ($constructions_in_town as $construction) {
                if ($construction->rarity > 0) {
                    $data['bpsAndConstructions']->{$construction->id}->found = TRUE;
                }
                if ($construction->actions < $data['bpsAndConstructions']->{$construction->id}->pa) {
                    $data['bpsAndConstructions']->{$construction->id}->paleft = $construction->actions;
                }
            }
        }
        $data['workshopConversions'] = $constructions_config->workshopConversions;

		//maybe here is our issue with updates often not processing in tense moments, archived old line
		//if ( !isset($data['timestamp']) || (isset($data['timestamp']) && (time() - $data['timestamp']) > 30) || $data['system']['owner_id'] == 3137 ) {
		//if ( !isset($data['timestamp']) || (isset($data['timestamp']) && (time() - $data['timestamp']) > 10)) {
		//$data['system']['icon'] = (string) $headers['iconurl'];
		if (isset($headers)) {
			$data['system']['icon'] = "https://myhord.es/build/images/";
			$data['system']['avatar'] = (string) $headers['avatarurl'];
			$data['system']['secure'] = (int) $headers['secure'];
			$data['system']['xmlversion'] = (string) $headers['version'];
		
			$data['system']['quarantine'] = (int) $game['quarantine'];
			$data['system']['gametime'] = (string) $game['datetime'];
			$data['system']['datatime'] = (string) $xmldata['cache-date'];
		}

		// zones
		if (isset($map)) {
			foreach ($map->children() as $zdata) {
				// core xml data
				$zx = (int) $zdata['x']; // x
				$zy = (int) $zdata['y']; // y
				$zv = (int) $zdata['nvt']; // visited (bool)

				$zz = (isset($zdata['z']) && !is_null($zdata['z']) ? (int) $zdata['z'] : null); // zombies
				$xmlz = $zz;
				$zt = (isset($zdata['tag']) && !is_null($zdata['tag']) ? (int) $zdata['tag'] : null); // tag

				//danger
				$zd = null;
				if ($zv == 0 && isset($zdata['danger']) && !is_null($zdata['danger'])) {
					if (array_key_exists(78, $data['constructions']) && (int) $zdata['danger'] == 3 && !is_null($zz) && $zz > 8) {
						$zd = 4;
					} else {
						$zd = (int) $zdata['danger'];
					}
				}
				if ($zv == 0 && $zd == null) {
					$zd = 0;
				}
				//$zd = (isset($zdata['danger']) && !is_null($zdata['danger']) ? (int) $zdata['danger'] : null); // danger


				// Upgrade map and missing zombies attribute
				if (is_null($zz) && $zv === 0 && array_key_exists(78, $data['constructions'])) {
					$zz = 0;
				}
				// Air Strike and missing zombies attribute
				if (($zx === $data['tx'] || $zy === $data['ty']) && array_key_exists(134, $data['constructions'])) {
					$zz = 0;
					$zv = 0;
					$zd = 0;
				}
				/*if (is_null($zd) && !is_null($zz)) {
					if ($zz === 0) {
						$zd = 0;
					} elseif ($zz > 0 && $zz < 2) {
						$zd = 1;
					} elseif ($zz > 1 && $zz < 5) {
						$zd = 2;
					} elseif ($zz > 4 && $zz < 9) {
						$zd = 3;
					} elseif ($zz > 8) {
						$zd = 4;
					}
				} elseif (is_null($zd) && $zv === 0) {
					$zd = 0;
				}*/
				if ($zv === 1 && isset($data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['danger']) && !is_null($data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['danger']) && $data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['danger'] != 0) {
					$zd = $data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['danger'];
				}

				if (!isset($data['map']['y' . ((string) $zy)]['x' . ((string) $zx)])) {
					$data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['dried'] = 0;
				}
				if ($newday) {
					$data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['zm'] = -1;
				}

				//clean zero zeds from inside eruin
				if ($newday) {

					#var_dump($data);exit;

					if (isset($data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['building']['ruin'])) {
						$eruin = $data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['building']['ruin'];
						foreach ($eruin as &$efloor) {
							if (is_array($efloor)) {
								foreach ($efloor as &$ey) {
									foreach ($ey as &$ex) {
										if (!array_key_exists('z', $ex) || $ex['z'] == 0) {
											$ex['z'] = -1;
										}
									}
								}
							}
						}
						$data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['building']['ruin'] = $eruin;
					}
				}

				$zm = max(-1, $zz); 

				// building data
				if ($building = $zdata->building) {
					// Learn building names.
					if (
						!isset($building_data[(int) $building['type']]) ||
						$building_data[(int) $building['type']]['de'] != $building['name-de'] ||
						$building_data[(int) $building['type']]['en'] != $building['name-en'] ||
						$building_data[(int) $building['type']]['fr'] != $building['name-fr'] ||
						$building_data[(int) $building['type']]['es'] != $building['name-es']
					) {
						$building_data[(int) $building['type']] = [
							'de' => (string) $building['name-de'],
							'en' => (string) $building['name-en'],
							'fr' => (string) $building['name-fr'],
							'es' => (string) $building['name-es'],
						];
						$updateBuildings = TRUE;
					}
					// Learn building possible positions.
					$distance = $this->km((int) $zx - $data['tx'], $data['ty'] - (int) $zy);
					if (
						!isset($building_data[(int) $building['type']]['mind']) ||
						$building_data[(int) $building['type']]['mind'] > $distance
					) {
						$building_data[(int) $building['type']]['mind'] = $distance;
						$updateBuildings = TRUE;
					}
					if (
						!isset($building_data[(int) $building['type']]['maxd']) ||
						$building_data[(int) $building['type']]['maxd'] < $distance
					) {
						$building_data[(int) $building['type']]['maxd'] = $distance;
						$updateBuildings = TRUE;
					}
					if (!isset($data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['building'])) {
						$data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['building'] = [
							'name' => (string) $building_data[(int) $building['type']][$langcode], 
							'type' => (int) $building['type'], 
							'dig' => (int) $building['dig'], 
							'content' => (string) $building['content']
						];
						if (in_array((int) $data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['building']['type'], [61, 62, 63])) {
							$data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['building']['ruin']['upper'] = [
								'y0' => [
									'x7' => [
										'z' => -1,
										'tile' => 5,
									],
								],
							];
						}
					} else {
						$data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['building']['name'] = (string) $building_data[(int) $building['type']][$langcode];
						$data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['building']['type'] = (int) $building['type'];
						$data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['building']['dig'] = (string) $building['dig'];
						$data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['building']['content'] = (string) $building['content'];
						if (in_array((int) $data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['building']['type'], [61, 62, 63]) && !isset($data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['building']['ruin'])) {
							$data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['building']['ruin']['upper'] = [
								'y0' => [
									'x7' => [
										'z' => -1,
										'tile' => 3,
									],
								],
							];
						}
					}
				} else {
					unset($data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['building']);
				}
				$data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['nyv'] = 0;
				if (!is_null($zz)) {
					$data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['z'] = $zz;
				}
				// Always set the xmlz number to current value.
				$data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['xmlz'] = $xmlz;
				if ($newday || !array_key_exists('zm', $data['map']['y' . ((string) $zy)]['x' . ((string) $zx)])) {
					$data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['zm'] = -1;
				}
				if (!is_null($zz) && $data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['zm'] < $zz) {
					$data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['zm'] = $zz;
				}
				if ($data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['zm'] < $zm) {
					$data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['zm'] = $zm;
				}
				if (!is_null($zv)) {
					$data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['nvt'] = $zv;
				}
				if (!is_null($zt)) {
					$data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['tag'] = $zt;
				}
				if (!is_null($zd)) {
					$data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['danger'] = $zd;
				}
				if (is_null($zd) && !is_null($zv) && $zv == 1) {
					unset($data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['danger']);
				}
				if (!is_null($zd) && $zd == 0 && !is_null($zv) && $zv == 0 && is_null($zz) && (!isset($data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['z']) || $data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['z'] > 0 && date("z", $data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['updatedOn']) != date("z", time()))) {
					$data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['z'] = 0;
					#$data['map']['y'.((string) $zy)]['x'.((string) $zx)]['updatedOn'] = time();
					#$data['map']['y'.((string) $zy)]['x'.((string) $zx)]['updatedBy'] = "Wachturm";
				}
				//if ( !$chaos ) {
				//clean all zones also in chaos
				$data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['citizens'] = [];
				//}
				$data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['rx'] = (int) $zx - $data['tx'];
				$data['map']['y' . ((string) $zy)]['x' . ((string) $zx)]['ry'] = $data['ty'] - (int) $zy;
			}
		}
		$data['map']['y' . ($data['ty'])]['x' . ($data['tx'])]['nyv'] = 0;

		// citizen
		if (isset($citizens)) {
			if (!array_key_exists('citizens', $data)) {
				$data['citizens'] = [];
			}
			$old_citizens = $data['citizens'];
			unset($data['citizens']);
			foreach ($citizens->children() as $ca) {
				$data['citizens'][(int) $ca['id']] = [
					'name' => (string) $ca['name'],
					'out' => (int) $ca['out'],
					'ban' => (int) $ca['ban'],
					'hero' => (int) $ca['hero'],
					'job' => (string) $ca['job'],
					'dead' => (int) $ca['dead'],
				];
				if (array_key_exists((int) $ca['id'], $old_citizens)) {
					$data['citizens'][(int) $ca['id']] = array_merge($data['citizens'][(int) $ca['id']], [
						// if chaos and if outside, take last value, if chaos and inside, take 0/0, if not chaos, take API values
						'x' => $chaos ? (((int) $ca['out']) ? $old_citizens[(int) $ca['id']]['x'] : (int) $data['tx']) : (int) $ca['x'],
						'y' => $chaos ? (((int) $ca['out']) ? $old_citizens[(int) $ca['id']]['y'] : (int) $data['ty']) : (int) $ca['y'],
						'rx' => $chaos ? (((int) $ca['out']) ? $old_citizens[(int) $ca['id']]['rx'] : 0) : ((int) $ca['x']) - $data['tx'],
						'ry' => $chaos ? (((int) $ca['out']) ? $old_citizens[(int) $ca['id']]['ry'] : 0) : $data['ty'] - ((int) $ca['y']),
						'lastUpdateTime' => $old_citizens[(int) $ca['id']]['lastUpdateTime'],
						'lastUpdateDay' => $old_citizens[(int) $ca['id']]['lastUpdateDay'],
					]);
				} else {
					$data['citizens'][(int) $ca['id']] = array_merge($data['citizens'][(int) $ca['id']], [
						// if chaos and if outside, take last value, if chaos and inside, take 0/0, if not chaos, take API values
						'x' => $chaos ? (int) $data['tx'] : (int) $ca['x'],
						'y' => $chaos ? (int) $data['ty'] : (int) $ca['y'],
						'rx' => $chaos ? 0 : ((int) $ca['x']) - $data['tx'],
						'ry' => $chaos ? 0 : $data['ty'] - ((int) $ca['y']),
						'lastUpdateTime' => 0,
						'lastUpdateDay' => 0,
					]);
				}
				unset($old_citizens[(int) $ca['id']]);
				//if ( !$chaos ) {
				//repopulate all zones from citizen list
				$data['map']['y' . $data['citizens'][(string) $ca['id']]['y']]['x' . $data['citizens'][(string) $ca['id']]['x']]['citizens'][(string) $ca['id']] = ['name' => (string) $ca['name'], 'job' => (string) $ca['job']];
				//}
			}
		}

		if (isset($owner) && isset($data['citizens'][(string) $owner['id']]) && isset($data['citizens'][(string) $owner['id']]['name'])) {
		//last update timestamp
			$data['citizens'][(string) $owner['id']]['lastUpdateTime'] = time();
			$data['citizens'][(string) $owner['id']]['lastUpdateDay'] = $day;
		}

		// cadavers
		if (isset($cadavers)) {
			foreach ($cadavers->children() as $ca) {
				if (!isset($data['cadavers'][(int) $ca['day']][(int) $ca['id']])) {
					$data['cadavers'][(int) $ca['day']][(int) $ca['id']] = [
						'name' => (string) $ca['name'],
						'dtype' => (int) $ca['dtype'],
					];
				}
				if (isset($old_citizens[(int) $ca['id']])) {
					$data['cadavers'][(int) $ca['day']][(int) $ca['id']]['x'] = $old_citizens[(int) $ca['id']]['x'];
					$data['cadavers'][(int) $ca['day']][(int) $ca['id']]['y'] = $old_citizens[(int) $ca['id']]['y'];
					$data['cadavers'][(int) $ca['day']][(int) $ca['id']]['rx'] = $old_citizens[(int) $ca['id']]['x'] - $data['tx'];
					$data['cadavers'][(int) $ca['day']][(int) $ca['id']]['ry'] = $data['ty'] - $old_citizens[(int) $ca['id']]['y'];
				}
			}
		}
		//		}

		if ((!isset($chaos) || !$chaos) && isset($myzone) && isset($data['citizens'][$data['system']['owner_id']])) {
			// myZone update
			$items = [];
			if (count((array) $myzone) > 0) {
				foreach ($myzone->item as $item) {
					$items[] = [
						'id' => pow(-1, ((int) $item['broken'])) * ((int) $item['id']),
						'count' => (int) $item['count'],
						'broken' => (int) $item['broken'],
					];

					$brokenId = pow(-1, ((int) $item['broken'])) * ((int) $item['id']);
					$defaultId = (int) $item['id'];
					//itemsfile with name updates
					if (
						!isset($item_data[$defaultId]) ||
						$item_data[$defaultId]['name']['de'] != (string)$item['name-de'] ||
						$item_data[$defaultId]['name']['en'] != (string)$item['name-en'] ||
						$item_data[$defaultId]['name']['fr'] != (string)$item['name-fr'] ||
						$item_data[$defaultId]['name']['es'] != (string)$item['name-es'] ||
						$item_data[$defaultId]['category'] != (string)$item['cat'] ||
						$item_data[$defaultId]['image'] != (string)$item['img']
					) {
						$item_data[$defaultId] = [
							'id' => $defaultId,
							'name' => [
								'de' => (string) $item['name-de'],
								'en' => (string) $item['name-en'],
								'fr' => (string) $item['name-fr'],
								'es' => (string) $item['name-es'],
							],
							'category' => (string) $item['cat'],
							'image' => (string) $item['img'],
							'broken' => (int) $item['broken'],
						];
						$updateItems = TRUE;
					}
					if (
						(($brokenId == -$defaultId) && isset($item_data[$defaultId])) &&
						(!isset($item_data[$brokenId]) ||
							$item_data[$brokenId]['name']['de'] != (string)$item['name-de'] ||
							$item_data[$brokenId]['name']['en'] != (string)$item['name-en'] ||
							$item_data[$brokenId]['name']['fr'] != (string)$item['name-fr'] ||
							$item_data[$brokenId]['name']['es'] != (string)$item['name-es'] ||
							$item_data[$brokenId]['category'] != (string)$item['cat'] ||
							$item_data[$brokenId]['image'] != (string)$item['img'])
					) {
						$item_data[$brokenId] = $item_data[$defaultId];
						$item_data[$brokenId]['broken'] = 1;
						$item_data[$brokenId]['id'] = $brokenId;
						$updateItems = TRUE;
					}
				}
			}

			$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['items'] = $items;
			$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['dried'] = (int) $myzone['dried'];
			$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['z'] = (int) $myzone['z'];
			$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['updatedOn'] = time();
			$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['updatedOnDay'] = $data['system']['day'];
			$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['updatedBy'] = (string) $owner['name'];
			$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['updatedById'] = (int) $owner['id'];
			if (!array_key_exists('zm', $data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])])) {
				$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['zm'] = -1;
			}
			$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['zm'] = max($data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['zm'], (int) $myzone['z']);

			//ruin searched and camped status updates
			if (isset($data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['building']['type']) && !in_array((int) $data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['building']['type'], [61, 62, 63])) {
				$buildingjson = $this->systemModel->retrieveRuinInfoSecure($key);

				if (!isset($buildingjson->error)) {
					foreach ($buildingjson->map->zones as $jsonzone) {
						if ($jsonzone->x == ((int) $owner['x']) && $jsonzone->y == ((int) $owner['y'])) {
							if (isset($jsonzone->building->dried) && $jsonzone->building->dried == TRUE) {
								$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['building']['dried'] = 1;
							}
							if (isset($jsonzone->building->dried) && $jsonzone->building->dried == FALSE) {
								$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['building']['dried'] = 0;
							}
							if (isset($jsonzone->building->camped) && $jsonzone->building->camped == TRUE) {
								$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['building']['blueprint'] = 1;
							}
							if (isset($jsonzone->building->camped) && $jsonzone->building->camped == FALSE) {
								$data['map']['y' . ((int) $owner['y'])]['x' . ((int) $owner['x'])]['building']['blueprint'] = 0;
							}
						}
					}
				}
			}
		}


		$bankItems = [];
		$bankCount = [];
		if (isset($bank)) {
			foreach ($bank->children() as $bia) {
				$bi_name = array(
					'de' => (string) $bia['name-de'],
					'en' => (string) $bia['name-en'],
					'fr' => (string) $bia['name-fr'],
					'es' => (string) $bia['name-es'],
				);
				$bi_count = (int) $bia['count'];
				$bi_cat = (string) $bia['cat'];
				$bi_img = (string) $bia['img'];
				$bi_broken = (int) $bia['broken'];
				$bi_id = pow(-1, ($bi_broken)) * ((int) $bia['id']);

				$bankItems[$bi_id] = array(
					'count' => $bi_count,
					'broken' => $bi_broken,
				);
				$bankCount[(int) $bi_id] = (int) $bi_count;

				if ($bi_id === 1) {
					$data['water']['bank'] = $bi_count;
				}


				//itemsfile with name updates
				if (
					!isset($item_data[$bi_id]) ||
					$item_data[$bi_id]['name']['de'] != $bi_name['de'] ||
					$item_data[$bi_id]['name']['en'] != $bi_name['en'] ||
					$item_data[$bi_id]['name']['fr'] != $bi_name['fr'] ||
					$item_data[$bi_id]['name']['es'] != $bi_name['es'] ||
					$item_data[$bi_id]['category'] != $bi_cat ||
					$item_data[$bi_id]['image'] != $bi_img
				) {
					$item_data[$bi_id] = array(
						'id' => $bi_id,
						'name' => array(
							'de' => (string) $bi_name['de'],
							'en' => (string) $bi_name['en'],
							'fr' => (string) $bi_name['fr'],
							'es' => (string) $bi_name['es'],
						),
						'category' => $bi_cat,
						'image' => $bi_img,
						'broken' => $bi_broken,
					);
					$updateItems = TRUE;
				}
				$brokenId = pow(-1, $bi_broken) * $bi_id;
				if (
					(($brokenId == -$bi_id) && isset($item_data[$bi_id])) &&
					(!isset($item_data[$brokenId]) ||
						$item_data[$brokenId]['name']['de'] != $bi_name['de'] ||
						$item_data[$brokenId]['name']['en'] != $bi_name['en'] ||
						$item_data[$brokenId]['name']['fr'] != $bi_name['fr'] ||
						$item_data[$brokenId]['name']['es'] != $bi_name['es'] ||
						$item_data[$brokenId]['category'] != $bi_cat ||
						$item_data[$brokenId]['image'] != $bi_img)
				) {
					$item_data[$brokenId] = $item_data[$bi_id];
					$item_data[$brokenId]['broken'] = 1;
					$item_data[$brokenId]['id'] = $brokenId;
					$updateItems = TRUE;
				}
			}
			arsort($bankCount);
			$bankItemsSorted = [];
			$i = 0;
			foreach ($bankCount as $id => $cnt) {
				$bankItemsSorted[$i] = $id;
				$i++;
			}
			$data['bank'] = $bankItems;
			$data['bankSorted'] = $bankItemsSorted;
		}

		if ($updateItems === TRUE) {
			if (!$item_stat) {
				touch($item_file, time());
			}
			ksort($item_data);
			file_put_contents($item_file, serialize($item_data), LOCK_EX);
		}
		$data['items'] = $item_data;

		if ($updateBuildings === TRUE) {
			if (!$building_stat) {
				touch($building_file, time());
			}
			ksort($building_data);
			file_put_contents($building_file, serialize($building_data), LOCK_EX);
		}
		$data['buildings'] = $building_data;

		$data['mapitems'] = [
			"Rsc" => ["cat" => "Rsc"], 
			"Food" => ["cat" => "Food"], 
			"Armor" => ["cat" => "Armor"], 
			"Drug" => ["cat" => "Drug"], 
			"Weapon" => ["cat" => "Weapon"], 
			"Misc" => ["cat" => "Misc"], 
			"Furniture" => ["cat" => "Furniture"], 
			"Box" => ["cat" => "Box"],
		];

		// expeditions
		$e = 0;
		#unset($data['expeditions']);
		if (isset($expeditions)) {
			foreach ($expeditions->children() as $exp) {
				$e = $data['system']['day'] . '.' . (string) $exp['name'] . ((int) $exp['authorId']);
				if (!isset($data['expeditions'][$e])) {
					$data['expeditions'][$e] = array(
						'day' => $data['system']['day'],
						'creator' => (int) $exp['authorId'],
						'creatorName' => (string) $exp['name'],
						'length' => (int) $exp['length'],
						'type' => 'public',
						'name' => $exp['name'] . ' [' . $exp['length'] . 'AP] (' . $exp['author'] . ' via MH)',
						'color' => '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT),
					);
					//
					//				$i = 0;
					//				$ox = $data['tx'];
					//				$oy = $data['ty'];
					$routeString = "";

					foreach ($exp->children() as $pa) {
						$px = (int)$pa['x'];
						$py = (int)$pa['y'];
						$routeString = $routeString . $px . "-" . $py . '_';
					}
					$data['expeditions'][$e]['route'] = $routeString;


					//					if ( $i != 0 && !($px == $ox && $py == $oy) ) {
					//						if ( abs($px - $ox) > 1 ) {
					//							if ( $px > $ox ) {
					//								for ( $tx = ($ox + 1); $tx <= $px; $tx++ ) {
					//									$data['expeditions'][$e]['route'][$i] = array('x' => $tx, 'y' => $py);
					//									$i++;
					//								}
					//							}
					//							else {
					//								for ( $tx = ($ox - 1); $tx >= $px; $tx-- ) {
					//									$data['expeditions'][$e]['route'][$i] = array('x' => $tx, 'y' => $py);
					//									$i++;
					//								}
					//							}
					//						}
					//						elseif ( abs($py - $oy) > 1 ) {
					//							if ( $py > $oy ) {
					//								for ( $ty = ($oy + 1); $ty <= $py; $ty++ ) {
					//									$data['expeditions'][$e]['route'][$i] = array('x' => $px, 'y' => $ty);
					//									$i++;
					//								}
					//							}
					//							else {
					//								for ( $ty = ($oy - 1); $ty >= $py; $ty-- ) {
					//									$data['expeditions'][$e]['route'][$i] = array('x' => $px, 'y' => $ty);
					//									$i++;
					//								}
					//							}
					//						}
					//						else {
					//							$data['expeditions'][$e]['route'][$i] = array('x' => $px, 'y' => $py);
					//							$i++;
					//						}
					//					}
					//					else {
					//						#$data['expeditions'][$e]['route'][$i] = array('x' => $px, 'y' => $py);
					//						$i++;
					//					}
					//					$ox = $px;
					//					$oy = $py;
					//				}
				}
			}
		}

		$data['door'] = isset($city) ? (int) $city['door'] : $data['door'];

		$data['timestamp'] = time();
		if ($this->saveTowndata($data['system']['gameid'], $data['system']['day'], $data['system']['owner_id'], serialize($data))) {
			#$append .= ';ajaxInfo("Database updated!")';
		}
		if (!isset($data['citizens'][$data['system']['owner_id']])) {
			$data['spy'] = 1;
		}
		// Remove private expeditions of other players without changing data locally
		if (isset($data['expeditions'])) {
			foreach ($data['expeditions'] as $expId => $exp) {

				if ($exp["type"] === "private" && $exp["creator"] !== $data['system']['owner_id']) {
					unset($data['expeditions'][$expId]);
				}
			}
		}
		return json_encode($data) . $append;
		#$this->debug = $data['map'];
	}
	
	static function km($x, $y)
	{
		return round(sqrt(pow(abs($x), 2) + pow(abs($y), 2)));
	}

}

<?php

namespace App\Models;

use CodeIgniter\Model;

class CitizenModel extends Model {
	
	protected $db;

	function __construct() {
		parent::__construct();
		$this->db = \Config\Database::connect();
	}
	
	public function load_user_by_skey($key) {
		$builder = $this->db->table('citizen');
		$builder->where('skey', $key);
		$q = $builder->get();
		
		if ($q->getNumRows() == 1) {
			return $q;
		}
		return false;
	}
	
	public function save_user_from_xml() {
		$builder = $this->db->table('citizen');
		$builder->where('skey', $this->request->getPost('user_name'));
		$q = $builder->get();
		
		if ($q->getNumRows() == 1) {
			// exists -> update
			return true;
		}
		else {
			// new -> insert
		}
	}
	
	public function load_user() {
		$builder = $this->db->table('citizen');
		$builder->where('name', $this->request->getPost('user_name'));
		$builder->where('okey', $this->request->getPost('user_okey'));
		$q = $builder->get();
		
		if ($q->getNumRows() == 1) {
			return true;
		}
		return false;
	}
	
	public function save_user() {
		$builder = $this->db->table('citizen');
		$builder->where('name', $this->request->getPost('user_name'));
		$q = $builder->get();
		
		if ($q->getNumRows() == 1) {
			// exists -> update
			return true;
		}
		else {
			// new -> insert
		}
	}

}
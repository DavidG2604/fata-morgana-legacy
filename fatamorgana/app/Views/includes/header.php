<!DOCTYPE HTML> 
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="UTF-8">
		<script src="<?=base_url()?>assets/js/jquery/1.5.1/jquery.min.js"></script>
		<script src="<?=base_url()?>assets/js/jqueryui/1.8.10/jquery-ui.min.js"></script>
		<script src="<?=base_url()?>assets/js/translations.js?v=<?php echo view('includes/version'); ?>"></script>
		<script src="<?=base_url()?>assets/js/fm.js?v=<?php echo view('includes/version'); ?>"></script>
		<script src="<?=base_url()?>assets/js/moment-with-locales.js"></script>
		<script src="<?=base_url()?>assets/js/moment-timezone-with-data.js"></script>
		<script src="<?=base_url()?>assets/js/canvas.js"></script>
		<script src="<?=base_url()?>assets/js/canvas2image.js"></script>
		<script src="<?=base_url()?>assets/js/base64.js"></script>
		<script src="<?=base_url()?>assets/js/farbtastic.js"></script>
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/style.css?v=<?php echo view('includes/version'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/canvas.css" />
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/farbtastic.css" />
		<link rel="icon" type="image/x-icon" href="<?=base_url()?>favicon.ico" /> 
		
		<title>Fata Morgana</title> 
		
		<!--[if IE]><script type="text/javascript" src="<?=base_url()?>assets/js/excanvas.js"></script><![endif]-->
	</head>
	<body>
        <div id="userInfoBox"></div>
		<div id="modeBar" class="clearfix">
			<h1 class="fatafont">fata morgana</h1>
			<h5><span data-trans="logged-in"></span> <span id="owner-name"></span></h5>
            <div class="mode-menu">
				<div class="lang-switcher click" id=lang-switcher data-trands="language" onclick="toggleLangButton(true)" onmouseleave="toggleLangButton()">
					<span data-trans="language"></span>
					<div id="lang-switcher-content" class="">
						<div id="lang-selection-default" class="lang-selection lang-default" data-trans="Standard" onclick="setUserLanguage('default')"></div>
						<div id="lang-selection-de" class="lang-selection lang-de" data-trans="Deutsch" onclick="setUserLanguage('de')"></div>
						<div id="lang-selection-en" class="lang-selection lang-en" data-trans="Englisch" onclick="setUserLanguage('en')"></div>
						<div id="lang-selection-fr" class="lang-selection lang-fr" data-trans="Französisch" onclick="setUserLanguage('fr')"></div>
						<div id="lang-selection-es" class="lang-selection lang-es" data-trans="Spanisch" onclick="setUserLanguage('es')"></div>
					</div>	
				</div>
                <div class="panel-switch map-panel-switch click active-panel" data-panel="map" id="mode-normal" data-trans="mode-map"></div>
                <div class="panel-switch eruin-panel-switch eruin-link click" data-panel="eruin" id="eruin-link" data-trans="e-ruin" onclick="openEruinPanel()"></div>
				<div class="panel-switch bps-panel-switch bptracker-link click" data-panel="bps" id="bptracker-link" data-trans="bp-tracker" onclick="openBpTrackerPanel()">
				</div>
				<!--<div class="expedition-link click" id="expedition-link" data-trans="expeditions" onclick="openExpeditionsPanel()"></div>-->
				<div class="help-switch click" id="mode-help" data-trans="mode-help"></div>

				<div class="logout-button click" id="logout-button" data-trans="logout-button"></div>
            </div>
		</div>
		<div id="townBar" class="clearfix hideme">
            <h2 id="townInfo">
                <span id="townDay"></span>
                <span id="townName"></span>
                <span id="townID"></span>
                <span id="townSpy"><a href="#" target="_new" data-trans="spylink"></a></span>
                <span id="townHistory"><span data-trans="day"></span> </span>
            </h2>
        </div>
		<div id="help-section" class="hidefaq">
			<h2 data-trans="faq-title"></h2>
			<ul>
				<li><h4 data-trans="faq-q1"></h4><p data-trans="faq-a1"></p></li>
				<li><h4 data-trans="faq-q2"></h4><p data-trans="faq-a2"></p></li>
				<li><h4 data-trans="faq-q3"></h4><p data-trans="faq-a3"></p></li>
				<li><h4 data-trans="faq-q4"></h4><p data-trans="faq-a4"></p></li>
			</ul>
		</div>
		<div id="fm-container" class="clearfix tl">
			<div id="fm-content" class="clearfix">

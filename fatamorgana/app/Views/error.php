<div id="login_form">
    <h2>Error</h2>
    <h4 style="color:#933;"><?=$msg?></h4>

    <?php
    helper('form');

    echo form_open('login/auth');
    // echo form_input('user_name', 'In-game nick');
    // echo form_input('user_okey', 'External ID');
    echo form_input('user_skey', '', ['placeholder' => 'Your MH key'], '');
    echo form_submit('submit', 'Login');
    echo ('<br/><br/><br/> If you experience technical issues, ask MisterD in our Discord.');
    ?>
</div>

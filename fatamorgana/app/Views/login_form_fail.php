<?php
$error_messages = [
    'missing_key' => 'Secure key is missing.',
    'user_not_found' => 'User not found.',
    'only_available_to_secure_request' => 'This action is only available to secure requests.',
    'horde_attacking' => 'Horde is attacking.',
    'rate_limit_reached' => 'Rate limit reached. Please try again later.',
];
?>

<div id="login_form">
    <h2>Error</h2>
    <h4 style="color:#933;">Login failed<?php print ($error_code != '') ? ': ' . ((array_key_exists($error_code, $error_messages)) ? $error_messages[$error_code] : $error_code) : '.'; ?></h4>

    <?php
    helper('form');

    echo form_open('login/auth');
    // echo form_input('user_name', 'In-game nick');
    // echo form_input('user_okey', 'External ID');
    echo form_input('user_skey', '', ['placeholder' => 'Your MH key'], '');
    echo form_submit('submit', 'Login');
    echo ('<br/><br/><br/> If you experience technical issues, ask MisterD in our Discord.');
    ?>
</div>

<div id="map-panel" class="panel active">
    <div id="map-wrapper" class="clearfix">
        <div id="map" class="clearfix"></div>
    </div>
    <div id="update-myzone">
        <button id="update-myzone-button" data-trans="update-myzone"></button>
    </div>
    <div id="box" style="display:none;" class="clearfix">
        <div id="box-protection" class="hideme"></div>
        <div id="box-content">
            <ul id="box-tabs" class="clearfix">
                <li id="tab-zone-info" ref="zone-info zone-info-extras" class="box-tab active click"
                    data-transtitle="zone"></li>
                <li id="tab-item-info" ref="item-info" class="box-tab click" data-transtitle="items"></li>
                <li id="tab-bank-info" ref="bank-info" class="box-tab click" data-transtitle="bank"></li>
                <li id="tab-town-info" ref="town-info" class="box-tab click" data-transtitle="city"></li>
                <li id="tab-citizens" ref="citizens" class="box-tab click" data-transtitle="citizens"></li>
                <li id="tab-storms" ref="storms" class="box-tab click" data-transtitle="desert-storms"></li>
                <li id="tab-ruins" ref="ruins" class="box-tab click" data-transtitle="ruins"></li>
                <li id="tab-expeditions" ref="expeditions" class="box-tab click" data-transtitle="expeditions"></li>
                <li id="tab-options" ref="options" class="box-tab click" data-transtitle="options"></li>
                <li id="tab-spread" class="box-spread click" data-transtitle="overview-expand">&lt;&gt;</li>
                <li id="tab-unspread" class="box-unspread click hideme" data-transtitle="overview-collapse">&gt;&lt;</li>
            </ul>

            <div id="zone-info" class="spread-no box-tab-content"></div>
            <div id="storms" class="spread-no box-tab-content hideme" data-trans="please-wait"></div>
            <div id="expeditions" class="spread-no box-tab-content hideme" data-trans="please-wait"></div>
            <div id="ruins" class="spread-me spread-4 box-tab-content hideme" data-trans="please-wait"></div>
            <div id="citizens" class="spread-me spread-3 box-tab-content hideme" data-trans="please-wait"></div>
            <div id="town-info" class="spread-me spread-5 box-tab-content hideme" data-trans="please-wait"></div>

            <div id="item-info" class="spread-me spread-1 hideme box-tab-content">
                <p class="desc" data-trans="item-info"></p>
                <h3 data-trans="search"></h3><input id="item-search" class="itemsearch">
                <div id="item-info-Rsc" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-rsc"></h3></div>
                <div id="item-info-Furniture" class="zone-item-cat clearfix click" state="0"><h3
                            data-trans="item-furniture"></h3></div>
                <div id="item-info-Weapon" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-weapon"></h3>
                </div>
                <div id="item-info-Box" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-box"></h3></div>
                <div id="item-info-Armor" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-armor"></h3>
                </div>
                <div id="item-info-Drug" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-drug"></h3>
                </div>
                <div id="item-info-Food" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-food"></h3>
                </div>
                <div id="item-info-Misc" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-misc"></h3>
                </div>
            </div>

            <div id="bank-info" class="spread-me spread-2 hideme box-tab-content">
                <p class="desc" data-trans="bank-info"></p>
                <h3 data-trans="search"></h3><input id="bank-search" class="itemsearch">
                <div id="bank-info-Rsc" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-rsc"></h3></div>
                <div id="bank-info-Furniture" class="zone-item-cat clearfix click" state="0"><h3
                            data-trans="item-furniture"></h3></div>
                <div id="bank-info-Weapon" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-weapon"></h3>
                </div>
                <div id="bank-info-Box" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-box"></h3></div>
                <div id="bank-info-Armor" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-armor"></h3>
                </div>
                <div id="bank-info-Drug" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-drug"></h3>
                </div>
                <div id="bank-info-Food" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-food"></h3>
                </div>
                <div id="bank-info-Misc" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-misc"></h3>
                </div>
            </div>

            <div id="options" class="spread-no hideme box-tab-content">
                <div id="options-radius" class="options-section">
                    <p class="desc" data-trans="options-radii"></p>
                    <div id="colorpicker"></div>
                    <br>
                    <!--<input class="hideme" type="text" id="opt-radius-color" name="opt-radius-color" value="#9999ff"/>-->
                    <span data-trans="Color"></span>
                    <input type="color" style="height: 18px; width: 35px;" id="opt-radius-color" name="opt-radius-color"
                        value="#9999ff"/>
                    <input type="text" id="opt-radius-number" name="opt-radius-number" value="9" size="4"
                        maxlength="2"/><input type="radio" id="opt-radius-ap" name="opt-radius-metric" value="ap"
                                                checked="checked"/> <span data-trans="AP"></span>&nbsp;&nbsp;<input
                            type="radio" id="opt-radius-km" name="opt-radius-metric" value="km"/> <span
                            data-trans="km"></span>&nbsp;&nbsp;
                    <br>


                    <div id="opt-radius-submit" class="click button" data-trans="button-add"></div>
                    <br>
                    <span class="hideme click interactive" onclick="removeRadius();" data-trans="delete-radii"></span>
                    <div id="radius-checklist">
                        <div id="radius-op" class="radius-checklist-item click" data-range="9" data-metric="ap" data-name="op" style="display:block;">
                        <div class="radius-color-example" style="background-color:#00ff66;"></div><div data-trans="default-radius"></div></div>
                        <div id="radius-tp" class="radius-checklist-item click" data-range="0" data-metric="km" data-name="tp" style="display:block;">
                        <div class="radius-color-example" style="background-color:rgb(0, 115, 255);;"></div><div data-trans="teleport"></div></div>
                        <div id="radius-hr" class="radius-checklist-item click" data-range="11" data-metric="km" data-name="hr" style="display:block;">
                        <div class="radius-color-example" style="background-color:#00ffff;"></div><div data-trans="hero-return"></div></div>
                        <div id="radius-tw" class="radius-checklist-item click" data-range="0" data-metric="ap" data-name="tw" style="display:block;">
                        <div class="radius-color-example" style="background-color:#ff7800;"></div><div data-trans="city"></div></div>
                        <div id="radius-gd" class="radius-checklist-item click" data-range="0" data-metric="km" data-name="gd" style="display:block;">
                        <div class="radius-color-example" style="background-color:rgba(255, 255, 255, 0.25);"></div><div data-trans="display-compass"></div></div>
                    </div>
                    <div id="radius-list"></div>
                    <!--<div id="radius-save" class="click button" data-trans="save-radii"></div>-->
                    <div id="radius-reset" class="click button" data-trans="reset-radii"></div>
                </div>
                <div id="options-display" class="options-section">
                    <p class="desc" data-trans="options-display"></p>
                    <div id="options-display-zonehover" class="click options-display-switch"
                        data-trans="display-hover"></div>
                    <div id="options-display-geodir" class="click options-display-switch"
                        data-trans="display-compass"></div>
                    <div id="options-display-driedzone" class="click options-display-option" ref="zone-status-dried"
                        data-trans="display-icon-dried"></div>
                    <div id="options-display-fullzone" class="click options-display-option" ref="zone-status-full"
                        data-trans="display-icon-full"></div>
                    <div id="options-display-stormzone" class="click options-display-option" ref="zone-storm-status"
                        data-trans="display-icon-storm"></div>
                    <div id="options-display-citizens" class="click options-display-option" ref="citizen"
                        data-trans="display-citidot"></div>
                    <div id="options-display-zombies" class="click options-display-option" ref="zombies"
                        data-trans="display-zombies"></div>
                    <div id="options-display-maxzombies" class="click options-display-option" ref="maxzombies"
                        data-trans="display-maxzombies"></div>
                    <div id="options-display-uptodate" class="click options-display-option" ref="zone-updated"
                        data-trans="display-status"></div>
                    <div id="options-display-localtime" class="click options-display-option" ref="localtime"
                        data-trans="display-localtime"></div>
                    <div id="options-display-ampmtime" class="click options-display-option" ref="ampmtime"
                        data-trans="display-ampmtime"></div>
                    <div id="highlight-bluechests" class="click highlight-filter"
                        data-trans="highlight-bluechests"></div>
                    <div id="highlight-purplechests" class="click highlight-filter"
                        data-trans="highlight-purplechests"></div>
                    <div id="highlight-fireworksparts" class="click highlight-filter"
                        data-trans="highlight-fireworksparts"></div>
                    <!--<div id="options-save" class="click button" data-trans="save-settings"></div>-->
                    <div id="options-reset" class="click button" data-trans="reset-settings"></div>
                    <div id="import-external-1" class="click button hideme">import external 1</div>
                </div>
                <div id="options-bookmark" class="options-section">
                    <p class="desc"><span data-trans="options-bookmark1"></span> <a href="<?= $bookmark ?>"
                                                                                    data-trans="bookmark"></a> <span
                                data-trans="options-bookmark2"></span></p>
                </div>
            </div>
        </div>
        <div id="box-extra-content">
            <br>
            <div id="zone-info-extras" class="spread-no box-tab-content"></div>
        </div>
    </div>
    <div id="map-hover">
        <div id="map-hover-content">
            <div id="map-hover-coords"></div>
            <div id="map-hover-building"></div>
            <div id="map-hover-status"></div>
            <div id="map-hover-citizens"></div>
            <div id="map-hover-zombies"></div>
            <div id="map-hover-items"></div>
        </div>
    </div>
</div>
<div id="bps-panel" class="panel">
    <div id="tabs-protection" class="hideme"></div>
    <div id="bps-constructions-wrapper">
        <div id="bps-constructions-wrapper-close" data-trans="close"></div>
        <div class="tab">
            <button id="BPS-TRACKER-TAB" class="tablinks" onclick="" data-trans="BPs Tracker"></button>
            <button id="CONSTRUCTIONS-TAB" class="tablinks" onclick="" data-trans="Constructions List"></button>
        </div>
        <div id="BPS-TRACKER" class="tabcontent">
            <div class="bps-tracker">
                <div class="bps-tracker-filter flex-row">
                    <h4 data-trans="Filter"></h4>
                    <input id="bps-filter-search">
                </div>
                <div class="bps-tracker-stats flex-column">
                    <div class="flex-row bps-tracker-stats-item"><img src="/assets/css/img/item_bplan_c.gif"/><h4><span id="bps-found-greens">0</span>/<span id="bps-total-greens">0</span></h4></div>
                    <div class="flex-row bps-tracker-stats-item"><img src="/assets/css/img/item_bplan_u.gif"/><h4><span id="bps-found-yellows">0</span>/<span id="bps-total-yellows">0</span></h4></div>
                    <div class="flex-row bps-tracker-stats-item"><img src="/assets/css/img/item_bplan_r.gif"/><h4><span id="bps-found-blues">0</span>/<span id="bps-total-blues">0</span></h4></div>
                    <div class="flex-row bps-tracker-stats-item"><img src="/assets/css/img/item_bplan_e.gif"/><h4><span id="bps-found-purples">0</span>/<span id="bps-total-purples">0</span></h4></div>
                </div>
                <div class="bps-tracker-checkboxes flex-row">
                    <div class="flex-column">
                        <label>
                            <div class="flex-row">
                                    <input autocomplete="off" type="checkbox" name="show-bp-bunker" id="show-bp-bunker" value="show-bp-bunker">
                                    <div class="bp-eruin">
                                        <img src="/assets/css/img/small_enter.gif"/>
                                        <div class="bp-eruin-center">1</div>
                                    </div>
                                    <h4 data-trans="Show BPs attainable from Bunker"></h4>
                            </div>
                        </label>
                        <label>
                            <div class="flex-row">
                                <input autocomplete="off" type="checkbox" name="show-bp-hotel" id="show-bp-hotel" value="show-bp-hotel">
                                <div class="bp-eruin">
                                    <img src="/assets/css/img/small_enter.gif"/>
                                    <div class="bp-eruin-center">2</div>
                                </div>
                                <h4 data-trans="Show BPs attainable from Hotel"></h4>
                            </div>
                        </label>
                        <label>
                            <div class="flex-row">
                                <input autocomplete="off" type="checkbox" name="show-bp-hospital" id="show-bp-hospital" value="show-bp-hospital">
                                <div class="bp-eruin">
                                    <img src="/assets/css/img/small_enter.gif"/>
                                    <div class="bp-eruin-center">3</div>
                                </div>
                                <h4 data-trans="Show BPs attainable from Hospital"></h4>
                            </div>
                        </label>
                    </div>
                    <div class="flex-column">
                        <label>
                            <div class="flex-row">
                                <input type="checkbox" name="hide-bp-found" id="hide-bp-found" value="hide-bp-found">
                                <h4 data-trans="Hide all BPs already found"></h4>
                            </div>
                        </label>
                        <label>
                            <div class="flex-row">
                                <input type="checkbox" name="hide-bp-blocked" id="hide-bp-blocked" value="hide-bp-blocked">
                                <h4 data-trans="Hide all BPs locked by Parent Construction"></h4>
                            </div>
                        </label>
                    </div>
                </div>
                <div class="bps-tracker-greens"></div>
                <div class="bps-tracker-divider" style="grid-area: bps-tracker-first-divider">
                    <hr>
                </div>
                <div class="bps-tracker-yellows"></div>
                <div class="bps-tracker-divider" style="grid-area: bps-tracker-second-divider">
                    <hr>
                </div>
                <div class="bps-tracker-blues"></div>
                <div class="bps-tracker-divider" style="grid-area: bps-tracker-third-divider">
                    <hr>
                </div>
                <div class="bps-tracker-purples"></div>
            </div>
        </div>
        <div id="CONSTRUCTIONS" class="tabcontent">
            <div class="bps-constructions">
                <div class="flex-row" style="padding: 5px; justify-content: center;">
                    <h4 data-trans="Filter"></h4>
                    <input id="constructions-filter-search">
                </div>
                <div style="gap:30px; padding:10px; justify-content: center;" class="constructions-checkboxes flex-row">
                    <div class="flex-row">
                        <label>
                            <input autocomplete="off" type="checkbox" name="hide-unavailable-constructions" id="hide-unavailable-constructions" value="hide-unavailable-constructions" checked>
                            <h4 style="display: inline;" data-trans="Hide unavailable constructions"></h4>
                        </label>
                    </div>
                    <div class="flex-row">
                        <label>
                            <input autocomplete="off" type="checkbox" name="hide-already-built-constructions" id="hide-already-built-constructions" value="hide-already-built-constructions">
                            <h4 style="display: inline;" data-trans="Hide already built constructions"></h4>
                        </label>
                    </div>
                </div>
                <div class="flex-row">
                    <div style="margin-left: 15px;" id="select-all" class="button click" data-trans="Select All"></div>
                    <div class="button click" id="deselect-all" data-trans="Deselect All"></div>
<!--                    <div class="button click" id="invert-selection">Invert Selection</div>-->
                </div>
                <div class="flex-row">
                    <table id="bps-constructions-table" class="bps-constructions-table">
                        <tbody id="CONSTRUCTIONS-BODY"></tbody>
                    </table>
                    <div class="bps-constructions-planner">
                        <div class="flex-column">
                            <h2 data-trans="Construction Planner"></h2>
                            <label>
                                <input autocomplete="off" type="checkbox" name="hacksaw-available" id="hacksaw-available" value="hacksaw-available">
                                <h4 style="display: inline;" data-trans="Hacksaw Available"></h4>
                            </label>
                            <label>
                                <input autocomplete="off" type="checkbox" name="factory-built" id="factory-built" value="factory-built">
                                <h4 style="display: inline;" data-trans="Factory Built"></h4>
                            </label>
                            <div class="flex-column" id="constructions-planner-list"></div>
                            <hr>
                            <div class="flex-column" id="resources-planner-list"></div>
                            <hr>
                            <div class="flex-column" id="workshop-planner-list"></div>
                            <hr>
                            <div class="flex-column">
                                <div class="flex-row">
                                    <span id="constructions-planner-conversions-ap">0</span>
                                    <img src="/assets/css/img/small_pa.gif"/>
                                    <span data-trans="for conversions"></span>
                                </div>
                                <div class="flex-row">
                                    <span id="constructions-planner-constructions-ap">0</span>
                                    <img src="/assets/css/img/small_pa.gif"/>
                                    <span data-trans="for buildings"></span>
                                </div>
                                <div class="flex-row">
                                    <span id="constructions-planner-ap">0</span>
                                    <img src="/assets/css/img/small_pa.gif"/>
                                    <span data-trans="total"></span>
                                </div>
                            </div>
                            <hr>
                            <div style="justify-content: space-between;" class="flex-row plan-languages">
                                <h4 style="width: 150px;" data-trans="Add Secondary Languages:"></h4>
                                <div>
                                    <label style="display: inline-block; vertical-align: bottom;">
                                        <input style="vertical-align: middle;" autocomplete="off" type="checkbox" name="lang-1" id="lang-1" value="lang-1">
                                        <img style="scale: 0.6;vertical-align: middle;" id="flag-lang-1" src="/assets/css/img/lang_de.png"/>
                                    </label>
                                    <label style="display: inline-block; vertical-align: bottom;">
                                        <input style="vertical-align: middle;" autocomplete="off" type="checkbox" name="lang-2" id="lang-2" value="lang-2">
                                        <img style="scale: 0.6;vertical-align: middle;" id="flag-lang-2" src="/assets/css/img/lang_fr.png"/>
                                    </label>
                                    <label style="display: inline-block; vertical-align: bottom;">
                                        <input style="vertical-align: middle;" autocomplete="off" type="checkbox" name="lang-3" id="lang-3" value="lang-3">
                                        <img style="scale: 0.6;vertical-align: middle;" id="flag-lang-3" src="/assets/css/img/lang_es.png"/>
                                    </label>
                                </div>
                            </div>
                            <hr>
                            <div class="flex-column" id="plan-generator-list">
                                <div id="generate-plan" class="click button" style="height: 20px; display: flex; align-items: center; justify-content: center;" data-trans="Generate Post & Copy to Clipboard"></div>
                                <textarea id="plan-box" style="resize: none; overflow-y: scroll;" rows="15" disabled></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="eruin-panel" class="panel">
    <div id="ruinmap-wrapper">
        <div id="ruinmap-wrapper-grid">
            <div id="ruinmap-ariadne-activate" data-trans="edit-ariadne" class="upper"></div>
            <div id="ruinmap-ariadne-deactivate" data-trans="close-ariadne" class="upper hideme"></div>
            <div id="ruinmap-wrapper-close" data-trans="close" class="upper"></div>
            <div id="ruinInfoBox" class="upper">
                <div id="ruinTileBox">
                    <h3 data-trans="hallways"></h3>
                    <div class="ruinOption ruinTile tile-7" tile="7" data-transtitle="hallway-news"><div class="ruinOptionKey" data-trans="intersec">5</div></div>
                    <div class="ruinOption ruinTile tile-5" tile="5" data-transtitle="hallway-ns"><div class="ruinOptionKey" data-trans="corri_ns">6</div></div>
                    <div class="ruinOption ruinTile tile-6" tile="6" data-transtitle="hallway-ew"><div class="ruinOptionKey" data-trans="corri_we">7</div></div>
                    <div class="ruinOption ruinTile tile-0" tile="0" data-transtitle="hallway-none"><div class="ruinOptionKey" data-trans="no_corri">8</div></div>
                    <div class="ruinOption ruinTile tile-12" tile="12" data-transtitle="hallway-new"><div class="ruinOptionKey" data-trans="tsec_nos">t</div></div>
                    <div class="ruinOption ruinTile tile-13" tile="13" data-transtitle="hallway-nes"><div class="ruinOptionKey" data-trans="tsec_now">z</div></div>
                    <div class="ruinOption ruinTile tile-14" tile="14" data-transtitle="hallway-ews"><div class="ruinOptionKey" data-trans="tsec_non">u</div></div>
                    <div class="ruinOption ruinTile tile-15" tile="15" data-transtitle="hallway-nws"><div class="ruinOptionKey" data-trans="tsec_noe">i</div></div>
                    <div class="ruinOption ruinTile tile-8" tile="8" data-transtitle="hallway-ne"><div class="ruinOptionKey" data-trans="cornerne">g</div></div>
                    <div class="ruinOption ruinTile tile-9" tile="9" data-transtitle="hallway-es"><div class="ruinOptionKey" data-trans="cornerse">h</div></div>
                    <div class="ruinOption ruinTile tile-10" tile="10" data-transtitle="hallway-ws"><div class="ruinOptionKey" data-trans="cornersw">j</div></div>
                    <div class="ruinOption ruinTile tile-11" tile="11" data-transtitle="hallway-nw"><div class="ruinOptionKey" data-trans="cornernw">k</div></div>
                    <div class="ruinOption ruinTile tile-1" tile="1" data-transtitle="hallway-n"><div class="ruinOptionKey" data-trans="deadendn">b</div></div>
                    <div class="ruinOption ruinTile tile-2" tile="2" data-transtitle="hallway-e"><div class="ruinOptionKey" data-trans="deadende">n</div></div>
                    <div class="ruinOption ruinTile tile-3" tile="3" data-transtitle="hallway-s"><div class="ruinOptionKey" data-trans="deadends">m</div></div>
                    <div class="ruinOption ruinTile tile-4" tile="4" data-transtitle="hallway-w"><div class="ruinOptionKey" data-trans="deadendw">,</div></div>
                </div>
                <div id="ruinDoorLockBox">
                    <h3 data-trans="doorlocks"></h3>
                    <div class="ruinOption ruinDoorLock doorlock-1" doorlock="1" data-transtitle="doorlock-locked"><div class="ruinOptionKey" data-trans="doorlock">a</div></div>
                    <div class="ruinOption ruinDoorLock doorlock-2" doorlock="2" data-transtitle="doorlock-open"><div class="ruinOptionKey" data-trans="dooropen">s</div></div>
                    <div class="ruinOption ruinDoorLock doorlock-0" doorlock="0" data-transtitle="doorlock-none"><div class="ruinOptionKey" data-trans="no_doors">d</div></div>
                    <div class="ruinOption ruinFloorSwitch" data-transtitle="switch-floor"><div class="ruinOptionKey" data-trans="floornxt">f</div></div>
                    <div class="ruinOption ruinDoorLock doorlock-3" doorlock="3" data-transtitle="doorlock-bottle"><div class="ruinOptionKey" data-trans="key_open">y</div></div>
                    <div class="ruinOption ruinDoorLock doorlock-4" doorlock="4" data-transtitle="doorlock-bump"><div class="ruinOptionKey" data-trans="key_bump">x</div></div>
                    <div class="ruinOption ruinDoorLock doorlock-5" doorlock="5" data-transtitle="doorlock-magnet"><div class="ruinOptionKey" data-trans="key_magn">c</div></div>
                    <div class="ruinOption ruinDoorLock doorlock-6" doorlock="6" data-transtitle="doorlock-stairs"><div class="ruinOptionKey" data-trans="elevator">v</div></div>
                </div>
                <div id="ruinZombieBox">
                    <h3 data-trans="zombies"></h3>
                    <div class="ruinOption ruinZombie zombie-1" zombie="1" data-transtitle="zombies-1"><div class="ruinOptionKey" data-trans="zombie_1">1</div></div>
                    <div class="ruinOption ruinZombie zombie-2" zombie="2" data-transtitle="zombies-2"><div class="ruinOptionKey" data-trans="zombie_2">2</div></div>
                    <div class="ruinOption ruinZombie zombie-3" zombie="3" data-transtitle="zombies-3"><div class="ruinOptionKey" data-trans="zombie_3">3</div></div>
                    <div class="ruinOption ruinZombie zombie-4" zombie="4" data-transtitle="zombies-4"><div class="ruinOptionKey" data-trans="zombie_4">4</div></div>
                    <div class="ruinOption ruinZombie zombie-0" zombie="0" data-transtitle="zombies-0"><div class="ruinOptionKey" data-trans="zombie_0">0</div></div>
                    <div class="ruinOption ruinZombie zombie--1" zombie="-1" data-transtitle="zombies--1"><div class="ruinOptionKey" data-trans="nozombie">ß</div></div>
                </div>
                <div id="ruinHoverInfo"></div>
            </div>
            <div id="ruinInfoBox2" class="upper">
                <h2 id="ruinName"></h2>
                <h4 id="ruinCoords">Floor: <span class="ruinFloor"></span> - X: <span class="ruinCoordsX"></span> - Y: <span
                            class="ruinCoordsY"></span></h4>
                <div id="ruinZone"></div>
                <div id="ruinCommentBox">
                    <h3 data-trans="comment"></h3>
                    <textarea id="ruinComment"></textarea>
                    <div id="ruinComment-save" data-trans="save"></div>
                </div>
                <div id="keyOverlay">
                    <label for="keyOverlayCheckbox"><input type="checkbox" id="keyOverlayCheckbox" checked="checked"><span data-trans="key-overlay"></span></label>
                </div>
            </div>
            <div id="ruinAriadne" class="upper hideme">
                <h2 data-trans="ariadne-thread"></h2>
                <div id="ariadneUndo" class="ariadne-button"><span data-trans="ariadne-undo"></span><div class="keyIndicator" data-trans="key-ind-backspace"></div></div>
                <div id="ariadneSave" class="ariadne-button"><span data-trans="ariadne-save"></span><div class="keyIndicator" data-trans="key-ind-enter"></div></div>
                <div id="ariadneClear" class="ariadne-button"><span data-trans="ariadne-clear"></span><div class="keyIndicator" data-trans="key-ind-delete"></div></div>
                <div id="ariadneReset" class="ariadne-button"><span data-trans="ariadne-reset"></span><div class="keyIndicator" data-trans="key-ind-pos1"></div></div>
            </div>
            <div id="upperruinlabel" data-trans="ground-floor"></div>
            <div id="upperruinmap" class="clearfix"></div>
            <div id="lowerruinlabel" data-trans="basement-floor"></div>
            <div id="lowerruinmap" class="clearfix"></div>
        </div>
    </div>
</div>

<div id="item-selector" class="hideme">
    <h3 data-trans="search"></h3><input id="item-selector-search" class="itemsearch">
    <div id="item-selector-Rsc" class="item-selector-cat clearfix"><h3 data-trans="item-rsc"></h3></div>
    <div id="item-selector-Furniture" class="item-selector-cat clearfix"><h3 data-trans="item-furniture"></h3></div>
    <div id="item-selector-Weapon" class="item-selector-cat clearfix"><h3 data-trans="item-weapon"></h3></div>
    <div id="item-selector-Box" class="item-selector-cat clearfix"><h3 data-trans="item-box"></h3></div>
    <div id="item-selector-Armor" class="item-selector-cat clearfix"><h3 data-trans="item-armor"></h3></div>
    <div id="item-selector-Drug" class="item-selector-cat clearfix"><h3 data-trans="item-drug"></h3></div>
    <div id="item-selector-Food" class="item-selector-cat clearfix"><h3 data-trans="item-food"></h3></div>
    <div id="item-selector-Misc" class="item-selector-cat clearfix"><h3 data-trans="item-misc"></h3></div>
</div>
<div id="building-selector" class="hideme">
<h3 id="headline-building-selector-suggested" data-trans="building-selector-probable"></h3>
    <span id="building-selector-suggested"></span>
    <h3 id="headline-building-selector-all" data-trans="building-selector-all"></h3>
    <span id="building-selector-all"></span>
</div>
<div id="eruin-popup" class="popup-container">
  <div class="popup-content">
    <div class="popup-close">❌</div>
    <p data-trans="which-eruin"></p>
    <button id="eruin-option1" class="popup-button"></button>
    <button id="eruin-option2" class="popup-button"></button>
  </div>
</div>
<script type="text/javascript">

    //var fm_url = 'https://fatamorgana.md26.eu/';
    // was data.fatamorgana.md26.eu previously. changed to subdomain because of issues.
    // was changes from hardcoded URL to auto-detect
    var fm_url = window.location.origin + '/';

    var radiusCounter = 1;
    var mapHoverActive = false;
    var mapHoverMoved = false;
    var data = <?=$gamemap?>;
</script>

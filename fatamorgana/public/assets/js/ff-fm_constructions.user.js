// ==UserScript==
// @name    Fata Morgana Constructions Updater
// @namespace   https://fatamorgana.md26.eu/*
// @description Construction Updater
// @include   http://www.dieverdammten.de/*
// @include   http://dieverdammten.de/*
// @version     0.04
// ==/UserScript==

function addJQuery() {
  var script = document.createElement("script");
  script.setAttribute("src", "//code.jquery.com/jquery-latest.min.js");
  script.addEventListener('load', function() {
    var script = document.createElement("script");
    script.setAttribute("src", "//fatamorgana.md26.eu/js/fm_constructions.js?r="+Math.random());
    document.body.appendChild(script);
  }, false);
  document.body.appendChild(script);
}

addJQuery();

// translation service
(function () {
  var _dict = {};
  var _notFoundError = false;

  window.jsIn = {
    addDict: function (dict) {
      for (var key in dict) {
        _dict[key] = dict[key];
      }
    },

    showNotFoundError: function (show) {
      if (typeof show !== "undefined") {
        _notFoundError = show === true;
      }
      return _notFoundError;
    },

    translate: function (text, param) {
      var newText = _dict[text] || (!_notFoundError ? text : (function () {
        throw "No entry found for key {" + text + "}";
      })());

      if (param) {
        for (var i = 0, maxi = param.length; i < maxi; i++) {
          var regex = new RegExp("{%" + (i + 1) + "}", "g");
          newText = newText.replace(regex, param[i]);
        }
      }

      return newText;
    }
  };

  window.__ = jsIn.translate;
})();
// Set to true for debugging purposes. If you want to disable debug mode, set this to false.
let debug = false;
// Global var necessary for expeditions (Maybe we can put this in localstorage so it wouold not be global vars?)
var newRouteX = new Array();
var newRouteY = new Array();
var newRouteCurrentX = null;
var newRouteCurrentY = null;
var currentX = null;
var currentY = null;
var startingPoint = true;
var isHorizontal = true;
var isIncreasing = true;
var editRouteId = null;
var expeditionPoint = 0;
var ruinEditorKeys = 0;
var ariadne = 0;
var ariadnePath = [];
var ariadneSavedPath = [];
var citidots = [];
var currentPanel = 'map';
var switchingPanels = 0;

// fm code

function escapeSelector(selector) {
  return selector.replaceAll(/[!"#$%&'()*+,.\/:;<=>?@[\\\]^`{|}~ ]/g, "\\$&");
}

function datetimeformat(timestamp) {
  var monat = new Array(
    __("Januar"),
    __("Februar"),
    __("März"),
    __("April"),
    __("Mai"),
    __("Juni"),
    __("Juli"),
    __("August"),
    __("September"),
    __("Oktober"),
    __("November"),
    __("Dezember")
  );

  var timezone = "Europe/Berlin";
  if (localStorage.getItem("fm_setting_localtime") == 1) {
    timezone = moment.tz.guess();
  }

  var ampm = false;
  if (localStorage.getItem("fm_setting_ampmtime") == 1) {
    ampm = true;
  }

  var date = new Date(timestamp * 1000);
  var momentDate = moment.tz(date, timezone);

  var formats = {
    de:
      __("um") +
      " <b>" +
      (momentDate.hour() < 10
        ? "0" + momentDate.hour()
        : momentDate.hour()) +
      ":" +
      (momentDate.minute() < 10
        ? "0" + momentDate.minute()
        : momentDate.minute()) +
      "</b> " +
      "(" +
      __("am") +
      " " +
      momentDate.date() +
      ". " +
      monat[momentDate.month()] +
      " " +
      momentDate.year() +
      ") ",
    en:
      __("um") +
      " <b>" +
      (momentDate.hour() == 0 && ampm
        ? "12"
        : momentDate.hour() > 12 && ampm
          ? momentDate.hour() - 12
          : momentDate.hour() < 10 && !ampm
            ? "0" + momentDate.hour()
            : momentDate.hour()) +
      ":" +
      (momentDate.minute() < 10
        ? "0" + momentDate.minute()
        : momentDate.minute()) +
      (ampm ? (momentDate.hour() < 12 ? " AM" : " PM") : "") +
      "</b> " +
      "(" +
      __("am") +
      " " +
      monat[momentDate.month()] +
      " " +
      momentDate.date() +
      (momentDate.date() % 10 == 1 && momentDate.date() != 11
        ? "st"
        : momentDate.date() % 10 == 2 && momentDate.date() != 12
        ? "nd"
        : momentDate.date() % 10 == 3 && momentDate.date() != 13
        ? "rd"
        : "th") +
      ", " +
      momentDate.year() +
      ") ",
    fr:
      __("um") +
      " <b>" +
      (momentDate.hour() < 10
        ? "0" + momentDate.hour()
        : momentDate.hour()) +
      "H" +
      (momentDate.minute() < 10
        ? "0" + momentDate.minute()
        : momentDate.minute()) +
      "</b> " +
      "(" +
      __("am") +
      " " +
      momentDate.date() +
      " " +
      monat[momentDate.month()] +
      " " +
      momentDate.year() +
      ") ",
    es:
      __("um") +
      " <b>" +
      (momentDate.hour() < 10
        ? "0" + momentDate.hour()
        : momentDate.hour()) +
      ":" +
      (momentDate.minute() < 10
        ? "0" + momentDate.minute()
        : momentDate.minute()) +
      "</b> " +
      "(" +
      __("am") +
      " " +
      momentDate.date() +
      " de " +
      monat[momentDate.month()] +
      " del " +
      momentDate.year() +
      ") ",
  };

  return formats[data.langcode];
}

function changePanel(panel) {
  if (debug) { console.log({ '_function': 'changePanel', 'from': currentPanel, 'to': panel }); }
  if (switchingPanels === 0) {
    const activePanel = document.querySelector('.panel.active');

    if (activePanel && panel !== currentPanel) {
      switchingPanels = 1;
      // Add the 'fall' class to trigger the falling animation
      activePanel.classList.add('fall');

      // After the animation ends, remove 'active' and 'fall' classes
      activePanel.addEventListener('animationend', () => {
        activePanel.classList.remove('active', 'fall');
        const tabsLinks = activePanel.querySelectorAll('.tablinks');
        Array.from(tabsLinks).forEach(tabLink => {  // Removes all tab links as active
          tabLink.classList.remove('active');
        });
        const tabs = activePanel.querySelectorAll('.tabcontent');
        Array.from(tabs).forEach(tab => {  // Hide all tabs
          tab.style.display = "none";
        });

        switchingPanels = 0;
      }, { once: true }); // Use 'once' to ensure the listener is removed after it triggers
    } else if (panel === 'bps') {
      // When we are already inside the tabs, we just switch to the first one
      const tabsLinks = activePanel.querySelectorAll('.tablinks');
      Array.from(tabsLinks).slice(1).forEach(tabLink => {  // Removes all tab links as active
        tabLink.classList.remove('active');
      });
      const tabs = activePanel.querySelectorAll('.tabcontent');
      Array.from(tabs).slice(1).forEach(tab => {  // Hide all tabs
        tab.style.display = "none";
      });
    }

    const nextPanel = document.querySelector('.panel#' + panel + '-panel');
    if (nextPanel) {
      nextPanel.classList.add('active');
      currentPanel = panel;
    }

    // Do the switches.
    document.querySelectorAll(".panel-switch").forEach(function(el) { el.classList.remove('active-panel'); });
    document.querySelector("." + panel + "-panel-switch").classList.add("active-panel");

    // Reset panel states.
    disableAriadneEditor();
    if (panel !== 'eruin') {
      ruinEditorKeys = 0;
    }
    else {
      ruinEditorKeys = 1;
    }
  }
}

function updateMapRulers(mapZone) {
  var rx = mapZone.getAttribute("rx");
  var ry = mapZone.getAttribute("ry");

  // Remove "hoverzone" class from all elements with the class "mapruler"
  document.querySelectorAll("#map .mapruler").forEach(function(element) {
      element.classList.remove("hoverzone");
  });

  // Add "hoverzone" class to elements with the class "mapruler" and specific classes based on rx and ry
  document.querySelectorAll("#map .mapruler.ruler_y" + ry + ", #map .mapruler.ruler_x" + rx).forEach(function(element) {
      element.classList.add("hoverzone");
  });
}

function updateLowerRuinMapRulers(mapZone) {
  var rx = mapZone.getAttribute("rx");
  var ry = mapZone.getAttribute("ry");

  document.querySelectorAll("#lowerruinmap .mapruler").forEach(function(element) {
      element.classList.remove("hoverzone");
  });

  document.querySelectorAll("#lowerruinmap .mapruler.ruler_y" + ry + ", #lowerruinmap .mapruler.ruler_x" + rx).forEach(function(element) {
      element.classList.add("hoverzone");
  });
}

function updateUpperRuinMapRulers(mapZone) {
  var rx = mapZone.getAttribute("rx");
  var ry = mapZone.getAttribute("ry");

  document.querySelectorAll("#upperruinmap .mapruler").forEach(function(element) {
      element.classList.remove("hoverzone");
  });

  document.querySelectorAll("#upperruinmap .mapruler.ruler_y" + ry + ", #upperruinmap .mapruler.ruler_x" + rx).forEach(function(element) {
      element.classList.add("hoverzone");
  });
}

function updateFloor(floor) {
  document.querySelector("#ruinCoords .ruinFloor").textContent = floor;
}

function resetNewExp() {
  editRouteIdRestore = null;
  if (editRouteId !== null) {
      editRouteIdRestore = editRouteId;
  }

  expeditionPoint = 0;

  document.querySelector("#editing-mode").classList.add("hideme");
  document.querySelector("#cancel-editing").classList.add("hideme");
  document.querySelector("#duplicate-route").classList.add("hideme");
  document.querySelector("#exp-form").classList.add("hideme");

  editRouteId = null;
  document.querySelector("input[name=expe-type]").disabled = false;
  document.querySelector("#\\30\\.1").innerHTML = __("Open route editor");
  cleanNewExp();

  if (editRouteIdRestore !== null) {
      document.getElementById(editRouteIdRestore).click();
      if (!document.getElementById(editRouteIdRestore).classList.contains("active-option")) {
          document.getElementById(editRouteIdRestore).click();
      }
      editRouteIdRestore = null;
  }
}

function randomizeExpColor() {
  document.querySelector('input[name="expe-color"]').value = "#" + Math.floor(Math.random() * 16777215).toString(16);
}

function cleanNewExp(resetInputs = true) {
  document.querySelectorAll(".new-exp-svg").forEach(element => element.remove());
  newRouteCurrentX = null;
  newRouteCurrentY = null;

  expeditionPoint = 0;

  newRouteX = [];
  newRouteY = [];
  document.getElementById("expedition-ap").textContent = 0;

  if (resetInputs) {
      document.querySelector("input[name=expe-name]").value = "";
      document.getElementById("exp-day").value = parseInt(data.system.day);
      document.getElementById("exp-public").checked = true;
      randomizeExpColor();
  }
}

function undoExpLastPoint() {
  if (newRouteX.length === 0) {
      return;
  } else if (newRouteX.length === 1) {
      cleanNewExp(false);
      return;
  }

  let length = 0;

  // Get coordinates of the last point and remove values from newRoute.
  const lastPointX = newRouteX.pop();
  const lastPointY = newRouteY.pop();

  // Get coordinates of second last point without removing it from newRoute.
  newRouteCurrentX = newRouteX.slice(-1);
  newRouteCurrentY = newRouteY.slice(-1);

  // Set current coords to the last point.
  currentX = lastPointX;
  currentY = lastPointY;

  expeditionPoint--;

  while (parseInt(currentY) !== parseInt(newRouteCurrentY)) {
      document.querySelectorAll(`#map #x${currentX - data.tx}y${data.ty - currentY} .new-exp-svg.expeditionPoint-${expeditionPoint}`).forEach(function (el) { el.remove(); });
      if (parseInt(newRouteCurrentY) > parseInt(currentY)) {
          currentY++;
          length++;
      } else {
          currentY--;
          length++;
      }
  }

  while (parseInt(currentX) !== parseInt(newRouteCurrentX)) {
      document.querySelectorAll(`#map #x${currentX - data.tx}y${data.ty - currentY} .new-exp-svg.expeditionPoint-${expeditionPoint}`).forEach(function (el) { el.remove(); });
      if (parseInt(newRouteCurrentX) > parseInt(currentX)) {
          currentX++;
          length++;
      } else {
          currentX--;
          length++;
      }
  }

  document.querySelectorAll(`#map #x${currentX - data.tx}y${data.ty - currentY} .new-exp-svg.undo.expeditionPoint-${expeditionPoint}`).forEach(function (el) { el.remove(); });

  currentX = null;
  currentY = null;

  document.getElementById("expedition-ap").textContent = parseInt(document.getElementById("expedition-ap").textContent) - length;
}

function drawExpToPoint(ax, ay, routeId, color, newRoute = false) {
  let svgClass = "exp-svg " + routeId;
  let length = 0;
  if (newRoute) {
    currentX = newRouteCurrentX;
    currentY = newRouteCurrentY;
    svgClass = "new-exp-svg expeditionPoint-" + expeditionPoint;
    expeditionPoint++;
  }

  if (currentX === null || currentY === null) {
    let zoneId = "routex" + (ax - data.tx) + "y" + (data.ty - ay);
    let svg = $("#map #" + zoneId);
    if (svg.length === 0) {
      $("#map #x" + (ax - data.tx) + "y" + (data.ty - ay)).append(
        '<svg xmlns="http://www.w3.org/2000/svg" id="' +
          zoneId +
          '" fill="currentColor" aria-hidden="true" viewBox="0 0 32 32"></svg>'
      );
      svg = $("#map #" + zoneId);
    }
    const circle = document.createElementNS(
      "http://www.w3.org/2000/svg",
      "circle"
    );
    circle.setAttribute("class", svgClass);
    circle.setAttribute("r", "3");
    circle.setAttribute("fill", color);
    circle.setAttribute("cx", "16");
    circle.setAttribute("cy", "16");
    svg[0].appendChild(circle);
    currentX = ax;
    currentY = ay;
  } else {
    // Horizontal move
    let firstHorizontal = true;
    let firstVertical = true;
    while (true) {
      let zoneId = "routex" + (currentX - data.tx) + "y" + (data.ty - currentY);
      let svg = $("#map #" + zoneId);
      if (svg.length === 0) {
        $("#map #x" + (currentX - data.tx) + "y" + (data.ty - currentY)).append(
          '<svg xmlns="http://www.w3.org/2000/svg" id="' +
            zoneId +
            '" fill="currentColor" aria-hidden="true" viewBox="0 0 32 32"></svg>'
        );
        svg = $("#map #" + zoneId);
      }
      if (parseInt(ax) > parseInt(currentX)) {
        if (startingPoint || !isHorizontal) {
          // First move or changing direction
          let path = document.createElementNS(
            "http://www.w3.org/2000/svg",
            "path"
          );
          path.setAttribute("fill", "none");
          path.setAttribute("stroke", color);
          path.setAttribute("d", "M 16,16 h 16");
          path.setAttribute("stroke-width", "4");
          path.setAttribute("class", svgClass + " undo");
          svg[0].appendChild(path);
          startingPoint = false;
        } else {
          // continue horizontal
          let path = document.createElementNS(
            "http://www.w3.org/2000/svg",
            "path"
          );
          path.setAttribute("fill", "none");
          path.setAttribute("stroke", color);
          path.setAttribute("d", "M 0,16 h 32");
          path.setAttribute("stroke-width", "4");
          path.setAttribute("class", svgClass);
          svg[0].appendChild(path);
        }
        isIncreasing = true;
        currentX++;
        length++;
      } else if (parseInt(ax) < parseInt(currentX)) {
        if (startingPoint || !isHorizontal) {
          // First move or changing direction
          let path = document.createElementNS(
            "http://www.w3.org/2000/svg",
            "path"
          );
          path.setAttribute("fill", "none");
          path.setAttribute("stroke", color);
          path.setAttribute("d", "M 16,16 h -16");
          path.setAttribute("stroke-width", "4");
          path.setAttribute("class", svgClass + " undo");
          svg[0].appendChild(path);
          startingPoint = false;
        } else {
          let path = document.createElementNS(
            "http://www.w3.org/2000/svg",
            "path"
          );
          path.setAttribute("fill", "none");
          path.setAttribute("stroke", color);
          path.setAttribute("d", "M 0,16 h 32");
          path.setAttribute("stroke-width", "4");
          path.setAttribute("class", svgClass);
          svg[0].appendChild(path);
        }
        isIncreasing = false;
        currentX--;
        length++;
      } else if (parseInt(ax) === parseInt(currentX)) {
        // Last click
        startingPoint = true;
        let arrow = document.createElementNS(
          "http://www.w3.org/2000/svg",
          "defs"
        );
        let arrowMarker = document.createElementNS(
          "http://www.w3.org/2000/svg",
          "marker"
        );
        arrowMarker.setAttribute("id", "arrow" + routeId);
        for (let classItem of svgClass.split(" ")) {
          arrowMarker.classList.add("class", classItem);
          arrow.classList.add("class", classItem);
        }
        arrowMarker.setAttribute("viewBox", "0 0 10 10");
        arrowMarker.setAttribute("refX", "5");
        arrowMarker.setAttribute("refY", "5");
        arrowMarker.setAttribute("markerWidth", "4");
        arrowMarker.setAttribute("markerHeight", "4");
        arrowMarker.setAttribute("fill", color);
        arrowMarker.setAttribute("orient", "auto");
        let arrowMarkerPath = document.createElementNS(
          "http://www.w3.org/2000/svg",
          "path"
        );
        arrowMarkerPath.setAttribute("d", "M 0 0 L 7 5 L 0 10 z");
        arrowMarker.appendChild(arrowMarkerPath);
        arrow.appendChild(arrowMarker);
        svg[0].appendChild(arrow);

        if (firstHorizontal) {
          // there are no horizontal moves in this click
          break;
        } else if (isIncreasing) {
          // Put arrows
          let path = document.createElementNS(
            "http://www.w3.org/2000/svg",
            "path"
          );
          path.setAttribute("fill", "none");
          path.setAttribute("stroke", color);
          path.setAttribute("d", "M 0,16 h 16");
          path.setAttribute("stroke-width", "4");
          path.setAttribute("class", svgClass);
          path.setAttribute(
            "marker-end",
            "url(#arrow" + escapeSelector(routeId) + ")"
          );
          svg[0].appendChild(path);

          startingPoint = false;
        } else {
          let path = document.createElementNS(
            "http://www.w3.org/2000/svg",
            "path"
          );
          path.setAttribute("fill", "none");
          path.setAttribute("stroke", color);
          path.setAttribute("d", "M 32,16 h -16");
          path.setAttribute("stroke-width", "4");
          path.setAttribute("class", svgClass);
          path.setAttribute(
            "marker-end",
            "url(#arrow" + escapeSelector(routeId) + ")"
          );
          svg[0].appendChild(path);
          startingPoint = false;
        }
        break; // We can't increment currentX here, so we have to break the while
      }
      firstHorizontal = false;
      isHorizontal = true;
    }

    // vertical move
    while (true) {
      let zoneId = "routex" + (currentX - data.tx) + "y" + (data.ty - currentY);
      let svg = $("#map #" + zoneId);
      if (svg.length === 0) {
        $("#map #x" + (currentX - data.tx) + "y" + (data.ty - currentY)).append(
          '<svg xmlns="http://www.w3.org/2000/svg" id="' +
            zoneId +
            '" fill="currentColor" aria-hidden="true" viewBox="0 0 32 32"></svg>'
        );
        svg = $("#map #" + zoneId);
      }
      if (parseInt(ay) > parseInt(currentY)) {
        if (startingPoint || isHorizontal) {
          // First move or changing direction
          let path = document.createElementNS(
            "http://www.w3.org/2000/svg",
            "path"
          );
          path.setAttribute("fill", "none");
          path.setAttribute("stroke", color);
          path.setAttribute("d", "M 16,16 v 16");
          path.setAttribute("stroke-width", "4");
          path.setAttribute("class", svgClass + " undo");
          svg[0].appendChild(path);
          startingPoint = false;
        } else {
          // continue vertical
          let path = document.createElementNS(
            "http://www.w3.org/2000/svg",
            "path"
          );
          path.setAttribute("fill", "none");
          path.setAttribute("stroke", color);
          path.setAttribute("d", "M 16,0 v 32");
          path.setAttribute("stroke-width", "4");
          path.setAttribute("class", svgClass);
          svg[0].appendChild(path);
        }
        isIncreasing = true;
        currentY++;
        length++;
      } else if (parseInt(ay) < parseInt(currentY)) {
        if (startingPoint || isHorizontal) {
          // First move or changing direction
          let path = document.createElementNS(
            "http://www.w3.org/2000/svg",
            "path"
          );
          path.setAttribute("fill", "none");
          path.setAttribute("stroke", color);
          path.setAttribute("d", "M 16,16 v -16");
          path.setAttribute("stroke-width", "4");
          path.setAttribute("class", svgClass + " undo");
          svg[0].appendChild(path);
          startingPoint = false;
        } else {
          let path = document.createElementNS(
            "http://www.w3.org/2000/svg",
            "path"
          );
          path.setAttribute("fill", "none");
          path.setAttribute("stroke", color);
          path.setAttribute("d", "M 16,0 v 32");
          path.setAttribute("stroke-width", "4");
          path.setAttribute("class", svgClass);
          svg[0].appendChild(path);
        }
        isIncreasing = false;
        currentY--;
        length++;
      } else if (parseInt(ay) === parseInt(currentY)) {
        // Last click
        startingPoint = true;
        let arrow = document.createElementNS(
          "http://www.w3.org/2000/svg",
          "defs"
        );
        let arrowMarker = document.createElementNS(
          "http://www.w3.org/2000/svg",
          "marker"
        );
        arrowMarker.setAttribute("id", "arrow" + routeId);
        for (let classItem of svgClass.split(" ")) {
          arrowMarker.classList.add("class", classItem);
          arrow.classList.add("class", classItem);
        }
        arrowMarker.setAttribute("viewBox", "0 0 10 10");
        arrowMarker.setAttribute("refX", "5");
        arrowMarker.setAttribute("refY", "5");
        arrowMarker.setAttribute("markerWidth", "4");
        arrowMarker.setAttribute("markerHeight", "4");
        arrowMarker.setAttribute("fill", color);
        arrowMarker.setAttribute("orient", "auto");
        let arrowMarkerPath = document.createElementNS(
          "http://www.w3.org/2000/svg",
          "path"
        );
        arrowMarkerPath.setAttribute("d", "M 0 0 L 7 5 L 0 10 z");
        arrowMarker.appendChild(arrowMarkerPath);
        arrow.appendChild(arrowMarker);
        svg[0].appendChild(arrow);

        if (firstVertical) {
          // there are no vertical moves in this click
          break;
        } else if (isIncreasing) {
          // Put arrows
          let path = document.createElementNS(
            "http://www.w3.org/2000/svg",
            "path"
          );
          path.setAttribute("fill", "none");
          path.setAttribute("stroke", color);
          path.setAttribute("d", "M 16,0 v 16");
          path.setAttribute("stroke-width", "4");
          path.setAttribute("class", svgClass);
          path.setAttribute(
            "marker-end",
            "url(#arrow" + escapeSelector(routeId) + ")"
          );
          svg[0].appendChild(path);
          startingPoint = false;
        } else {
          let path = document.createElementNS(
            "http://www.w3.org/2000/svg",
            "path"
          );
          path.setAttribute("fill", "none");
          path.setAttribute("stroke", color);
          path.setAttribute("d", "M 16,32 v -16");
          path.setAttribute("stroke-width", "4");
          path.setAttribute("class", svgClass);
          path.setAttribute(
            "marker-end",
            "url(#arrow" + escapeSelector(routeId) + ")"
          );
          svg[0].appendChild(path);
          startingPoint = false;
        }
        break; // We can't increment currentY here, so we have to break the while
      }
      firstVertical = false;
      isHorizontal = false;
    }
  }

  // Updating tracking of the new route and ap
  if (newRoute) {
    newRouteCurrentX = currentX;
    newRouteCurrentY = currentY;
    $("#expedition-ap").text(parseInt($("#expedition-ap").text()) + length);
  }
}

function addZoneToExp(mapZone) {
  var ax = mapZone.getAttribute("ax");
  var ay = mapZone.getAttribute("ay");
  newRouteX.push(ax);
  newRouteY.push(ay);
  drawExpToPoint(ax, ay, "newRoute", document.querySelector('input[name="expe-color"]').value, true);
}

function selectZone(mapZone) {
  document.querySelector("#item-selector").classList.add("hideme");
  document.querySelector("#building-selector").classList.add("hideme");
  var rx = mapZone.getAttribute("rx");
  var ry = mapZone.getAttribute("ry");
  document.querySelectorAll("#map .mapzone, #map .mapruler").forEach(function (zone) {
    zone.classList.remove("selectedZone");
  });
  document.querySelectorAll(
    "#map #x" + rx + "y" + ry + ",#map .mapruler.ruler_y" + ry + ",#map .mapruler.ruler_x" + rx
  ).forEach(function (zone) {
    zone.classList.add("selectedZone");
  });
  var ax = mapZone.getAttribute("ax");
  var ay = mapZone.getAttribute("ay");
  updateBox(ax, ay, rx, ry);

  document.querySelectorAll("li.box-tab").forEach(function (tab) {
    tab.classList.remove("active");
  });
  document.querySelector('li.box-tab[ref="zone-info zone-info-extras"]').classList.add("active");
  document.querySelectorAll("div.box-tab-content").forEach(function (content) {
    content.classList.add("hideme");
  });
  document.querySelector("div#zone-info").classList.remove("hideme");
  document.querySelector("div#zone-info-extras").classList.remove("hideme");
}

function boxScoutZombies(x, y) {
  var zom = 0;
  if (
    data.scout !== undefined &&
    data.scout["y" + y + "x" + x] !== undefined
  ) {
    zom = data.scout["y" + y + "x" + x]["zom"];
  }

  var pElement = document.createElement("p");
  pElement.classList.add("zone-scoutzombies");

  var imgElement = document.createElement("img");
  imgElement.src = "/assets/css/img/item_vest_on.gif";
  pElement.appendChild(imgElement);

  var spanElement1 = document.createElement("span");
  spanElement1.textContent = " ~ ";
  pElement.appendChild(spanElement1);

  if (data.spy === undefined) {
    var spanElement2 = document.createElement("span");
    spanElement2.classList.add("hideme", "scoutzombie-count-change", "plus");
    spanElement2.textContent = "◄ ";
    pElement.appendChild(spanElement2);
  }

  var spanElement3 = document.createElement("span");
  spanElement3.id = "scoutzombie-count-display";
  spanElement3.textContent = zom + " ";
  pElement.appendChild(spanElement3);

  if (data.spy === undefined) {
    var spanElement4 = document.createElement("span");
    spanElement4.classList.add("hideme", "scoutzombie-count-change", "minus");
    spanElement4.textContent = " ►";
    pElement.appendChild(spanElement4);
  }

  var spanElement5 = document.createElement("span");
  spanElement5.textContent = ' ' + __("Zombies") + " ";
  pElement.appendChild(spanElement5);

  if (data.spy === undefined) {
    var aElement1 = document.createElement("a");
    aElement1.classList.add("toggle-scout-zombie-update", "interactive");
    aElement1.href = "/update/scoutzombies";
    aElement1.textContent = __("aktualisieren");
    pElement.appendChild(aElement1);

    var aElement2 = document.createElement("a");
    aElement2.classList.add("hideme", "interactive", "plus", "ajaxlink");
    aElement2.href = "/update/scoutzombies";
    aElement2.id = "UPDATE-SCOUTZOMBIES";
    aElement2.setAttribute("ocx", x);
    aElement2.setAttribute("ocy", y);
    aElement2.textContent = __("speichern");
    pElement.appendChild(aElement2);

    pElement.appendChild(document.createTextNode(" "));

    var aElement3 = document.createElement("a");
    aElement3.classList.add("hideme", "interactive", "minus");
    aElement3.id = "CANCEL-UPDATE-SCOUTZOMBIES";
    aElement3.textContent = __("cancel");
    pElement.appendChild(aElement3);
  }

  return pElement;
}

function boxScoutUpdate(x, y) {
  if (
    data.scout !== undefined &&
    data.scout["y" + y + "x" + x] !== undefined &&
    data.scout["y" + y + "x" + x]["updatedOnDay"] !== undefined
  ) {/*
    var pElement = document.createElement("p");
    pElement.classList.add("zone-lastupdate");

    pElement.appendChild(document.createTextNode(__("Letzte Aktualisierung") + ' ' + __("am") + ' '));
    var bElement1 = document.createElement("b");
    bElement1.appendChild(document.createTextNode(__("Tag") + ' ' + data.scout["y" + y + "x" + x]["updatedOnDay"]));
    pElement.appendChild(bElement1);
    pElement.appendChild(document.createTextNode(" " + datetimeformat(data.scout["y" + y + "x" + x]["updatedOn"]) + " " + __("durch") + " "));
    var bElement2 = document.createElement("b");
    bElement2.appendChild(document.createTextNode(data.scout["y" + y + "x" + x]["updatedBy"]));
    pElement.appendChild(bElement2);

    return pElement;
    */
   // replaced this with jquery function due to inline <b></b>; quickfix

    var updText = __("Letzte Aktualisierung") + " ";
    if (data.scout["y" + y + "x" + x]["updatedOnDay"] !== undefined) {
      updText +=
        __("am") + " <b>" + __("Tag") + " " + data.scout["y" + y + "x" + x]["updatedOnDay"] + "</b> ";
    }
    if (data.scout["y" + y + "x" + x]["updatedOn"] !== undefined) {
      updText += datetimeformat(data.scout["y" + y + "x" + x]["updatedOn"]);
    }
    if (data.scout["y" + y + "x" + x]["updatedBy"]!== undefined && data.scout["y" + y + "x" + x]["updatedBy"] !== "") {
      updText += __("durch") + " <b>" + data.scout["y" + y + "x" + x]["updatedBy"] + "</b>";
    }
    pElement = document.createElement("p");
    pElement.classList.add("zone-lastupdate");
    pElement.innerHTML = updText;
    return pElement;

  }
  return false;
}

function boxSouls(x, y) {
  let lostsoul;
  if (
  data.souls != undefined &&
  data.souls['y' + y] !== undefined &&
  data.souls['y' + y]['x' + x] !== undefined &&
  data.souls['y' + y]['x' + x]["lostsoul"] !== undefined) {
    lostsoul = data.souls['y' + y]['x' + x]["lostsoul"];
  }
  else {
    lostsoul = 0;
  }
  if (lostsoul === 0) {
    pElement = document.createElement("p");
    pElement.classList.add("zone-soul", "zone-soul-0");
    pElement.appendChild(document.createTextNode(__("Keine verirrte Seele") + ' '));
    if (data.spy === undefined) {
      aElement = document.createElement("a");
      aElement.classList.add("interactive", "plus", "ajaxlink");
      aElement.setAttribute("ocx", x);
      aElement.setAttribute("ocy", y);
      aElement.textContent = __("hinzufügen");
      aElement.id = "ZONE-ADDSOUL";
      aElement.setAttribute("href", "/update/zone/addsoul");
      pElement.appendChild(aElement);
    }
  } else {
    pElement = document.createElement("p");
    pElement.classList.add("zone-soul", "zone-soul-1");

    imgElement1 = document.createElement("img");
    imgElement1.src = "/assets/css/img/tag_12.gif";
    pElement.appendChild(imgElement1);
    spanElement1 = document.createElement("span");
    spanElement1.classList.add("plus");
    spanElement1.textContent = __("Hier spukt es.") + ' ';
    pElement.appendChild(spanElement1);
    if (data.spy === undefined) {
      aElement = document.createElement("a");
      aElement.classList.add("interactive", "minus", "ajaxlink");
      aElement.setAttribute("ocx", x);
      aElement.setAttribute("ocy", y);
      aElement.textContent = __("Seele entfernen");
      aElement.id = "ZONE-DELSOUL";
      aElement.setAttribute("href", "/update/zone/delsoul");
      pElement.appendChild(aElement);
    }
  }
  return pElement;
}

function updateBox(x, y, rx, ry) {
  var zone;
  rx = parseInt(rx);
  ry = parseInt(ry);
  document.getElementById("item-selector").classList.add("hideme");
  delete data.saveItems;

  // Initialize the box(es).
  var infoBox = document.getElementById("box-content").querySelector("#zone-info");
  var infoBoxExtra = document.getElementById("box-extra-content").querySelector("#zone-info-extras");
  infoBox.innerHTML = "";
  infoBoxExtra.innerHTML = "";

  // H3 zone header.
  var h3Element = document.createElement("h3");
  if (!(rx === 0 && ry === 0)) {
    h3Element.textContent = __("Zone") + " [" + rx + "|" + ry + "]";
    infoBox.appendChild(h3Element);

    var pElement = document.createElement("p");
    pElement.textContent = __("Entfernung") + ": " + calcAP(rx, ry) + __("AP") + ", " + calcKM(rx, ry) + __("km");
    infoBox.appendChild(pElement);
  } else {
    h3Element.textContent = __("city") + " [" + rx + "|" + ry + "]";

    var pElement = document.createElement("p");
    pElement.classList.add("water");
    pElement.appendChild(document.createTextNode(__("water") + ": "));
    pElement.appendChild(document.createElement("br"));
    pElement.appendChild(document.createTextNode(data.water.well + " " + __("in_well")));
    pElement.appendChild(document.createElement("br"));
    pElement.appendChild(document.createTextNode(data.water.bank + " " + __("in_bank")));
    infoBox.appendChild(pElement);

    if (data.door === 1) {
      var pElement = document.createElement("p");
      pElement.classList.add("minus");
      pElement.textContent = __("gate") + ": " + __("open");
      infoBox.appendChild(pElement);
    } else {
      var pElement = document.createElement("p");
      pElement.classList.add("plus");
      pElement.textContent = __("gate") + ": " + __("closed");
      infoBox.appendChild(pElement);
    }
  }

  // Check and load zone data.
  if (
    data.map["y" + y] !== undefined &&
    data.map["y" + y]["x" + x] !== undefined
  ) {
    if ((zone = data.map["y" + y]["x" + x])) {
      // Check building.
      if (zone.building !== undefined) {
        if (!isExplorable(zone.building.type) && zone.building.type !== -1) {
          // This is normal building, no e-ruin, not buried.
          if (zone.building.dried !== undefined && zone.building.dried === 1) {
            // Building is empty.
            var pElement = document.createElement("p");
            pElement.classList.add("zone-building");

            var strongElement = document.createElement("strong");
            strongElement.textContent = data.buildings[zone.building.type][data.langcode];
            pElement.appendChild(strongElement);

            var brElement = document.createElement("br");
            pElement.appendChild(brElement);

            var spanElement = document.createElement("span");
            spanElement.classList.add("minus");
            spanElement.textContent = __("Gebäude ist leer.") + ' ';
            pElement.appendChild(spanElement);

            if (data.spy === undefined) {
              pElement.appendChild(document.createTextNode(" "));
              var aElement = document.createElement("a");
              aElement.classList.add("interactive", "plus", "ajaxlink");
              aElement.href = "/update/building/regenerate";
              aElement.id = "BUILDING-REGENERATE";
              aElement.setAttribute("ocx", x);
              aElement.setAttribute("ocy", y);
              aElement.textContent = __("regenerieren");
              pElement.appendChild(aElement);
            }

            infoBox.appendChild(pElement);
          } else {
            // Building is not empty.
            var pElement = document.createElement("p");
            pElement.classList.add("zone-building");

            var strongElement = document.createElement("strong");
            strongElement.textContent = data.buildings[zone.building.type][data.langcode];
            pElement.appendChild(strongElement);

            var brElement = document.createElement("br");
            pElement.appendChild(brElement);

            var spanElement = document.createElement("span");
            spanElement.classList.add("plus");
            spanElement.textContent = __("Gebäude ist durchsuchbar.") + ' ';
            pElement.appendChild(spanElement);

            if (data.spy === undefined) {
              pElement.appendChild(document.createTextNode(" "));
              var aElement = document.createElement("a");
              aElement.classList.add("interactive", "minus", "ajaxlink");
              aElement.href = "/update/building/deplete";
              aElement.id = "BUILDING-DEPLETE";
              aElement.setAttribute("ocx", x);
              aElement.setAttribute("ocy", y);
              aElement.textContent = __("leeren");
              pElement.appendChild(aElement);
            }

            infoBox.appendChild(pElement);
          }

          // Blueprint information.
          if (
            zone.building.blueprint !== undefined &&
            zone.building.blueprint === 1
          ) {
            // Blueprint is available.
            var pElement = document.createElement("p");
            pElement.classList.add("zone-building");

            var spanElement = document.createElement("span");
            spanElement.classList.add("minus");
            spanElement.textContent = __("Blaupause wurde gefunden.") + ' ';
            pElement.appendChild(spanElement);

            if (data.spy === undefined) {
              var aElement = document.createElement("a");
              aElement.classList.add("interactive", "plus", "ajaxlink");
              aElement.href = "/update/blueprint/available";
              aElement.id = "BLUEPRINT-AVAILABLE";
              aElement.setAttribute("ocx", x);
              aElement.setAttribute("ocy", y);
              aElement.textContent = __("ist noch erhältlich") + ' ';
              pElement.appendChild(aElement);
            }

            infoBox.appendChild(pElement);
          } else {
            // Blueprint is not available.
            var pElement = document.createElement("p");
            pElement.classList.add("zone-building");

            var spanElement = document.createElement("span");
            spanElement.classList.add("plus");
            spanElement.textContent = __("Blaupause ist noch erhältlich.") + ' ';
            pElement.appendChild(spanElement);

            if (data.spy === undefined) {
              var aElement = document.createElement("a");
              aElement.classList.add("interactive", "minus", "ajaxlink");
              aElement.href = "/update/blueprint/found";
              aElement.id = "BLUEPRINT-FOUND";
              aElement.setAttribute("ocx", x);
              aElement.setAttribute("ocy", y);
              aElement.textContent = __("bereits gefunden") + ' ';
              pElement.appendChild(aElement);
            }

            infoBox.appendChild(pElement);
          }
        } else if (isExplorable(zone.building.type)) {
          // Explorable ruin.
          var pElement = document.createElement("p");
          pElement.classList.add("zone-building");

          var strongElement = document.createElement("strong");
          strongElement.textContent = data.buildings[zone.building.type][data.langcode];
          pElement.appendChild(strongElement);

          var brElement = document.createElement("br");
          pElement.appendChild(brElement);

          var spanElement = document.createElement("span");
          spanElement.textContent = __("e-ruin") + ": ";
          pElement.appendChild(spanElement);

          var aElement = document.createElement("a");
          aElement.classList.add("interactive", "explore");
          aElement.href = "#";
          aElement.id = "ENTER-BUILDING";
          aElement.setAttribute("ocx", x);
          aElement.setAttribute("ocy", y);
          aElement.textContent = __("enter-e-ruin");
          pElement.appendChild(aElement);

          infoBox.appendChild(pElement);
        } else {
          // Buried building.
          var pElement = document.createElement("p");
          pElement.classList.add("zone-building");

          var strongElement = document.createElement("strong");
          strongElement.textContent = data.buildings[zone.building.type][data.langcode];
          pElement.appendChild(strongElement);

          var brElement = document.createElement("br");
          pElement.appendChild(brElement);

          var spanElement = document.createElement("span");
          spanElement.classList.add("minus");
          spanElement.textContent = zone.building.dig + " ";

          var imgElement = document.createElement("img");
          imgElement.src = "/assets/css/img/h_dig.gif";
          spanElement.appendChild(imgElement);

          pElement.appendChild(spanElement);

          infoBox.appendChild(pElement);

          // Scout's building guess.
          var pElement = document.createElement("p");
          pElement.classList.add("scout-building");

          if (zone.building.guess !== undefined) {
            var spanElement = document.createElement("span");
            spanElement.textContent = __("Aufklärer") + ": " + data.buildings[zone.building.guess][data.langcode];
            pElement.appendChild(spanElement);

            var brElement = document.createElement("br");
            pElement.appendChild(brElement);
          }

          if (data.spy === undefined) {
            var aElement = document.createElement("a");
            aElement.classList.add("interactive", "toggle-building-update");
            aElement.href = "#";
            aElement.id = "GUESS-BUILDING";
            aElement.setAttribute("ocx", x);
            aElement.setAttribute("ocy", y);
            aElement.textContent = (zone.building.guess !== undefined) ? __("Vermutung ändern") : __("Vermutung angeben");
            pElement.appendChild(aElement);

            if (zone.building.guess !== undefined) {
              pElement.appendChild(document.createTextNode(" "));
              var deleteGuessButton = document.createElement("a");
              deleteGuessButton.classList.add("interactive", "minus", "delete-building-update");
              deleteGuessButton.href = "#";
              deleteGuessButton.id = "GUESS-BUILDING";
              deleteGuessButton.setAttribute("ocx", x);
              deleteGuessButton.setAttribute("ocy", y);
              deleteGuessButton.textContent = __("Vermutung löschen") ;
              pElement.appendChild(deleteGuessButton);
            }
        }
          infoBox.appendChild(pElement);
        }
      }

      // Regeneration status.
      if (zone.dried !== undefined && zone.dried === 1) {
        // Zone empty.
        var pElement = document.createElement("p");
        pElement.classList.add("zone-status", "zone-status-empty");

        var imgElement = document.createElement("img");
        imgElement.src = "/assets/css/img/tag_5.gif";
        pElement.appendChild(imgElement);

        var spanElement = document.createElement("span");
        spanElement.classList.add("minus");
        spanElement.textContent = __("Zone ist leer.");
        pElement.appendChild(spanElement);

        if (data.spy === undefined) {
          pElement.appendChild(document.createTextNode(" "));
          var aElement = document.createElement("a");
          aElement.classList.add("interactive", "plus", "ajaxlink");
          aElement.href = "/update/zone/regenerate";
          aElement.id = "ZONE-REGENERATE";
          aElement.setAttribute("ocx", x);
          aElement.setAttribute("ocy", y);
          aElement.textContent = __("regenerieren");
          pElement.appendChild(aElement);
        }

        infoBox.appendChild(pElement);

        // Check if any storms were happening since last dried update.
        if (
          zone.updatedOn !== undefined &&
          (data.system.gametype === "uhu" ||
            (data.system.gametype === "far" && calcKM(rx, ry) >= 3))
        ) {
          if (data.stormstamp !== undefined) {
            var stormcounter = 0;
            for (var i = 1; i <= data.system.day; i++) {
              if (data.stormstamp[i] !== undefined) {
                if (data.stormstamp[i] > zone.updatedOn) {
                  if (calcGD(rx, ry) === data.stormnames[data.storm[i] - 1]) {
                    stormcounter++;
                  }
                }
              }
            }
            if (stormcounter > 0) {
              var pElement = document.createElement("p");
              pElement.classList.add("zone-status", "zone-storm-status", "hideme");

              var imgElement = document.createElement("img");
              imgElement.src = "/assets/css/img/storm.gif";
              pElement.appendChild(imgElement);

              var spanElement = document.createElement("span");
              spanElement.textContent = stormcounter + " " + (stormcounter === 1 ? __("storm") : __("storms")) + " " + __("since-last-update");
              pElement.appendChild(spanElement);

              infoBox.appendChild(pElement);
            }
          }
        }
      } else if (!(rx === 0 && ry === 0)) {
        // Zone can still be searched.
        var pElement = document.createElement("p");
        pElement.classList.add("zone-status", "zone-status-full");

        var imgElement = document.createElement("img");
        imgElement.src = "/assets/css/img/small_gather.gif";
        pElement.appendChild(imgElement);

        var spanElement = document.createElement("span");
        spanElement.classList.add("plus");
        spanElement.textContent = __("Zone ist regeneriert.") + ' ';
        pElement.appendChild(spanElement);

        if (data.spy === undefined) {
          var aElement = document.createElement("a");
          aElement.classList.add("interactive", "minus", "ajaxlink");
          aElement.href = "/update/zone/deplete";
          aElement.id = "ZONE-DEPLETE";
          aElement.setAttribute("ocx", x);
          aElement.setAttribute("ocy", y);
          aElement.textContent = __("leeren");
          pElement.appendChild(aElement);
        }

        infoBox.appendChild(pElement);
      }

      // Zombies status.
      if (!(rx === 0 && ry === 0)) {
        var diff = 0, days = 0;
        if (zone.updatedOn !== undefined && zone.nvt !== 0) {
          var date = new Date(zone.updatedOn * 1000);
          var dateN = new Date();
          dateN.setHours(23);
          dateN.setMinutes(59);
          dateN.setSeconds(59);
          if (date.getDate() !== dateN.getDate) {
            days = Math.floor((dateN.getTime() - date.getTime()) / 86400000);
            diff = days;
            if (zone.building !== undefined) {
              diff = 2 * days;
            }
          }
        }
        // Temporary disabling zombie estimation.
        diff = 0;

        // Current zombie count.
        var pElement = document.createElement("p");
        pElement.classList.add("zone-zombies");

        if (data.spy === undefined) {
          var spanElement1 = document.createElement("span");
          spanElement1.classList.add("hideme", "zombie-count-change", "plus");
          spanElement1.textContent = "◄ ";
          pElement.appendChild(spanElement1);
        }

        var spanElement2 = document.createElement("span");
        spanElement2.id = "zombie-count-display";
        spanElement2.textContent = zone.z !== undefined ? parseInt(zone.z) + diff : diff;
        pElement.appendChild(spanElement2);

        if (data.spy === undefined) {
          var spanElement3 = document.createElement("span");
          spanElement3.classList.add("hideme", "zombie-count-change", "minus");
          spanElement3.textContent = " ►";
          pElement.appendChild(spanElement3);
        }

        var spanElement4 = document.createElement("span");
        spanElement4.textContent = ' ' + __("Zombie") + (zone.z && zone.z === 1 ? "" : "s") + ' ';
        pElement.appendChild(spanElement4);

        if (data.spy === undefined) {
          var aElement1 = document.createElement("a");
          aElement1.classList.add("toggle-zombie-update", "interactive");
          aElement1.href = "/update/zombies";
          aElement1.textContent = __("aktualisieren");
          pElement.appendChild(aElement1);

          var aElement2 = document.createElement("a");
          aElement2.classList.add("hideme", "interactive", "plus", "ajaxlink");
          aElement2.href = "/update/zombies";
          aElement2.id = "UPDATE-ZOMBIES";
          aElement2.setAttribute("ocx", x);
          aElement2.setAttribute("ocy", y);
          aElement2.textContent = __("speichern");
          pElement.appendChild(aElement2);

          pElement.appendChild(document.createTextNode(" "));

          var aElement3 = document.createElement("a");
          aElement3.classList.add("hideme", "interactive", "minus");
          aElement3.id = "CANCEL-UPDATE-ZOMBIES";
          aElement3.textContent = __("cancel");
          pElement.appendChild(aElement3);
        }

        infoBox.appendChild(pElement);

        if (data.spy === undefined) {
          var divElement = document.createElement("div");
          divElement.classList.add("hideme");

          var inputElement = document.createElement("input");
          inputElement.type = "hidden";
          inputElement.value = zone.z !== undefined ? parseInt(zone.z) + diff : diff;
          inputElement.id = "zombie-count-input";

          divElement.appendChild(inputElement);

          infoBox.appendChild(divElement);
        }

        // Zombie count scouts.
        if (data.spy === undefined) {
          var pElement = document.createElement("p");
          pElement.classList.add("zone-zombies-scout");

          var emElement = document.createElement("em");

          if (days > 0 && zone.z !== undefined) {
            if (days === 1 && zone.z === 1) {
              emElement.textContent = __("Vor 1 Tag war es ein Zombie.");
            } else if (days === 1 && zone.z > 1) {
              emElement.textContent = __("Vor 1 Tag waren es {%1} Zombies.", [zone.z]);
            } else if (days > 1 && zone.z === 1) {
              emElement.textContent = __("Vor {%1} Tagen war es 1 Zombie.", [days]);
            } else if (days > 1 && zone.z > 1) {
              emElement.textContent = __("Vor {%1} Tagen waren es {%2} Zombies.", [days, zone.z]);
            }
          } else if (days > 0 && zone.z === undefined) {
            if (days === 1) {
              emElement.textContent = __("Vor 1 Tag waren es 0 Zombies.");
            } else if (days > 1) {
              emElement.textContent = __("Vor {%1} Tagen waren es 0 Zombies.", [days]);
            }
          }

          pElement.appendChild(emElement);
          infoBox.appendChild(divElement);
        }

        // Extra information (scouts).
        infoBoxExtra.appendChild(boxScoutZombies(x, y));

        var zom = 0;
        if (
          data.scout !== undefined &&
          data.scout["y" + y + "x" + x] !== undefined
        ) {
          zom = data.scout["y" + y + "x" + x]["zom"];
        }

        if (data.spy === undefined) {
          var divElement = document.createElement("div");
          divElement.classList.add("hideme");
          var inputElement = document.createElement("input");
          inputElement.type = "hidden";
          inputElement.value = zom ? zom : 0;

          inputElement.id = "scoutzombie-count-input";
          divElement.appendChild(inputElement);
          infoBoxExtra.appendChild(divElement);
        }

        let updateBoxData = boxScoutUpdate(x, y);
        if (updateBoxData) {
          infoBoxExtra.appendChild(updateBoxData);
        }

        // Maximum zombies seen.
        var pElement = document.createElement("p");
        pElement.classList.add("zone-maxzombies");
        pElement.appendChild(document.createTextNode(__("Maximal") + ' '));

        spanElement1 = document.createElement("span");
        spanElement1.classList.add("hideme", "maxzombie-count-change", "plus");
        spanElement1.textContent = "◄ ";
        pElement.appendChild(spanElement1);

        spanElement2 = document.createElement("span");
        spanElement2.id = "maxzombie-count-display"
        spanElement2.textContent = (zone.zm !== -1 ? parseInt(zone.zm) : 0);
        pElement.appendChild(spanElement2);

        spanElement3 = document.createElement("span");
        spanElement3.classList.add("hideme", "maxzombie-count-change", "minus");
        spanElement3.textContent = " ►";
        pElement.appendChild(spanElement3);

        pElement.appendChild(document.createTextNode(' ' + __("Zombie") + (zone.zm && zone.zm === 1 ? "" : "s") + " " + __("heute") + ' '));

        if (data.spy === undefined) {
          aElement1 = document.createElement("a");
          aElement1.classList.add("toggle-maxzombie-update", "interactive");
          aElement1.href = "/update/maxzombies";
          aElement1.textContent = __("aktualisieren");
          pElement.appendChild(aElement1);

          divElement = document.createElement("div");
          divElement.classList.add("hideme", "maxzombie-notice");
          divElement.textContent = __("maxzombiesnotice");
          pElement.appendChild(divElement);

          aElement2 = document.createElement("a");
          aElement2.classList.add("hideme", "interactive", "plus", "ajaxlink");
          aElement2.href = "/update/maxzombies";
          aElement2.setAttribute("ocx", x);
          aElement2.setAttribute("ocy", y);
          aElement2.textContent = __("speichern");
          aElement2.id = "UPDATE-MAXZOMBIES";
          pElement.appendChild(aElement2);

          pElement.appendChild(document.createTextNode(" "));

          var aElement3 = document.createElement("a");
          aElement3.classList.add("hideme", "interactive", "minus");
          aElement3.id = "CANCEL-UPDATE-MAXZOMBIES";
          aElement3.textContent = __("cancel");
          pElement.appendChild(aElement3);

          divElement1 = document.createElement("div");
          divElement1.classList.add("hideme");
          inputElement = document.createElement("input");
          inputElement.type = "hidden";
          inputElement.value = zone.zm ? parseInt(zone.zm) : 0;
          inputElement.id = "maxzombie-count-input";
          divElement1.appendChild(inputElement);
          pElement.appendChild(divElement1);
        }
      }
      infoBox.appendChild(pElement);

      // Let the player update their location in CHAOS mode.
      if (data.system.chaos === 1 && data.spy === undefined) {
        pElement = document.createElement("p");
        pElement.classList.add("zone-chaos-citizen");

        imgElement1 = document.createElement("img");
        imgElement1.src = "/assets/css/img/small_arma.gif";
        imgElement2 = document.createElement("img");
        imgElement2.src = "/assets/css/img/small_human.gif";
        pElement.appendChild(imgElement1);
        pElement.appendChild(imgElement2);

        spanElement1 = document.createElement("span");
        spanElement1.classList.add("minus");
        spanElement1.textContent = __("CHAOS") + ": ";
        pElement.appendChild(spanElement1);

        aElement1 = document.createElement("a");
        aElement1.classList.add("interactive", "plus", "ajaxlink");
        aElement1.href = "/update/citizen";
        aElement1.id = "CITIZEN-LOCATION";
        aElement1.setAttribute("ocx", x);
        aElement1.setAttribute("ocy", y);
        aElement1.textContent = __("Ich bin HIER!");
        pElement.appendChild(aElement1);
        infoBox.appendChild(pElement);
      }

      // List citizens in the current zone.
      if (zone["citizens"] !== undefined) {
        var cc = 0, ci = 0;
        var clist = document.createElement("p");
        clist.classList.add("zone-citizens");
        var ilist = document.createElement("p");
        ilist.classList.add("city-citizens");

        for (cid in zone["citizens"]) {
          cc++;
          spanElement1 = document.createElement("span");
          spanElement1.textContent = zone["citizens"][cid]["name"];
          spanElement1.classList.add("zone-citizen", "zone-citizen-" + zone["citizens"][cid]["job"]);

          if (rx === 0 && ry === 0 && data["citizens"][cid]["out"] === 0) {
            ci++;
            ilist.append(spanElement1);
            ilist.append(document.createTextNode(" "));
          } else {
            clist.append(spanElement1);
            clist.append(document.createTextNode(" "));
          }
        }
        if (ci > 0) {
          ilist.prepend(document.createTextNode(ci + " " + __("Bürger") + " (" + __("inside") + "): "));
          infoBox.append(ilist);
        }
        if (cc > ci) {
          clist.prepend(document.createTextNode((cc - ci) + " " + __("Bürger") + (rx === 0 && ry === 0 ? " (" + __("atgate") + ")" : "") + ": "));
          infoBox.append(clist);
        }
      }

      // Lost souls in the current zone.
      if (!(rx === 0 && ry === 0)) {
        infoBox.append(boxSouls(x, y));
      }

      // Items in the current zone.
      if (!(rx === 0 && ry === 0)) {
        pElement = document.createElement("p");
        pElement.classList.add("zone-items-header");

        spanElement = document.createElement("span");
        spanElement.textContent = __("Gegenstände") + ' ';
        pElement.appendChild(spanElement);

        if (data.spy === undefined) {
          aElement = document.createElement("a");
          aElement.classList.add("open-item-update", "interactive");
          aElement.textContent = __("aktualisieren");
          aElement.id = "ZONE-ITEMS";
          aElement.setAttribute("href", "/update/items");
          aElement.setAttribute("ocx", x);
          aElement.setAttribute("ocy", y);
          pElement.appendChild(aElement);

          pElement.appendChild(document.createTextNode(" "));

          aElement1 = document.createElement("a");
          aElement1.classList.add("close-item-selector", "interactive", "minus", "hideme");
          aElement1.textContent = __("schließen");
          aElement1.setAttribute("href", "close");
          pElement.appendChild(aElement1);

          pElement.appendChild(document.createTextNode(" "));

          aElement2 = document.createElement("a");
          aElement2.classList.add("ajaxsave", "interactive", "plus", "hideme");
          aElement2.textContent = __("speichern");
          aElement2.setAttribute("href", "/update/items");
          aElement2.setAttribute("ocx", x);
          aElement2.setAttribute("ocy", y);
          pElement.appendChild(aElement2);
        }

        divElement = document.createElement("div");
        divElement.classList.add("zone-items", "clearfix");
        divElement.id = "zi_x" + rx + "_y" + ry;

        if (zone["items"] !== undefined) {
          let item, itemDisplay;
          for (i in zone.items) {
            item = zone.items[i];
            itemDisplay = createItemDisplay(item.id, item.count, item.broken);
            if (itemDisplay) {
              divElement.appendChild(itemDisplay);
            }
          }
        }

        // Sort items by name.
        const items = Array.from(divElement);
        items.sort((a, b) => {
          let itemA = $(a).attr("title").toString();
          let itemB = $(b).attr("title").toString();
          let firstcharA = itemA.substring(0, 1);
          if (
            firstcharA === "«" ||
            firstcharA === '"' ||
            firstcharA === "“" ||
            firstcharA === "'"
          ) {
            return 1;
          }
          var firstcharB = itemB.substring(0, 1);
          if (
            firstcharB === "«" ||
            firstcharB === '"' ||
            firstcharB === "“" ||
            firstcharB === "'"
          ) {
            return -1;
          }
          return itemA.localeCompare(itemB);
        });

        // Append the sorted items back to the list
        items.forEach(item => divElement.appendChild(item));

        pElement.appendChild(divElement);


        if (data.spy === undefined) {

        }
        infoBox.append(pElement);
      }

      // update status
      if (!(rx === 0 && ry === 0)) {
        var updText = __("Letzte Aktualisierung") + " ";
        if (zone.updatedOnDay !== undefined) {
          updText +=
            __("am") + " <b>" + __("Tag") + " " + zone.updatedOnDay + "</b> ";
        }
        if (zone.updatedOn !== undefined) {
          updText += datetimeformat(zone.updatedOn);
        }
        if (zone.updatedBy !== undefined && zone.updatedBy !== "") {
          updText += __("durch") + " <b>" + zone.updatedBy + "</b>";
        }
        pElement = document.createElement("p");
        pElement.classList.add("zone-lastupdate");
        pElement.innerHTML = updText;
        infoBox.append(pElement);
      }
    }
  } else {
    // zombies scout
    if (data.spy === undefined) {
      if (
        data.scout !== undefined &&
        data.scout["y" + y + "x" + x] !== undefined
      ) {
        var zom = data.scout["y" + y + "x" + x]["zom"];
        var pbl = data.scout["y" + y + "x" + x]["pbl"];
        var upo = data.scout["y" + y + "x" + x]["updatedOn"];
        var upb = data.scout["y" + y + "x" + x]["updatedBy"];
      } else {
        var zom = 0;
        var pbl = 0;
        var upo = 0;
        var upb = 0;
      }
      infoBoxExtra.appendChild(boxScoutZombies(x, y));

      var divElement = document.createElement("div");
      divElement.classList.add("hideme");
      var inputElement = document.createElement("input");
      inputElement.type = "hidden";
      inputElement.value = zom ? zom : 0;
      inputElement.id = "scoutzombie-count-input";
      divElement.appendChild(inputElement);
      infoBoxExtra.appendChild(divElement);

      let updateBoxData = boxScoutUpdate(x, y);
      if (updateBoxData) {
        infoBoxExtra.appendChild(updateBoxData);
      }

      // Possible building?
      pElement = document.createElement("p");
      pElement.classList.add("zone-status");
      imgElement = document.createElement("img");
      spanElement = document.createElement("span");
      aElement = document.createElement("a");
      aElement.classList.add("interactive", "ajaxlink");
      if (pbl === 0) {
        imgElement.src = "/assets/css/img/tag_5.gif";
        spanElement.classList.add("minus");
        spanElement.textContent = __("Gebäude unwahrscheinlich") + ' ';
        aElement.classList.add("plus");
        aElement.setAttribute("href", "/update/zone/buildingprobable");
        aElement.setAttribute("id", "BUILDING-PROBABLE");
        aElement.setAttribute("ocx", x);
        aElement.setAttribute("ocy", y);
        aElement.textContent = __("vermutlich doch");
      } else {
        imgElement.src = "/assets/css/img/small_gather.gif";
        spanElement.classList.add("plus");
        spanElement.textContent = __("vermutlich ein Gebäude") + ' ';
        aElement.classList.add("minus");
        aElement.setAttribute("href", "/update/zone/buildingnotprobable");
        aElement.setAttribute("id", "BUILDING-NOTPROBABLE");
        aElement.setAttribute("ocx", x);
        aElement.setAttribute("ocy", y);
        aElement.textContent = __("eher nicht");
      }
      pElement.appendChild(imgElement);
      pElement.appendChild(spanElement);
      pElement.appendChild(aElement);
      infoBox.append(pElement);
    } else {
      infoBoxExtra.appendChild(boxScoutZombies(x, y));

      let updateBoxData = boxScoutUpdate(x, y);
      if (updateBoxData) {
        infoBoxExtra.appendChild(updateBoxData);
      }
    }

    // Lost souls.
    if (debug) { console.log('Checking lostsoul for nyv zone', rx, ry); }
    if (!(rx === 0 && ry === 0)) {
      infoBox.append(boxSouls(x, y));
    }
  }
}

function popZone(i, j) {
  var rx = j - data["tx"];
  var ry = data["ty"] - i;

  //we don't know why but the wait makes the turn successful
  setTimeout(wait1ms, 1);
  function wait1ms() {
    $("#x" + rx + "y" + ry).addClass("popZone");
  }

  //wait the duration of the animation before removing the class
  setTimeout(wait250ms, 250);
  function wait250ms() {
    $("#x" + rx + "y" + ry).removeClass("popZone");
  }
}

function selectLowerRuinZone(mapZone) {
  var rx = mapZone.getAttribute("rx");
  var ry = mapZone.getAttribute("ry");
  data.crx = rx;
  data.cry = ry;
  data.floor = "lower";

  document.querySelectorAll("#upperruinmap .mapzone, #upperruinmap .mapruler").forEach(function (zone) {
    zone.classList.remove("selectedZone");
  });

  document.querySelectorAll("#lowerruinmap .mapzone, #lowerruinmap .mapruler").forEach(function (zone) {
    zone.classList.remove("selectedZone");
  });

  document.querySelectorAll(
    "#lowerruinmap #x" +
      rx +
      "y" +
      ry +
      ",#lowerruinmap .mapruler.ruler_y" +
      ry +
      ",#lowerruinmap .mapruler.ruler_x" +
      rx
  ).forEach(function (zone) {
    zone.classList.add("selectedZone");
  });

  document.getElementById("ruinZone").innerHTML =
    __("basement-floor") +
    ":" +
    " " +
    '<span id="ruinCX">' +
    rx +
    "</span>" +
    "|" +
    '<span id="ruinCY">' +
    ry +
    "</span>";

  document.getElementById("ruinComment").value = mapZone.getAttribute("comment");

  moveRuinBoxes("lower");
}

function selectUpperRuinZone(mapZone) {
  var rx = mapZone.getAttribute("rx");
  var ry = mapZone.getAttribute("ry");
  data.crx = rx;
  data.cry = ry;
  data.floor = "upper";

  document.querySelectorAll("#lowerruinmap .mapzone, #lowerruinmap .mapruler").forEach(function (zone) {
    zone.classList.remove("selectedZone");
  });

  document.querySelectorAll("#upperruinmap .mapzone, #upperruinmap .mapruler").forEach(function (zone) {
    zone.classList.remove("selectedZone");
  });

  document.querySelectorAll(
    "#upperruinmap #x" +
      rx +
      "y" +
      ry +
      ",#upperruinmap .mapruler.ruler_y" +
      ry +
      ",#upperruinmap .mapruler.ruler_x" +
      rx
  ).forEach(function (zone) {
    zone.classList.add("selectedZone");
  });

  var ax = mapZone.getAttribute("ax");
  var ay = mapZone.getAttribute("ay");

  document.getElementById("ruinZone").innerHTML =
    __("ground-floor") +
    ":" +
    " " +
    '<span id="ruinCX">' +
    rx +
    "</span>" +
    "|" +
    '<span id="ruinCY">' +
    ry +
    "</span>";

  document.getElementById("ruinComment").value = mapZone.getAttribute("comment");

  moveRuinBoxes("upper");
}

function moveRuinBoxes(level) {
  if (level === "lower") {
    document.querySelectorAll("#ruinmap-wrapper-grid > div").forEach(function (zone) {
      zone.classList.remove("upper");
      zone.classList.add("lower");
    });
  } else {
    document.querySelectorAll("#ruinmap-wrapper-grid > div").forEach(function (zone) {
      zone.classList.remove("lower");
      zone.classList.add("upper");
    });
  }
  //document.getElementById(level + "ruinmap").scrollIntoView(true);
}

function ariadneAddNode(level, mapZone) {
  if (ariadnePath.length === 0) {
    ariadnePath.push({ level: "upper", ax: 7, ay: 0 });
  }
  // Move editor box.
  moveRuinBoxes(level);
  // Get last point in path.
  var lastPoint = ariadnePath[ariadnePath.length - 1];
  // Get clicked zone's coordinates (absolute).
  let zax = parseInt(mapZone.getAttribute("ax"));
  let zay = parseInt(mapZone.getAttribute("ay"));
  // Don't allow first row in upper level except for entry.
  if (zay === 0 && zax !== 7) {
    ajaxInfoBad(__("ariadne-no-first-row"));
    return;
  }
  // Don't allow double entries.
  if (lastPoint.ax === zax && lastPoint.ay === zay) {
    ajaxInfoBad(__("ariadne-go-on"));
    return;
  }
  // Don't allow diagonal moves.
  if (lastPoint.ax !== zax && lastPoint.ay !== zay) {
    ajaxInfoBad(__("ariadne-orthogonally-only"));
    return;
  }
  // Add virtual point to path if floor is changed.
  if (
    lastPoint.level !== level &&
    !(lastPoint.ax === zax && lastPoint.ay === zay)
  ) {
    ariadnePath.push({
      level: level,
      ax: lastPoint.ax,
      ay: lastPoint.ay,
      virtual: true,
    });
  }
  ariadnePath.push({ level: level, ax: zax, ay: zay });
  redrawAriadneRoute(document.querySelector('.mapzone.selectedZone').getAttribute('ax'), document.querySelector('.mapzone.selectedZone').getAttribute('ay'));
}

function resetAriadneRoute() {
  ariadnePath = JSON.parse(JSON.stringify(ariadneSavedPath));
  $(".ariadne-path").remove();
  redrawAriadneRoute(document.querySelector('.mapzone.selectedZone').getAttribute('ax'), document.querySelector('.mapzone.selectedZone').getAttribute('ay'));
}

function clearAriadneRoute() {
  ariadnePath.length = 0;
  $(".ariadne-path").remove();
}

function undoAriadneRoute() {
  if (ariadnePath.length > 1) {
    ariadnePath.pop();
    if (ariadnePath[ariadnePath.length - 1].virtual === true) {
      ariadnePath.pop();
    }
    redrawAriadneRoute(document.querySelector('.mapzone.selectedZone').getAttribute('ax'), document.querySelector('.mapzone.selectedZone').getAttribute('ay'));
  } else {
    ajaxInfoBad(__("ariadne-no-undo"));
  }
}

function saveAriadneRoute() {
  // Prepare data for AJAX call.
  var ariadneJson = JSON.stringify(ariadnePath);
  let payload = "route=" + ariadneJson + "&x=" + document.querySelector('.mapzone.selectedZone').getAttribute('ax') + "&y=" + document.querySelector('.mapzone.selectedZone').getAttribute('ay');
  fire("/map/updateAriadne", payload, false, function() {
    ariadneSavedPath = JSON.parse(JSON.stringify(ariadnePath));
  });
}

function initAriadne(pathdata) {
  // Initialize Ariadne route.
  ariadneSavedPath =
    pathdata !== null &&
    pathdata !== undefined &&
    pathdata !== ""
      ? JSON.parse(pathdata)
      : [];
  ariadnePath = JSON.parse(JSON.stringify(ariadneSavedPath));
}

function redrawAriadneRoute(x, y) {
  if (debug) { console.log({ '_function': 'redrawAriadneRoute', 'x': x, 'y': y }); }
  let px = "x" + x;
  let py = "y" + y;

  if (!data.map[py][px] || !data.map[py][px]['building'] || !data.map[py][px]['building']['ruin']) {
    return;
  }

  if (currentPanel !== 'eruin') {
    return;
  }

  if (ariadnePath.length === 0) {
    initAriadne(data.map[py][px]["building"]["ruin"]["ariadne"]);
  }

  if (ariadnePath.length === 0) {
    return;
  }
  $(".ariadne-path").remove();
  var ariadneTiles = {
    // Starting points.
    xx: "M 16 14 A 1 1 0 0 0 16 18 A 1 1 0 0 0 16 14",
    xn: "M 18 0 V 12",
    xs: "M 14 32 V 20 A 1 1 0 0 0 14 16 A 1 1 0 0 0 14 20",
    xw: "M 0 14 H 12",
    xe: "M 20 18 H 32",
    // East incoming.
    // North incoming.
    nx: "M 14 0 V 8 H 16 L 14 12 L 12 8 H 14",
    nn: "M 14 0 V 12 A 1 1 0 0 0 18 12 V 0",
    ns: "M 14 0 V 32",
    nw: "M 14 0 V 10 A 4 4 0 0 1 10 14 H 0",
    ne: "M 14 0 V 12 A 8 8 0 0 0 20 18 H 32",
    // South incoming.
    sx: "M 18 32 V 24 H 16 L 18 20 L 20 24 H 18",
    ss: "M 18 32 V 20 A 1 1 0 0 0 14 20 V 32",
    sn: "M 18 32 V 0",
    sw: "M 18 32 V 20 A 8 8 0 0 0 12 14 H 0",
    se: "M 18 32 V 22 A 4 4 0 0 1 22 18 H 32",
    // West incoming.
    wx: "M 0 18 L 8 18 L 8 16 L 12 18 L 8 20 L 8 18",
    ww: "M 0 18 H 12 A 1 1 0 0 0 12 14 H 0",
    we: "M 0 18 H 32",
    wn: "M 0 18 H 12 A 8 8 0 0 0 18 12 V 0",
    ws: "M 0 18 H 10 A 4 4 0 0 1 14 22 V 32",
    // East incoming.
    ex: "M 32 14 H 24 V 16 L 20 14 L 24 12 V 14",
    ee: "M 32 14 H 20 A 1 1 0 0 0 20 18 H 32",
    ew: "M 32 14 H 0",
    en: "M 32 14 H 22 A 4 4 0 0 1 18 10 V 0",
    es: "M 32 14 H 20 A 8 8 0 0 0 14 20 V 32",
  };
  let cx = undefined;
  let cy = undefined;
  let cl = "upper";
  let step = 0;
  for (let i = 0; i < ariadnePath.length; i++) {
    let nx = ariadnePath[i].ax;
    let ny = ariadnePath[i].ay;
    let nl = ariadnePath[i].level;
    let from = "";
    let to = "";
    let fromSteps = 0;
    let toSteps = 0;
    if (cx === undefined || cy === undefined) {
      // Entry point, can only go south.
      cx = nx;
      cy = ny;
      addAriadneSvg(cl, cx, cy, step++, ariadneTiles["xs"]);
    } else {
      if (nx === cx && ny === cy) {
        // We change the floor level.
        from = "x";
        fromSteps = 1;
        cl = nl;
      } else if (nx === cx) {
        // Vertical line.
        if (ny < cy) {
          from = "s";
        } else {
          from = "n";
        }
        fromSteps = Math.abs(ny - cy);
      } else if (ny === cy) {
        // Horizontal line.
        if (nx < cx) {
          from = "e";
        } else {
          from = "w";
        }
        fromSteps = Math.abs(nx - cx);
      }
      if (i >= ariadnePath.length - 1) {
        // Last element, end path.
        to = "x";
        toSteps = 0;
      } else {
        let next = ariadnePath[i + 1];
        let fx = next.ax;
        let fy = next.ay;
        let fl = next.level;
        if (fl !== nl) {
          to = "x";
          toSteps = 0;
        } else if (fx === nx) {
          // Vertical line.
          if (fy < ny) {
            to = "n";
          } else {
            to = "s";
          }
          toSteps = Math.abs(fy - ny);
        } else if (fy === ny) {
          // Horizontal line.
          if (fx < nx) {
            to = "w";
          } else {
            to = "e";
          }
          toSteps = Math.abs(fx - nx);
        }
      }

      // Got from and to, let's create the path.
      // console.log(cx, cy, nx, ny, from, fromSteps, to, toSteps);
      let interPath = "";
      switch (from) {
        case "n":
          interPath = "ns";
          break;
        case "s":
          interPath = "sn";
          break;
        case "w":
          interPath = "we";
          break;
        case "e":
          interPath = "ew";
          break;
      }
      while (fromSteps > 1) {
        // Take one step at a time.
        switch (from) {
          case "n":
            cy++;
            break;
          case "s":
            cy--;
            break;
          case "w":
            cx++;
            break;
          case "e":
            cx--;
            break;
        }
        addAriadneSvg(cl, cx, cy, step++, ariadneTiles[interPath]);
        fromSteps--;
      }
      addAriadneSvg(nl, nx, ny, step++, ariadneTiles[from + to]);
      cx = nx;
      cy = ny;
      cl = nl;
      // End insert.
    }
  }
  // console.log(ariadnePath);
  // console.log('Ariadne\'s thread: ' + ariadnePath.length + ' steps, ' + step + ' zones to visit.');
}

function addAriadneSvg(level, cx, cy, step, svgpath) {
  // console.log(level, cx, cy, svgpath);
  if (svgpath === undefined) {
    return;
  }
  let zoneId = "x" + (cx - 7) + "y" + cy;
  let zoneSelector = "#" + level + "ruinmap #" + zoneId;
  let svg = $(zoneSelector);
  if (svg.length !== 0) {
    $(zoneSelector).append(
      '<svg xmlns="http://www.w3.org/2000/svg" ' +
        'id="ariadne-s' +
        step +
        "--" +
        zoneId +
        '"' +
        'class="ariadne-path" ' +
        'fill="currentColor" aria-hidden="true" viewBox="0 0 32 32"></svg>'
    );
    svg = $("#ariadne-s" + step + "--" + zoneId);
  }

  // First move or changing direction
  let path = document.createElementNS("http://www.w3.org/2000/svg", "path");
  path.setAttribute("fill", "none");
  path.setAttribute("stroke", "blue");
  path.setAttribute("d", svgpath);
  path.setAttribute("stroke-width", "2");
  svg[0].appendChild(path);
}

function createItemDisplay(id, count, broken) {
  if (data.items[id] !== undefined) {
    var raw_item = data.items[id];
    var classBroken = broken === 1 ? "broken" : id < 1 ? "broken" : "";
    var classDef = raw_item.category === "Armor" ? "defense" : "";

    var divElement = document.createElement("div");
    divElement.classList.add("zone-item", "click");
    if (classBroken !== '') {
      divElement.classList.add(classBroken);
    }
    if (classDef !== '') {
      divElement.classList.add(classDef);
    }
    divElement.setAttribute("state", "0");
    divElement.setAttribute("ref", raw_item.id);
    divElement.setAttribute("count", count);

    var imgElement = document.createElement("img");
    imgElement.src = data.system.icon + raw_item.image;
    divElement.title = raw_item.name[data.langcode] + " (ID: " + Math.abs(id) + ")";
    divElement.appendChild(imgElement);
    divElement.appendChild(document.createTextNode(' '));

    spanElement = document.createElement("span");
    spanElement.classList.add("count");
    spanElement.textContent = count;
    divElement.appendChild(spanElement);
    return divElement;
  } else {
    return false;
  }
}

function createItemDisplaySmall(id) {
  var raw_item = data.items[id];

  // Create the main div element
  var div = document.createElement("div");
  div.classList.add("select-item", "click");

  // Add the "broken" class if id is less than 1
  if (id < 1) {
    div.classList.add("broken");
  }

  // Add the "defense" class if the item's category is "Armor"
  if (raw_item.category === "Armor") {
    div.classList.add("defense");
  }

  // Set the ref attribute to the item ID
  div.setAttribute("ref", raw_item.id);

  // Create the img element
  var img = document.createElement("img");
  img.src = data.system.icon + raw_item.image;
  img.title = raw_item.name[data.langcode] + " (ID: " + Math.abs(id) + ")";

  // Append the img to the div
  div.appendChild(img);
  div.title = raw_item.name[data.langcode] + " (ID: " + Math.abs(id) + ")";

  // Return the created div element as HTML
  return div;
}


function createBuildingDisplaySmall(id) {
  var name = data.buildings[id][data.langcode];
  var div = document.createElement("div");
  div.classList.add("select-building", "click");
  div.setAttribute("ref", id);
  div.textContent = name;

  return div.outerHTML;
}

function calcAP(x, y) {
  return Math.abs(x) + Math.abs(y);
}

function calcKM(x, y) {
  return Math.round(Math.sqrt(x * x + y * y));
}

function calcGD(x, y) {
  if (x === 0 && y === 0) {
    return false;
  }
  if (x > Math.floor(y / 2) && y > Math.floor(x / 2)) {
    return "NE";
  }
  if (x > Math.floor(-y / 2) && -y > Math.floor(x / 2)) {
    return "SE";
  }
  if (-x > Math.floor(y / 2) && y > Math.floor(-x / 2)) {
    return "NW";
  }
  if (-x > Math.floor(-y / 2) && -y > Math.floor(-x / 2)) {
    return "SW";
  }
  if (Math.abs(x) > Math.abs(y)) {
    return x > 0 ? "E" : "W";
  }
  return y > 0 ? "N" : "S";
}

function calcGDN(x, y) {
  if (x === 0 && y === 0) {
    return false;
  }
  if (x > Math.floor(y / 2) && y > Math.floor(x / 2)) {
    return 1;
  }
  if (x > Math.floor(-y / 2) && -y > Math.floor(x / 2)) {
    return 3;
  }
  if (-x > Math.floor(y / 2) && y > Math.floor(-x / 2)) {
    return 7;
  }
  if (-x > Math.floor(-y / 2) && -y > Math.floor(-x / 2)) {
    return 5;
  }
  if (Math.abs(x) > Math.abs(y)) {
    return x > 0 ? 2 : 6;
  }
  return y > 0 ? 0 : 4;
}

function calcGDBorder(x, y, z) {
  if (x === 0 && y === 0) {
    return false;
  }
  if (
    data.system.gametype === "far" &&
    (x === 1 || y === 1 || x === -1 || y === -1)
  ) {
    return false;
  }

  if (x > Math.floor(y / 2) && y > Math.floor(x / 2)) {
    //return 'NE';
    if (z == "N") {
      y++;
      if (calcGD(x, y) != "NE") {
        return true;
      }
    }
    if (z == "E") {
      x++;
      if (calcGD(x, y) != "NE") {
        return true;
      }
    }
    if (z == "S") {
      y--;
      if (calcGD(x, y) != "NE") {
        return true;
      }
    }
    if (z == "W") {
      x--;
      if (calcGD(x, y) != "NE") {
        return true;
      }
    }
    return false;
  }
  if (x > Math.floor(-y / 2) && -y > Math.floor(x / 2)) {
    //return 'SE';
    if (z == "N") {
      y++;
      if (calcGD(x, y) != "SE") {
        return true;
      }
    }
    if (z == "E") {
      x++;
      if (calcGD(x, y) != "SE") {
        return true;
      }
    }
    if (z == "S") {
      y--;
      if (calcGD(x, y) != "SE") {
        return true;
      }
    }
    if (z == "W") {
      x--;
      if (calcGD(x, y) != "SE") {
        return true;
      }
    }
    return false;
  }
  if (-x > Math.floor(y / 2) && y > Math.floor(-x / 2)) {
    //return 'NW';
    if (z == "N") {
      y++;
      if (calcGD(x, y) != "NW") {
        return true;
      }
    }
    if (z == "E") {
      x++;
      if (calcGD(x, y) != "NW") {
        return true;
      }
    }
    if (z == "S") {
      y--;
      if (calcGD(x, y) != "NW") {
        return true;
      }
    }
    if (z == "W") {
      x--;
      if (calcGD(x, y) != "NW") {
        return true;
      }
    }
    return false;
  }
  if (-x > Math.floor(-y / 2) && -y > Math.floor(-x / 2)) {
    //return 'SW';
    if (z == "N") {
      y++;
      if (calcGD(x, y) != "SW") {
        return true;
      }
    }
    if (z == "E") {
      x++;
      if (calcGD(x, y) != "SW") {
        return true;
      }
    }
    if (z == "S") {
      y--;
      if (calcGD(x, y) != "SW") {
        return true;
      }
    }
    if (z == "W") {
      x--;
      if (calcGD(x, y) != "SW") {
        return true;
      }
    }
    return false;
  }
  return false;
}

function highlightZoneWithItem(zoneitem) {
  var itemid = parseInt(zoneitem.attr("ref"));
  if (zoneitem.hasClass("broken")) {
    itemid *= -1;
  }
  for (i = 0; i < data["height"]; i++) {
    for (j = 0; j < data["width"]; j++) {
      var mx = "x" + j;
      var my = "y" + i;
      var rx = j - data["tx"];
      var ry = data["ty"] - i;
      if (data.map[my] !== undefined && data.map[my][mx] !== undefined) {
        if (
          data.map[my][mx]["items"] !== undefined &&
          data.map[my][mx]["items"].length > 0 &&
          !(rx === 0 && ry === 0)
        ) {
          var zoneItems = data.map[my][mx]["items"];
          for (zi in zoneItems) {
            var zitem = zoneItems[zi];
            if (zitem.id === itemid) {
              highlightZone(rx, ry);
            }
          }
        }
      }
    }
  }
}

function highlightPotentialChestZones() {
  $(".desertzone").removeClass("highlight-chest");

  if ($("#highlight-bluechests").hasClass("active-filter")) {
    $(".potential-bluechest").addClass("highlight-chest");
  }

  if ($("#highlight-purplechests").hasClass("active-filter")) {
    $(".potential-purplechest").addClass("highlight-chest");
  }

  if ($("#highlight-fireworksparts").hasClass("active-filter")) {
    $(".potential-fireworksparts").addClass("highlight-chest");
  }
}


function highlightZonesWithItem() {
  $(".mapzone").removeClass("highlight");
  $('div.zone-item[state="1"]').each(function () {
    highlightZoneWithItem($(this));
  });
}

function highlightZone(x, y) {
  $("#x" + x + "y" + y).addClass("highlight highlight-pink");
}

function downdarkZone(x, y) {
  $("#x" + x + "y" + y).removeClass("highlight");
}

function highlightSpecialZone(x, y) {
  $("#x" + x + "y" + y).addClass("highlightSpecial");
}

function downdarkSpecialZone(x, y) {
  $("#x" + x + "y" + y).removeClass("highlightSpecial");
}


function addRadius(range, metric, color, name = false) {
  $("#dynascript").append(
    '<style type="text/css">li.mapzone.highlight-' +
      radiusCounter +
      "-border-n { border-top-color: " +
      color +
      "; } li.mapzone.highlight-" +
      radiusCounter +
      "-border-e { border-right-color: " +
      color +
      "; } li.mapzone.highlight-" +
      radiusCounter +
      "-border-s { border-bottom-color: " +
      color +
      "; } li.mapzone.highlight-" +
      radiusCounter +
      "-border-w { border-left-color: " +
      color +
      "; }</style>"
  );
  $(".mapzone[" + metric + '="' + range + '"]').each(function (e) {
    var change = $(this).attr(metric + "c");
    for (c = 0; c < change.length; c++) {
      $(this)
        .addClass("highlight-radius")
        .addClass("highlight-" + radiusCounter + "-border-" + change.charAt(c));
    }
  });
  if (!name) {
    name = __("Radius") + " " + radiusCounter;
  }
  $("#radius-list").append(
    '<div class="radius-list-item radius-delete hideme click" data-range="' +
      range +
      '" data-metric="' +
      metric +
      '" data-color="' +
      color +
      '" data-name="' +
      name +
      '" id="radius-delete-' +
      radiusCounter +
      '" onclick="removeRadius(' +
      radiusCounter +
      ');"><div class="radius-color-example" style="background-color:' +
      color +
      ';"></div><span>' +
      __("löschen") +
      "</span>" +
      name +
      ": " +
      range +
      " " +
      metric +
      "</div>"
  );
  $("#radius-delete-" + radiusCounter).slideDown(750);

  radiusCounter++;
}


function removeAllRadius() {
  $(".mapzone.highlight-border-n")
    .removeClass("highlight-border-n")
    .css("border-color", "rgba(0,0,0,.2)");
  $(".mapzone.highlight-border-e")
    .removeClass("highlight-border-e")
    .css("border-color", "rgba(0,0,0,.2)");
  $(".mapzone.highlight-border-s")
    .removeClass("highlight-border-s")
    .css("border-color", "rgba(0,0,0,.2)");
  $(".mapzone.highlight-border-w")
    .removeClass("highlight-border-w")
    .css("border-color", "rgba(0,0,0,.2)");
}


function removeRadius(rid) {
  $(".mapzone.highlight-" + rid + "-border-n").removeClass(
    "highlight-" + rid + "-border-n"
  );
  $(".mapzone.highlight-" + rid + "-border-e").removeClass(
    "highlight-" + rid + "-border-e"
  );
  $(".mapzone.highlight-" + rid + "-border-s").removeClass(
    "highlight-" + rid + "-border-s"
  );
  $(".mapzone.highlight-" + rid + "-border-w").removeClass(
    "highlight-" + rid + "-border-w"
  );
  $("#radius-delete-" + rid).slideUp(750);
  $("#radius-delete-" + rid).remove();
  saveRadii();
}


function walkInTheDesert(e) {
  if (data.cx === undefined || data.cy === undefined) {
    // current coords not set
    return;
  }
  if ($("#zone-info").hasClass("hideme")) {
    // zone info tab not active
    return;
  }
  let validKeys = ["ArrowLeft", "ArrowRight", "ArrowUp", "ArrowDown"];
  if (!validKeys.includes(e.key)) {
    // Not an arrow key.
    return;
  }
  e.preventDefault();
  var initClick = false;
  if (e.key === "ArrowLeft" && data.cx > -data.tx) {
    data.cx -= 1;
    initClick = true;
  }
  if (e.key === "ArrowUp" && data.cy < data.ty) {
    data.cy = parseInt(data.cy) + 1;
    initClick = true;
  }
  if (e.key === "ArrowRight" && data.cx < data.width - data.tx - 1) {
    data.cx = parseInt(data.cx) + 1;
    initClick = true;
  }
  if (e.key === "ArrowDown" && data.cy > data.ty - data.height + 1) {
    data.cy -= 1;
    initClick = true;
  }
  if (initClick) {
    $("#map #x" + data.cx + "y" + data.cy).click();
  }
}

function hasValue(obj, value) {
  return Object.values(obj).some((propValue) => propValue === value);
}

function findPropertyByValue(obj, value) {
  for (const prop in obj) {
    if (obj.hasOwnProperty(prop) && obj[prop] === value) {
      return prop;
    }
  }
  return null; // Return null if no matching property is found
}

function walkInTheRuin(e) {
  if (data.crx === undefined || data.cry === undefined) {
    // current coords not set
    data.crx = 0;
    data.cry = 1;
    data.floor = "upper";
  }
  const validWalkKeys = ["ArrowLeft", "ArrowRight", "ArrowUp", "ArrowDown"];
  const validEditKeys = {
    de: {
      zombie_0: "0",
      zombie_1: "1",
      zombie_2: "2",
      zombie_3: "3",
      zombie_4: "4",
      nozombie: "ß",
      doorlock: "a",
      dooropen: "s",
      no_doors: "d",
      floornxt: "f",
      key_open: "y",
      key_bump: "x",
      key_magn: "c",
      elevator: "v",
      intersec: "5",
      corri_ns: "6",
      corri_we: "7",
      no_corri: "8",
      tsec_nos: "t",
      tsec_now: "z",
      tsec_non: "u",
      tsec_noe: "i",
      cornerne: "g",
      cornerse: "h",
      cornersw: "j",
      cornernw: "k",
      deadendn: "b",
      deadende: "n",
      deadends: "m",
      deadendw: ",",
    },
    en: {
      zombie_0: "0",
      zombie_1: "1",
      zombie_2: "2",
      zombie_3: "3",
      zombie_4: "4",
      nozombie: "-",
      doorlock: "a",
      dooropen: "s",
      no_doors: "d",
      floornxt: "f",
      key_open: "z",
      key_bump: "x",
      key_magn: "c",
      elevator: "v",
      intersec: "5",
      corri_ns: "6",
      corri_we: "7",
      no_corri: "8",
      tsec_nos: "t",
      tsec_now: "y",
      tsec_non: "u",
      tsec_noe: "i",
      cornerne: "g",
      cornerse: "h",
      cornersw: "j",
      cornernw: "k",
      deadendn: "b",
      deadende: "n",
      deadends: "m",
      deadendw: ",",
    },
    fr: {
      zombie_0: "0",
      zombie_1: "1",
      zombie_2: "2",
      zombie_3: "3",
      zombie_4: "4",
      nozombie: "°",
      doorlock: "q",
      dooropen: "s",
      no_doors: "d",
      floornxt: "f",
      key_open: "w",
      key_bump: "x",
      key_magn: "c",
      elevator: "v",
      intersec: "5",
      corri_ns: "6",
      corri_we: "7",
      no_corri: "8",
      tsec_nos: "t",
      tsec_now: "y",
      tsec_non: "u",
      tsec_noe: "i",
      cornerne: "g",
      cornerse: "h",
      cornersw: "j",
      cornernw: "k",
      deadendn: "b",
      deadende: "n",
      deadends: "?",
      deadendw: ".",
    },
    es: {
      zombie_0: "0",
      zombie_1: "1",
      zombie_2: "2",
      zombie_3: "3",
      zombie_4: "4",
      nozombie: "-",
      doorlock: "a",
      dooropen: "s",
      no_doors: "d",
      floornxt: "f",
      key_open: "z",
      key_bump: "x",
      key_magn: "c",
      elevator: "f",
      intersec: "5",
      corri_ns: "6",
      corri_we: "7",
      no_corri: "8",
      tsec_nos: "t",
      tsec_now: "y",
      tsec_non: "u",
      tsec_noe: "i",
      cornerne: "g",
      cornerse: "h",
      cornersw: "j",
      cornernw: "k",
      deadendn: "b",
      deadende: "n",
      deadends: "m",
      deadendw: "<",
    },
  };
  if (validWalkKeys.includes(e.key) && ariadne === 0) {
    e.preventDefault();
    var initClick = false;
    if (e.key === "ArrowLeft" && data.crx > -7) {
      data.crx -= 1;
      initClick = true;
    }
    if (e.key === "ArrowDown" && data.cry < 13) {
      data.cry = parseInt(data.cry) + 1;
      initClick = true;
    }
    if (e.key === "ArrowRight" && data.crx < 5) {
      data.crx = parseInt(data.crx) + 1;
      initClick = true;
    }
    if (e.key === "ArrowUp" && data.cry > 1) {
      data.cry -= 1;
      initClick = true;
    }
    if (initClick) {
      $("#" + data.floor + "ruinmap #x" + data.crx + "y" + data.cry).click();
    }
  } else if (ruinEditorKeys === 1 && ariadne === 0 && hasValue(validEditKeys[data.langcode], e.key)) {
    // Allow keyboard ruin map editing.
    if (debug) { console.log("Edit key pressed: " + e.key); }
    e.preventDefault();
    let pressed = findPropertyByValue(validEditKeys[data.langcode], e.key);
    switch (pressed) {
      case "zombie_1":
        $(".ruinOption.ruinZombie.zombie-1").click();
        break;

      case "zombie_2":
        $(".ruinOption.ruinZombie.zombie-2").click();
        break;

      case "zombie_3":
        $(".ruinOption.ruinZombie.zombie-3").click();
        break;

      case "zombie_4":
        $(".ruinOption.ruinZombie.zombie-4").click();
        break;

      case "zombie_0":
        $(".ruinOption.ruinZombie.zombie-0").click();
        break;

      case "nozombie":
        $(".ruinOption.ruinZombie.zombie--1").click();
        break;

      case "doorlock":
        $(".ruinOption.ruinDoorLock.doorlock-1").click();
        break;

      case "dooropen":
        $(".ruinOption.ruinDoorLock.doorlock-2").click();
        break;

      case "no_doors":
        $(".ruinOption.ruinDoorLock.doorlock-0").click();
        break;

      case "key_open":
        $(".ruinOption.ruinDoorLock.doorlock-3").click();
        break;

      case "key_bump":
        $(".ruinOption.ruinDoorLock.doorlock-4").click();
        break;

      case "key_magn":
        $(".ruinOption.ruinDoorLock.doorlock-5").click();
        break;

      case "elevator":
        $(".ruinOption.ruinDoorLock.doorlock-6").click();
        break;

      case "intersec":
        $(".ruinOption.ruinTile.tile-7").click();
        break;

      case "corri_ns":
        $(".ruinOption.ruinTile.tile-5").click();
        break;

      case "corri_we":
        $(".ruinOption.ruinTile.tile-6").click();
        break;

      case "no_corri":
        $(".ruinOption.ruinTile.tile-0").click();
        break;

      case "tsec_nos":
        $(".ruinOption.ruinTile.tile-12").click();
        break;

      case "tsec_now":
        $(".ruinOption.ruinTile.tile-13").click();
        break;

      case "tsec_non":
        $(".ruinOption.ruinTile.tile-14").click();
        break;

      case "tsec_noe":
        $(".ruinOption.ruinTile.tile-15").click();
        break;

      case "cornerne":
        $(".ruinOption.ruinTile.tile-8").click();
        break;

      case "cornerse":
        $(".ruinOption.ruinTile.tile-9").click();
        break;

      case "cornersw":
        $(".ruinOption.ruinTile.tile-10").click();
        break;

      case "cornernw":
        $(".ruinOption.ruinTile.tile-11").click();
        break;

      case "deadendn":
        $(".ruinOption.ruinTile.tile-1").click();
        break;

      case "deadende":
        $(".ruinOption.ruinTile.tile-2").click();
        break;

      case "deadends":
        $(".ruinOption.ruinTile.tile-3").click();
        break;

      case "deadendw":
        $(".ruinOption.ruinTile.tile-4").click();
        break;

      case "floornxt":
        $(
          "#" +
            (data.floor === "upper" ? "lower" : "upper") +
            "ruinmap #x" +
            data.crx +
            "y" +
            data.cry
        ).click();
        break;
    }
  }
}

function moveMapHover(e) {
  var mapHover = $("#map-hover");
  var offset = 18;
  var x = e.pageX + offset;
  var y = e.pageY + offset;
  if (y > $(window).height() - mapHover.height() - offset * 4) {
    y -= mapHover.height() + offset * 2;
    if (y < offset * 2) {
      y = $(window).height() / 2 - mapHover.height() / 2;
    }
  }
  mapHover.css({ left: x, top: y });
  mapHoverMoved = true;
}

function hideMapHover() {
  $("#map-hover").hide();
  mapHoverMoved = false;
}

function showMapHover(e, z) {
  if (!mapHoverMoved) {
    moveMapHover(e);
  }
  var selectedZone = $("#" + z);
  if (selectedZone.attr("rx") === "0" && selectedZone.attr("ry") === "0") {
    return;
  }
  $("#map-hover").html(fillMapHover(selectedZone));
  $("#map-hover").show();
  mapHoverMoved = false;
}

function fillMapHover(z) {
  var rx = z.attr("rx");
  var ry = z.attr("ry");
  var ax = z.attr("ax");
  var ay = z.attr("ay");

  $("#map-hover-coords").html(
    "<strong>[" +
      rx +
      "|" +
      ry +
      "]</strong> - " +
      calcAP(rx, ry) +
      " AP / " +
      calcKM(rx, ry) +
      " km"
  );
  if (
    data.map["y" + ay] !== undefined &&
    data.map["y" + ay]["x" + ax] !== undefined
  ) {
    if (data.map["y" + ay]["x" + ax]["building"] !== undefined) {
      //TODOHERE
      $("#map-hover-building").html(
        "<strong>" +
          data.buildings[data.map["y" + ay]["x" + ax]["building"]["type"]][
            data.langcode
          ] +
          "</strong>"
      );
    } else {
      $("#map-hover-building").html(" ");
    }
    if (data.map["y" + ay]["x" + ax]["dried"] !== undefined) {
      if (data.map["y" + ay]["x" + ax]["dried"] === 1) {
        $("#map-hover-status").html(
          '<strong class="minus">' + __("Zone ist leer.") + "</strong>"
        );
      } else {
        $("#map-hover-status").html(
          '<strong class="plus">' + __("Zone ist buddelbar.") + "</strong>"
        );
      }
    } else {
      $("#map-hover-status").html(" ");
    }
    if (data.map["y" + ay]["x" + ax]["citizens"] !== undefined) {
      var cc = 0;
      var clist = "";
      var zone = data.map["y" + ay]["x" + ax];
      for (cid in zone["citizens"]) {
        cc++;
        //if ( clist !== '' ) { clist += ', '; }
        clist +=
          '<span class="zone-citizen zone-citizen-' +
          zone["citizens"][cid]["job"] +
          '">' +
          zone["citizens"][cid]["name"] +
          "</span> ";
      }
      if (cc > 0) {
        //infoBox.append('<p class="zone-citizens">'+ cc +' Bürger: '+ clist +'</p>');
        $("#map-hover-citizens").html(clist);
      } else {
        $("#map-hover-citizens").html(" ");
      }
    } else {
      $("#map-hover-citizens").html(" ");
    }
    if (
      data.map["y" + ay]["x" + ax]["xmlz"] !== undefined &&
      data.map["y" + ay]["x" + ax]["xmlz"] !== null
    ) {
      var zcnt = parseInt(data.map["y" + ay]["x" + ax]["xmlz"]);
      if (zcnt === 1) {
        $("#map-hover-zombies").html(
          '<strong class="minus">1 ' +
            __("Zombie") +
            " " +
            __("Currently").toLowerCase() +
            "</strong>"
        );
      } else {
        $("#map-hover-zombies").html(
          '<strong class="minus">' +
            zcnt +
            " " +
            __("Zombies") +
            " " +
            __("Currently").toLowerCase() +
            "</strong>"
        );
      }
    } else if (
      data.map["y" + ay]["x" + ax]["z"] !== undefined &&
      data.map["y" + ay]["x" + ax]["nvt"] === 0
    ) {
      var zcnt = parseInt(data.map["y" + ay]["x" + ax]["z"]);
      var updDay = "";
      if (data.map["y" + ay]["x" + ax]["updatedOnDay"] !== undefined) {
        updDay +=
          " " +
          __("am") +
          " " +
          __("Tag") +
          " " +
          data.map["y" + ay]["x" + ax]["updatedOnDay"];
      }
      if (zcnt === 1) {
        $("#map-hover-zombies").html(
          '<strong class="minus">1 ' + __("Zombie") + updDay + "</strong>"
        );
      } else {
        $("#map-hover-zombies").html(
          '<strong class="minus">' +
            zcnt +
            " " +
            __("Zombies") +
            updDay +
            "</strong>"
        );
      }
    } else if (data.map["y" + ay]["x" + ax]["z"] !== undefined) {
      var zone = data.map["y" + ay]["x" + ax];
      var diff = 0,
        days = 0;
      if (zone.updatedOn !== undefined && zone.nvt !== 0) {
        var date = new Date(zone.updatedOn * 1000);
        var dateN = new Date();
        dateN.setHours(23);
        dateN.setMinutes(59);
        dateN.setSeconds(59);
        if (date.getDate() !== dateN.getDate) {
          days = Math.floor((dateN.getTime() - date.getTime()) / 86400000);
          // diff = days;
          if (zone.building !== undefined) {
            // diff = 2 * days;
          }
        }
      }
      // diff = 0;
      var updDay = "";
      if (data.map["y" + ay]["x" + ax]["updatedOnDay"] !== undefined) {
        updDay +=
          " " +
          __("am") +
          " " +
          __("Tag") +
          " " +
          data.map["y" + ay]["x" + ax]["updatedOnDay"];
      }
      var zest = "";
      if (days > 0) {
        if (days === 1 && zone.z === 1) {
          zest = __("Vor 1 Tag war es ein Zombie.");
        } else if (days === 1 && zone.z > 1) {
          zest = __("Vor 1 Tag waren es {%1} Zombies.", [zone.z]);
        } else if (days > 1 && zone.z === 1) {
          zest = __("Vor {%1} Tagen war es ein Zombie.", [days]);
        } else if (days > 1 && zone.z > 1) {
          zest = __("Vor {%1} Tagen waren es {%2} Zombies.", [days, zone.z]);
        }
        //zest = '<br/><em style="color:#ccc;">' + zest + '</em>';
        $("#map-hover-zombies").html(
          '<strong class="minus">' + zest + "</strong>"
        );
      } else if (zone.updatedOn === undefined) {
        if (zone.z === 1) {
          zest = __("Irgendwann wurde 1 Zombie gesichtet.");
        } else {
          zest = __("Irgendwann wurden {%1} Zombies gesichtet.", [zone.z]);
        }
        $("#map-hover-zombies").html(
          '<strong class="minus">' + zest + "</strong>"
        );
      } else {
        var zcnt = parseInt(data.map["y" + ay]["x" + ax]["z"]) + diff;
        if (zcnt === 1) {
          $("#map-hover-zombies").html(
            '<strong class="minus">1 ' + __("Zombie") + updDay + "</strong>"
          );
        } else {
          $("#map-hover-zombies").html(
            '<strong class="minus">' +
              zcnt +
              " " +
              __("Zombies") +
              updDay +
              "</strong>"
          );
        }
      }
    } else if (data.map["y" + ay]["x" + ax]["updatedOn"] !== undefined) {
      var zone = data.map["y" + ay]["x" + ax];
      var diff = 0,
        days = 0;
      var date = new Date(zone.updatedOn * 1000);
      var dateN = new Date();
      dateN.setHours(23);
      dateN.setMinutes(59);
      dateN.setSeconds(59);
      if (date.getDate() !== dateN.getDate) {
        days = Math.floor((dateN.getTime() - date.getTime()) / 86400000);
        diff = days;
        if (zone.building !== undefined) {
          diff = 2 * days;
        }
      }
      diff = 0;
      var zest = "";
      if (days > 0) {
        if (days === 1) {
          zest = __("Vor 1 Tag waren es 0 Zombies.");
        } else if (days > 1) {
          zest = __("Vor {%1} Tagen waren es 0 Zombies.", [days]);
        }
        $("#map-hover-zombies").html(
          '<strong class="minus">' + zest + "</strong>"
        );
      } else {
        /*var zcnt = diff;

				if (zcnt === 1) {
					$('#map-hover-zombies').html('<strong class="minus">1 ' + __('Zombie') + '</strong>' + zest);
				} else {
					$('#map-hover-zombies').html('<strong class="minus">' + zcnt + ' ' + __('Zombies') + '</strong>' + zest);
				}*/
        $("#map-hover-zombies").html(
          '<strong class="minus">' + "?? " + __("Zombies") + "</strong>"
        );
      }
    } else {
      $("#map-hover-zombies").html(" ");
    }
    if (
      data.map["y" + ay]["x" + ax]["items"] !== undefined &&
      data.map["y" + ay]["x" + ax]["items"].length > 0
    ) {
      var zone = data.map["y" + ay]["x" + ax];
      var itemBox = $("#map-hover-items");
      itemBox.html(
        "<p><strong>" + __("Gegenstände auf dem Boden") + "</strong></p>"
      );
      itemBox.append(
        '<div id="zh_x' +
          rx +
          "_y" +
          ry +
          '" class="zone-items clearfix"></div>'
      );
      for (i in zone.items) {
        var item = zone.items[i];
        $("#zh_x" + rx + "_y" + ry).append(
          createItemDisplay(item.id, item.count, item.broken)
        );
      }

      //sort items by name
      let divParent = document.getElementById("zh_x" + rx + "_y" + ry);
      let divList = $(divParent).children(".zone-item");

      divList.sort((a, b) => {
        let itemA = $(a).children("img").attr("title").toString();
        let itemB = $(b).children("img").attr("title").toString();
        let firstcharA = itemA.substring(0, 1);
        if (
          firstcharA === "«" ||
          firstcharA === '"' ||
          firstcharA === "“" ||
          firstcharA === "'"
        ) {
          return 1;
        }
        var firstcharB = itemB.substring(0, 1);
        if (
          firstcharB === "«" ||
          firstcharB === '"' ||
          firstcharB === "“" ||
          firstcharB === "'"
        ) {
          return -1;
        }
        return itemA.localeCompare(itemB);
      });
      $("#zh_x" + rx + "_y" + ry).append(divList);
    } else {
      $("#map-hover-items").html(" ");
    }
    if (
      data.scout !== undefined &&
      data.scout["y" + ay + "x" + ax] !== undefined
    ) {
      var updatedOn = " " + __("am") + " " + __("Tag") + " ";
      updatedOn += data.scout["y" + ay + "x" + ax]["updatedOnDay"] + " ";
      if (parseInt(data.scout["y" + ay + "x" + ax]["zom"]) === 1) {
        $("#map-hover-zombies").append(
          '<br/><img src="/assets/css/img/item_vest_on.gif" /><strong class="minus">&nbsp;~&nbsp;1&nbsp;' +
            __("Zombie") +
            updatedOn +
            "</strong>"
        );
      } else if (data.scout["y" + ay + "x" + ax]["zom"] > 1) {
        $("#map-hover-zombies").append(
          '<br/><img src="/assets/css/img/item_vest_on.gif" /><strong class="minus">&nbsp;~&nbsp;' +
            data.scout["y" + ay + "x" + ax]["zom"] +
            "&nbsp;" +
            __("Zombie") +
            updatedOn +
            "</strong>"
        );
      }
    }
  } else {
    $("#map-hover-building").html(" ");
    $("#map-hover-status").html(" ");
    $("#map-hover-citizens").html(" ");
    $("#map-hover-zombies").html(" ");
    $("#map-hover-items").html(" ");
    if (
      data.scout !== undefined &&
      data.scout["y" + ay + "x" + ax] !== undefined
    ) {
      var updatedOn = " " + __("am") + " " + __("Tag") + " ";
      updatedOn += data.scout["y" + ay + "x" + ax]["updatedOnDay"] + " ";
      if (data.scout["y" + ay + "x" + ax]["pbl"] === 1) {
        $("#map-hover-building").html(
          "<strong>" + __("Hier ist wahrscheinlich ein Gebäude.") + "</strong>"
        );
      }
      if (parseInt(data.scout["y" + ay + "x" + ax]["zom"]) === 1) {
        $("#map-hover-zombies").append(
          '<br/><img src="/assets/css/img/item_vest_on.gif" /><strong class="minus">&nbsp;~&nbsp;1&nbsp;' +
            __("Zombie") +
            updatedOn +
            "</strong>"
        );
      } else if (data.scout["y" + ay + "x" + ax]["zom"] > 1) {
        $("#map-hover-zombies").append(
          '<br/><img src="/assets/css/img/item_vest_on.gif" /><strong class="minus">&nbsp;~&nbsp;' +
            data.scout["y" + ay + "x" + ax]["zom"] +
            "&nbsp;" +
            __("Zombie") +
            updatedOn +
            "</strong>"
        );
      }
    }
  }
}

function ajaxMyzoneUpdate() {
  if (window.softreloads >= 100) {
    location.reload();
  } else {
    window.softreloads++;
    fire("/map/updatemyzone", '', "box");
  }
}

function ajaxUpdate(ec, z, zm) {
  var el = $("#" + ec);
  var ocX = el.attr("ocx");
  var ocY = el.attr("ocy");
  var ocAction = ec;
  let payload = "action=" + ocAction + "&x=" + ocX + "&y=" + ocY + "&z=" + z + "&zm=" + zm;
  fire("/map/update", payload, "box");
}

function expeditionUpdate(expeditionId, action) {
  if (action === "DELETE-ROUTE") {
    if (debug) { console.log("ExpeditionRoute DELETE: " + expeditionId, escapeSelector(expeditionId)); }
    let payload = "action=" + action + "&expeditionid=" + expeditionId;
    fire("/map/updateExpedition", payload, "box", function () {
      document.querySelector('#' + escapeSelector(expeditionId)).remove();
      delete data.expeditions[expeditionId];
      document.querySelectorAll("." + escapeSelector(expeditionId)).forEach(function(el) { el.remove(); });
    });
  }
  else if (newRouteX !== null && newRouteY !== null) {
    var ap = $("#expedition-ap").text();
    if (parseInt(ap) === 0) {
      ajaxInfoBad("Cannot create empty route.");
      return false;
    }
    var routeString = "";

    for (i in newRouteX) {
      routeString = routeString + newRouteX[i] + "-" + newRouteY[i] + "_";
    }

    var name = $("#exp-form-name").val();
    if (name === "") {
      name = data.system.owner_name;
    }
    var day = $("#exp-day").val();
    var type = $('input[name="expe-type"]:checked').val();
    var color = $('input[name="expe-color"]').val();

    if (editRouteId !== null) {
      expeditionId = editRouteId;
    }

    let successFunction = function (msg) {
      let time = msg.match(/time=(.*?);/i)[1]; // IMO It is easier to read this way, but this can be returned as js script from server if wanted
        let completeName =
          name + " [" + ap + "AP] (" + data.system.owner_name + " via FM)";
        let creator = data.system.owner_id;
        let creatorName = data.system.owner_name;
        if (expeditionId === null || expeditionId === undefined) {
          expeditionId = "fm." + data.system.owner_id + "." + time;
        } else {
          completeName =
            name +
            " [" +
            ap +
            "AP] (" +
            data.expeditions[expeditionId]["creatorName"] +
            ", edited by " +
            data.system.owner_name +
            " via FM)";
          creator = data.expeditions[expeditionId]["creator"];
          creatorName = data.expeditions[expeditionId]["creatorName"];
        }

        if (data.expeditions === undefined) {
          data.expeditions = {};
        }
        data.expeditions[expeditionId] = {
          editor: data.system.owner_name,
          creator: creator,
          creatorName: creatorName,
          day: day,
          name: completeName,
          route: routeString,
          type: type,
          color: color,
        };
        editRouteId = null;
        generateExpeditionList();
        $("#" + escapeSelector(expeditionId)).addClass("active-option");
        highlightRoute(expeditionId);

        //expand the day to which the route belongs
        if (!$("#expand-exp-" + day).hasClass("caret-down")) {
          $("#expand-exp-" + day).click();
        }
        //expand the current day
        if (!$("#expand-exp-" + data.system.day).hasClass("caret-down")) {
          $("#expand-exp-" + data.system.day).click();
        }
    };
    let payload = "action=" + action +
        "&expeditionid=" + expeditionId +
        "&route=" + routeString +
        "&ap=" + ap +
        "&name=" + name +
        "&day=" + day +
        "&type=" + type +
        "&color=" + color;
    fire('/map/updateExpedition', payload, "box", successFunction, true);

    return true;
  } else {
    ajaxInfo(__("")); // TODO
  }
}

function updateFromExternal(source) {
  fire("/map/updateFromExternal?source=" + source);
}

function ajaxInfo(msg) {
  var responseItem = $(document.createElement("p"))
    .addClass("ajaxInfo")
    .addClass("hideme")
    .html(msg);
  $("#userInfoBox").append(responseItem);
  responseItem
    .hide()
    .removeClass("hideme")
    .slideDown(250)
    .delay(2500)
    .slideUp(500);
}

function ajaxInfoBad(msg) {
  var responseItem = $(document.createElement("p"))
    .addClass("ajaxInfoBad")
    .addClass("hideme")
    .html(msg);
  $("#userInfoBox").append(responseItem);
  responseItem
    .hide()
    .removeClass("hideme")
    .slideDown(250)
    .delay(2500)
    .slideUp(500);
}

function ajaxRuinUpdate(ocAction, z) {
  var ocX = $(".ruinCoordsX").html();
  var ocY = $(".ruinCoordsY").html();
  var floor = $(".ruinFloor").html();
  var riX = parseInt($("#ruinCX").html());
  var riY = parseInt($("#ruinCY").html());
  if (
    riY > 0 ||
    (riY == 0 && riX == 0 && ocAction == "RUIN-COMMENT") ||
    floor == "lower"
  ) {
    var ocAD = riX + "|" + riY + "|" + z;
    let payload = "action=" + ocAction +
        "&x=" + ocX +
        "&y=" + ocY +
        "&z=" + ocAD +
        "&floor=" + floor;
    fire('/map/update', payload);
  } else {
    ajaxInfo(__("Bitte gültiges Feld wählen."));
  }
}

function reMoveCitizen(id) {
  for (i = 0; i < data["height"]; i++) {
    for (j = 0; j < data["width"]; j++) {
      var ax = j;
      var ay = i;
      var mx = "x" + j;
      var my = "y" + i;
      var rx = j - data["tx"];
      var ry = data["ty"] - i;
      var rzd = null;
      if (
        data.map[my] !== undefined &&
        data.map[my][mx] !== undefined &&
        data.map[my][mx]["citizens"] !== undefined &&
        data.map[my][mx]["citizens"][id] !== undefined
      ) {
        delete data.map[my][mx]["citizens"][id];
        replaceMapZone(ay, ax);
      }
    }
  }
}

function reMoveCitidot(x, y) {
  if (x && y) {
    var ax = x;
    var ay = y;
    var rx = x - data["tx"];
    var ry = data["ty"] - y;
    replaceMapZone(ay, ax);
    reCalculateCitizenCitidots();
    checkCitidots();
  }
}

function reCalculateCitizenCitidots() {
  citidots = citidots.filter(item => !(item.cs === 2));
  for (let c in data.citizens) {
    let rx = data.citizens[c]["rx"];
    let ry = data.citizens[c]["ry"];
    if (!(rx === 0 && ry === 0)) {
      let zoneId = `x${rx}y${ry}`;
      let index = citidots.findIndex(item => item.zone === zoneId && item.cs === 2);
      if (index !== -1) {
        citidots[index]["cc"]++;
      }
      else {
        citidots.push({ zone: zoneId, cs: 2, cc: 1 });
      }
    }
  }
  checkCitidots();
}

function populateItemSelector() {
  for (i in data.items) {
    document.getElementById("item-selector-" + data.items[i]["category"]).append(createItemDisplaySmall(i));
  }

  //sort items by name
  for (c in data.mapitems) {
    let divParent = document.getElementById(
      "item-selector-" + data.mapitems[c]["cat"]
    );
    let divList = $(divParent).children(".select-item");

    divList.sort((a, b) => {
      let itemA = $(a).children("img").attr("title").toString();
      let itemB = $(b).children("img").attr("title").toString();
      let firstcharA = itemA.substring(0, 1);
      if (
        firstcharA === "«" ||
        firstcharA === '"' ||
        firstcharA === "“" ||
        firstcharA === "'"
      ) {
        return 1;
      }
      var firstcharB = itemB.substring(0, 1);
      if (
        firstcharB === "«" ||
        firstcharB === '"' ||
        firstcharB === "“" ||
        firstcharB === "'"
      ) {
        return -1;
      }
      return itemA.localeCompare(itemB);
    });
    $("#item-selector-" + data.mapitems[c]["cat"]).append(divList);
  }

  aElement1 = document.createElement("a");
  aElement1.classList.add("close-item-selector", "close-item-selector-inboxbutton", "interactive", "hideme");
  aElement1.textContent = __("schließen");
  aElement1.setAttribute("href", "close");
  $("#item-selector").append(aElement1);

}

function populateBuildingSelector(distance = null) {
  buildingBlacklist = ["-1", "61", "62", "63"];

  if (!distance) {
    $("#building-selector-all").empty();
    for (i in data.buildings) {
      if (!buildingBlacklist.includes(i)) {
        $("#building-selector-all").append(createBuildingDisplaySmall(i));
      }
    }

    var showButton = document.createElement("a");
    showButton.classList.add("interactive", "show-building-selector-all");
    showButton.href = "#";
    showButton.textContent = __("show-all-buildings");
    $("#building-selector").append(showButton);

    var closeButton = document.createElement("a");
    closeButton.classList.add("interactive", "close-building-update");
    closeButton.href = "#";
    closeButton.textContent = __("schließen");
    $("#building-selector").append(closeButton);

    return;
  }

  $("#building-selector-suggested").empty();
  for (i in data.buildings) {
    if (distance >= data.buildings[i].mind && distance <= data.buildings[i].maxd && !buildingBlacklist.includes(i))
    $("#building-selector-suggested").append(createBuildingDisplaySmall(i));
  }
  $("#building-selector-all").hide();
  $("#headline-building-selector-all").hide();
  $(".show-building-selector-all").show()
}

function zoneItemList(x, y) {
  this.x = x;
  this.y = y;
}

function saveZoneItems(x, y, serial) {
  let payload = "action=ZONE-ITEMS&x=" + (parseInt(x) + parseInt(data.tx)) +
      "&y=" + (parseInt(data.ty) - parseInt(y)) +
      "&z=" + serial;
  fire('/map/update', payload, "box");
}

function saveBuildingGuess(x, y, bID) {
  let payload = "action=BUILDING-GUESS&x=" + (parseInt(x) + parseInt(data.tx)) +
      "&y=" + (parseInt(data.ty) - parseInt(y)) +
      "&z=" + bID;
  fire('/map/update', payload, "box");
}

function deleteBuildingGuess(x, y) {
  let payload = "action=BUILDING-GUESS&x=" + (parseInt(x) + parseInt(data.tx)) +
      "&y=" + (parseInt(data.ty) - parseInt(y)) +
      "&z=-2";
  fire('/map/update', payload, true);
}

function protectBox(state) {
  if (state) {
    $("#box-protection").css("height", $("#box").css("height"));
    $("#box-protection").css("width", $("#box").css("width"));
    $("#box-protection").removeClass("hideme");
  } else {
    $("#box-protection").addClass("hideme");
  }
}

function protectTabs(state) {
  if (state) {
    $("#tabs-protection").css("height", $("#bps-panel").css("height"));
    $("#tabs-protection").css("width", $("#bps-panel").css("width"));
    $("#tabs-protection").removeClass("hideme");
  } else {
    $("#tabs-protection").addClass("hideme");
  }
}

function toggleGeoDirDisplay() {
  $(".mapzone").toggleClass("geodir");
}

function generateStormList() {
  if (data.storm === undefined) {
    return false;
  }

  var stormBox = document.getElementById("storms");
  var stormList = document.createElement("ul");
  stormList.classList.add("storm-list");
  var stormForm = document.createElement("div");
  stormForm.classList.add("storm-form");
  var stormChart = document.createElement("div");
  stormChart.classList.add("storm-chart");

  stormForm.innerHTML += '<select id="storm-today" name="storm-today"><option value="0">' + __("keine Beobachtung") + '</option></select>';
  stormForm.innerHTML += '<a class="interactive ajaxupdate" href="/update/storm" id="UPDATE-STORM">' + __("speichern") + '</a>';
  
  stormBox.innerHTML = "";
  stormBox.appendChild(stormForm);

  for (var i = 1; i < 9; i++) {
    document.getElementById("storm-today").innerHTML += '<option value="' + i + '">' + __(data.stormnames[i - 1]) + '</option>';
  }

  let stormSummary = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0};
  for (var s in data.storm) {
    if (parseInt(s) === parseInt(data.system.day)) {
      document.getElementById("storm-today").value = s;
    }
    if (data.storm[s] > 0) {
      stormSummary[data.storm[s]]++;
      stormList.innerHTML = "<li>" + __("Tag") + " " + s + ": <strong>" + __(data.stormnames[data.storm[s] - 1]) + "</strong></li>" + stormList.innerHTML;
    } else {
      stormList.innerHTML = "<li>" + __("Tag") + " " + s + ": <strong>" + __("keine Beobachtung") + "</strong></li>" + stormList.innerHTML;
    }
  }

  let stormMaxCount = Math.max(1, ...Object.values(stormSummary));
  for (let d = 1; d <= 8; d++) {
    let stormChartItem = document.createElement("div");
    stormChartItem.classList.add("storm-chart-item");
    stormChartItem.classList.add("segment" + d);

    let stormColorCode = Math.round(stormSummary[d] / stormMaxCount * 10) * 10;
    stormChartItem.classList.add("intensity-" + stormColorCode);
    
    let stormChartItemDirection = document.createElement("span");
    stormChartItemDirection.classList.add("storm-chart-item-direction");
    stormChartItemDirection.textContent = data.stormnames[d - 1];
    
    let stormChartItemCount = document.createElement("span");
    stormChartItemCount.classList.add("storm-chart-item-count");
    stormChartItemCount.textContent = stormSummary[d];
    
    stormChartItem.appendChild(stormChartItemDirection);
    stormChartItem.appendChild(stormChartItemCount);

    stormChart.appendChild(stormChartItem);
  }
  for (let i = 1; i < 5; i++) {
    let stormChartBar = document.createElement("div");
    stormChartBar.classList.add("storm-chart-bar");
    stormChartBar.classList.add("storm-chart-bar-" + i);
    stormChart.appendChild(stormChartBar);
  }

  stormBox.appendChild(stormChart);
  stormBox.appendChild(stormList);
  

  if (data.spy == 1 || data.stormSetByApi == 1) {
    stormForm.classList.add("hideme");
  }
}

function generateExpeditionList() {
  let activeExpeditions = $("#expeditions .exp-item.active-option")
    .map(function () {
      if (this.id !== "0.0" && this.id !== "0.1") {
        return this.id;
      }
    })
    .get();

  var expBox = $("#expeditions");
  expBox.html("");

  // Form to new expeditions
  if (data.spy === undefined && data.spy !== 1) {
    var formExp = $(document.createElement("ul"))
      .addClass("hideme")
      .attr("id", "exp-form");

    let editingForm = $(document.createElement("div"))
      .addClass("desc")
      .addClass("hideme")
      .attr("id", "editing-mode");
    editingForm.html(
      '<p class="desc"><strong>' +
        __("Editing") +
        "</strong><br>" +
        __(
          "You are editing a route. Please cancel the edition if you want to create a new route instead!"
        ) +
        "<br></p><br>"
    );

    formExp.append(editingForm);

    var liExp = $(document.createElement("li"))
      .attr("id", "exp-form-item")
      .attr("style", "display: list-item;");

    liExp.html(
      '<div class="exp-form-item">' +
        '<span data-trans="expedition-name">' +
        __("Expedition Name") +
        "</span> " +
        '(<span id="expedition-ap">0</span> ' +
        __("AP") +
        ")" +
        '<input type="text" placeholder=' +
        data.system.owner_name +
        ' maxlength="45" id="exp-form-name" name="expe-name">' +
        "</div>"
    );

    liExp.append(
      '<div class="exp-form-item">' +
        "<span>" +
        __("Tag") +
        "  </span>" +
        '<input type="number" id="exp-day" name="expe-day" value=' +
        data.system.day +
        ' min="1">' +
        "</div>"
    );

    liExp.append(
      '<div class="exp-form-item">' +
        "<span>" +
        __("Color") +
        "  </span>" +
        '<input type="color" style="height: 18px; width: 35px;" id="expe-color" name="expe-color" value="#' +
        Math.floor(Math.random() * 16777215).toString(16) +
        '" />' +
        "</div>"
    );

    liExp.append(
      '<div class="exp-form-item">' +
        '<input type="radio" name="expe-type" id="exp-public" value="public" checked="checked">' +
        '<span class="tooltip">' +
        __("Public") +
        '<span class="tooltiptext">' +
        __("Everyone will see your route. Edit by others not allowed.") +
        "</span></span><br>" +
        '<input type="radio" name="expe-type" id="exp-collaborative" value="collaborative">' +
        '<span class="tooltip">' +
        __("Collaborative") +
        '<span class="tooltiptext">' +
        __("Everyone will see your route. Edit allowed.") +
        "</span></span><br>" +
        '<input type="radio" name="expe-type" id="exp-private" value="private">' +
        '<span class="tooltip">' +
        __("Private") +
        '<span class="tooltiptext">' +
        __("Only visible to you.") +
        "</span></span>" +
        "</div>"
    );

    liExp.append(
      '<a class="interactive click" id="UNDO-ROUTE" data-trans="clean-route" href="">' +
        __("Undo last point") +
        "  </a>"
    );
    liExp.append(
      '<span class=smalltext data-trans="backspace-info">' +
        __("(Auch mit BACKSPACE)") +
        "</span> "
    );
    liExp.append(
      '<br><a class="interactive click plus" id="ADD-ROUTE" data-trans="save-route" href="">' +
        __("Save route") +
        "</a>"
    );
    liExp.append(
      '  <a class="interactive click minus" id="CLEAN-ROUTE" data-trans="clean-route" href="">' +
        __("Clear") +
        "</a>"
    );
    liExp.append(
      '<br><a id="cancel-editing" href="" class="hideme interactive click minus">' +
        __("Cancel Editing") +
        "</a>"
    );
    liExp.append(
      '  <a id="duplicate-route" href="" class="hideme interactive click">' +
        __("Duplicate Route") +
        "</a>"
    );

    formExp.append(liExp);

    expBox.prepend(formExp);
  }

  var expList = $(document.createElement("ul")).addClass("exp-list");

  expBox.append(expList);

  let sortable = [];
  for (let exp in data.expeditions) {
    sortable.push([exp, data.expeditions[exp]]);
  }

  sortable.sort(function (a, b) {
    return b[1].day - a[1].day;
  });

  for (let exp in sortable) {
    var expData = sortable[exp][1];
    var expId = sortable[exp][0];
    var currentDayList = $("#exp-day-" + expData.day);
    if (currentDayList.length === 0) {
      // TODO put css to css file

      var currentDay = null;
      currentDayList = $(document.createElement("ul")).attr(
        "id",
        "exp-day-" + expData.day
      );
      if (expData.day !== data.system.day) {
        currentDayList.css("display", "none");
        currentDay = $(document.createElement("li"))
          .css("list-style-type", "none")
          .html(
            '<a id="expand-exp-' +
              expData.day +
              '" class="expand click caret">' +
              __("Tag") +
              " " +
              expData.day +
              "</a>"
          );
      } else {
        currentDay = $(document.createElement("li"))
          .css("list-style-type", "none")
          .html(
            '<a id="expand-exp-' +
              expData.day +
              '" class="expand click caret caret-down">' +
              __("Tag") +
              " " +
              expData.day +
              "</a>"
          );
      }

      currentDay.append(currentDayList);
      expList.append(currentDay);
    }

    let expEntry = null;
    if (activeExpeditions.includes(expId)) {
      expEntry = $(document.createElement("li"))
        .addClass("exp-item exp-route click active-option")
        .attr("id", expId)
        .attr("style", "display: flex;align-items: center;")
        .html(
          '<span style="flex: 1; display: block;word-break: break-word;padding: 2px;">' +
            expData.name +
            "</span>"
        ); // TODO put this style at css file
    } else {
      expEntry = $(document.createElement("li"))
        .addClass("exp-item exp-route click")
        .attr("id", expId)
        .attr("style", "display: flex;align-items: center;")
        .html(
          '<span style="flex: 1; display: block;word-break: break-word;padding: 2px;">' +
            expData.name +
            "</span>"
        ); // TODO put this style at css file
    }

    if (expData.type === "private") {
      var privateImg = $(document.createElement("img"))
        .addClass("exp-type")
        .attr("title", __("Private"))
        .attr("src", "/assets/css/img/item_lock.gif");
      expEntry.prepend(privateImg);
    } else if (expData.type === "collaborative") {
      var privateImg = $(document.createElement("img"))
        .addClass("exp-type")
        .attr("title", __("Collaborative"))
        .attr("src", "/assets/css/img/small_strategy.gif");
      expEntry.prepend(privateImg);
    }

    if (
      data.spy === undefined &&
      data.spy !== 1 &&
      (expData.type === "collaborative" ||
        expData.creator === data.system.owner_id)
    ) {
      var edit = $(document.createElement("div"))
        .addClass("click")
        .attr("id", "EDIT-ROUTE")
        .attr("title", __("Bearbeiten"))
        .html('<img src="/assets/css/img/editpen4.gif">');
      expEntry.append(edit);
    }
    if (
      data.spy === undefined &&
      data.spy !== 1 &&
      expData.creator === data.system.owner_id
    ) {
      var trash = $(document.createElement("div"))
        .addClass("click")
        .attr("id", "DELETE-ROUTE")
        .attr("title", __("Löschen"))
        .html('<img src="/assets/css/img/trash.gif">');
      expEntry.append(trash);
    }

    currentDayList.prepend(expEntry);
  }

  //if we don't have any routes for today we still create a empty dropdown to not confuse people with yesterdays route block
  if (document.getElementById("expand-exp-" + data.system.day) == null) {
    let todayList = $(document.createElement("li"))
      .css("list-style-type", "none")
      .html(
        '<a id="expand-exp-' +
          data.system.day +
          '" class="expand click caret caret-down">' +
          __("Tag") +
          " " +
          data.system.day +
          "</a>"
      );
    expList.prepend(todayList);
  }

  //routeplanner button added
  if (data.spy === undefined && data.spy !== 1) {
    expList.prepend(
      $(document.createElement("li"))
        .addClass("exp-item click active-option")
        .attr("id", "0.1")
        .html(__("Open route editor"))
    );
  }

  //deselect all button added
  if (data.expeditions !== undefined) {
    expList.append(
      $(document.createElement("li"))
        .addClass("exp-item click active-option")
        .attr("id", "0.0")
        .html(__("Anzeige ausblenden"))
    ); // Hide display of expedictions
  }
}

function highlightRoute(r) {
  if (r !== "0.0" || r !== "0.1") {
    var route = data.expeditions[r]["route"];
    var color = data.expeditions[r]["color"];
    if (color === undefined) {
      color = "red";
    }
    var rp = route.split("_");
    currentX = null;
    currenty = null;
    for (rc of rp) {
      routepoints = rc.split("-");
      if (routepoints[0] === "") break;
      drawExpToPoint(routepoints[0], routepoints[1], r, color);
    }
  }
}

function generateRuinList() {
  var awrBox = $("#ruins");
  var awrTable = $(document.createElement("table")).addClass("awr-table");
  var awr = {};
  var a = 0;
  var zones = 0;
  var ruins = 0;
  var pbl = 0;
  for (y in data.map) {
    if (y.match(/^y\d{1,2}$/i) === null) {
      continue;
    }
    var yrow = data.map[y];
    for (x in yrow) {
      if (x.match(/^x\d{1,2}$/i) === null) {
        continue;
      }
      zones++;
      var xcell = yrow[x];
      if (xcell.building !== undefined && xcell.building.name !== undefined) {
        ruins++;
        var bdata = xcell.building;
        bdata["x"] = xcell.rx;
        bdata["y"] = xcell.ry;
        bdata["ap"] = calcAP(xcell.rx, xcell.ry);
        bdata["km"] = calcKM(xcell.rx, xcell.ry);
        if (bdata["blueprint"] === undefined) {
          bdata["blueprint"] = 0;
        }
        bdata["explorable"] = isExplorable(bdata["type"]);
        var gdn = calcGDN(xcell["rx"], xcell["ry"]);
        if (awr[gdn] === undefined) {
          awr[gdn] = {};
        }
        if (bdata["x"] !== undefined && bdata["y"] !== undefined) {
          awr[gdn][a] = bdata;
        }
        a++;
      }
    }
  }
  for (c in data.scout) {
    var xcell = data.scout[c];
    if (xcell.pbl !== undefined && xcell.pbl === 1) {
      var coord = c.split("y")[1].split("x");
      if (
        data.map["y" + coord[0]] === undefined ||
        data.map["y" + coord[0]]["x" + coord[1]] === undefined
      ) {
        pbl++;
        var bdata = {};
        bdata["name"] =
          '<em style="color:#666;">' + __("Vermutetes Gebäude") + "</em>";
        bdata["x"] = coord[1] - data.tx;
        bdata["y"] = data.ty - coord[0];
        bdata["ap"] = calcAP(bdata["x"], bdata["y"]);
        bdata["km"] = calcKM(bdata["x"], bdata["y"]);
        bdata["blueprint"] = 0;
        var gdn = calcGDN(bdata["x"], bdata["y"]);
        if (awr[gdn] === undefined) {
          awr[gdn] = {};
        }
        awr[gdn][a] = bdata;
        a++;
      }
    }
  }

  for (gdn in awr) {
    var stn = __(data.stormnames[gdn]);
    var stc = 0;
    var stt = "";
    for (sto in data.storm) {
      if (parseInt(data.storm[sto]) === parseInt(gdn) + 1) {
        stc++;
      }
    }
    if (stc > 0) {
      stt = " <em>(" + stc + "x " + __("Sturm") + ")</em>";
    }
    var awrHeaderRow = $(document.createElement("tr"))
      .addClass("awr-header")
      .html('<th colspan="3">' + stn + stt + "</th>");
    awrTable.append(awrHeaderRow);
    var gdl = awr[gdn];
    for (i in gdl) {
      var b = gdl[i];
      var awrRow = $(document.createElement("tr"))
        .addClass("awr-entry")
        .html(
          '<td class="pos-stat ' +
            (b.dried !== undefined && b.dried === 1
              ? "ruin-empty"
              : "ruin-regen") +
            '"><abbr onmouseover="highlightSpecialZone(' +
            b.x +
            "," +
            b.y +
            ');" onmouseout="downdarkSpecialZone(' +
            b.x +
            "," +
            b.y +
            ');" onclick="$(\'#x' +
            b.x +
            "y" +
            b.y +
            '\').click();" class="ruin-coords" title="' +
            b.ap +
            "AP (" +
            b.km +
            'km)">[' +
            b.x +
            "|" +
            b.y +
            "]</abbr></td><td>" +
            '<img src="/assets/css/img/' +
            (b.explorable === true
              ? "explore"
              : b.blueprint !== undefined && b.blueprint === 1
              ? "no-bp"
              : b.km >= 10
              ? "bp"
              : "gp") +
            '.png">' +
            "</td><td>" +
            ((b.type === undefined
              ? b.name
              : data.buildings[b.type][data.langcode]) !==
            __("Ein nicht freigeschaufeltes Gebäude.")
              ? b.type === undefined
                ? b.name
                : data.buildings[b.type][data.langcode]
              : '<em style="color:#c00;"> ' +
                b.dig +
                " " +
                __("Haufen") +
                ' <img style="vertical-align:text-bottom;height:14px;" src="/assets/css/img/h_dig.gif"></em>') +
            "</td>"
        );
      awrTable.append(awrRow);
    }
  }
  let totalRuins = 21;
  if (data.system.hard === 1) {
    totalRuins = 22;
  } else if (
    data.height >= 12 &&
    data.height <= 14 &&
    data.width >= 12 &&
    data.width <= 14
  ) {
    totalRuins = 7;
  }
  awrBox
    .html(
      '<p class="desc">' +
        __("{%1} ", [zones]) +
        __("von") +
        __(" {%1} ", [data.height * data.width]) +
        __("Zonen entdeckt") +
        __(" ({%1}%).", [
          Math.round((zones / data.height / data.width) * 100),
        ]) +
        "<br>" +
        __("{%1} ", [ruins]) +
        __("von") +
        " " +
        totalRuins +
        " " +
        "( +" +
        __("{%1} ", [pbl]) +
        __("probably") +
        " ) " +
        __("Ruinen entdeckt") +
        __(" ({%1}%).", [Math.round((ruins / totalRuins) * 100)]) +
        "</p>"
    )
    .append(awrTable);
}

function generateCitizenList() {
  var citCount = 0,
    liveCount = 0,
    deadCount = 0;
  if (data.citizens !== undefined) {
    let count = 0;
    for (let c in data.citizens) {
      count++;
    }
    citCount = count;
    liveCount = count;
  }
  if (data.cadavers !== undefined) {
    let count = 0;
    for (let d in data.cadavers) {
      if (d >= 0) {
        for (let c in data.cadavers[d]) {
          count++;
        }
      }
    }
    citCount = citCount + count;
    deadCount = count;
  }

  var citBox = $("#citizens");
  var citStats = $(document.createElement("p"))
      .addClass("cit-stats")
      .html(
        citCount +
          " " +
          __("Bürger") +
          ": " +
          liveCount +
          " " +
          __("alive") +
          " / " +
          deadCount +
          " " +
          __("dead")
      );

    var profStats = $(document.createElement("div"));
    profStats.addClass("prof-stats-panel");

    var profession = [
      "basic",
      "guardian",
      "hunter",
      "collec",
      "survivalist",
      "tamer",
      "tech",
      "shaman"
    ];

    for (p in profession) {
      profCount = 0;
      for (let c in data.citizens) {
        if (data.citizens[c].job == profession[p]) {
          profCount++;
        }
      }
      profStatsJob = $(document.createElement("div")).addClass("prof-stats").append($(document.createElement("span")).addClass("prof-stats-job zone-citizen-" + profession[p]).append("x" + profCount));
      if (!(profession[p] == "shaman" && profCount == 0)) {
        profStats.append(profStatsJob);
      }

    }
    citBox.html("").append(citStats).append(profStats);

  if (data.citizens !== undefined) {
    var citTable = $(document.createElement("table")).addClass("cit-table");
    var cit = {};

    for (cid in data.citizens) {
      let name = data.citizens[cid]["name"].trim().toUpperCase();
      cit[name] = cid;
    }
    let keys = Object.keys(cit),
      i,
      len = keys.length;

    keys.sort();
    for (i = 0; i < len; i++) {
      let k = keys[i];
      let cid = cit[k];
      var ctd = data.citizens[cid];
      let shunnedicon = "";
      let shunneddiv = "";

      //activity icon still todo
      //let acticityicon = "";
      //acticityicon = '<img src="css/img/uptodate.png" class="citizen-activity"></span>'

      if (ctd.ban == 1) {
        shunnedicon =
          '<span class="zone-citizen zone-citizen-shunnedicon"></span>';
      }
      if (ctd.ban == 1) {
        shunneddiv = "-shunned";
      }
      var citRow = $(document.createElement("tr"))
        .addClass("cit-entry")
        .html(
          '<td class="pos-stat"><abbr onmouseover="highlightSpecialZone(' +
            ctd.rx +
            "," +
            ctd.ry +
            ');" onmouseout="downdarkSpecialZone(' +
            ctd.rx +
            "," +
            ctd.ry +
            ');" onclick="$(\'#x' +
            ctd.rx +
            "y" +
            ctd.ry +
            '\').click();" class="ruin-coords">[' +
            ctd.rx +
            "|" +
            ctd.ry +
            ']</abbr></td><td><span class="zone-citizen' +
            shunneddiv +
            " zone-citizen-" +
            ctd.job +
            '">' +
            ctd.name +
            "</span>" +
            shunnedicon +
            "</td>"
        );
      citTable.append(citRow);
    }
    citBox.append($(document.createElement("hr"))).append(citTable);
  }
  if (data.cadavers !== undefined) {
    var cadTable = $(document.createElement("table")).addClass("cad-table");

    for (var d = data.system.day; d >= 0; d--) {
      if (data.cadavers[d] !== undefined) {
        var cadRow = $(document.createElement("tr"))
          .addClass("cad-day")
          .html('<td colspan="3">' + __("Tag") + " " + d + "</td>");
        cadTable.append(cadRow);

        var cad = [];
        for (cid in data.cadavers[d]) {
          cad.push([cid, data.cadavers[d][cid]["name"]]);
          reMoveCitizen(cid);
        }
        cad.sort(function (a, b) {
          return a[1] > b[1];
        });

        for (cco in cad) {
          var ctd = data.cadavers[d][cad[cco][0]];
          var cadRow = $(document.createElement("tr"))
            .addClass("cad-entry")
            .html(
              '<td class="dtype dtype-' +
                ctd.dtype +
                '" title="' +
                __("Tod Typ " + ctd.dtype) +
                '"></td><td>' +
                ctd.name +
                '</td><td class="pos-stat" ' +
                (ctd.rx !== undefined
                  ? 'title="' +
                    __("last-known-position") +
                    ": [" +
                    ctd.rx +
                    "|" +
                    ctd.ry +
                    ']"'
                  : "") +
                ">" +
                (ctd.rx !== undefined
                  ? '<abbr onmouseover="highlightSpecialZone(' +
                    ctd.rx +
                    "," +
                    ctd.ry +
                    ');" onmouseout="downdarkSpecialZone(' +
                    ctd.rx +
                    "," +
                    ctd.ry +
                    ');" onclick="$(\'#x' +
                    ctd.rx +
                    "y" +
                    ctd.ry +
                    '\').click();" class="ruin-coords">[' +
                    ctd.rx +
                    "|" +
                    ctd.ry +
                    "]</abbr>"
                  : "") +
                "</td>"
            );
          cadTable.append(cadRow);
        }
      }
    }
    citBox.append(cadTable);
  }
}

function updateTabsInfo(bpsAndConstructions, constructions) {
  data.bpsAndConstructions = bpsAndConstructions;
  data.constructions = constructions;
  generateBPsTab();
  generateConstructionTab();
}

function alternateColorsConstructionTable() {
  let visibleRows = document.querySelectorAll('.bps-constructions-table tbody #bps-constructions-row:not([style*="display: none"]):not(.hideme)');
  visibleRows.forEach((row, index) => {
    if (index % 2 === 0) {
      row.style.backgroundColor = '#fff';
    } else {
      row.style.backgroundColor = '#eee';
    }
  });
}

function updateConstructionsTabHeight() {
  document.getElementById('CONSTRUCTIONS').style.minHeight = `${document.querySelector('.bps-constructions-planner').offsetHeight + 60}px`;
  $("#fm-content").css(
      "height",
      Math.max(
          $("#CONSTRUCTIONS").height() + $(".tab").height(),
          $("#map-wrapper").height()
      ).toString() + "px"
  );
}

function updateConstructionsFilterHighlight() {
  let filter = document.getElementById("constructions-filter-search").value.trim().toLowerCase();
  let rows = document.querySelectorAll(".bps-constructions-table tr");

  rows.forEach(function (row) {
    let constructionNameElement = row.querySelector(".construction-name");
    if (constructionNameElement !== null) {
      let text = constructionNameElement.textContent;
      let regex = new RegExp(filter, "ig");

      text = text.replace(regex, (match) => {
        return "<mark>" + match + "</mark>";
      });

      constructionNameElement.innerHTML = text;

      let isMatch = constructionNameElement.textContent.toLowerCase().includes(filter);
      // Code to hide items that do not match
      if (isMatch) {
        row.style.display = "";  // Show the row
      } else {
        row.style.display = "none";  // Hide the row
      }

      // hide related progress
      let previousRow = row.previousElementSibling;
      if (previousRow.classList.contains("progress-container")) {
        previousRow.style.display = isMatch ? "" : "none";
      }

    }
  });

  // Alternative color in rows
  alternateColorsConstructionTable();

  // Update height
  updateConstructionsTabHeight();
}

function updateBPTrackerFilterHighlight() {
  var filter = $("#bps-filter-search").val().trim().toLowerCase();
  $(".bps-tracker-field").each(function () {
    var text = $(this).find("span").text();
    var regex = new RegExp(filter, "ig");
    text = text.replace(regex, (match, $1) => {
      return "<mark>" + match + "</mark>";
    });

    $(this).find("span").html(text);

    // Code to hide items that does not match
    // if ($(this).find("span").text().toLowerCase().includes(filter)) {
    // 	$(this).show();
    // } else {
    // 	$(this).hide();
    // }
  });
}

function generateConstructionsTabsOnPageLoad() {
  for (y in data.map) {
    if (y.match(/^y\d{1,2}$/i) === null) {
      continue;
    }
    var yrow = data.map[y];
    for (x in yrow) {
      if (x.match(/^x\d{1,2}$/i) === null) {
        continue;
      }
      var xcell = yrow[x];
      if (xcell.building !== undefined && xcell.building.name !== undefined) {
        var bdata = xcell.building;
        if (!isExplorable(bdata["type"])) {
          continue;
        }
        if (bdata["type"] === 61) {
          $("#show-bp-bunker").attr("checked", "checked");
        } else if (bdata["type"] === 62) {
          $("#show-bp-hotel").attr("checked", "checked");
        } else if (bdata["type"] === 63) {
          $("#show-bp-hospital").attr("checked", "checked");
        }
      }
    }
  }
  generateBPsTab();
  generateConstructionTab();
}

function toggleConstructionCheckbox(constructionRow) {
  const checkbox = constructionRow.querySelector('input[type="checkbox"]');
  checkbox.checked = !checkbox.checked;

  const event = new Event('click', { bubbles: true });
  checkbox.dispatchEvent(event);
}

function repositionConstructionPlanner(){
  const planner = document.querySelector('.bps-constructions-planner');
  const scrollY = window.scrollY + window.visualViewport.offsetTop;
  if (scrollY < 187) {
    planner.style.top = '100px';
  } else {
    planner.style.top = `${100 + (scrollY - 187)}px`;
  }
  planner.style.maxHeight = `${window.innerHeight-100}px`;
}

function calculateWorkshopConversions(){
  let bankTemporary = JSON.parse(JSON.stringify(data.bank));

  // Remove already necessary items from the available bank of resources
  const resources = document.querySelectorAll('[id^="rplanner-"]');
  Array.from(resources).map(resource => {
    const id = resource.id.replace('rplanner-', '');
    const bankResources = resource.querySelector('.total-resources')?.textContent.trim() || '0';
    if (bankTemporary[id] !== undefined) bankTemporary[id]["count"] -= parseInt(bankResources);
  });

  let conversionsNecessary = [];  // ex. [ "49" : [["139", "20", "20"]]]  => means [ "plank" : [["rotting log", "20 conversions"]]]
  Array.from(document.getElementById("resources-planner-list").children).forEach(function (div) {
    let totalRes = parseInt(div.querySelector('.total-resources').textContent);
    let bankRes = parseInt(div.querySelector('.bank-resources').textContent);
    let resourceId = div.id.split("-")[1];
    if (bankRes < totalRes) {
      let missingCount = parseInt(totalRes) - parseInt(bankRes);
      // Check workshop
      if (data.workshopConversions[resourceId] !== undefined) {
        conversionsNecessary[resourceId] = [];
        let conversionCount = 0;
        for (let conversionSource in data.workshopConversions[resourceId]) {
          let conversionSourceId = data.workshopConversions[resourceId][conversionSource][0];
          let conversionSourceCost = parseInt(data.workshopConversions[resourceId][conversionSource][1]);
          if (bankTemporary[conversionSourceId] !== undefined && bankTemporary[conversionSourceId]["count"] > 0 ){
            conversionCount = Math.min(bankTemporary[conversionSourceId]["count"], missingCount);
            missingCount -= conversionCount;
            bankTemporary[conversionSourceId]["count"] -= conversionCount;
            conversionsNecessary[resourceId].push([conversionSourceId, (conversionCount * conversionSourceCost).toString(), conversionCount]);
          }
          if (missingCount === 0)
            break;
        }
        let missingMsg = ")";
        if (missingCount > 0) {
          missingMsg = ", " + missingCount + " " + __("missing!") + ")";
        } else {
          document.getElementById("rplanner-" + resourceId).style.color = 'black';
        }
        document.getElementById("workshop-message-" + resourceId).innerHTML = "(<span class='total-conversions'>" + conversionCount + "</span> " + __("via conversion") + missingMsg;
      }
    } else {
      document.getElementById("workshop-message-" + resourceId).innerHTML = "";
    }
  });

  // Fill this info in the planner
  let cost = 3;
  if(document.getElementById('hacksaw-available').checked)
    cost--;
  if(document.getElementById('factory-built').checked)
    cost--;

  let workshopPlannerListEl = document.getElementById('workshop-planner-list');
  let totalConversionAp = 0;
  workshopPlannerListEl.innerHTML = "";
  for (let resourceSourceId in conversionsNecessary) {
    for (let resourceConversion in conversionsNecessary[resourceSourceId]) {
      totalConversionAp += parseInt(conversionsNecessary[resourceSourceId][resourceConversion][1]) * cost;
      let conversionStr = "<span>" +
          // '<span class="total-conversions">' + conversionsNecessary[resourceSourceId][resourceConversion][1] + '</span>' + 'x' +
          '<span class="total-conversions">' + conversionsNecessary[resourceSourceId][resourceConversion][2] + '&nbsp</span>' + 'x&nbsp' +
          '<span class="from-resource-id" style="display: none;">' + conversionsNecessary[resourceSourceId][resourceConversion][0] + '</span>' +
          '<span class="to-resource-id" style="display: none;">' + resourceSourceId + '</span>' +
          '<img style="width: 15px;" class="construction-workshop" src="https://myhord.es/build/images/' +
          data.items[conversionsNecessary[resourceSourceId][resourceConversion][0]]["image"] + '"/>' +
          "\u2794" +
          '<img style="width: 15px;" class="construction-workshop" src="https://myhord.es/build/images/' +
          data.items[resourceSourceId]["image"] + '"/>' +
          '&nbsp;&nbsp;&nbsp;' + (parseInt(conversionsNecessary[resourceSourceId][resourceConversion][1]) * cost).toString() +
          '<img className="construction-icon" src="/assets/css/img/small_pa.gif"/>' +
          // + " aps \"\u2794 to produce " +
          // + conversionsNecessary[resourceSourceId][resourceConversion][2]
          "</span>";
      workshopPlannerListEl.insertAdjacentHTML('beforeend', conversionStr);
    }
  }

  $("#constructions-planner-conversions-ap").text(totalConversionAp);
  $("#constructions-planner-ap").text(totalConversionAp + parseInt($("#constructions-planner-constructions-ap").text()));

}

function existJobShaman() {
  for (const citizen in data.citizens) {
    if (data.citizens[citizen].job === "shaman") {
      return true;
    }
  }
  return false;
}

function generateConstructionTab() {
  document.getElementById('CONSTRUCTIONS-BODY').innerHTML = '';
  document.getElementById('resources-planner-list').innerHTML = '';
  document.getElementById('workshop-planner-list').innerHTML = '';
  document.getElementById('constructions-planner-list').innerHTML = '';
  document.getElementById('constructions-planner-ap').textContent = '0';
  document.getElementById('constructions-planner-conversions-ap').textContent = '0';
  document.getElementById('constructions-planner-constructions-ap').textContent = '0';
  document.getElementById('plan-box').innerHTML = '';
  document.getElementById('constructions-filter-search').value = '';
  document.getElementById('hide-unavailable-constructions').checked = true;
  document.getElementById('hide-already-built-constructions').checked = false;

  // Check for hacksaw and factory
  if (140 in data.bank)
    document.getElementById('hacksaw-available').checked = true;

  if (60 in data.constructions)
    document.getElementById('factory-built').checked = true;

  // Put the flags that are not the current language
  const lang = ["en", "fr", "de", "es"].filter(lang => lang !== data.langcode);
  document.getElementById("flag-lang-1").src = "/assets/css/img/lang_" + lang[0] + ".png";
  document.getElementById("lang-1").value = lang[0];
  document.getElementById("flag-lang-2").src = "/assets/css/img/lang_" + lang[1] + ".png";
  document.getElementById("lang-2").value = lang[1];
  document.getElementById("flag-lang-3").src = "/assets/css/img/lang_" + lang[2] + ".png";
  document.getElementById("lang-3").value = lang[2];

  // Check what are the root constructions
  let rootConstructions = [];
  for (let constructionId in data.bpsAndConstructions) {
    if (data.bpsAndConstructions[constructionId]["parent"] === 0) {
      rootConstructions.push(constructionId);
    }
  }

  // Create the rows, starting with root constructions
  for (let constructionId in rootConstructions) {
    addConstructionToTable(rootConstructions[constructionId], 0);
  }

  // Put the all row clickable to select the checkbox
  document.querySelectorAll('#bps-constructions-table #bps-constructions-row:not(.construction-already-built)').forEach(function(row) {
    row.addEventListener('click', function(event) {
      // Check if the target is inside the checkbox itself, to not click two times
      if (event.target.type === 'checkbox') {
        return;
      }

      // Otherwise, toggle the checkbox inside the row
      toggleConstructionCheckbox(row);
    });
  });

  // Alternative color in rows
  alternateColorsConstructionTable();

  // Fix the footer
  document.querySelector('.bps-constructions-planner').style.maxHeight = `${window.innerHeight}px`;
}

function addConstructionToTable(constructionId, depth) {

  // If it is the shaman construction, and donºt have shaman in town, do not add it
  if ([91, 93, 92, 90].includes(constructionId) && !existJobShaman())
    return;

  let bpIcon = "";
  let constructionTemporary = "";
  let constructionResources = "";

  let trclass = "construction-" + constructionId + " ";
  let checkBoxId = constructionId;
  let waterAdded = "";
  let ap = "";
  let progressDiv = "";
  let availableForConstruction = '<input style="width: 15px; height: 15px" autocomplete="off" type="checkbox" name="select-construction" id="select-construction" value="' + checkBoxId + '">';

  // Calculate AP considering the workshop level
  let workshopLvls = [0, 0.06, 0.12, 0.18, 0.25, 0.35];
  let workshopPerc = 0;
  if (data.constructions !== undefined && data.constructions[76] !== undefined && data.constructions[76].level !== undefined)
    workshopPerc = workshopLvls[data.constructions[76].level];
  let paWithWorkshop = parseInt(data.bpsAndConstructions[constructionId]["pa"]) * (1 - workshopPerc);

  // Build name and check if available
  let name = '<span class="construction-name">' + data.bpsAndConstructions[constructionId]["name"][data.langcode] + '</span>';
  let notAvailable = '';
  if ((data.bpsAndConstructions[constructionId]['rarity'] > 0 && data.bpsAndConstructions[constructionId]['found'] === null) ||
      (data.bpsAndConstructions[constructionId]['rarity'] > 0 && !data.bpsAndConstructions[constructionId]['found'])) {
    name = '<span class="construction-name">' + data.bpsAndConstructions[constructionId]["name"][data.langcode] + '</span>';
    trclass += "construction-not-available hideme";
    notAvailable = '<div style="position: absolute;top: 0;left: 0;right: 0;bottom: 0;background-color: rgba(196, 190, 180, 0.60);z-index: 1;"></div>';
  }

  // Check if it is already built
  if (constructionId in data.constructions) {
    trclass += "construction-already-built ";
    availableForConstruction = '<img style="width: 15px; height: 15px; vertical-align: middle;" src="/assets/css/img/check.png"</img>';
  } else {
    // Make the list of necessary resources
    for (let resource in data.bpsAndConstructions[constructionId]["resources"]) {
      let resourceId = data.bpsAndConstructions[constructionId]["resources"][resource]["rsc"]["id"];
      resourceAmountInBank = 0;
      if (data.bank[resourceId] !== undefined)
        resourceAmountInBank = data.bank[resourceId]["count"]

      let spanResourceStyle = 'display: inline-block; ';
      if (resourceAmountInBank < data.bpsAndConstructions[constructionId]["resources"][resource]["amount"])
        spanResourceStyle += 'color: red;';
      else
        spanResourceStyle += 'color: green;';

      constructionResources += '<span style="' + spanResourceStyle + '">';
      constructionResources += '<img class="construction-resource" src="https://myhord.es/build/images/' +
          data.bpsAndConstructions[constructionId]["resources"][resource]["rsc"]["img"] + '"/>' +
          resourceAmountInBank + '/' + data.bpsAndConstructions[constructionId]["resources"][resource]["amount"]
      constructionResources += '</span>';
    }

    if (data.bpsAndConstructions[constructionId]["water"] !== undefined) {
      waterAdded = "<span style='display: inline-block; text-decoration: none;'>" + data.bpsAndConstructions[constructionId]["water"] + "</span><img class=\"construction-icon\" src=\"/assets/css/img/item_water.gif\"/>";
    }

    // Calculate AP
    if (data.bpsAndConstructions[constructionId]["paleft"] !== undefined) {
      // pa * 0.82 - (pa - actions)
      let paLeft = paWithWorkshop - (
          parseInt(data.bpsAndConstructions[constructionId]["pa"]) - parseInt(data.bpsAndConstructions[constructionId]["paleft"]));
      let percentage = Math.trunc((1 - Math.round(paLeft) / Math.round(paWithWorkshop)) * 100);
      progressDiv = "<div class=\"progress-container\"><div class=\"progress-bar\" style=\"width: " + percentage + "%;\"></div></div>";
      ap = "<span style='display: inline-block; text-decoration: none;'>" +
          Math.round(paLeft) + "</span>" +
          "<img style='" + (notAvailable !== '' ? 'opacity: 0.4': '') + "' class=\"construction-icon\" src=\"/assets/css/img/small_pa.gif\"/>" +
          "<div class='hoverbox-left'>Total AP: " + Math.round(paWithWorkshop) + "</div";
    } else {
      ap = "<span style='display: inline-block; text-decoration: none;'>" +
          Math.round(paWithWorkshop) + "</span>" +
          "<img style='" + (notAvailable !== '' ? 'opacity: 0.4': '') + "' class=\"construction-icon\" src=\"/assets/css/img/small_pa.gif\"/>";
    }

    if (data.bpsAndConstructions[constructionId]["rarity"] === 1) {
      bpIcon = '<img ' + ' src="/assets/css/img/item_bplan_c.gif"/>';
    } else if (data.bpsAndConstructions[constructionId]["rarity"] === 2) {
      bpIcon = '<img ' + ' src="/assets/css/img/item_bplan_u.gif"/>';
    } else if (data.bpsAndConstructions[constructionId]["rarity"] === 3) {
      bpIcon = '<img ' + ' src="/assets/css/img/item_bplan_r.gif"/>';
    } else if (data.bpsAndConstructions[constructionId]["rarity"] === 4) {
      bpIcon = '<img ' + ' src="/assets/css/img/item_bplan_e.gif"/>';
    } else if (data.bpsAndConstructions[constructionId]["rarity"] === 6) {
      return; // wood dump and other constructions does not exist in this season and are return rarity 6
    }
  }

  // Check if it is a temporary construction
  if (data.bpsAndConstructions[constructionId]["temporary"])
    constructionTemporary = '<img class="construction-icon" src="/assets/css/img/small_warning.gif"/>';


  // Calculate de defense, and the defense per water, nuts and ap
  let def = "";
  if (data.bpsAndConstructions[constructionId]["def"] > 0) {
    const constructionDef = data.bpsAndConstructions[constructionId]["def"]
    let defPerWater = '';
    let defPerNuts = '';
    for (let resource in data.bpsAndConstructions[constructionId]["resources"]) {
      if (data.bpsAndConstructions[constructionId]["resources"][resource]["rsc"]["id"] === 35) {
        defPerNuts = __('Defense') + ' ' + __('per') + ' ' + __('N&B') + ': ' + (constructionDef / parseInt(data.bpsAndConstructions[constructionId]["resources"][resource]["amount"])).toFixed(2) + "<br>";
      } else if (data.bpsAndConstructions[constructionId]["resources"][resource]["rsc"]["id"] === 1) {
        defPerWater = __('Defense') + ' ' + __('per') + ' ' + __('water') + ': ' + (constructionDef / parseInt(data.bpsAndConstructions[constructionId]["resources"][resource]["amount"])).toFixed(2) + "<br>";
      }
    }
    def = "<span style='display: inline-block; text-decoration: none;'>" +
        constructionDef + "</span>" +
        "<img style='" + (notAvailable !== '' ? 'opacity: 0.4': '') + "' class=\"construction-icon\" src=\"/assets/css/img/small_def.gif\"/>" +
        "<div class='hoverbox-right'>" + __('Defense') + ' ' + __('per') + ' ' + __('AP') + ": " + (constructionDef / paWithWorkshop).toFixed(2) + "<br>" + defPerWater + defPerNuts + "</div>";
  }

  // If it is a root construction add the background
  let constructionTree = '';
  let prefix = '';
  if (depth > 0) {
    constructionTree = '<img src="/assets/css/img/small_parent2.gif"/>'.repeat(Math.max(0, depth - 1)) +
        '<img src="/assets/css/img/small_parent.gif"/>';
  } else {
    // Is a root construction
    let spaceBetweenClass = "";
    if (constructionId !== "28") // the first root don't have space before
      spaceBetweenClass = "constructions-separator";

    let imgClass = "";
    if (constructionId === "28")
      imgClass = "constructions-wall-separator";
    else if (constructionId === "50")
      imgClass = "constructions-pump-separator";
    else if (constructionId === "76")
      imgClass = "constructions-workshop-separator";
    else if (constructionId === "103")
      imgClass = "constructions-watchtower-separator";
    else if (constructionId === "137")
      imgClass = "constructions-foundations-separator";
    else if (constructionId === "156")
      imgClass = "constructions-sanctuary-separator";
    prefix = "<tr>" +
        '<td colspan="8" class="' + spaceBetweenClass + '"></td>' +
        "</tr>" +
        "<tr>" +
        '<td colspan="8" class="' + imgClass + '"></td>' +
        "</tr>";
  }

  document.getElementById("CONSTRUCTIONS-BODY").insertAdjacentHTML('beforeend',
      prefix +
      progressDiv +
      "<tr id='bps-constructions-row' class='" + trclass + "'>" +
      "<td style='width: 300px;'>" +
      notAvailable +
      availableForConstruction +
      constructionTree +
      '<img class="construction-icon" src="https://myhord.es/build/images/' +
      data.bpsAndConstructions[constructionId]["img"] + '"/>' +
      name +
      "</td>" +
      "<td>" + bpIcon + "</td>" +
      "<td>" + constructionTemporary + "</td>" +
      "<td class='hover-container' style='width: 44px; z-index: 2; text-align: end;'>" + ap + "</td>" +
      "<td class='hover-container' style='width: 44px; z-index: 2; text-align: end;'>" + def + "</td>" +
      "<td style='width: 44px; text-align: end;'>" + waterAdded + "</td>" +
      "<td class='resource-field'>" + constructionResources + "</td>" +
      "<td>" +
      '<a class="wiki-link" style="' + (notAvailable !== '' ? 'opacity: 0.4': '') + '" href="https://en.mhwiki.org/wiki/' + data.bpsAndConstructions[constructionId]["name"]["en"] + '" target="_blank">' +
      '<img src="/assets/css/img/mhwiki.png" style="vertical-align: middle; width: 16px; height: 16px;"></a>' +
      "</td>" +
      "</tr>");

  // Prevent clicking in row when accessing the wiki
  const links = document.querySelectorAll('.wiki-link');
  links.forEach(link => {
    link.addEventListener('click', function(event) {
      event.stopPropagation();
    });
  });

  for (let childrenId in data.bpsAndConstructions[constructionId]["children"]) {
    addConstructionToTable(data.bpsAndConstructions[constructionId]["children"][childrenId], depth + 1);
  }
}

function generatePlan(lang) {
  document.getElementById('plan-box').insertAdjacentHTML('beforeend',
      '[rp=Fata Morgana ' + dict[lang]["Constructions Planner"] + ' ' + dict[lang]["for"] + ' ' + data.system.gamename + ']\n');

  let totalConstructions = document.getElementById("constructions-planner-list").children.length;
  document.getElementById('plan-box').insertAdjacentHTML('beforeend',
      ':build: [i]' + totalConstructions + ' ' + dict[lang]["Konstruktionen"] + ' [/i]:build:\n');

  Array.from(document.getElementById("constructions-planner-list").children).forEach(function (div) {
    let constructionId = div.id.split("-")[1];
    document.getElementById('plan-box').insertAdjacentHTML('beforeend',
        ':arrowright: ' + data.bpsAndConstructions[constructionId]["name"][lang] + '\n');
  });

  document.getElementById('plan-box').insertAdjacentHTML('beforeend',
      '\n[collapse=' + dict[lang]["Details"] + ']\n');

  let constructionsAp = document.getElementById("constructions-planner-constructions-ap").textContent;
  document.getElementById('plan-box').insertAdjacentHTML('beforeend',
      '[i]' + dict[lang]["Building Cost"] + ': [/i] ' + constructionsAp + ':ap:\n');

  let conversionsAp = document.getElementById("constructions-planner-conversions-ap").textContent;
  document.getElementById('plan-box').insertAdjacentHTML('beforeend',
      '[i]' + dict[lang]["Conversion Cost"] + ': [/i] ' + conversionsAp + ':ap:\n');

  let totalAp = document.getElementById("constructions-planner-ap").textContent;
  document.getElementById('plan-box').insertAdjacentHTML('beforeend',
      '[b]' + dict[lang]["Total Cost"] + ': [/b] ' + totalAp + ':ap:\n');

  document.getElementById('plan-box').insertAdjacentHTML('beforeend',
      '{hr}\n');

  document.getElementById('plan-box').insertAdjacentHTML('beforeend',
      ':bag: [i]' + dict[lang]['Resources from Bank'] + '[/i] :bag:\n');

  Array.from(document.getElementById("resources-planner-list").children).forEach(function (div) {
    let totalRes = parseInt(div.querySelector('.total-resources').textContent);
    let bankRes = parseInt(div.querySelector('.bank-resources').textContent);

    let fromBankRes =  Math.min(totalRes, bankRes);

    if (fromBankRes > 0) {
      document.getElementById('plan-box').insertAdjacentHTML('beforeend',
          ":middot: [b]x" + fromBankRes + "[/b] " + (data.items[
              parseInt(div.querySelector('.resource-id').textContent)]?.["name"]?.[lang] ?? '') + '\n');
    }
  });

  document.getElementById('plan-box').insertAdjacentHTML('beforeend',
      '\n');

  let firstC = true;
  Array.from(document.getElementById("workshop-planner-list").children).forEach(function (span) {
    let totalConversions = parseInt(span.querySelector('.total-conversions').textContent);
    let from = parseInt(span.querySelector('.from-resource-id').textContent);
    let to = parseInt(span.querySelector('.to-resource-id').textContent);

    if (firstC) {
      document.getElementById('plan-box').insertAdjacentHTML('beforeend',
          ':refine: [i]' + dict[lang]["Conversions"] + '[/i] :refine:\n');
      firstC = false;
    }
    document.getElementById('plan-box').insertAdjacentHTML('beforeend',
        ":middot: [b]x" + totalConversions + "[/b] " + data.items[from]["name"][lang] + " :arrowright: " + data.items[to]["name"][lang] + '\n');
  });

  document.getElementById('plan-box').insertAdjacentHTML('beforeend',
      '\n');

  let firstM = true;
  Array.from(document.getElementById("resources-planner-list").children).forEach(function (div) {
    let bankResources = parseInt(div.querySelector('.bank-resources').textContent);
    let totalResources = parseInt(div.querySelector('.total-resources').textContent);
    let totalConversions = 0;

    if (div.querySelector('.total-conversions') !== null) {
      totalConversions = parseInt(div.querySelector('.total-conversions').textContent);
    }

    let missing = totalResources - bankResources - totalConversions;

    if (missing > 0) {
      if (firstM) {
        document.getElementById('plan-box').insertAdjacentHTML('beforeend',
            ':warning: [i]' + dict[lang]["missing!"] + '[/i] :warning:\n');
        firstM = false;
      }
      document.getElementById('plan-box').insertAdjacentHTML('beforeend',
          ":middot: [b]x" + missing + "[/b] " +  (data.items[
              parseInt(div.querySelector('.resource-id').textContent)]?.["name"]?.[lang] ?? '') + '\n');
    }
  });

  document.getElementById('plan-box').insertAdjacentHTML('beforeend',
      '[/collapse]\n');

  document.getElementById('plan-box').insertAdjacentHTML('beforeend', '[/rp]\n');
}

function generateBPsTab() {
  let greensBps = $(".bps-tracker-greens");
  let yellowsBps = $(".bps-tracker-yellows");
  let bluesBps = $(".bps-tracker-blues");
  let purplesBps = $(".bps-tracker-purples");

  let greensTotalCount = 0;
  let yellowsTotalCount = 0;
  let bluesTotalCount = 0;
  let purplesTotalCount = 0;
  let greensFoundCount = 0;
  let yellowsFoundCount = 0;
  let bluesFoundCount = 0;
  let purplesFoundCount = 0;

  $("#bps-total-greens").html(greensTotalCount);
  $("#bps-total-yellows").html(yellowsTotalCount);
  $("#bps-total-blues").html(bluesTotalCount);
  $("#bps-total-purples").html(purplesTotalCount);

  $("#bps-found-greens").html(greensFoundCount);
  $("#bps-found-yellows").html(yellowsFoundCount);
  $("#bps-found-blues").html(bluesFoundCount);
  $("#bps-found-purples").html(purplesFoundCount);

  greensBps.html("");
  yellowsBps.html("");
  bluesBps.html("");
  purplesBps.html("");

  let bpSortable = [];
  for (let bp in data.bpsAndConstructions) {
    bpSortable.push([data.bpsAndConstructions[bp]["name"][data.langcode], bp]);
  }
  bpSortable.sort((a, b) =>
    a[0].toUpperCase().localeCompare(b[0].toUpperCase())
  );

  for (let bpSortedIndex in bpSortable) {
    let bp = bpSortable[bpSortedIndex][1];
    if (data.bpsAndConstructions[bp]["rarity"] === 0) continue;

    let bpSpan = "";
    let bpClass = "";
    let bpDisabled = "";
    let bpBlocked = false;
    if (data.spy === 1) {
      bpDisabled = 'disabled="disabled" ';
    }
    let bpEruin = "";
    let bpEditedBy = "";

    if (
      data.bpsAndConstructions[bp]["parent"] === undefined ||
      data.bpsAndConstructions[data.bpsAndConstructions[bp]["parent"]][
        "rarity"
      ] === 0 ||
      (data.bpsAndConstructions[data.bpsAndConstructions[bp]["parent"]][
        "found"
      ] !== undefined &&
        data.bpsAndConstructions[data.bpsAndConstructions[bp]["parent"]][
          "found"
        ])
    ) {
      bpSpan =
        "<span>" +
        data.bpsAndConstructions[bp]["name"][data.langcode] +
        "</span>";
    } else {
      bpBlocked = true;
      bpDisabled = 'disabled="disabled" ';
      bpSpan =
        '<span style="text-decoration: line-through;">' +
        data.bpsAndConstructions[bp]["name"][data.langcode] +
        "</span>";
      if ($("#hide-bp-blocked").attr("checked")) {
        bpClass += "hidemeimportant bp-blocked ";
      } else {
        bpClass += "bp-blocked ";
      }
    }

    if (data.bpsAndConstructions[bp]["parent"] !== undefined) {
      bpClass +=
        "bps-parent-" + data.bpsAndConstructions[bp]["parent"].toString() + " ";
    }

    if (data.bpsAndConstructions[bp]["eruin"] !== undefined) {
      if (data.bpsAndConstructions[bp]["eruin"].includes("bunker")) {
        let eruinHide = "hideme ";
        if ($("#show-bp-bunker").attr("checked")) eruinHide = "";
        bpEruin +=
          ' <div class="' +
          eruinHide +
          " bp-bunker" +
          ' bp-eruin">' +
          ' <img src="/assets/css/img/small_enter.gif"/> ' +
          ' <div class="bp-eruin-center">1</div> ' +
          "</div> ";
      }
      if (data.bpsAndConstructions[bp]["eruin"].includes("hotel")) {
        let eruinHide = "hideme ";
        if ($("#show-bp-hotel").attr("checked")) eruinHide = "";
        bpEruin +=
          ' <div class="' +
          eruinHide +
          " bp-hotel" +
          ' bp-eruin">' +
          ' <img src="/assets/css/img/small_enter.gif"/> ' +
          ' <div class="bp-eruin-center">2</div> ' +
          "</div> ";
      }
      if (data.bpsAndConstructions[bp]["eruin"].includes("hospital")) {
        let eruinHide = "hideme ";
        if ($("#show-bp-hospital").attr("checked")) eruinHide = "";
        bpEruin +=
          ' <div class="' +
          eruinHide +
          " bp-hospital" +
          ' bp-eruin">' +
          ' <img src="/assets/css/img/small_enter.gif"/> ' +
          ' <div class="bp-eruin-center">3</div> ' +
          "</div> ";
      }
    }

    if (data.bpsAndConstructions[bp]["found"]) {
      if ($("#hide-bp-found").attr("checked")) {
        bpClass += "hidemeimportant bp-found ";
      } else {
        bpClass += "bp-found ";
      }
    }

    if (data.bpsAndConstructions[bp]["username"] !== undefined) {
      bpEditedBy =
        'title="Last edited by ' +
        data.bpsAndConstructions[bp]["username"] +
        '"';
      bpEditedBy =
        '<img height="14" width="14" src="/assets/css/img/info.png" title="Last edited by ' +
        data.bpsAndConstructions[bp]["username"] +
        '"/>';
    }

    let parentHovering = '';
    if (data.bpsAndConstructions[bp]["parent"] !== undefined &&
        data.bpsAndConstructions[data.bpsAndConstructions[bp]["parent"]]["rarity"] !== 0)
      parentHovering = "<div class='hoverbox-down'>" + __("Parent Building") + ": " +
          data.bpsAndConstructions[data.bpsAndConstructions[bp]["parent"]]["name"][data.langcode] +
          "</div>";

    let foundImg = '';
    if (data.bpsAndConstructions[bp]["rarity"] === 1) {
      greensTotalCount++;
      if (data.bpsAndConstructions[bp]["found"]) {
        greensFoundCount++;
        foundImg = '<img style="width: 18px; height: 18px" src="/assets/css/img/check.png"/>';
      } else {
        if (bpBlocked) {
          foundImg = '<img style="opacity: 0.5;width: 18px; height: 18px" src="/assets/css/img/not_found.png"/>'
        } else {
          foundImg = '<img style="width: 18px; height: 18px" src="/assets/css/img/not_found.png"/>'
        }
      }
      greensBps.append(
          '<div class="hover-container ' +
          bpClass +
          'flex-row bps-tracker-field">' +
          foundImg +
          '<img class="bps-tacker-icon" src="/assets/css/img/item_bplan_c.gif"/>' +
          bpEruin +
          bpSpan +
          bpEditedBy +
          parentHovering +
          "</div>"
      );
    } else if (data.bpsAndConstructions[bp]["rarity"] === 2) {
      yellowsTotalCount++;
      if (data.bpsAndConstructions[bp]["found"]) {
        yellowsFoundCount++;
        foundImg = '<img style="width: 18px; height: 18px" src="/assets/css/img/check.png"/>';
      } else {
        if (bpBlocked) {
          foundImg = '<img style="opacity: 0.5;width: 18px; height: 18px" src="/assets/css/img/not_found.png"/>'
        } else {
          foundImg = '<img style="width: 18px; height: 18px" src="/assets/css/img/not_found.png"/>'
        }
      }
      yellowsBps.append(
        '<div class="hover-container ' +
          bpClass +
          'flex-row bps-tracker-field">' +
          foundImg +
          '<img class="bps-tacker-icon" src="/assets/css/img/item_bplan_u.gif"/>' +
          bpEruin +
          bpSpan +
          bpEditedBy +
          parentHovering +
          "</div>"
      );
    } else if (data.bpsAndConstructions[bp]["rarity"] === 3) {
      bluesTotalCount++;
      if (data.bpsAndConstructions[bp]["found"]) {
        bluesFoundCount++;
        foundImg = '<img style="width: 18px; height: 18px" src="/assets/css/img/check.png"/>';
      } else {
        if (bpBlocked) {
          foundImg = '<img style="opacity: 0.5;width: 18px; height: 18px" src="/assets/css/img/not_found.png"/>'
        } else {
          foundImg = '<img style="width: 18px; height: 18px" src="/assets/css/img/not_found.png"/>'
        }
      }
      bluesBps.append(
        '<div class="hover-container ' +
          bpClass +
          'flex-row bps-tracker-field">' +
          foundImg +
          '<img class="bps-tacker-icon" src="/assets/css/img/item_bplan_r.gif"/>' +
          bpEruin +
          bpSpan +
          bpEditedBy +
          parentHovering +
          "</div>"
      );
    } else if (data.bpsAndConstructions[bp]["rarity"] === 4) {
      purplesTotalCount++;
      if (data.bpsAndConstructions[bp]["found"]) {
        purplesFoundCount++;
        foundImg = '<img style="width: 18px; height: 18px" src="/assets/css/img/check.png"/>';
      } else {
        if (bpBlocked) {
          foundImg = '<img style="opacity: 0.5;width: 18px; height: 18px" src="/assets/css/img/not_found.png"/>'
        } else {
          foundImg = '<img style="width: 18px; height: 18px" src="/assets/css/img/not_found.png"/>'
        }
      }
      purplesBps.append(
        '<div class="hover-container ' +
          bpClass +
          'flex-row bps-tracker-field">' +
          foundImg +
          '<img class="bps-tacker-icon" src="/assets/css/img/item_bplan_e.gif"/>' +
          bpEruin +
          bpSpan +
          bpEditedBy +
          parentHovering +
          "</div>"
      );
    }
  }

  $("#bps-total-greens").html(greensTotalCount);
  $("#bps-total-yellows").html(yellowsTotalCount);
  $("#bps-total-blues").html(bluesTotalCount);
  $("#bps-total-purples").html(purplesTotalCount);

  $("#bps-found-greens").html(greensFoundCount);
  $("#bps-found-yellows").html(yellowsFoundCount);
  $("#bps-found-blues").html(bluesFoundCount);
  $("#bps-found-purples").html(purplesFoundCount);

  document.querySelectorAll('.bp-selector').forEach(function(checkbox) {
    checkbox.addEventListener('click', function(e) {
      e.preventDefault(); // Prevent changing the checkbox state
    });
  });

  updateBPTrackerFilterHighlight();
}

function addConstructionToPlanner(constructionId) {
  const parentId = data.bpsAndConstructions[constructionId]["parent"];
  if (parentId !== undefined) {
    const parentCheckbox = document.querySelector('tr.construction-' + parentId +' #select-construction');
    if (parentCheckbox && !parentCheckbox.checked) {
      parentCheckbox.click();
    }
  }

  let workshopLvls = [0, 0.06, 0.12, 0.18, 0.25, 0.35];
  let workshopPerc = 0;
  if (data.constructions !== undefined && data.constructions[76] !== undefined && data.constructions[76].level !== undefined)
    workshopPerc = workshopLvls[data.constructions[76].level];
  let paWithWorkshop = parseInt(data.bpsAndConstructions[constructionId]["pa"]) * (1 - workshopPerc);

  let paLeft = paWithWorkshop;
  if (data.bpsAndConstructions[constructionId]["paleft"] !== undefined) {
    paLeft = paWithWorkshop - (
        parseInt(data.bpsAndConstructions[constructionId]["pa"]) - parseInt(data.bpsAndConstructions[constructionId]["paleft"]));
  }

  $("#constructions-planner-constructions-ap").text(parseInt($("#constructions-planner-constructions-ap").text()) + Math.round(paLeft));
  $("#constructions-planner-list").append("<div id='cplanner-" + constructionId + "'>" +
      '<button style="margin: 2px" id="up-construction-plan">&uarr;</button>' +
      '<button style="margin: 2px" id="down-construction-plan">&darr;</button>' +
      '<img class="construction-icon" src="' + 'https://myhord.es/build/images/' +
      data.bpsAndConstructions[constructionId]["img"] + '"/>' +
      data.bpsAndConstructions[constructionId]["name"][data.langcode] +
      "</div>");

  for (let resourceId in data.bpsAndConstructions[constructionId]["resources"]) {
    let resourceAmountInBank = 0;
    let resourceBankId = data.bpsAndConstructions[constructionId]["resources"][resourceId]["rsc"]["id"];
    if (data.bank[resourceBankId] !== undefined)
      resourceAmountInBank = data.bank[resourceBankId]["count"]

    let newAmount = data.bpsAndConstructions[constructionId]["resources"][resourceId]["amount"];
    if ($("#rplanner-" + resourceBankId + " .total-resources").length === 1) {
      newAmount += parseInt($("#rplanner-" + resourceBankId + " .total-resources").text());
      if (newAmount > resourceAmountInBank)
        $("#rplanner-" + resourceBankId).css('color', 'red');
      $("#rplanner-" + resourceBankId + " .total-resources").text(newAmount);
    } else {
      let missingCss = '';
      if (newAmount > resourceAmountInBank)
        missingCss = 'style="color: red;"'
      let constructionResources = '<div ' + missingCss + ' class="flex-row" id="rplanner-' + resourceBankId + '"><img style="width: 16px; height: 16px;" class="construction-resource" src="https://myhord.es/build/images/' +
          data.bpsAndConstructions[constructionId]["resources"][resourceId]["rsc"]["img"] + '"/>' +
          '<span class="bank-resources">' + resourceAmountInBank + '</span>' + '/' +
          '<span class="total-resources">' + data.bpsAndConstructions[constructionId]["resources"][resourceId]["amount"] + '</span>' +
          '<span class="resource-id" style="display: none;">' + resourceBankId + '</span>' +
          '<span style="color: black;" id="workshop-message-' + resourceBankId + '"></span>' +
          "</div>";
      $("#resources-planner-list").append(constructionResources);

    }
  }
}

function removeConstructionsFromPlanner(constructionId) {
  const children = data.bpsAndConstructions[constructionId]["children"];
  for (const childId of children) {
    const childCheckbox = document.querySelector('tr.construction-' + childId + ' #select-construction');
    if (childCheckbox && childCheckbox.checked) {
      childCheckbox.click();
    }
  }

  $("#cplanner-" + constructionId).remove();
  let workshopLvls = [0, 0.06, 0.12, 0.18, 0.25, 0.35];
  let workshopPerc = 0;
  if (data.constructions !== undefined && data.constructions[76] !== undefined && data.constructions[76].level !== undefined)
    workshopPerc = workshopLvls[data.constructions[76].level];
  let paWithWorkshop = parseInt(data.bpsAndConstructions[constructionId]["pa"]) * (1 - workshopPerc);

  let paLeft = paWithWorkshop;
  if (data.bpsAndConstructions[constructionId]["paleft"] !== undefined) {
    paLeft = paWithWorkshop - (
        parseInt(data.bpsAndConstructions[constructionId]["pa"]) - parseInt(data.bpsAndConstructions[constructionId]["paleft"]));
  }

  $("#constructions-planner-constructions-ap").text(parseInt($("#constructions-planner-constructions-ap").text()) - Math.round(paLeft));
  for (let resourceId in data.bpsAndConstructions[constructionId]["resources"]) {
    let resourceAmountInBank = 0;
    let resourceBankId = data.bpsAndConstructions[constructionId]["resources"][resourceId]["rsc"]["id"];
    if (data.bank[resourceBankId] !== undefined)
      resourceAmountInBank = data.bank[resourceBankId]["count"]

    let newAmount = parseInt($("#rplanner-" + resourceBankId + " .total-resources").text()) - data.bpsAndConstructions[constructionId]["resources"][resourceId]["amount"];
    if (newAmount === 0) {
      $("#rplanner-" + resourceBankId).remove();
    } else {
      if (newAmount <= resourceAmountInBank)
        $("#rplanner-" + resourceBankId).css('color', '');
      $("#rplanner-" + resourceBankId + " .total-resources").text(newAmount);
    }
  }
}

function generateConstructionList() {
  var conBox = $("#town-info");
  conBox.html("");


  conBox.append(
    '<div id="OPEN-BP-TRACKER" class="click button"><div style="height: 20px; display: flex; align-items: center; justify-content: center; margin-top: 3px">' +
      __("button-open-bp-tracker") +
      "</div></div>"
  );
  conBox.append(
    '<div id="OPEN-CONSTRUCTION-PAGE" class="click button"><div style="height: 20px; display: flex; align-items: center; justify-content: center; margin-top: 3px">' +
      __("button-open-construction-page") +
      "</div></div>"
  );


  if (
    data.constructions !== undefined &&
    Object.keys(data.constructions).length > 0 &&
    data.mconstructions !== undefined &&
    data.mconstructions.length > 0
  ) {
    var conHelpBox = $(document.createElement("p"))
      .addClass("desc")
      .html(
        __(
          "Die XML-Daten sind aktuell, aber nicht sehr informativ. Die manuell übertragenen Daten haben höheren Informationswert, können aber nur von Hand an die FM gesendet werden."
        )
      );
    var switchXML = $(document.createElement("a"))
      .addClass("constructions-xml interactive click")
      .html("XML");
    var switchMAN = $(document.createElement("a"))
      .addClass("constructions-man interactive click")
      .html("Manuell");
    conHelpBox.prepend(switchMAN).prepend(switchXML);
    conBox.append(conHelpBox);
  }
  var con = {};
  var a = 0;
  if (
    data.constructions !== undefined &&
    Object.keys(data.constructions).length > 0
  ) {
    var conTable = $(document.createElement("table")).addClass(
      "construction-table construction-table-xml"
    );
    var conHeaderRow = $(document.createElement("tr"))
      .addClass("construction-header")
      .html('<th colspan="2">' + __("Konstruktionen") + "</th>");
    conTable.append(conHeaderRow);
    for (c in data.constructions) {
      var concon = data.constructions[c];
      if (concon.level !== undefined) {
        var level = " (" + concon.level + ")";
      } else {
        var level = "";
      }
      var conRow = $(document.createElement("tr"))
        .addClass("con-entry")
        .html(
          '<td class="con-icon' +
            (concon.temp === 1 ? " temporary" : "") +
            '"><img src="https://myhord.es/build/images/' +
            concon.image +
            '" /></td><td>' +
            concon.name[data.langcode] +
            level +
            "</td>"
        );
      conTable.append(conRow);
    }
    conBox.append(conTable);
  }
  if (data.mconstructions !== undefined && data.mconstructions.length > 0) {
    var conTable = $(document.createElement("table")).addClass(
      "construction-table construction-table-man"
    );
    if (
      data.constructions !== undefined &&
      Object.keys(data.constructions).length > 0
    ) {
      conTable.addClass("hideme");
    }
    if (data.mconststamp !== undefined) {
      var conUpdateRow = $(document.createElement("tr"))
        .addClass("construction-update")
        .html(
          '<td colspan="3">' +
            __("Stand: ") +
            data.mconststamp +
            " " +
            __("Uhr") +
            "</td>"
        );
      conTable.append(conUpdateRow);
    }
    var conHeaderRow = $(document.createElement("tr"))
      .addClass("construction-header")
      .html(
        '<th colspan="2">' +
          __("Konstruktion") +
          "</th><th>" +
          __("???") +
          "</th>"
      );
    conTable.append(conHeaderRow);
    var mconid = 0;
    for (c in data.mconstructions) {
      mconid++;
      var concon = data.mconstructions[c];
      if (concon.level !== undefined) {
        var level = " (" + concon.level + ")";
      } else {
        var level = "";
      }
      var conRow = $(document.createElement("tr"))
        .addClass("mcon-entry")
        .addClass(
          concon.done === 1
            ? "mcon-done"
            : concon.lock === 1
            ? "mcon-lock"
            : "mcon-active"
        )
        .addClass(mconid % 2 === 1 ? "odd" : "even")
        .html(
          '<td class="con-icon' +
            '">' +
            (concon.indent !== undefined && concon.indent > 1
              ? '<img src="/assets/css/img/small_parent.gif" />'
              : "") +
            (concon.indent !== undefined && concon.indent > 0
              ? '<img src="/assets/css/img/small_parent.gif" />'
              : "") +
            '<img src="' +
            concon.image +
            '" /></td><td class="' +
            (concon.indent !== undefined && concon.indent === 0
              ? "item"
              : "subitem") +
            '">' +
            concon.name[data.langcode] +
            level +
            "</td><td>" +
            (concon.done === 1
              ? __("✓")
              : concon.lock === 1
              ? __("x")
              : '<abbr title="' + __(" AP") + '">' + concon.ap + "</abbr>") +
            "</td>"
        );
      conTable.append(conRow);
    }
    conBox.append(conTable);
  }
}

function isExplorable(buildingID) {
  var eba = new Array(61, 62, 63);
  var length = eba.length;
  for (var i = 0; i < length; i++) {
    if (eba[i] === buildingID) return true;
  }
  return false;
}

function generateRuinMap(x, y, floor) {
  let ax,ay,mx,my,rx,ry,rzd,px,py,maprow;
  // Upper Floor
  document.getElementById("upperruinmap").innerHTML = "";
  for (let i = 0; i < 14; i++) {
    maprow = document.createElement("ul");
    maprow.classList.add("maprow");
    for (let j = 0; j < 13; j++) {
      ax = j;
      ay = i;
      mx = "x" + j;
      my = "y" + i;
      rx = j - 8;
      ry = i;
      rzd = null;

      px = "x" + x;
      py = "y" + y;
      maprow.appendChild(generateRuinMapZone(i, j, data.map[py][px]["building"]["ruin"], "upper"));
    }
    let ruinmaprulerF = document.createElement("li");
    ruinmaprulerF.classList.add("mapruler");
    ruinmaprulerF.classList.add("ruler_y" + ry);
    ruinmaprulerF.classList.add("first");
    ruinmaprulerF.innerHTML = ry;
    let ruinmaprulerL = ruinmaprulerF.cloneNode(true);
    ruinmaprulerL.classList.remove("first");
    ruinmaprulerL.classList.add("last");
    maprow.insertBefore(ruinmaprulerF, maprow.firstChild);
    maprow.appendChild(ruinmaprulerL);
    document.getElementById("upperruinmap").appendChild(maprow);
  }
  let ruinmaprulebarT = document.createElement("ul");
  ruinmaprulebarT.classList.add("maprow");
  ruinmaprulebarT.classList.add("maprulebar");
  ruinmaprulebarT.classList.add("maprulebar-top");
  for (let j = 0; j < 13; j++) {
    let ruinmaprulex = document.createElement("li");
    ruinmaprulex.classList.add("mapruler");
    ruinmaprulex.classList.add("ruler_x" + (j - 7));
    ruinmaprulex.innerHTML = j - 7;
    ruinmaprulebarT.appendChild(ruinmaprulex);
  }
  let ruinmapruleoF = document.createElement("li");
  ruinmapruleoF.classList.add("mapcorner");
  ruinmapruleoF.classList.add("first");
  let ruinmapruleoL = ruinmapruleoF.cloneNode(true);
  ruinmapruleoL.classList.remove("first");
  ruinmapruleoL.classList.add("last");
  ruinmaprulebarT.insertBefore(ruinmapruleoF, ruinmaprulebarT.firstChild);
  ruinmaprulebarT.appendChild(ruinmapruleoL);
  let ruinmaprulebarB = ruinmaprulebarT.cloneNode(true);
  ruinmaprulebarB.classList.remove("maprulebar-top");
  ruinmaprulebarB.classList.add("maprulebar-bottom");
  document.getElementById("upperruinmap").insertBefore(ruinmaprulebarT, document.getElementById("upperruinmap").firstChild);
  document.getElementById("upperruinmap").appendChild(ruinmaprulebarB);
  document.getElementById("ruinName").innerHTML = data.map[py][px]["building"]["name"];
  document.getElementsByClassName("ruinCoordsX")[0].innerHTML = x;
  document.getElementsByClassName("ruinCoordsY")[0].innerHTML = y;
  document.getElementsByClassName("ruinFloor")[0].innerHTML = "upper";

  if (
    document.getElementById("ruinCX") &&
    document.getElementById("ruinCY") &&
    document.getElementById("ruinCX").innerHTML !== "" &&
    document.getElementById("ruinCY").innerHTML !== "" &&
    document.getElementById("ruinCX").innerHTML !== undefined &&
    document.getElementById("ruinCY").innerHTML !== undefined &&
    document.getElementById("ruinCX").innerHTML +
    document.getElementById("ruinCY").innerHTML !== "00"
  ) {
    selectUpperRuinZone(document.getElementById("lowerruinmap").querySelector("#x" + document.getElementById("ruinCX").innerHTML + "y" + document.getElementById("ruinCY").innerHTML));
    data.crx = parseInt(document.getElementById("ruinCX").innerHTML);
    data.cry = parseInt(document.getElementById("ruinCY").innerHTML);
  } else {
    selectUpperRuinZone(document.getElementById("upperruinmap").querySelector("#x0y1"));
    data.crx = 0;
    data.cry = 1;
  }

  // Lower Floor
  document.getElementById("lowerruinmap").innerHTML = "";
  for (let i = 1; i < 14; i++) {
    let maprow = document.createElement("ul");
    maprow.classList.add("maprow");
    for (let j = 0; j < 13; j++) {
      ax = j;
      ay = i;
      mx = "x" + j;
      my = "y" + i;
      rx = j - 8;
      ry = i;
      rzd = null;

      px = "x" + x;
      py = "y" + y;
      maprow.appendChild(generateRuinMapZone(i, j, data.map[py][px]["building"]["ruin"], "lower"));
    }
    let ruinmaprulerF = document.createElement("li");
    ruinmaprulerF.classList.add("mapruler");
    ruinmaprulerF.classList.add("ruler_y" + ry);
    ruinmaprulerF.classList.add("first");
    ruinmaprulerF.innerHTML = ry;
    let ruinmaprulerL = ruinmaprulerF.cloneNode(true);
    ruinmaprulerL.classList.remove("first");
    ruinmaprulerL.classList.add("last");
    maprow.insertBefore(ruinmaprulerF, maprow.firstChild);
    maprow.appendChild(ruinmaprulerL);
    document.getElementById("lowerruinmap").appendChild(maprow);
  }
  ruinmaprulebarT = document.createElement("ul");
  ruinmaprulebarT.classList.add("maprow");
  ruinmaprulebarT.classList.add("maprulebar");
  ruinmaprulebarT.classList.add("maprulebar-top");
  for (let j = 0; j < 13; j++) {
    let ruinmaprulex = document.createElement("li");
    ruinmaprulex.classList.add("mapruler");
    ruinmaprulex.classList.add("ruler_x" + (j - 7));
    ruinmaprulex.innerHTML = j - 7;
    ruinmaprulebarT.appendChild(ruinmaprulex);
  }
  ruinmapruleoF = document.createElement("li");
  ruinmapruleoF.classList.add("mapcorner");
  ruinmapruleoF.classList.add("first");
  ruinmapruleoL = ruinmapruleoF.cloneNode(true);
  ruinmapruleoL.classList.remove("first");
  ruinmapruleoL.classList.add("last");
  ruinmaprulebarT.insertBefore(ruinmapruleoF, ruinmaprulebarT.firstChild);
  ruinmaprulebarT.appendChild(ruinmapruleoL);
  ruinmaprulebarB = ruinmaprulebarT.cloneNode(true);
  ruinmaprulebarB.classList.remove("maprulebar-top");
  ruinmaprulebarB.classList.add("maprulebar-bottom");
  document.getElementById("lowerruinmap").insertBefore(ruinmaprulebarT, document.getElementById("lowerruinmap").firstChild);
  document.getElementById("lowerruinmap").appendChild(ruinmaprulebarB);
  document.getElementById("ruinName").innerHTML = data.map[py][px]["building"]["name"];
  document.getElementsByClassName("ruinCoordsX")[0].innerHTML = x;
  document.getElementsByClassName("ruinCoordsY")[0].innerHTML = y;
  document.getElementsByClassName("ruinFloor")[0].innerHTML = "lower";

  if (
    document.getElementById("ruinCX").innerHTML !== "" &&
    document.getElementById("ruinCY").innerHTML !== "" &&
    document.getElementById("ruinCX").innerHTML !== undefined &&
    document.getElementById("ruinCY").innerHTML !== undefined
  ) {
    selectLowerRuinZone(document.getElementById("lowerruinmap").querySelector("#x" + document.getElementById("ruinCX").innerHTML + "y" + document.getElementById("ruinCY").innerHTML));
    data.crx = parseInt(document.getElementById("ruinCX").innerHTML);
    data.cry = parseInt(document.getElementById("ruinCY").innerHTML);
  } else {
    selectLowerRuinZone(document.getElementById("lowerruinmap").querySelector("#x0y1"));
    data.crx = 0;
    data.cry = 1;
  }

  var upperFloorLabel = document.createElement("div");
  upperFloorLabel.classList.add("ruin-floor-label");
  upperFloorLabel.textContent = __("ground-floor");
  document.getElementById("upperruinmap").insertBefore(upperFloorLabel, document.getElementById("upperruinmap").firstChild);

  var lowerFloorLabel = document.createElement("div");
  lowerFloorLabel.classList.add("ruin-floor-label");
  lowerFloorLabel.textContent = __("basement-floor");
  document.getElementById("lowerruinmap").insertBefore(lowerFloorLabel, document.getElementById("lowerruinmap").firstChild);


  redrawAriadneRoute(document.querySelector('.mapzone.selectedZone').getAttribute('ax'), document.querySelector('.mapzone.selectedZone').getAttribute('ay'));
}

function generateRuinMapZone(i, j, ruin, floor) {
  var ax = j;
  var ay = i;
  var mx = "x" + j;
  var my = "y" + i;
  var rx = j - 7;
  var ry = i;
  var rzd = null;
  var mapzone = document.createElement("li");

  mapzone.classList.add("mapzone");
  mapzone.setAttribute("rx", rx);
  mapzone.setAttribute("ry", ry);
  mapzone.setAttribute("ax", ax);
  mapzone.setAttribute("ay", ay);
  mapzone.setAttribute("id", "x" + rx + "y" + ry);

  if (7 === j && 0 === i && floor === "upper") {
    mapzone.classList.add("ruinEntry");
  } else if (0 === i) {
    mapzone.classList.add("ruinFirstRow");
  }

  if (
    ruin[floor] !== undefined &&
    ruin[floor][my] !== undefined &&
    ruin[floor][my][mx] !== undefined
  ) {
    if (
      ruin[floor][my][mx]["items"] !== undefined &&
      ruin[floor][my][mx]["items"].length > 0 &&
      !(rx === 0 && ry === 0)
    ) {
      var zoneItems = ruin[floor][my][mx]["items"];
      for (zi in zoneItems) {
        var zitem = zoneItems[zi];
        if (data.items[zitem.id] !== undefined) {
          var ritem = data.items[zitem.id];
          if (data.mapitems[ritem.category][ritem.id] !== undefined) {
            data.mapitems[ritem.category][ritem.id] += zitem.count;
          } else {
            data.mapitems[ritem.category][ritem.id] = zitem.count;
          }
        }
      }
    }

    var zone = ruin[floor][my][mx];

    if (ruin[floor][my][mx]["tile"] !== undefined) {
      mapzone.classList.add("tile-" + ruin[floor][my][mx]["tile"]);
    }
    if (
      ruin[floor][my][mx]["doorlock"] !== undefined &&
      ruin[floor][my][mx]["doorlock"] !== "undefined"
    ) {
      mapzone.classList.add("doorlock-" + ruin[floor][my][mx]["doorlock"]);
    } else {
      if (ruin[floor][my][mx]["door"] !== undefined) {
        mapzone.classList.add("door-" + ruin[floor][my][mx]["door"]);
      }
      if (ruin[floor][my][mx]["lock"] !== undefined) {
        mapzone.classList.add("lock-" + ruin[floor][my][mx]["lock"]);
      }
    }
    if (ruin[floor][my][mx]["z"] !== undefined) {
      mapzone.setAttribute("z", ruin[floor][my][mx]["z"]);
      mapzone.classList.add("zombie-" + ruin[floor][my][mx]["z"]);
    } else {
      var zc = 0;
      mapzone.setAttribute("z", 0);
      mapzone.classList.add("danger0");
    }
    if (ruin[floor][my][mx]["comment"] !== undefined) {
      mapzone.setAttribute("comment", ruin[floor][my][mx]["comment"]);
      if (ruin[floor][my][mx]["comment"] !== "") {
        mapzone.classList.add("with-comment");
      }
    } else {
      mapzone.setAttribute("comment", "");
    }

    if (
      ruin[floor][my][mx]["updatedOn"] !== undefined &&
      !(rx === 0 && ry === 0)
    ) {
      var udate = new Date(ruin[floor][my][mx]["updatedOn"] * 1000);
      var cdate = new Date();
      var ydate = new Date();
      ydate.setDate(ydate.getDate() - 1);
      var bdate = new Date();
      bdate.setDate(bdate.getDate() - 2);
      if (
        udate.getDate() === cdate.getDate() &&
        udate.getMonth() === cdate.getMonth()
      ) {
        var utd = document.createElement("div");
        utd.classList.add("zone-updated");
        utd.classList.add("zone-updated-today");
        utd.setAttribute("title", __("Heute aktualisiert"));
        mapzone.appendChild(utd);
      } else if (
        udate.getDate() === ydate.getDate() &&
        udate.getMonth() === ydate.getMonth()
      ) {
        var utd = document.createElement("div");
        utd.classList.add("zone-updated");
        utd.classList.add("zone-updated-yesterday");
        utd.setAttribute("title", __("Gestern aktualisiert"));
        mapzone.appendChild(utd);
      } else if (
        udate.getDate() === bdate.getDate() &&
        udate.getMonth() === bdate.getMonth()
      ) {
        var utd = document.createElement("div");
        utd.classList.add("zone-updated");
        utd.classList.add("zone-updated-b4yesterday");
        utd.setAttribute("title", __("Vorgestern aktualisiert"));
        mapzone.appendChild(utd);
      }
    }
  } else if (i > 0) {
    // mapzone.classList.add('nyv');
  }

  return mapzone;
}

function saveRoute() {
  if ($('input[name="routeSave-name"]').val().trim().length === 0) {
    $('input[name="routeSave-name"]').addClass("error");
    ajaxInfo(__("Bitte einen Namen für die Route eingeben."));
    return false;
  }
  if (
    $('input[name="routeSave-no"]:checked').val() === undefined ||
    $('input[name="routeSave-no"]:checked').val() === 0 ||
    $('input[name="routeSave-no"]:checked').val() > 5
  ) {
    ajaxInfo(__("Bitte eine Route wählen."));
    return false;
  }
  eval(
    "var coX = clickRX" + $('input[name="routeSave-no"]:checked').val() + ";"
  );
  eval(
    "var coY = clickRY" + $('input[name="routeSave-no"]:checked').val() + ";"
  );
  var routeString = "";
  for (i in coX) {
    routeString = routeString + coX[i] + "-" + coY[i] + "_";
  }
  if (routeString === "") {
    ajaxInfo(__("Diese Route wurde nicht eingezeichnet."));
    return false;
  }
  let payload = "action=ADDROUTE&rname=" + $('input[name="routeSave-name"]').val().trim() +
      "&route=" + routeString;
  fire("/map/updateRoutes", payload);
}

function populateInterface(dictionary) {
  for (let k in dictionary) {
    let v = dictionary[k];
    let i = $('*[data-trans="' + k + '"]');
    if (i.length > 0) {
      for (let j = 0; j < i.length; j++) {
        let o = i[j];
        o.innerHTML = v;
      }
    }
    let m = $('*[data-transtitle="' + k + '"]');
    if (m.length > 0) {
      for (let l = 0; l < m.length; l++) {
        let p = m[l];
        p.title = v;
      }
    }
  }
}

function enableAriadneEditor() {
  $("#ruinmap-ariadne-activate").addClass("hideme");
  $("#ruinmap-ariadne-deactivate").removeClass("hideme");
  $("#ruinInfoBox").addClass("hideme");
  $("#ruinInfoBox2").addClass("hideme");
  $("#ruinAriadne").removeClass("hideme");
  ariadne = 1;
}

function disableAriadneEditor() {
  $("#ruinmap-ariadne-deactivate").addClass("hideme");
  $("#ruinmap-ariadne-activate").removeClass("hideme");
  $("#ruinInfoBox").removeClass("hideme");
  $("#ruinInfoBox2").removeClass("hideme");
  $("#ruinAriadne").addClass("hideme");
  resetAriadneRoute();
  redrawAriadneRoute(document.querySelector('.mapzone.selectedZone').getAttribute('ax'), document.querySelector('.mapzone.selectedZone').getAttribute('ay'));
  ariadnePath = [];
  ariadneSavedPath = [];
  ariadne = 0;
}

function editRoute(routeId) {
  if (editRouteId !== undefined && editRouteId !== null) {
    document.getElementById(editRouteId).classList.remove("editing-mode");
  }

  resetNewExp();

  newRouteCurrentX = null;
  newRouteCurrentY = null;
  newRouteX = [];
  newRouteY = [];
  currentX = null;
  currentY = null;

  document.getElementById("editing-mode").classList.remove("hideme");
  document.getElementById("cancel-editing").classList.remove("hideme");
  document.getElementById("duplicate-route").classList.remove("hideme");
  document.getElementById("exp-form").classList.remove("hideme");

  document.getElementById("0.1").innerHTML = __("Close route editor");

  // Remove elements with class "new-exp-svg"
  document.querySelectorAll(".new-exp-svg").forEach(function(el) {
      el.remove();
  });

  // Remove parent element of clicked element
  document.querySelectorAll("." + CSS.escape(routeId)).forEach(function(el) {
      el.remove();
  });

  // Route data processing
  const routeData = data.expeditions[routeId];
  const route = routeData.route;
  document.getElementById("expedition-ap").textContent = 0;

  const nameregex = /.*(?= \[)/g;
  document.querySelector('input[name="expe-name"]').value = routeData.name.match(nameregex)[0];
  document.getElementById("exp-day").value = routeData.day;
  document.getElementById("exp-" + routeData.type).checked = true;
  document.querySelector('input[name="expe-color"]').value = routeData.color;

  if (routeData.type === "collaborative" && routeData.creator !== data.system.owner_id) {
      document.querySelectorAll('input[name="expe-type"]').forEach(function(el) {
          el.disabled = true;
      });
  }

  // Process the route points
  const rp = route.split("_");
  for (const rc of rp) {
      const routepoints = rc.split("-");
      if (routepoints[0] === "") break;
      newRouteX.push(routepoints[0]);
      newRouteY.push(routepoints[1]);
      drawExpToPoint(
          routepoints[0],
          routepoints[1],
          routeId,
          routeData.color,
          true
      );
  }

  const editElement = document.getElementById(routeId);
  editElement.classList.add("active-option");
  editElement.classList.add("editing-mode");

  editRouteId = routeId;
}

function cancelRouteEditing() {
  if (editRouteId != undefined && editRouteId != null) {
    document.getElementById(editRouteId).classList.remove("editing-mode");
  }
  resetNewExp();
}

function duplicateRoute() {
  if (editRouteId !== undefined && editRouteId !== null) {
    const element = document.getElementById(editRouteId);
    element.classList.remove("editing-mode");
    element.classList.remove("active-option");
  }

  document.getElementById("editing-mode").classList.add("hideme");
  document.getElementById("cancel-editing").classList.add("hideme");
  document.getElementById("duplicate-route").classList.add("hideme");

  document.querySelector('input[name="expe-name"]').value = "";

  randomizeExpColor();

  const expColor = document.querySelector('input[name="expe-color"]').value;
  document.querySelectorAll(".new-exp-svg").forEach(function(svg) {
      svg.setAttribute("fill", expColor);
      svg.setAttribute("stroke", expColor);
  });

  editRouteId = null;
}

function deleteRoute(routeId) {
  if (confirm(__("Are you sure you want to delete this route?"))) {
    expeditionUpdate(routeId, "DELETE-ROUTE");
    document.querySelectorAll("." + CSS.escape(routeId)).forEach(function(el) {
      el.remove();
    });
    if (editRouteId === routeId) {
      resetNewExp();
    }
  }
}

function addRoute() {
  if (expeditionUpdate(null, "ADD-ROUTE")) {
    // Only if no errors on validators
    $("#exp-form").toggleClass("hideme");
    cleanNewExp();
  }
}

function cleanRoute() {
  if (editRouteId == null) {
    cleanNewExp();
  } else {
    cleanNewExp(false);
  }
}

function toggleDisplayOption(event) {
  const element = event.target;
  const itemClass = element.getAttribute("ref");

  element.classList.toggle("active-option");

  // Toggle visibility of elements with the class stored in itemClass
  document.querySelectorAll("." + itemClass).forEach(el => {
      el.classList.toggle("hideme");
  });

  // Store the state in localStorage
  if (element.classList.contains("active-option")) {
      localStorage.setItem("fm_setting_" + itemClass, 1);
  } else {
      localStorage.setItem("fm_setting_" + itemClass, 0);
  }

  // Perform additional action for specific classes
  if (itemClass === "localtime" || itemClass === "ampmtime") {
      ajaxInfo(__("refresh-info"));
  }
}

function getCookie(name) {
  const value = "; " + document.cookie;
  const parts = value.split("; " + name + "=");
  if (parts.length === 2) {
    return parts.pop().split(";").shift();
  }
}

/**
 * Fire an ajax request to FM server.
 *
 * @param {string} url
 * @param {string} payload
 */
function fire(url, payload, spinner = "", onSuccess = null, callbackMsg = false) {
  if (spinner === "tabs")
    protectTabs(true);
  else if (spinner === "box")
    protectBox(true);

  // Get the JWT token from the cookie
  const token = getCookie("auth_token");

  if (token !== null && token !== '') {
    fetch(url, {
      method: 'POST',
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Requested-With": "XMLHttpRequest",
        "Authorization": `Bearer ${token}`
      },
      body: payload
    })
    .then(response => response.text())
    .then(msg => {
      if (typeof onSuccess === 'function') {
        if (callbackMsg) {
          onSuccess(msg);
        }
        else {
          onSuccess();
        }
      }
      runDynamicScript(msg);
      if (spinner === "tabs")
        protectTabs(false);
      else if (spinner === "box")
        protectBox(false);
    })
    .catch(error => console.error("Error:", error));
  }
  else {
    runDynamicScript(__('CHAOS'));
  }
}

/**
 * Run javascript code from a string. Used to execute code received by asynchronous requests.
 * 
 * @param {string} msg 
 */
function runDynamicScript(msg) {
  const dynascript = document.getElementById('dynascript');

  // Save radius content
  var radiusContent = dynascript.innerHTML;

  dynascript.insertAdjacentHTML('beforeend', msg);
  
  // Extract and execute <script> tags
  const scripts = dynascript.getElementsByTagName('script');
  for (let i = 0; i < scripts.length; i++) {
      eval(scripts[i].innerHTML);
  }

  // Load radius content, so next time it is still executed
  dynascript.innerHTML = radiusContent;
}

function activateEventListeners() {
  // EventListeners for key presses.
  document.addEventListener('keydown', function(e) {
    if (debug) { console.log("Key pressed:", e.key); }
    var filterSearch;
    if (document.getElementById('BPS-TRACKER-TAB').classList.contains('active')) {
      filterSearch = document.getElementById('bps-filter-search');
    } else {
      filterSearch = document.getElementById('constructions-filter-search');
    }

    if (
      currentPanel === 'bps' &&
      (e.key === 'F3' || (e.ctrlKey && e.key === 'f'))
    ) {
      if (document.activeElement === filterSearch) {
        return;
      } else {
        e.preventDefault();
        filterSearch.focus();
      }
    }
  });

  // EventListeners for key releases.
  document.addEventListener('keyup', function(e) {
    if (e.target) {
      switch (e.target.id) {
        case "bps-filter-search":
          updateBPTrackerFilterHighlight();
          break;
        case "constructions-filter-search":
          updateConstructionsFilterHighlight();
          break;

      }
    }
  });  

  // EventListeners for focus events.
  document.addEventListener("focusin", function(e) {
    if (e.target) {
      switch (e.target.id) {
        case "ruinComment":
          ruinEditorKeys = 0;
          break;

      }
    }
  });

  // EventListeners for blur events.
  document.addEventListener("focusout", function(e) {
    if (e.target) {
      switch (e.target.id) {
        case "ruinComment":
          ruinEditorKeys = 1;
          break;

      }
    }
  });

  // EventListeners for click events.
  document.addEventListener("click", function (e) {
    // console.log("Clicked: " + e.target.id);
    // Target has an ID.
    if (e.target && e.target.id) {
      switch (e.target.id) {
        case "select-all":
          const rowsS = document.querySelectorAll('#CONSTRUCTIONS-BODY tr');
          rowsS.forEach(row => {
            const isHiddenByClass = row.classList.contains('hideme');
            const isHiddenByStyle = window.getComputedStyle(row).display === 'none';

            // Proceed only if the row is visible
            if (!isHiddenByClass && !isHiddenByStyle) {
              const checkbox = row.querySelector('input[type="checkbox"][name="select-construction"]');
              if (checkbox && !checkbox.checked) {
                checkbox.click()
              }
            }
          });
          break;
        case "deselect-all":
          const rowsD = document.querySelectorAll('#CONSTRUCTIONS-BODY tr');
          rowsD.forEach(row => {
            const isHiddenByClass = row.classList.contains('hideme');
            const isHiddenByStyle = window.getComputedStyle(row).display === 'none';

            // Proceed only if the row is visible
            if (!isHiddenByClass && !isHiddenByStyle) {
              const checkbox = row.querySelector('input[type="checkbox"][name="select-construction"]');
              if (checkbox && checkbox.checked) {
                checkbox.click()
              }
            }
          });
          break;
        case "invert-selection":
          const rowsI = document.querySelectorAll('#CONSTRUCTIONS-BODY tr');
          rowsI.forEach(row => {
            const isHiddenByClass = row.classList.contains('hideme');
            const isHiddenByStyle = window.getComputedStyle(row).display === 'none';

            // Proceed only if the row is visible
            if (!isHiddenByClass && !isHiddenByStyle) {
              const checkbox = row.querySelector('input[type="checkbox"][name="select-construction"]');
              if (checkbox) {
                checkbox.click()
              }
            }
          });
          break;
        case "hacksaw-available":
          calculateWorkshopConversions();
          break;
        case "factory-built":
          calculateWorkshopConversions();
          break;
        case "generate-plan":
          // Clear the plan-box content
          document.getElementById('plan-box').innerHTML = '';

          generatePlan(data.langcode);
          const langs = {
            'fr': 'Français',
            'de': 'Deutsch',
            'en': 'English',
            'es': 'Español',
          }
          document.querySelectorAll('.plan-languages input[type="checkbox"]').forEach((checkbox) => {
            if (checkbox.checked) {
              const lang = checkbox.value;
              document.getElementById('plan-box').insertAdjacentHTML('beforeend',
                  '[collapse=' + langs[lang] + ']\n');
              generatePlan(lang);
              document.getElementById('plan-box').insertAdjacentHTML('beforeend',
                  '[/collapse]\n');
            }
          });

          // Copy plan
          navigator.clipboard.writeText(document.getElementById("plan-box").value).then(
              () => {
                ajaxInfo('clipboard successfully set');
              },
              () => {
                ajaxInfoBad('clipboard write failed');
              }
          );

          break;
        case "up-construction-plan":
          const prevSibling = e.target.parentNode.previousElementSibling;
          if (prevSibling) {
            e.target.parentNode.parentNode.insertBefore(e.target.parentNode, prevSibling);
          }
          break;
        case "down-construction-plan":
          const nextSibling = e.target.parentNode.nextElementSibling;
          if (nextSibling) {
            e.target.parentNode.parentNode.insertBefore(nextSibling, e.target.parentNode);
          }
          break;
        case "select-construction":
          let constructionId = e.target.value;
          if (e.target.checked) {
            addConstructionToPlanner(constructionId);
          } else {
            removeConstructionsFromPlanner(constructionId)
          }
          // Recalculate workshop
          calculateWorkshopConversions();
          // Update height
          updateConstructionsTabHeight();
          break;
        case "ruinmap-ariadne-activate":
          enableAriadneEditor();
          break;
        case "ruinmap-ariadne-deactivate":
          disableAriadneEditor();
          break;
        case "ariadneSave":
          e.preventDefault();
          saveAriadneRoute();
          break;
        case "ariadneReset":
          e.preventDefault();
          resetAriadneRoute();
          break;
        case "ariadneClear":
          e.preventDefault();
          clearAriadneRoute();
          break;
        case "ariadneUndo":
          e.preventDefault();
          undoAriadneRoute();
          break;
        case "keyOverlayCheckbox":
          $(".ruinOptionKey").toggleClass("hideme");
          break;
        case "show-bp-bunker":
          $(".bp-bunker").toggleClass("hideme");
          break;
        case "show-bp-hospital":
          $(".bp-hospital").toggleClass("hideme");
          break;
        case "show-bp-hotel":
          $(".bp-hotel").toggleClass("hideme");
          break;
        case "hide-bp-blocked":
          $(".bp-blocked").toggleClass("hidemeimportant");
          break;
        case "hide-bp-found":
          $(".bp-found").toggleClass("hidemeimportant");
          break;
        case "hide-unavailable-constructions":
          if (e.target.checked)
            document.querySelectorAll(".construction-not-available").forEach(function(el) { el.classList.add('hideme'); });
          else if (!e.target.checked && document.getElementById("hide-already-built-constructions").checked)
            document.querySelectorAll(".construction-not-available:not(.construction-already-built)").forEach(function(el) { el.classList.remove('hideme'); });
          else
            document.querySelectorAll(".construction-not-available").forEach(function(el) { el.classList.remove('hideme'); });
          // Update height
          updateConstructionsTabHeight();
          alternateColorsConstructionTable();
          break;
        case "hide-already-built-constructions":
          if (e.target.checked)
            document.querySelectorAll(".construction-already-built").forEach(function(el) { el.classList.add('hideme'); });
          else if (!e.target.checked && document.getElementById("hide-unavailable-constructions").checked)
            document.querySelectorAll(".construction-already-built:not(.construction-not-available)").forEach(function(el) { el.classList.remove('hideme'); });
          else
            document.querySelectorAll(".construction-already-built").forEach(function(el) { el.classList.remove('hideme'); });
          // Update height
          updateConstructionsTabHeight();
          alternateColorsConstructionTable();
          break;
        case "cancel-editing":
          e.preventDefault();
          cancelRouteEditing();
          break;
        case "duplicate-route":
          e.preventDefault();
          duplicateRoute();
          break;
        case "EDIT-ROUTE":
          e.preventDefault();
          e.stopPropagation();
          editRoute(e.target.parentElement.id);
          break;
        case "DELETE-ROUTE":
          e.preventDefault();
          e.stopPropagation();
          deleteRoute(e.target.parentElement.id);
          break;
        case "ADD-ROUTE":
          e.preventDefault();
          addRoute();
          break;
        case "UNDO-ROUTE":
          e.preventDefault();
          undoExpLastPoint();
          break;
        case "CLEAN-ROUTE":
          e.preventDefault();
          cleanRoute();
          break;

        default:
          break;
      }
    }

    // Target has a class.
    if (e.target && e.target.classList.contains('options-display-option')) {
      toggleDisplayOption(e);
    }
  });

  window.addEventListener('touchmove', repositionConstructionPlanner);
  window.addEventListener('resize', repositionConstructionPlanner);
  window.addEventListener('scroll', repositionConstructionPlanner);

  // EventListeners for change events.
  document.addEventListener('change', function(e) {
    // Target has an ID.
    if (e.target && e.target.id) {
      switch (e.target.id) {
        case "bps-filter-search":
          updateBPTrackerFilterHighlight();
        break;
        case "constructions-filter-search":
          updateConstructionsFilterHighlight();
          break;
        case "expe-color":
          document.querySelectorAll(".new-exp-svg").forEach(function (svg) {
            svg.setAttribute("fill", e.target.value);
            svg.setAttribute("stroke", e.target.value);
          });
        break;
      }
    }

    // Target has a class.
    if (e.target && e.target.classList.contains('bp-selector')) {
      // const bpid = e.target.value;
      // const action = e.target.checked ? 'ADD-BP' : 'REMOVE-BP';
      // fire('/map/updateBpsTracking', `action=${action}&bpid=${bpid}`);
    }
  });

  // Event listeners for MAP.
  document.querySelector("#map").addEventListener("click", function (e) {
    if (e.target.classList.contains("mapzone")) {
      data.cx = e.target.getAttribute("rx");
      data.cy = e.target.getAttribute("ry");
      if (document.querySelector("#exp-form") === null || document.querySelector("#exp-form").classList.contains("hideme")) {
        selectZone(e.target);
      } else {
        addZoneToExp(e.target);
      }
    }
  });

  document.querySelector("#map").addEventListener("mouseover", function (e) {
    if (e.target.classList.contains("mapzone")) {
      updateMapRulers(e.target);
      if (mapHoverActive) {
        showMapHover(e, e.target.getAttribute("id"));
      }
    }
  });

  document.querySelector("#map").addEventListener("mouseout", function (e) {
    if (e.target.classList.contains("mapzone")) {
      hideMapHover();
    }
  });

  document.querySelector("#map").addEventListener("mousemove", function (e) {
    if (e.target.classList.contains("mapzone")) {
      moveMapHover(e);
    }
  });

  // Event listeners for upper ruin map.
  document.querySelector("#upperruinmap").addEventListener("click", function (e) {
    if (e.target.classList.contains("mapzone")) {
      if (ariadne === 0) {
        selectUpperRuinZone(e.target);
      } else {
        ariadneAddNode("upper", e.target);
      }
      updateFloor("upper");
    }
  });

  document.querySelector("#upperruinmap").addEventListener("mouseover", function (e) {
    if (e.target.classList.contains("mapzone")) {
      updateUpperRuinMapRulers(e.target);
    }
  });

  // Event listeners for lower ruin map.
  document.querySelector("#lowerruinmap").addEventListener("click", function (e) {
    if (e.target.classList.contains("mapzone")) {
      if (ariadne === 0) {
        selectLowerRuinZone(e.target);
      } else {
        ariadneAddNode("lower", e.target);
      }
      updateFloor("lower");
    }
  });

  document.querySelector("#lowerruinmap").addEventListener("mouseover", function (e) {
    if (e.target.classList.contains("mapzone")) {
      updateLowerRuinMapRulers(e.target);
    }
  });

  // fetch item clicks
  $("#item-info .zone-item, #bank-info .zone-item").live({
    click: function (e) {
      e.stopPropagation();
      $(this).attr(
        "state",
        parseInt($(this).attr("state")) + Math.pow(-1, $(this).attr("state"))
      );
      highlightZonesWithItem();
    },
  });

  // fetch category clicks
  $("#item-info .zone-item-cat, #bank-info .zone-item-cat").live({
    click: function () {
      var newState =
        parseInt($(this).attr("state")) + Math.pow(-1, $(this).attr("state"));
      $(this).attr("state", newState);
      $("#" + $(this).attr("id") + " .zone-item").attr("state", newState);
      highlightZonesWithItem();
    },
  });

  // item selector
  $("#item-selector .select-item").live({
    click: function () {
      data["saveItems"] = new zoneItemList(data.cx, data.cy);
      var selItemId = $(this).attr("ref");
      var zoneItemContainer = $("#zi_x" + data.cx + "_y" + data.cy);
      if (zoneItemContainer !== undefined) {
        zoneItemContainer.children(".zone-item").each(function () {
          data.saveItems[$(this).attr("ref")] = $(this).attr("count");
        });
      } else {
        return false;
      }
      if (data.saveItems[selItemId] !== undefined) {
        data.saveItems[selItemId]++;
        zoneItemContainer
          .children('.zone-item[ref="' + selItemId + '"]')
          .attr("count", data.saveItems[selItemId]);
        zoneItemContainer
          .children('.zone-item[ref="' + selItemId + '"]')
          .children(".count")
          .html(data.saveItems[selItemId]);
      } else {
        data.saveItems[selItemId] = 1;
        zoneItemContainer.append(createItemDisplay(selItemId, 1, 0));
      }
    },
  });
  $(".zone-items .zone-item.changeable").live({
    click: function () {
      data["saveItems"] = new zoneItemList(data.cx, data.cy);
      var selItemId = $(this).attr("ref");
      var zoneItemContainer = $("#zi_x" + data.cx + "_y" + data.cy);
      if (zoneItemContainer !== undefined) {
        zoneItemContainer.children(".zone-item").each(function () {
          data.saveItems[$(this).attr("ref")] = $(this).attr("count");
        });
      } else {
        return false;
      }
      if (parseInt(data.saveItems[selItemId]) > 0) {
        data.saveItems[selItemId]--;
      }
      zoneItemContainer
        .children('.zone-item[ref="' + selItemId + '"]')
        .attr("count", data.saveItems[selItemId]);
      zoneItemContainer
        .children('.zone-item[ref="' + selItemId + '"]')
        .children(".count")
        .html(data.saveItems[selItemId]);
    },
  });
  // manual item update
  $("a.open-item-update").live({
    click: function (e) {
      e.preventDefault();
      $("#item-selector").removeClass("hideme");
      $(".close-item-selector").removeClass("hideme")
      $(".ajaxsave").removeClass("hideme")
      $(".open-item-update").addClass("hideme")
      $(".zone-items .zone-item").addClass("changeable");
    },
  });
  $("a.close-item-selector").live({
    click: function (e) {
      e.preventDefault();
      $("#item-selector").addClass("hideme");
      $(".close-item-selector").addClass("hideme")
      $(".ajaxsave").addClass("hideme")
      $(".open-item-update").removeClass("hideme")
      $(".zone-items .zone-item").removeClass("changeable");
      //reset panel
      $("#x" + data.cx + "y" + data.cy).click();
    },
  });

  $("a#toggle-routeSave").live({
    click: function (e) {
      e.preventDefault();
      $("a#toggle-routeSave").hide();
      $("#routeSave-form").slideToggle(500);
    },
  });
  $("a#routeSave-cancel").live({
    click: function (e) {
      e.preventDefault();
      $("#routeSave-form").slideUp(500, function () {
        $("a#toggle-routeSave").show();
      });
    },
  });
  $("a#routeSave-save").live({
    click: function (e) {
      e.preventDefault();
      $('input[name="routeSave-name"]').removeClass("error");
      saveRoute();
    },
  });

  $("a.toggle-building-update").live({
    click: function (e) {
      e.preventDefault();
      populateBuildingSelector(Number(($("#x" + data.cx + "y" + data.cy)).attr("km")));
      $("#building-selector").toggleClass("hideme");
    },
  });

  $("a.delete-building-update").live({
    click: function (e) {
      e.preventDefault();
      deleteBuildingGuess(data.cx, data.cy);
    },
  });



  $("a.close-building-update").live({
    click: function (e) {
      e.preventDefault();
      $("#building-selector").addClass("hideme");
    },
  });

  $("a.show-building-selector-all").live({
    click: function (e) {
      e.preventDefault();
      $("#building-selector-all").show();
      $("#headline-building-selector-all").show();
      $(".show-building-selector-all").hide()
    },
  });

  $("a.constructions-xml").live({
    click: function (e) {
      e.preventDefault();
      $(".construction-table-man").addClass("hideme");
      $(".construction-table-xml").removeClass("hideme");
    },
  });
  $("a.constructions-man").live({
    click: function (e) {
      e.preventDefault();
      $(".construction-table-xml").addClass("hideme");
      $(".construction-table-man").removeClass("hideme");
    },
  });

  $("a.ajaxsave").live({
    click: function (e) {
      e.preventDefault();
      $("#item-selector").addClass("hideme");
      $(".close-item-selector").addClass("hideme")
      $(".ajaxsave").addClass("hideme")
      $(".open-item-update").removeClass("hideme")
      $(".zone-items .zone-item").removeClass("changeable");
      if (data.saveItems !== undefined) {
        //ajaxInfo('Einen Moment noch bitte');
        var pas = "";
        var pac = 0;
        for (i in data.saveItems) {
          if (i !== "x" && i !== "y") {
            pas += "i:" + i + ";i:" + data.saveItems[i] + ";";
            pac++;
          }
        }
        pas = "a:" + pac + ":{" + pas + "}";
        saveZoneItems(data.saveItems["x"], data.saveItems["y"], pas);
      } else {
        ajaxInfo(__("Keine Änderungen registriert."));
      }
    },
  });

  $("div.select-building").live({
    click: function (e) {
      e.preventDefault();
      var bID = $(this).attr("ref");
      saveBuildingGuess(data.cx, data.cy, bID);
    },
  });

  $("div.ruinTile").live({
    click: function () {
      ajaxRuinUpdate("RUIN-TILE", $(this).attr("tile"));
    },
  });
  $("div.ruinDoorLock").live({
    click: function () {
      ajaxRuinUpdate("RUIN-DOORLOCK", $(this).attr("doorlock"));
    },
  });
  $("div.ruinDoor").live({
    click: function () {
      ajaxRuinUpdate("RUIN-DOOR", $(this).attr("door"));
    },
  });
  $("div.ruinLock").live({
    click: function () {
      ajaxRuinUpdate("RUIN-LOCK", $(this).attr("lock"));
    },
  });
  $("div.ruinZombie").live({
    click: function () {
      ajaxRuinUpdate("RUIN-ZOMBIE", $(this).attr("zombie"));
    },
  });
  $("div.ruinFloorSwitch").live({
    click: function () {
      $(
        "#" +
          (data.floor === "upper" ? "lower" : "upper") +
          "ruinmap #x" +
          data.crx +
          "y" +
          data.cry
      ).click();
    },
  });
  $("#ruinComment-save").live({
    click: function () {
      ajaxRuinUpdate("RUIN-COMMENT", $("#ruinComment").val());
    },
  });

  $("#update-myzone-button").live({
    click: function () {
      ajaxMyzoneUpdate();
    },
  });

  $(".itemsearch").live({
    keyup: function (e) {
      let filter = $(this).val().trim().toLowerCase();

      let category = ".zone-item-cat";
      let item = ".zone-item";
      if ($(this).attr("id") == "item-selector-search") {
        category = ".item-selector-cat";
        item = ".select-item";
      }

      $(this)
        .parent()
        .children(category)
        .children(item)
        .each(function () {
          let itemname = $(this)
            .children()
            .attr("title")
            .toLowerCase()
            .indexOf(filter);
          if (itemname >= 0) {
            $(this).removeClass("hideme");
          } else {
            $(this).addClass("hideme");
          }
        });
    },
  });

  // generate bank data
  for (var a in data.bankSorted) {
    var b = data.bankSorted[a];
    var item = data.bank[b];
    if (data.items[b] !== undefined) {
      var raw_item = data.items[b];
      var brokenItem = item.broken === 1 ? " broken" : "";
      var defItem = raw_item.category === "Armor" ? " defense" : "";
      $("#bank-info-" + raw_item.category).append(
        '<div class="zone-item click' +
          brokenItem +
          defItem +
          '" state="0" ref="' +
          raw_item.id +
          '" title="' +
          raw_item.name[data.langcode] +
          " (" +
          __("ID") +
          ": " +
          Math.abs(raw_item.id) +
          ')"><img src="' +
          data.system.icon +
          raw_item.image +
          '" title="' +
          raw_item.name[data.langcode] +
          " (" +
          __("ID") +
          ": " +
          Math.abs(raw_item.id) +
          ')" />&nbsp;' +
          item.count +
          "</div>"
      );
    }
  }

  // tab switcher box
  $("ul#box-tabs li.box-tab").click(function (e) {
    e.preventDefault();
    $("li.box-tab").removeClass("active");
    $(this).addClass("active");
    $("div.box-tab-content").addClass("hideme");
    var boxes = $(this).attr("ref").split(" ");
    boxes.forEach((box) => {
      $("div#" + box).removeClass("hideme");
    });
    if (box !== "zone-info") {
      $("#item-selector").addClass("hideme");
    }
  });
  $("ul#box-tabs li.box-spread").click(function (e) {
    e.preventDefault();
    $("#map-wrapper").css("opacity", 0.1);
    $("#item-selector").addClass("hideme");
    $("li.box-tab").addClass("hideme");
    $("div.box-tab-content").each(function () {
      $(this).removeClass("hideme").addClass("spread-out");
    });
    $(this).addClass("hideme");
    $("ul#box-tabs li.box-unspread").removeClass("hideme");
  });
  $("ul#box-tabs li.box-unspread").click(function (e) {
    e.preventDefault();
    $("div.box-tab-content").each(function () {
      $(this).removeClass("spread-out").addClass("hideme");
    });
    $("li.box-tab").removeClass("hideme");
    $("#map-wrapper").css("opacity", 1);
    $(this).addClass("hideme");
    $("ul#box-tabs li.box-spread").removeClass("hideme");
    $("#x" + data.cx + "y" + data.cy).click();
  });

  // manual regeneration update
  $("a.ajaxlink").live({
    click: function (e) {
      e.preventDefault();
      if ($(this).attr("id") === "UPDATE-SCOUTZOMBIES") {
        ajaxUpdate(
          $(this).attr("id"),
          $("#scoutzombie-count-input").val(),
          $("#maxzombie-count-input").val()
        );
      } else {
        ajaxUpdate(
          $(this).attr("id"),
          $("#zombie-count-input").val(),
          $("#maxzombie-count-input").val()
        );
      }
    },
  });
  // manual zombie update
  $("a.toggle-zombie-update").live({
    click: function (e) {
      e.preventDefault();
      $(".zombie-count-change").fadeIn(250, function () {});
      $(".toggle-zombie-update").fadeOut(250, function () {
        $("#UPDATE-ZOMBIES").fadeIn(500);
        $("#CANCEL-UPDATE-ZOMBIES").fadeIn(500);
      });
    },
  });
  // scout zombie update
  $("a.toggle-scout-zombie-update").live({
    click: function (e) {
      e.preventDefault();
      $(".scoutzombie-count-change").fadeIn(250, function () {});
      $(".toggle-scout-zombie-update").fadeOut(250, function () {
        $("#UPDATE-SCOUTZOMBIES").fadeIn(500);
        $("#CANCEL-UPDATE-SCOUTZOMBIES").fadeIn(500);
      });
    },
  });
  $("a#CANCEL-UPDATE-SCOUTZOMBIES").live({
    click: function (e) {
      e.preventDefault();
      $("#x" + data.cx + "y" + data.cy).click();
    }
  });
  $("a.toggle-maxzombie-update").live({
    click: function (e) {
      e.preventDefault();
      $(".maxzombie-count-change").fadeIn(250, function () {});
      $(".toggle-maxzombie-update").fadeOut(250, function () {
        $(".maxzombie-notice").fadeIn(500);
        $("#UPDATE-MAXZOMBIES").fadeIn(1000);
        $("#CANCEL-UPDATE-MAXZOMBIES").fadeIn(1000);
      });
    },
  });
  $("a#CANCEL-UPDATE-MAXZOMBIES").live({
    click: function (e) {
      e.preventDefault();
      $("#x" + data.cx + "y" + data.cy).click();
    }
  });
  $("a#UPDATE-ZOMBIES").live({
    click: function (e) {
      e.preventDefault();
      $(".zombie-count-change").fadeOut(250, function () {});
      $("#CANCEL-UPDATE-ZOMBIES").fadeOut(250, function () {
        $(".toggle-zombie-update").fadeIn(500);
        $("#UPDATE-ZOMBIES").fadeOut(250);
      });
    },
  });
  $("a#CANCEL-UPDATE-ZOMBIES").live({
    click: function (e) {
      e.preventDefault();
      $("#x" + data.cx + "y" + data.cy).click();
    }
  });
  $("a#UPDATE-MAXZOMBIES").live({
    click: function (e) {
      e.preventDefault();
      $(".maxzombie-notice").fadeOut(125);
      $(".maxzombie-count-change").fadeOut(250, function () {});
      $("#UPDATE-MAXZOMBIES").fadeOut(250, function () {
        $(".toggle-maxzombie-update").fadeIn(500);
      });
    },
  });
  $("a#UPDATE-SCOUTZOMBIES").live({
    click: function (e) {
      e.preventDefault();
      $(".scoutzombie-count-change").fadeOut(250, function () {});
      $("#UPDATE-SCOUTZOMBIES").fadeOut(250, function () {
        $(".toggle-scout-zombie-update").fadeIn(500);
      });
    },
  });
  $("button.tablinks").live({
    click: function (e) {
      e.preventDefault();
      $(".tablinks").removeClass("active");
      $(this).addClass("active");
      $(".tabcontent").css("display", "none");
      var tabContentId =
        "#" +
        $(this)
          .attr("id")
          .substring(0, $(this).attr("id").length - 4);
      $(tabContentId).css("display", "flex");
      $("#fm-content").css(
        "height",
        Math.max(
          $(tabContentId).height() + $(".tab").height(),
          $("#map-wrapper").height()
        ).toString() + "px"
      );
    },
  });
  $("div#OPEN-BP-TRACKER").live({
    click: function (e) {
      e.preventDefault();
      openBpTrackerPanel();
    },
  });
  $("div#OPEN-CONSTRUCTION-PAGE").live({
    click: function (e) {
      e.preventDefault();
      openConTrackerPanel();
    },
  });
  $("div#bps-constructions-wrapper-close").click(function () {
    changePanel('map');
  });
  $("a#ENTER-BUILDING").live({
    click: function (e) {
      e.preventDefault();
      //if (ruinEditorKeys === 0) {
        generateRuinMap($(this).attr("ocx"), $(this).attr("ocy"));
        changePanel('eruin');
        moveRuinBoxes("upper");
        document.querySelector("#upperruinmap #x0y0").click();
        redrawAriadneRoute($(this).attr("ocx"), $(this).attr("ocy"));
      //}
    },
  });
  $("div#ruinmap-wrapper-close").click(function () {
    $("#ruinComment").blur();
    changePanel('map');
  });
  $(".scoutzombie-count-change.minus").live({
    click: function (e) {
      var cval = $("#scoutzombie-count-input").val();
      var nval = parseInt(cval) + 1;
      $("#scoutzombie-count-input").val(nval);
      $("#scoutzombie-count-display").html(nval);
    },
  });
  $(".scoutzombie-count-change.plus").live({
    click: function (e) {
      var cval = $("#scoutzombie-count-input").val();
      var nval = parseInt(cval) - 1;
      if (nval < 0) {
        nval = 0;
      }
      $("#scoutzombie-count-input").val(nval);
      $("#scoutzombie-count-display").html(nval);
    },
  });
  $(".zombie-count-change.minus").live({
    click: function (e) {
      var cval = $("#zombie-count-input").val();
      var nval = parseInt(cval) + 1;
      $("#zombie-count-input").val(nval);
      $("#zombie-count-display").html(nval);
    },
  });
  $(".zombie-count-change.plus").live({
    click: function (e) {
      var cval = $("#zombie-count-input").val();
      var nval = parseInt(cval) - 1;
      if (nval < 0) {
        nval = 0;
      }
      $("#zombie-count-input").val(nval);
      $("#zombie-count-display").html(nval);
    },
  });
  $(".maxzombie-count-change.minus").live({
    click: function (e) {
      var cval = $("#maxzombie-count-input").val();
      var nval = parseInt(cval) + 1;
      $("#maxzombie-count-input").val(nval);
      $("#maxzombie-count-display").html(nval);
    },
  });
  $(".maxzombie-count-change.plus").live({
    click: function (e) {
      var cval = $("#maxzombie-count-input").val();
      var nval = parseInt(cval) - 1;
      if (nval < 0) {
        nval = 0;
      }
      $("#maxzombie-count-input").val(nval);
      $("#maxzombie-count-display").html(nval);
    },
  });

  $("a.ajaxupdate").live({
    click: function (e) {
      e.preventDefault();
      ajaxUpdate($(this).attr("id"), $("#storm-today").val(), 0);
    },
  });

  // fetch arrow keys
  document.addEventListener("keydown", (e) => {
    if (e.isComposing || e.keyCode === 229) {
      return;
    }
    if ($("#ruinmap-wrapper:visible").length === 1) {
      walkInTheRuin(e);
    } else {
      walkInTheDesert(e);
    }
  });

  // load color wheel
  //$('#colorpicker').farbtastic('#opt-radius-color');
  /*$('#colorpicker').append('<div class="opt-radius-color">' +
	'<span>' + __("Color") + '  </span>' +
	'<input type="color" style="height: 18px; width: 35px;" id="opt-radius-color" name="opt-radius-color" value="#' +
	Math.floor(Math.random() * 16777215).toString(16) + '" />' +
	'</div>');*/

  // add radius
  $("#opt-radius-submit").click(function () {
    //var color = $('input[name="opt-radius-color"]').val();
    var color = $('input[name="opt-radius-color"]').val();
    var radius = $('input[name="opt-radius-number"]').val();
    var metric = $('input[name="opt-radius-metric"]:checked').val();
    addRadius(radius, metric, color);
    saveRadii();
  });

  // toggle display radiuschecklist
  $(".radius-checklist-item").click(function () {
    //get opservation platform vote
    let op_vote = 0;
    if (
      data.hasOwnProperty("constructions") &&
      data.constructions.hasOwnProperty("153") &&
      data.constructions[153].hasOwnProperty("level")
    ) {
      op_vote = parseInt(data.constructions[153].level);
    }
    //fix distances with votes
    if (op_vote == 5) {
      document.querySelector("#radius-op").setAttribute("data-range", 11);
      document.querySelector("#radius-tp").setAttribute("data-range", 2);
    }
    if (op_vote == 4) {
      document.querySelector("#radius-op").setAttribute("data-range", 10);
      document.querySelector("#radius-tp").setAttribute("data-range", 1);
    }

    var result = $(this).toggleClass("active-option");
    let radiusname = $(this).attr("data-name");
    let range = $(this).attr("data-range");
    let metric = $(this).attr("data-metric");
    //not used, pushed to css file
    //let color = $(this).attr("data-color");

    //geodir radius marker
    if (radiusname == "gd") {
      if (result.hasClass("active-option")) {
        //replaced in css file
        //$('#dynascript').append('<style type="text/css">li.mapzone.highlight-' + radiusname + '-border-n { border-top-color: ' + color + '; } li.mapzone.highlight-' + radiusname + '-border-e { border-right-color: ' + color + '; } li.mapzone.highlight-' + radiusname + '-border-s { border-bottom-color: ' + color + '; } li.mapzone.highlight-' + radiusname + '-border-w { border-left-color: ' + color + '; }</style>');
        $('.mapzone[geodirborder-n="1"]').each(function (e) {
          $(this)
            .addClass("highlight-radius")
            .addClass("highlight-" + radiusname + "-border-n");
        });
        $('.mapzone[geodirborder-s="1"]').each(function (e) {
          $(this)
            .addClass("highlight-radius")
            .addClass("highlight-" + radiusname + "-border-s");
        });
        $('.mapzone[geodirborder-w="1"]').each(function (e) {
          $(this)
            .addClass("highlight-radius")
            .addClass("highlight-" + radiusname + "-border-w");
        });
        $('.mapzone[geodirborder-e="1"]').each(function (e) {
          $(this)
            .addClass("highlight-radius")
            .addClass("highlight-" + radiusname + "-border-e");
        });
        localStorage.setItem("fm_radius_" + radiusname, 1);
      } else {
        $(".mapzone.highlight-" + radiusname + "-border-n").removeClass(
          "highlight-" + radiusname + "-border-n"
        );
        $(".mapzone.highlight-" + radiusname + "-border-e").removeClass(
          "highlight-" + radiusname + "-border-e"
        );
        $(".mapzone.highlight-" + radiusname + "-border-s").removeClass(
          "highlight-" + radiusname + "-border-s"
        );
        $(".mapzone.highlight-" + radiusname + "-border-w").removeClass(
          "highlight-" + radiusname + "-border-w"
        );
        localStorage.setItem("fm_radius_" + radiusname, 0);
      }
    }

    if ($(this).attr("data-range") > 0 || $(this).attr("data-metric") != "km") {
      if (result.hasClass("active-option")) {
        //replaced in css file
        //$('#dynascript').append('<style type="text/css">li.mapzone.highlight-' + radiusname + '-border-n { border-top-color: ' + color + '; } li.mapzone.highlight-' + radiusname + '-border-e { border-right-color: ' + color + '; } li.mapzone.highlight-' + radiusname + '-border-s { border-bottom-color: ' + color + '; } li.mapzone.highlight-' + radiusname + '-border-w { border-left-color: ' + color + '; }</style>');
        $(".mapzone[" + metric + '="' + range + '"]').each(function (e) {
          var change = $(this).attr(metric + "c");
          for (c = 0; c < change.length; c++) {
            $(this)
              .addClass("highlight-radius")
              .addClass(
                "highlight-" + radiusname + "-border-" + change.charAt(c)
              );
          }
        });
        localStorage.setItem("fm_radius_" + radiusname, 1);
      } else {
        $(".mapzone.highlight-" + radiusname + "-border-n").removeClass(
          "highlight-" + radiusname + "-border-n"
        );
        $(".mapzone.highlight-" + radiusname + "-border-e").removeClass(
          "highlight-" + radiusname + "-border-e"
        );
        $(".mapzone.highlight-" + radiusname + "-border-s").removeClass(
          "highlight-" + radiusname + "-border-s"
        );
        $(".mapzone.highlight-" + radiusname + "-border-w").removeClass(
          "highlight-" + radiusname + "-border-w"
        );
        localStorage.setItem("fm_radius_" + radiusname, 0);
      }
    }
  });

  // toggle hover switch
  $("#options-display-zonehover.options-display-switch").click(function () {
    mapHoverActive = mapHoverActive === true ? false : true;
    $(this).toggleClass("active-option");
    if (mapHoverActive == true) {
      localStorage.setItem("fm_setting_zonehover", 1);
    } else {
      localStorage.setItem("fm_setting_zonehover", 0);
    }
  });
  // toggle geodir switch
  $("#options-display-geodir.options-display-switch").click(function () {
    toggleGeoDirDisplay();
    $(this).toggleClass("active-option");
    if ($(this).hasClass("active-option")) {
      localStorage.setItem("fm_setting_geodir", 1);
    } else {
      localStorage.setItem("fm_setting_geodir", 0);
    }
  });


  $("#highlight-purplechests").click(function () {
    $("#highlight-purplechests").toggleClass("active-filter");
    highlightPotentialChestZones();
  });
  $("#highlight-bluechests").click(function () {
    $("#highlight-bluechests").toggleClass("active-filter");
    highlightPotentialChestZones();
  });
  $("#highlight-fireworksparts").click(function () {
    $("#highlight-fireworksparts").toggleClass("active-filter");
    highlightPotentialChestZones();
  });


  //logout button
  $(".logout-button").click(function () {
    window.location.assign('/logout');
    //fire('/logout', '');
  });

  // help switch
  $(".help-switch").click(function () {
    $(".help-switch").toggleClass("active");
    $("#help-section").toggleClass("hidefaq showfaq");
  });
  $("#help-section ul li").click(function () {
    $("#help-section ul li").removeClass("active");
    $(this).addClass("active");
  });
  // panel switch
  $(".panel-switch").click(function () {
    if ($(this).attr("data-panel") !== 'eruin') {
     changePanel($(this).attr("data-panel"));
    }
  });

  // toggle display expeditions
  $(".exp-item").live({
    click: function () {
      var rid = $(this).attr("id");
      if (rid === "0.1") {
        let result = $("#exp-form").toggleClass("hideme");
        if (result.hasClass("hideme")) {
          $("#0\\.1").html(__("Open route editor"));
          $('.exp-item.editing-mode').removeClass("editing-mode");
        } else {
          $("#0\\.1").html(__("Close route editor"));
        }
      } else if (rid === "0.0") {
        $(".exp-item").removeClass("active-option");
        $(this).toggleClass("active-option");
        $("#0\\.1").toggleClass("active-option");
        $(".exp-svg").remove();
        saveExpeditions(true);
      } else {
        if (rid == editRouteId) {
          return;
        } //prevent unselecting the currently edited route
        var result = $(this).toggleClass("active-option");
        if (result.hasClass("active-option")) {
          highlightRoute(rid);
        } else {
          $("." + escapeSelector(rid)).remove();
        }
        saveExpeditions();
      }
    },
  });

  $(".expand").live({
    click: function () {
      $("ul", $(this).parent()).eq(0).toggle();
      $(this).toggleClass("caret-down");
    },
  });

  $("#box").slideDown(500);

  $("li.city").live({
    mouseover: function () {
      toggleGeoDirDisplay();
    },
    mouseout: function () {
      toggleGeoDirDisplay();
    },
  });

  $(".ruinOption").live({
    mouseover: function () {
      $("#ruinHoverInfo").addClass("hovering").html($(this).attr("title"));
    },
    mouseout: function () {
      $("#ruinHoverInfo").removeClass("hovering").html("");
    },
  });

  $("#options-save").click(function () {
    saveSettings();
  });
  $("#options-reset").click(function () {
    saveSettings(true);
  });

  $("#radius-save").click(function () {
    saveRadii();
  });

  $("#radius-reset").click(function () {
    saveRadii(true);
  });

  $("#import-external-1").click(function () {
    updateFromExternal(1);
  });

  $("html").keydown(function (e) {
    // Undo expeditions routes with backspace
    if (
      !$("#exp-form").hasClass("hideme") &&
      !$(e.target).is("input") &&
      e.keyCode === 8
    ) {
      e.preventDefault();
      undoExpLastPoint();
    } else if (ariadne === 1) {
      switch (e.keyCode) {
        case 8: // Backspace
          undoAriadneRoute();
          break;
        case 13: // Enter
          saveAriadneRoute();
          break;
        case 46: // Delete
          clearAriadneRoute();
          break;
        case 36: // Home
          resetAriadneRoute();
          break;
      }
    }
  });
}

function saveSettings(reset = false) {
  if (reset) {
    //disable all of them
    document
      .querySelectorAll(".options-display-switch.active-option")
      .forEach(function (el) {
        el.click();
      });

    document
      .querySelectorAll(".options-display-option.active-option")
      .forEach(function (el) {
        el.click();
      });

    //delete from localstorage
    localStorage.removeItem("fm_setting_zonehover");
    localStorage.removeItem("fm_setting_geodir");
    localStorage.removeItem("fm_setting_zone-status-dried");
    localStorage.removeItem("fm_setting_zone-status-full");
    localStorage.removeItem("fm_setting_zone-storm-status");
    localStorage.removeItem("fm_setting_citizen");
    localStorage.removeItem("fm_setting_zombies");
    localStorage.removeItem("fm_setting_maxzombies");
    localStorage.removeItem("fm_setting_zone-updated");
    localStorage.removeItem("fm_setting_localtime");
    localStorage.removeItem("fm_setting_ampmtime");



    $(".refresh-notice").fadeIn(500);

    //success message and reload defaults
    ajaxInfo(__("settings-reset"));
    loadSettings();
  }

  /*
	var settings = 0;
	var options = {
		'zonehover': 1,
		'geodir': 2,
		'driedzone': 4,
		'fullzone': 8,
		'citizens': 16,
		'zombies': 32,
		'uptodate': 64,
		'stormzone': 128,
	}
	for(let o in options) {
		let option = document.getElementById('options-display-' + o);
		if (option.classList.contains('active-option')) {
			settings += options[o];
		}
	}
	if (reset) {
		localStorage.setItem('fm_settings', 217);
		loadSettings();
	}
	else {
		localStorage.setItem('fm_settings', settings);
	} */
  //ajaxInfo(__('settings-saved'));
}

function loadSettings() {

  //delete the old stuff
  localStorage.removeItem("fm_settings");
  localStorage.removeItem("fm_setting_zombies_reset");

  //load from stroage or set new defaults

  if (localStorage.getItem("fm_setting_zonehover") == null) {
    localStorage.setItem("fm_setting_zonehover", 1);
  }

  if (localStorage.getItem("fm_setting_geodir") == null) {
    localStorage.setItem("fm_setting_geodir", 0);
  }

  if (localStorage.getItem("fm_setting_zone-status-dried") == null) {
    localStorage.setItem("fm_setting_zone-status-dried", 0);
  }

  if (localStorage.getItem("fm_setting_zone-status-full") == null) {
    localStorage.setItem("fm_setting_zone-status-full", 1);
  }

  if (localStorage.getItem("fm_setting_zone-storm-status") == null) {
    localStorage.setItem("fm_setting_zone-storm-status", 1);
  }

  if (localStorage.getItem("fm_setting_citizen") == null) {
    localStorage.setItem("fm_setting_citizen", 1);
  }

  if (localStorage.getItem("fm_setting_zombies") == null) {
    localStorage.setItem("fm_setting_zombies", 1);
  }

  if (localStorage.getItem("fm_setting_maxzombies") == null) {
    localStorage.setItem("fm_setting_maxzombies", 0);
  }

  if (localStorage.getItem("fm_setting_zone-updated") == null) {
    localStorage.setItem("fm_setting_zone-updated", 1);
  }

  if (localStorage.getItem("fm_setting_localtime") == null) {
    localStorage.setItem("fm_setting_localtime", 0);
  }

  if (localStorage.getItem("fm_setting_ampmtime") == null) {
    localStorage.setItem("fm_setting_ampmtime", 0);
  }

  //restore fixed radius

  if (localStorage.getItem("fm_setting_zonehover") == 1) {
    document.getElementById("options-display-zonehover").click();
  }

  if (localStorage.getItem("fm_setting_geodir") == 1) {
    document.getElementById("options-display-geodir").click();
  }

  if (localStorage.getItem("fm_setting_zone-status-dried") == 1) {
    document.getElementById("options-display-driedzone").click();
  }

  if (localStorage.getItem("fm_setting_zone-status-full") == 1) {
    document.getElementById("options-display-fullzone").click();
  }

  if (localStorage.getItem("fm_setting_zone-storm-status") == 1) {
    document.getElementById("options-display-stormzone").click();
  }

  if (localStorage.getItem("fm_setting_citizen") == 1) {
    document.getElementById("options-display-citizens").click();
  }

  if (localStorage.getItem("fm_setting_zombies") == 1) {
    document.getElementById("options-display-zombies").click();
  }

  if (localStorage.getItem("fm_setting_maxzombies") == 1) {
    document.getElementById("options-display-maxzombies").click();
  }


  if (localStorage.getItem("fm_setting_zone-updated") == 1) {
    document.getElementById("options-display-uptodate").click();
  }

  if (localStorage.getItem("fm_setting_localtime") == 1) {
    document
      .getElementById("options-display-localtime")
      .classList.add("active-option");
  }

  if (localStorage.getItem("fm_setting_ampmtime") == 1) {
    document
      .getElementById("options-display-ampmtime")
      .classList.add("active-option");
  }
}

function saveExpeditions(reset = false) {
  if (data.spy == 1) {
    return;
  }
  let routes = [];
  if (reset) {
    localStorage.removeItem("fm_routes_gameid");
    localStorage.removeItem("fm_routes_gameday");
    localStorage.removeItem("fm_routes_selection");
  } else {
    document
      .querySelectorAll(".exp-route.click.active-option")
      .forEach(function (el) {
        routes.push([el.id]);
      });
    localStorage.setItem("fm_routes_gameid", data.system.gameid);
    localStorage.setItem("fm_routes_gameday", data.system.day);
    //localStorage.setItem('fm_routes_selection', routes.join('"'));
    localStorage.setItem("fm_routes_selection", JSON.stringify(routes));
  }
}

function loadExpeditions() {
  if (data.spy == 1) {
    return;
  }

  if (localStorage.getItem("fm_routes_gameid") != data.system.gameid) {
    localStorage.removeItem("fm_routes_gameid");
    localStorage.removeItem("fm_routes_gameday");
    localStorage.removeItem("fm_routes_selection");
  }
  if (localStorage.getItem("fm_routes_gameday") != data.system.day) {
    localStorage.removeItem("fm_routes_gameid");
    localStorage.removeItem("fm_routes_gameday");
    localStorage.removeItem("fm_routes_selection");
  }

  var routes_serial = localStorage.getItem("fm_routes_selection");

  if (routes_serial) {
    //let routes = routes_serial.split('"');
    let routes = JSON.parse(routes_serial);
    for (let r in routes) {
      let route = routes[r];
      if (route !== undefined && route !== "") {
        let routeId = document.getElementById(route);
        if (routeId !== undefined && route !== "" && routeId !== null) {
          routeId.click();
        }
      }
    }
  }
}

function saveRadii(reset = false) {
  let radii = [];
  if (reset) {
    localStorage.removeItem("fm_radii");
    // fixed radius settings wipe
    localStorage.removeItem("fm_radius_hr");
    localStorage.removeItem("fm_radius_tp");
    localStorage.removeItem("fm_radius_op");
    localStorage.removeItem("fm_radius_gd");
    ajaxInfo(__("radii-reset"));
    loadRadii();
  } else {
    document.querySelectorAll(".radius-list-item").forEach(function (el) {
      //console.log(el.dataset.range, el.dataset.metric, el.dataset.color);
      radii.push(
        [
          el.dataset.range,
          el.dataset.metric,
          el.dataset.color,
          el.dataset.name,
        ].join("|")
      );
    });
    //localStorage.setItem('fm_radii', radii.join('+'));
    localStorage.setItem("fm_radii", JSON.stringify(radii));
  }
  //ajaxInfo(__('radii-saved'));
}

function loadRadii() {
  //radii presets

  //default to 1

  if (localStorage.getItem("fm_radius_op") == null) {
    localStorage.setItem("fm_radius_op", 1);
  }

  if (localStorage.getItem("fm_radius_tp") == null) {
    localStorage.setItem("fm_radius_tp", 1);
  }

  if (localStorage.getItem("fm_radius_hr") == null) {
    localStorage.setItem("fm_radius_hr", 1);
  }

  if (localStorage.getItem("fm_radius_gd") == null) {
    localStorage.setItem("fm_radius_gd", 1);
  }

  if (localStorage.getItem("fm_radius_tw") == null) {
    localStorage.setItem("fm_radius_tw", 1);
  }

  //restore fixed radius
  if (localStorage.getItem("fm_radius_gd") == 1) {
    document.querySelector("#radius-gd").click();
    if (
      !document.getElementById("radius-gd").classList.contains("active-option")
    ) {
      document.querySelector("#radius-gd").click();
    }
  }

  if (localStorage.getItem("fm_radius_op") == 1) {
    document.querySelector("#radius-op").click();
    if (
      !document.getElementById("radius-op").classList.contains("active-option")
    ) {
      document.querySelector("#radius-op").click();
    }
  }

  if (localStorage.getItem("fm_radius_tw") == 1) {
    document.querySelector("#radius-tw").click();
    if (
      !document.getElementById("radius-tw").classList.contains("active-option")
    ) {
      document.querySelector("#radius-tw").click();
    }
  }

  if (localStorage.getItem("fm_radius_tp") == 1) {
    document.querySelector("#radius-tp").click();
    if (
      !document.getElementById("radius-tp").classList.contains("active-option")
    ) {
      document.querySelector("#radius-tp").click();
    }
  }

  if (localStorage.getItem("fm_radius_hr") == 1) {
    document.querySelector("#radius-hr").click();
    if (
      !document.getElementById("radius-hr").classList.contains("active-option")
    ) {
      document.querySelector("#radius-hr").click();
    }
  }

  /*
	document.querySelectorAll('.radius-checklist-item').forEach(function (el) {
		el.click();
	});*/

  //custom radii

  document.querySelectorAll(".radius-list-item").forEach(function (el) {
    el.click();
  });
  var radii_serial = localStorage.getItem("fm_radii");
  /*
	if (!radii_serial) {
		let wt_ap = 9;
		if (data.hasOwnProperty('constructions') && data.constructions.hasOwnProperty('153')) {
			var wt = data.constructions[153];
			if (wt.level) {
				if (parseInt(wt.level) === 5) {
					wt_ap = 11;
				} else if (parseInt(wt.level) === 4) {
					wt_ap = 10;
				}
			}
		}
		let radii = [];
		// old automatic radii
		//radii.push([wt_ap,'ap','#00ff66',__('default-radius')].join('|'));
		//radii.push([11,'km','#00ffff',__('hero-return')].join('|'));
		radii_serial = radii.join('+');
		localStorage.setItem('fm_radii', radii_serial);
	}
	*/

  //delete old stuff
  if (!localStorage.getItem("fm_radii_reset_done2")) {
    localStorage.removeItem("fm_radii");
    localStorage.setItem("fm_radii_reset_done2", 1);
  } else {
    if (radii_serial) {
      //let radii = radii_serial.split('+');
      let radii = JSON.parse(radii_serial);
      for (let r in radii) {
        let radius = radii[r];
        if (radius !== undefined && radius !== "") {
          let rdata = radius.split("|");
          addRadius(rdata[0], rdata[1], rdata[2], rdata[3]);
        }
      }
    }
  }
}

function toggleLangButton(show = false) {
  if (show == false) {
    document.getElementById("lang-switcher-content").classList.remove("show");
  } else {
    document.getElementById("lang-switcher-content").classList.toggle("show");
  }
}

function setUserLanguage(lang = null) {
  //set default
  if (!localStorage.getItem("fm_user_lang")) {
    localStorage.setItem("fm_user_lang", "default");
  }

  //load and set
  if (!lang) {
    var storedLang = localStorage.getItem("fm_user_lang");
    if (storedLang != "default") {
      data.langcode = storedLang;
    }
    document
      .getElementById("lang-switcher")
      .classList.add("lang-" + storedLang);
  }

  //customize user language
  if (lang) {
    localStorage.setItem("fm_user_lang", lang);
    location.reload();
  }
}

function openExpeditionsPanel() {
  document.getElementById("tab-expeditions").click();
  if (document.getElementById("exp-form").classList.contains("hideme")) {
    document.getElementById("0.1").click();
  }
}

function handleEscapeKey(event) {
  if (event.key === 'Escape' && document.getElementById('eruin-popup').style.display === 'flex') {
    document.getElementById('eruin-popup').style.display = 'none';
    document.removeEventListener('keydown', handleEscapeKey);
  }
}

function openEruinPanel() {
  if (debug) { console.log({ '_function': 'openEruinPanel' }); }
  // Find all element with the classes "building explorable-building"
  const buildingElements = document.querySelectorAll('.building.explorable-building');

  if (buildingElements && buildingElements.length === 2) {
    let rx1,ry1,rx2,ry2,rt1,rt2;
    rx1 = buildingElements[0].parentElement.getAttribute('rx');
    ry1 = buildingElements[0].parentElement.getAttribute('ry');
    rt1 = data.buildings[buildingElements[0].getAttribute('ruin')][data.langcode];
    rx2 = buildingElements[1].parentElement.getAttribute('rx');
    ry2 = buildingElements[1].parentElement.getAttribute('ry');
    rt2 = data.buildings[buildingElements[1].getAttribute('ruin')][data.langcode];

    document.getElementById('eruin-popup').style.display = 'flex';
    document.getElementById('eruin-option1').textContent = rt1 + " (" + rx1 + "|" + ry1 + ")";
    document.getElementById('eruin-option2').textContent = rt2 + " (" + rx2 + "|" + ry2 + ")";
    document.getElementById('eruin-option1').addEventListener('click', function() {
      openEruinPanelForBuilding(buildingElements[0]);
      document.getElementById('eruin-popup').style.display = 'none';
    });

    document.getElementById('eruin-option2').addEventListener('click', function() {
      openEruinPanelForBuilding(buildingElements[1]);
      document.getElementById('eruin-popup').style.display = 'none';
    });

    document.querySelector('#eruin-popup .popup-close').addEventListener('click', function() {
      document.getElementById('eruin-popup').style.display = 'none';
      document.removeEventListener('keydown', handleEscapeKey);
    });
    document.addEventListener('keydown', handleEscapeKey);

  }
  else if (buildingElements && buildingElements.length === 1) {
    openEruinPanelForBuilding(buildingElements[0]);
  }
  else {
    ajaxInfoBad(__("no-eruin-available"));
  }
}

function openEruinPanelForBuilding(buildingElement) {
  if (debug) { console.log({ '_function': 'openEruinPanelForBuilding', 'x': buildingElement.parentElement.getAttribute('rx'), 'y': buildingElement.parentElement.getAttribute('ry') }); }

  // Create a MutationObserver to watch for the creation of the "ENTER-BUILDING" element
  const observer = new MutationObserver((mutationsList, observer) => {
    // Check if the "ENTER-BUILDING" element is added
    if (debug) { console.log("Observer triggered"); }
    const enterButton = document.getElementById("ENTER-BUILDING");
    if (enterButton &&
      buildingElement.parentElement.getAttribute('ax') === enterButton.getAttribute('ocx') &&
      buildingElement.parentElement.getAttribute('ay') === enterButton.getAttribute('ocy')
    ) {
      observer.disconnect(); // Stop observing after the element is found and clicked.
      if (debug) { console.log("Observer disconnected"); }
      enterButton.click(); // Click the button.
    }
  });

  // Start observing the document body for changes
  observer.observe(document.body, { childList: true, subtree: true });

  // Click the parent element to trigger the creation of the "ENTER-BUILDING" element
  buildingElement.parentElement.click();
}

function openBpTrackerPanel() {
  if (debug) { console.log({ '_function': 'openBpTrackerPanel' }); }


  $("#BPS-TRACKER-TAB").addClass("active");
  $("#BPS-TRACKER").css("display", "flex");

  if (data.spy === undefined) {
    fire('/map/updateTabsInfo', '', 'tabs');
  }

  changePanel('bps');
}

function openConTrackerPanel() {
  if (debug) { console.log({ '_function': 'openConTrackerPanel' }); }
  $("#CONSTRUCTIONS-TAB").addClass("active");
  $("#CONSTRUCTIONS").css("display", "flex");

  if (data.spy === undefined) {
    fire('/map/updateTabsInfo', '', 'tabs');
  }

  changePanel('bps');
}

function generateMap() {
  let ax, ay, mx, my, rx, ry, rzd;
  for (let i = 0; i < data.height; i++) {
    let maprow = document.createElement("ul");
    maprow.className = "maprow";

    for (let j = 0; j < data.width; j++) {
      ax = j;
      ay = i;
      mx = "x" + j;
      my = "y" + i;
      rx = j - data.tx;
      ry = data.ty - i;
      rzd = null;

      maprow.appendChild(generateMapZone(i, j));
    }

    let maprulerF = document.createElement("li");
    maprulerF.className = "mapruler ruler_y" + ry + " first";
    maprulerF.textContent = ry;

    let maprulerL = maprulerF.cloneNode(true);
    maprulerL.className = "mapruler ruler_y" + ry + " last";

    maprow.insertBefore(maprulerF, maprow.firstChild);
    maprow.appendChild(maprulerL);

    document.getElementById("map").appendChild(maprow);
  }

  let maprulebarT = document.createElement("ul");
  maprulebarT.className = "maprow maprulebar maprulebar-top";

  for (let j = 0; j < data.width; j++) {
    let maprulex = document.createElement("li");
    maprulex.className = "mapruler ruler_x" + (j - data.tx);
    maprulex.textContent = j - data.tx;

    maprulebarT.appendChild(maprulex);
  }

  let mapruleoF = document.createElement("li");
  mapruleoF.className = "mapcorner first";

  let mapruleoL = mapruleoF.cloneNode(true);
  mapruleoL.className = "mapcorner last";

  maprulebarT.insertBefore(mapruleoF, maprulebarT.firstChild);
  maprulebarT.appendChild(mapruleoL);

  let maprulebarB = maprulebarT.cloneNode(true);
  maprulebarB.className = "maprow maprulebar maprulebar-bottom";

  document.getElementById("map").insertBefore(maprulebarT, document.getElementById("map").firstChild);
  document.getElementById("map").appendChild(maprulebarB);

  for (let dots in citidots) {
    let cd = citidots[dots];
    createCitidotSvg(cd.zone, cd.cs, cd.cc);
  }
}

function generateMapZone(i, j) {
  // Define variables.
  let ax = j;
  let ay = i;
  let mx = "x" + j;
  let my = "y" + i;
  let rx = j - data["tx"];
  let ry = data["ty"] - i;
  let rzd = null;
  let ap = calcAP(rx, ry);
  let km = calcKM(rx, ry);
  let gd = "gd-" + calcGD(rx, ry);
  let day = parseInt(data.system.day);
  let updatezoneday, updatescoutday, daysSinceZoneUpdate, daysSinceScoutUpdate, utd;

  // save currently selected routes
  let currentExpeditions = document.getElementById("routex" + rx + "y" + ry);

  //save current classes
  let currentZone = document.querySelector(
    "#x" + rx + "y" + ry + ".desertzone"
  );
  let currentClasses = null;
  if (currentZone !== null && currentZone !== undefined) {
    currentClasses = currentZone.classList;
    for (let i = 0; currentClasses[i] !== undefined; i++) {
      if (!currentClasses[i].startsWith("highlight")) {
        currentClasses.remove(currentClasses[i]);
        i--;
      }
    }
  }

  let mapzone = document.createElement("li");
  mapzone.classList.add("mapzone", "desertzone");
  mapzone.setAttribute("rx", rx);
  mapzone.setAttribute("ry", ry);
  mapzone.setAttribute("ax", ax);
  mapzone.setAttribute("ay", ay);
  mapzone.setAttribute("id", "x" + rx + "y" + ry);
  if (data.ox === j && data.oy === i) {
    mapzone.classList.add("selectedZone");
  }

  mapzone.setAttribute("ap", ap);
  mapzone.setAttribute("km", km);
  mapzone.classList.add(gd);

  //mapzone borders
  if (calcGDBorder(rx, ry, "N")) {
    mapzone.setAttribute("geodirborder-N", "1");
  }
  if (calcGDBorder(rx, ry, "S")) {
    mapzone.setAttribute("geodirborder-S", "1");
  }
  if (calcGDBorder(rx, ry, "W")) {
    mapzone.setAttribute("geodirborder-W", "1");
  }
  if (calcGDBorder(rx, ry, "E")) {
    mapzone.setAttribute("geodirborder-E", "1");
  }

  let apc = "";
  let kmc = "";
  if (ax > 0 && rx <= 0) {
    // west borders
    let aap = calcAP(rx - 1, ry);
    let akm = calcKM(rx - 1, ry);
    if (aap !== ap) {
      apc += "w";
    }
    if (akm !== km) {
      kmc += "w";
    }
  }
  if (ax < data.width - 1 && rx >= 0) {
    // east borders
    let aap = calcAP(rx + 1, ry);
    let akm = calcKM(rx + 1, ry);
    if (aap !== ap) {
      apc += "e";
    }
    if (akm !== km) {
      kmc += "e";
    }
  }
  if (ay < data.height - 1 && ry <= 0) {
    // south borders
    let aap = calcAP(rx, ry - 1);
    let akm = calcKM(rx, ry - 1);
    if (aap !== ap) {
      apc += "s";
    }
    if (akm !== km) {
      kmc += "s";
    }
  }
  if (ay > 0 && ry >= 0) {
    // north borders
    let aap = calcAP(rx, ry + 1);
    let akm = calcKM(rx, ry + 1);
    if (aap !== ap) {
      apc += "n";
    }
    if (akm !== km) {
      kmc += "n";
    }
  }
  mapzone.setAttribute("apc", apc);
  mapzone.setAttribute("kmc", kmc);

  if (data.map[my] !== undefined && data.map[my][mx] !== undefined) {
    if (
      data.map[my][mx]["items"] !== undefined &&
      data.map[my][mx]["items"].length > 0 &&
      !(rx === 0 && ry === 0)
    ) {
      let zoneItems = data.map[my][mx]["items"];
      for (zi in zoneItems) {
        let zitem = zoneItems[zi];
        if (data.items[zitem.id] !== undefined) {
          let ritem = data.items[zitem.id];
          if (data.mapitems[ritem.category][ritem.id] !== undefined) {
            data.mapitems[ritem.category][ritem.id] += zitem.count;
          } else {
            data.mapitems[ritem.category][ritem.id] = zitem.count;
          }
        }
      }
    }

    let diff = 0, days = 0;
    let zc = null;
    let zone = data.map[my][mx];
    if (zone.updatedOn !== undefined) {
      let date = new Date(zone.updatedOn * 1000);
      let dateN = new Date();
      dateN.setHours(23);
      dateN.setMinutes(59);
      dateN.setSeconds(59);
      if (date.getDate() !== dateN.getDate) {
        days = Math.floor((dateN.getTime() - date.getTime()) / 86400000);
        diff = days;
        if (zone.building !== undefined) {
          diff = 2 * days;
        }
      }
    }
    // Temporarily disabling zombie estimation.
    diff = 0;

    if (data.map[my][mx]["z"] !== undefined) {
      zc = parseInt(data.map[my][mx]["z"]) + diff;
      var zl = parseInt(data.map[my][mx]["z"]);
    } else {
      zc = diff;
      var zl = 0;
    }

    if (data.map[my][mx]["z"] !== undefined) {
      mapzone.setAttribute("z", data.map[my][mx]["z"]);
    }
    if (data.map[my][mx]["xmlz"] !== undefined) {
      mapzone.setAttribute("xmlz", data.map[my][mx]["xmlz"]);
    }

    if (
      data.map[my][mx]["danger"] !== undefined &&
      data.map[my][mx]["danger"] !== null
    ) {
      mapzone.classList.add("danger" + data.map[my][mx]["danger"]);
      rzd = data.map[my][mx]["danger"];
    } else if (zc !== null) {
      if (zc > 0) {
        mapzone.setAttribute("e", zc);
      }
      if (zl === 0) {
        mapzone.classList.add("danger0");
      } else if (zl === 1) {
        mapzone.classList.add("danger1");
        rzd = 1;
      } else if (zl >= 2 && zl <= 4) {
        mapzone.classList.add("danger2");
        rzd = 2;
      } else if (zl >= 5 && zl <= 8) {
        mapzone.classList.add("danger3");
        rzd = 3;
      } else if (zl > 8) {
        mapzone.classList.add("danger4");
        rzd = 4;
      } else {
        mapzone.classList.add("danger0");
      }
    } else {
      mapzone.classList.add("danger0");
    }
    if (
      data.map[my][mx]["tag"] !== undefined &&
      data.map[my][mx]["tag"] !== null
    ) {
      mapzone.classList.add("tag" + data.map[my][mx]["tag"]);
    }
    if (
      data.map[my][mx]["nvt"] !== undefined &&
      data.map[my][mx]["nvt"] === 1
    ) {
      mapzone.classList.add("nvt");
    }
    if (data.map[my][mx]["building"] !== undefined && !(rx === 0 && ry === 0)) {
      let building = document.createElement("div");
      building.classList.add("building");
      if (!isExplorable(data.map[my][mx]["building"]["type"])) {
        if (data.map[my][mx]["building"]["dried"] === 1) {
          building.classList.add("depleted-building");
        }
        if (
          data.map[my][mx]["building"]["blueprint"] === undefined ||
          data.map[my][mx]["building"]["blueprint"] === 0
        ) {
          building.classList.add("building-blueprint");
          if (km >= 10) {
            building.classList.add("building-blueprint-far");
          }
        }
      } else {
        building.classList.add("explorable-building");
      }
      building.setAttribute("ruin", data.map[my][mx]["building"]["type"]);
      mapzone.appendChild(building);
    }
    if (
      data.souls != undefined &&
      data.souls[my] !== undefined &&
      data.souls[my][mx] !== undefined &&
      data.souls[my][mx]["lostsoul"] !== undefined &&
      data.souls[my][mx]["lostsoul"] === 1 &&
      !(rx === 0 && ry === 0)
    ) {
      mapzone.classList.add("lostsoul");
      let lostsoul = document.createElement("div");
      lostsoul.classList.add("lostsoul");
      mapzone.appendChild(lostsoul);
    }
    if (data.map[my][mx]["dried"] !== undefined && !(rx === 0 && ry === 0)) {
      let zsi = document.createElement("img");
      zsi.classList.add("zone-status-img");
      if (data.map[my][mx]["dried"] === 1) {
        zsi.setAttribute("src", "/assets/css/img/tag_5.gif");
        zsi.classList.add("zone-status-dried");
        if (
          !document
            .getElementById("options-display-driedzone")
            .classList.contains("active-option")
        ) {
          zsi.classList.add("hideme");
        }
      } else {
        zsi.setAttribute("src", "/assets/css/img/small_gather.gif");
        zsi.classList.add("zone-status-full");
        if (
          !document
            .getElementById("options-display-fullzone")
            .classList.contains("active-option")
        ) {
          zsi.classList.add("hideme");
        }
      }
      mapzone.appendChild(zsi);

      if (data.map[my][mx]["dried"] === 1) {
        if (
          data.map[my][mx]["updatedOn"] !== undefined &&
          (data.system.gametype === "uhu" ||
            (data.system.gametype === "far" && calcKM(rx, ry) >= 3))
        ) {
          if (data.stormstamp !== undefined) {
            let stormcounter = 0;
            for (let i = 1; i <= data.system.day; i++) {
              if (data.stormstamp[i] !== undefined) {
                if (data.stormstamp[i] > data.map[my][mx]["updatedOn"]) {
                  if (calcGD(rx, ry) === data.stormnames[data.storm[i] - 1]) {
                    stormcounter++;
                  }
                }
              }
            }
            if (stormcounter > 0) {
              let zss = document.createElement("div");
              zss.classList.add("zone-storm-status");
              zss.setAttribute('title',
                stormcounter +
                " " +
                (stormcounter === 1 ? __("storm") : __("storms")) +
                " " +
                __("since-last-update"));
              zss.innerHTML = stormcounter;
              if (
                !document
                  .getElementById("options-display-stormzone")
                  .classList.contains("active-option")
              ) {
                zss.classList.add("hideme");
              }
              mapzone.appendChild(zss);
            }
          }
        }
      }
    }

    // Clockwork orange.
    if (
      (data.map[my][mx].updatedOn !== undefined || data.scout[`y${ay}x${ax}`] !== undefined) &&
      !(rx === 0 && ry === 0)
    ) {
        updatezoneday = parseInt(data.map[my][mx].updatedOnDay) || null;
        updatescoutday = parseInt(data.scout[`y${ay}x${ax}`]?.updatedOnDay) || null;

        daysSinceZoneUpdate = updatezoneday !== null ? day - updatezoneday : null;
        daysSinceScoutUpdate = updatescoutday !== null ? day - updatescoutday : null;

        // Determine the most recent update
        let daysSinceLastUpdate = null;
        if (daysSinceZoneUpdate !== null && daysSinceScoutUpdate !== null) {
            daysSinceLastUpdate = Math.min(daysSinceZoneUpdate, daysSinceScoutUpdate);
        } else if (daysSinceZoneUpdate !== null) {
            daysSinceLastUpdate = daysSinceZoneUpdate;
        } else if (daysSinceScoutUpdate !== null) {
            daysSinceLastUpdate = daysSinceScoutUpdate;
        }

        let daysMapping = [
            { condition: (days) => days >= 3, class: "zone-updated-longago", title: __("Irgendwann mal aktualisiert") },
            { condition: (days) => days === 2, class: "zone-updated-b4yesterday", title: __("Vorgestern aktualisiert") },
            { condition: (days) => days === 1, class: "zone-updated-yesterday", title: __("Gestern aktualisiert") },
            { condition: (days) => days === 0, class: "zone-updated-today", title: __("Heute aktualisiert") }
        ];

        if (daysSinceLastUpdate !== null) {
            for (let { condition, class: className, title } of daysMapping) {
                if (condition(daysSinceLastUpdate)) {
                    utd = document.createElement("div");
                    utd.classList.add("zone-updated", className);
                    utd.setAttribute("title", title);
                    break;
                }
            }
        }

        if (utd !== undefined && utd !== null) {
          if (!document.getElementById("options-display-uptodate").classList.contains("active-option")) {
              utd.classList.add("hideme");
          }

          mapzone.appendChild(utd);
        }
    }

    if (data.map[my][mx]["citizens"] !== undefined && !(rx === 0 && ry === 0)) {
      let cc = 0;
      for (cid in data.map[my][mx]["citizens"]) {
        cc++;
      }
      if (cc > 0) {
        citidots.push({
          zone: mapzone.id,
          cs: 2,
          cc: cc,
        });
      }
      else {
        citidots = citidots.filter(item => !(item.zone === mapzone.id && item.cs === 2));
      }
    }
    if (
      data.map[my][mx]["xmlz"] !== undefined &&
      data.map[my][mx]["xmlz"] > 0 &&
      !(rx === 0 && ry === 0)
    ) {
      let zombdot = document.createElement("div");
      zombdot.classList.add("zombies");
      zombdot.classList.add("zombie-exact");
      zombdot.innerHTML = "<span>" + data.map[my][mx]["xmlz"] + "</span>";
      if (
        !document
          .getElementById("options-display-zombies")
          .classList.contains("active-option")
      ) {
        zombdot.classList.add("hideme");
      }
      mapzone.appendChild(zombdot);
    } else if (
      data.map[my][mx]["z"] !== undefined &&
      data.map[my][mx]["z"] > 0 &&
      !(rx === 0 && ry === 0) &&
      data.map[my][mx]["nvt"] === 0
    ) {
      let zombdot = document.createElement("div");
      zombdot.classList.add("zombies");
      zombdot.classList.add("zombie-exact");
      zombdot.innerHTML = "<span>" + data.map[my][mx]["z"] + "</span>";
      if (
        !document
          .getElementById("options-display-zombies")
          .classList.contains("active-option")
      ) {
        zombdot.classList.add("hideme");
      }
      mapzone.appendChild(zombdot);
    } else if (rzd > 0) {
      citidots.push({
        zone: mapzone.id,
        cs: 1,
        cc: rzd,
      });
    }

    //maxzeds here
    if (
      data.map[my][mx]["zm"] !== undefined &&
      data.map[my][mx]["zm"] > 0 &&
      !(rx === 0 && ry === 0)
    ) {
      let maxzombdot = document.createElement("div");
      maxzombdot.classList.add("maxzombies");
      maxzombdot.classList.add("maxzombie-exact");
      maxzombdot.innerHTML = "<span>" + data.map[my][mx]["zm"] + "</span>";
      if (
        !document
          .getElementById("options-display-maxzombies")
          .classList.contains("active-option")
      ) {
        maxzombdot.classList.add("hideme");
      }
      mapzone.appendChild(maxzombdot);
    }


  } else {
    mapzone.classList.add("nyv");
    if (
      data.scout["y" + ay + "x" + ax] !== undefined &&
      !(rx === 0 && ry === 0)
    ) {
      day = parseInt(data.system.day);

      if (data.scout["y" + ay + "x" + ax] !== undefined) {
        updatescoutday = parseInt(
          data.scout["y" + ay + "x" + ax]["updatedOnDay"]
        );
      }

      if (updatescoutday) {
        daysSinceScoutUpdate = day - updatescoutday;
      }

      //set the clock
      if (daysSinceScoutUpdate !== undefined) {
        if (daysSinceScoutUpdate >= 3) {
          utd = document.createElement("div");
          utd.classList.add("zone-updated");
          utd.classList.add("zone-updated-longago");
          utd.setAttribute("title", __("Irgendwann mal aktualisiert"));
        }

        if (daysSinceScoutUpdate == 2) {
          utd = document.createElement("div");
          utd.classList.add("zone-updated");
          utd.classList.add("zone-updated-b4yesterday");
          utd.setAttribute("title", __("Vorgestern aktualisiert"));
        }

        if (daysSinceScoutUpdate == 1) {
          utd = document.createElement("div");
          utd.classList.add("zone-updated");
          utd.classList.add("zone-updated-yesterday");
          utd.setAttribute("title", __("Gestern aktualisiert"));
        }

        if (daysSinceScoutUpdate == 0) {
          utd = document.createElement("div");
          utd.classList.add("zone-updated");
          utd.classList.add("zone-updated-today");
          utd.setAttribute("title", __("Heute aktualisiert"));
        }
      }

      if (utd !== undefined && utd !== null) {
        if (
          !document
            .getElementById("options-display-uptodate")
            .classList.contains("active-option")
        ) {
          utd.classList.add("hideme");
        }
        mapzone.appendChild(utd);
      }
    }
    if (data.scout !== undefined && data.scout[my + mx] !== undefined) {
      if (data.scout[my + mx]["pbl"] === 1) {
        let building = document.createElement("div");
        building.classList.add("possible-building");
        mapzone.appendChild(building);
      }
    }
    if (
      data.souls != undefined &&
      data.souls[my] !== undefined &&
      data.souls[my][mx] !== undefined &&
      data.souls[my][mx]["lostsoul"] !== undefined &&
      data.souls[my][mx]["lostsoul"] === 1 &&
      !(rx === 0 && ry === 0)
    ) {
      mapzone.classList.add("lostsoul");
      let lostsoul = document.createElement("div");
      lostsoul.classList.add("lostsoul");
      mapzone.appendChild(lostsoul);
    }
  }

  if ((updatezoneday === null || updatezoneday === undefined) && km >= 6 && km <= 15) {
    mapzone.classList.add("potential-bluechest");
  }
  if ((updatezoneday === null || updatezoneday === undefined) && km >= 9 && km <= 21) {
    mapzone.classList.add("potential-purplechest");
  }
  if ((updatezoneday === null || updatezoneday === undefined) && km >= 1 && km <= 15) {
    mapzone.classList.add("potential-fireworksparts");
  }


  if (rx === 0 && ry === 0) {
    mapzone.classList.add("city");
  }

  // Return previous classes
  if (currentClasses != null) {
    for (let i = 0; currentClasses.item(i) != null; i++) {
      mapzone.classList.add(currentClasses.item(i));
    }
  }

  //return previously saved expeditions
  if (currentExpeditions != null && currentExpeditions != undefined) {
    mapzone.append(currentExpeditions);
  }

  return mapzone;
}

function replaceMapZone(i, j) {
  let ax = j;
  let ay = i;
  let rx = ax - data['tx'];
  let ry = data['ty'] - ay;
  let elementId = "x" + rx + "y" + ry;
  let zone = document.getElementById(elementId);
  let newZone = generateMapZone(ay, ax);
  if (zone && newZone) {
    zone.replaceWith(newZone);
  }
}

function checkCitidots() {
  for (let dots in citidots) {
    let cd = citidots[dots];
    let citidot = document.getElementById('citidot-' + cd.cs + '--' + cd.zone);
    if (citidot !== null) {
      citidot.remove();
    }
    let hidden = cd.cs == 2
    ? ((localStorage.getItem("fm_setting_citizen") == undefined || localStorage.getItem("fm_setting_citizen") == 1) ? false : true)
    : ((localStorage.getItem("fm_setting_zombies") == 1) ? false : true);

    createCitidotSvg(cd.zone, cd.cs, cd.cc, hidden);

  }
}

function createCitidotSvg(zone, cs, cc, hidden = true) {
  let ink, rim, hide;
  if (cs === 1) {
    //override to make zombie dots disappear, as they are redundant
    return;
    rim = "rgb(201,0,0)";
    ink = "rgb(255,0,0)";
    typ = "zombies";
  } else if (cs === 2) {
    rim = "rgb(255,0,0)";
    ink = "rgb(255,255,0)";
    typ = "citizen";
  }
  let template = "M 3 1 A 1 1 0 0 0 3 4 A 1 1 0 0 0 3 1";
  let cx = 5;
  let cy = cs === 1 ? 24 : 4;
  let cpath = "";
  for (let c = 0; c < cc; c++) {
    if (cx > 25) {
      cx = 5;
      cy += 5;
    }
    let dy = cy + 5;
    cpath += `M ${cx} ${cy} A 1 1 0 0 0 ${cx} ${dy} A 1 1 0 0 0 ${cx} ${cy} `;
    cx += 5;
  }

  let svgTemplate = document.createElement("template");
  svgTemplate.innerHTML =
    '<svg xmlns="http://www.w3.org/2000/svg" ' +
    'id="citidot-' +
    cs +
    "--" +
    zone +
    '" ' +
    'class="' +
    typ +
    (hidden ? ' hideme"' : '"') +
    'fill="currentColor" aria-hidden="true" viewBox="0 0 32 32"></svg>';
  const svgResult = svgTemplate.content.children;
  document.getElementById(zone).append(svgResult[0]);

  // First move or changing direction
  let path = document.createElementNS("http://www.w3.org/2000/svg", "path");
  path.setAttribute("fill", ink);
  path.setAttribute("stroke", rim);
  path.setAttribute("d", cpath);
  path.setAttribute("stroke-width", "1");

  document
    .getElementById(zone)
    .querySelector("#citidot-" + cs + "--" + zone)
    .appendChild(path);
}

function generateMapItems() {
  // Generate item data
  for (let c in data.mapitems) {
    let itemsubbox = document.getElementById("item-info-" + data.mapitems[c]["cat"]);

    for (let d in data.mapitems[c]) {
      if (d !== "cat") {
        let itemcount = data.mapitems[c][d];

        if (data.items[Math.abs(d)] !== undefined && itemcount > 0) {
          let raw_item = data.items[Math.abs(d)];
          let brokenItem = d < 0 ? " broken" : "";
          let defItem = raw_item.category === "Armor" ? " defense" : "";

          let itemDiv = document.createElement("div");
          itemDiv.className = "zone-item click" + brokenItem + defItem;
          itemDiv.setAttribute("state", "0");
          itemDiv.setAttribute("ref", raw_item.id);

          let itemImg = document.createElement("img");
          itemImg.src = data.system.icon + raw_item.image;
          itemImg.title = raw_item.name[data.langcode] + " (ID: " + Math.abs(raw_item.id) + ")";

          let itemCountSpan = document.createElement("span");
          itemCountSpan.className = "count";
          itemCountSpan.textContent = itemcount;

          itemDiv.appendChild(itemImg);
          itemDiv.appendChild(itemCountSpan);
          itemDiv.title = raw_item.name[data.langcode] + " (ID: " + Math.abs(raw_item.id) + ")";

          itemsubbox.appendChild(itemDiv);
        }
      }
    }

    // Sort items by name
    let divParent = document.getElementById("item-info-" + data.mapitems[c]["cat"]);
    let divList = divParent.getElementsByClassName("zone-item");

    Array.from(divList).sort((a, b) => {
      let itemA = a.querySelector("img").title;
      let itemB = b.querySelector("img").title;
      let firstcharA = itemA.charAt(0);

      if (firstcharA === "«" || firstcharA === '"' || firstcharA === "“" || firstcharA === "'") {
        return 1;
      }

      let firstcharB = itemB.charAt(0);

      if (firstcharB === "«" || firstcharB === '"' || firstcharB === "“" || firstcharB === "'") {
        return -1;
      }

      return itemA.localeCompare(itemB);
    }).forEach(item => divParent.appendChild(item));
  }
}

function funChanges() {
  if (data.system.gameid === 4932) {
    data.buildings[46].en = "The binto-Mobile";
    data.buildings[46].fr = "Camion du maire binto";
  }
}

function initiateFM() {
  setUserLanguage();
  funChanges();

  var system = data["system"];
  var username = system["owner_name"];
  var langcode = data["langcode"];

  window.softreloads = 0;
  populateInterface(trans[langcode]);
  jsIn.addDict(dict[langcode]);

  if (username == "Spion") {
    document.getElementById("owner-name").innerHTML = __("Spion");
  } else {
    document.getElementById("owner-name").innerHTML = username;
  }
  document.getElementById("townName").innerHTML = system["gamename"];
  document.getElementById("townDay").innerHTML = __("Tag") + " " + system["day"];
  document.getElementById("townID").innerHTML = __("ID") + ": " + system["gameid"];
  document.getElementById("townSpy").querySelector("a").href = fm_url + "spy/town/" + system["gameid"];

  if (
    data.spy !== undefined && 
    data.spy === 1 && 
    data.spyUndercover !== undefined && 
    data.spyUndercover === 1) {
      let backlink = document.createElement("a");
      backlink.classList.add("back2live");
      backlink.title = __('back2live');
      backlink.href = fm_url + "map";
      backlink.innerHTML = __('Live');
      document.getElementById("townHistory").appendChild(backlink);
  }

  for (let i in system["days"]) {
    if (parseInt(system["days"][i]) === parseInt(system["day"])) {
      let hislink = document.createElement("span");
      hislink.classList.add("current");
      hislink.innerHTML = system["days"][i];
      document.getElementById("townHistory").appendChild(hislink);
    }
    else {
      let hislink = document.createElement("a");
      hislink.href = fm_url + "spy/town/" + system["gameid"] + "/" + system["days"][i];
      hislink.innerHTML = system["days"][i];
      document.getElementById("townHistory").appendChild(hislink);
    }
  }

  document.getElementById("townBar").style.display = "block";

  if (data.spy !== undefined && data.spy === 1) {
    window.ajaxUpdate = function () {
      return false;
    };
    document.getElementById("update-myzone").remove();
    document.getElementById("ruinInfoBox").remove();
  }

  if (data.system.error_code !== undefined) {
    document.getElementById("box").remove();
    document.querySelector(".mode-switch").remove();
    document.getElementById("fm-content").innerHTML =
      '<p style="background:rgba(0,0,0,.7);padding:6px;border-radius:6px;color:#c00;">' +
      __(
        "Es ist ein Fehler aufgetreten. Von MyHordes wurde folgender Fehlercode gesendet"
      ) +
      ": <strong>" +
      data.system.error_code +
      "</strong>.</p>";
  }

  // Fix menu positioning to be in relation to map size.
  document.getElementById("fm-container").style.width = data.width * 32 + 370 + "px";
  document.querySelector(".spread-5").style.left = 0 - (data.width * 32 + 85 - 1000) + "px";
  document.querySelector(".spread-4").style.left = 0 - (data.width * 32 + 85 - 750) + "px";
  document.querySelector(".spread-3").style.left = 0 - (data.width * 32 + 85 - 500) + "px";
  document.querySelector(".spread-2").style.left = 0 - (data.width * 32 + 85 - 250) + "px";
  document.querySelector(".spread-1").style.left = 0 - (data.width * 32 + 85) + "px";
  document.getElementById("item-selector").style.left = data.width * 32 - 320 + "px";

  // Map generation
  generateMap();
  generateMapItems();

  activateEventListeners();

  // populate item selector
  populateItemSelector();
  // populate building selector
  populateBuildingSelector();
  // populate info tabs
  generateStormList();
  generateExpeditionList();
  generateRuinList();
  generateCitizenList();
  generateConstructionList();
  generateConstructionsTabsOnPageLoad();

  loadSettings();
  loadRadii();
  loadExpeditions();

  initAriadne();

  // "click".current-zone
  data.cx = data.ox - data.tx;
  data.cy = data.ty - data.oy;
  document.getElementById("x" + data.cx + "y" + data.cy).click();
}

/*
 * ######################
 * # GENERATE THE MAP   #
 * # AND ALL INFO BOXES #
 * ######################
 */
document.addEventListener("DOMContentLoaded", function () {
  initiateFM();
}); // END GENERATE
